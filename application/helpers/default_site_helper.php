<?php
function format_rupiah($num){
	return "Rp. ".number_format($num,0,",",".").",-";
}
function repair_date($d){
	$s1 = explode(" ",$d)[0];
	$s2 = explode("-",$s1);
	return $s2[2]."/".$s2[1]."/".$s2[0];
}
function repair_date2($d){
	$s1 = explode(" ",$d);
	$s2 = explode("-",$s1[0]);
	return $s2[2]."/".$s2[1]."/".$s2[0]." <br/>".$s1[1];
}
function get_category($arrdata, $parent = "0", $strip=""){
	$strip .="-&nbsp;";
	$res = array();
	foreach($arrdata as $val){
		if(trim($val->parent) == $parent){
			$col['id'] = $val->id;
			$col['name'] = $strip.$val->name;
			$col['parent'] = $val->parent;
			$col['site_url'] = rawurlencode($val->name);
			$col['site_name'] = $val->name;
			$col['discount_general'] = $val->discount_general;
			$res[] = $col;
			$get_child = get_category($arrdata, $val->id, $strip);
			
			foreach($get_child as $vget_child){
				$col['id'] = $vget_child['id'];
				$col['name'] = $strip.$vget_child['name'];
				$col['parent'] = $vget_child['parent'];
				$col['site_url'] = rawurlencode($vget_child['name']);
				$col['site_name'] = $vget_child['name'];
				$col['discount_general'] = $val->discount_general;
				$res[] = $col;
			}
			
		}
	}
	return $res;
}
function status_text($status){
	echo ($status == 1)?"Active":"Not Active";
}
function shipping_mode($status){
	echo ($status == 1)?"Dropship":"-";
}
function loadPaging($total_page, $page, $url){
	$htm = "";
	if($total_page > 1){
		$htm .= "<span class=\"paging\">";
			if($page>1) {
				$htm .= "<a href=\"".$url.($page-1)."\">Prev</a> | ";
				$htm .= "<a href=\"".$url.($page-1)."\">".($page-1)."</a> | ";
			}
				$htm .= "<a href=\"".$url.($page)."\" class=\"active\">".$page."</a> | ";
			if($page<$total_page){
				$htm .= "<a href=\"".$url.($page+1)."\">".($page+1)."</a> | ";
				$htm .= "<a href=\"".$url.($page+1)."\">Next</a>";
			}
		$htm .= "</span>";
	}
	return $htm;
}
function urlstrip($str){
	return str_replace(" ","-",$str);
}
function nama_bulan($monthNum){
	$dateObj   = DateTime::createFromFormat('!m', $monthNum);
	$monthName = $dateObj->format('F');
	return $monthName;
}
function d($x){
	echo "<pre>"; print_r($x); echo "</pre>";
}
function dd($x){
	echo "<pre>"; print_r($x); echo "</pre>";
	die();
}
function statLabel($x){
	$res="";
	if($x==1){
		$res="<span class=\"badge badge-success p-2\">Aktif</span>";
	}else if($x==0){
		$res="<span class=\"badge badge-danger p-2\">Non Aktif</span>";
	}
	return $res;
}
function btnStatLabel($x){
	$res="";
	if($x==1){
		$res="<button class=\"btn btn-success btn-sm btn-update-status\" data-status=\"0\" >Aktif</button>";
	}else if($x==0){
		$res="<button class=\"btn btn-danger btn-sm btn-update-status\" data-status=\"1\" >Non Aktif</button>";
	}
	return $res;
}
function numLength($num, $length){
	return sprintf("%0".$length."d", $num);
}
function numberToRoman($number) {
    $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
    $returnValue = '';
    while ($number > 0) {
        foreach ($map as $roman => $int) {
            if($number >= $int) {
                $number -= $int;
                $returnValue .= $roman;
                break;
            }
        }
    }
    return $returnValue;
}
function changeArray($x){
	$obja = json_decode($x,true);
	$aNewArray = array();
	if($obja != NULL){
		$aNoSeri = $obja['no_seri'];
		$aQty = $obja['qty'];
		foreach($aNoSeri as $key => $val){
			$no_seri = $aNoSeri[$key];
			$qty = $aQty[$key];
			$aNewArray[$no_seri] = $qty;
		}
	}
	return $aNewArray;
}
function joinJSONSTRNoSeri($a, $b){
	$arA = changeArray($a);
	$arB = changeArray($b);
	$arjoin = array_merge_recursive($arA,$arB);
	$newAr = array();
	foreach($arjoin as $key => $val){
		if(is_array($val)){
			$tqty = 0;
			foreach($val as $qty){
				$tqty += $qty;
			}
			$newAr[$key] = "".$tqty."";
		}else{
			$newAr[$key] = "".$val."";
		}
	}
	$newJSONObj = array();
	$arNoSeri = array();
	$arQty = array();
	foreach($newAr as $key => $val){
		$arNoSeri[] = $key;
		$arQty[] = $val;
	}
	$newJSONObj['no_seri'] = $arNoSeri;
	$newJSONObj['qty'] = $arQty;
	return json_encode($newJSONObj, JSON_FORCE_OBJECT);
}
function getNoSeriFromDetailBarang($id_barang, $resDataDetailBarang){
	$condIdBarang = $id_barang;
	$resNoSeri = "";
	foreach($resDataDetailBarang as $row){
		if($row['id_barang'] == $condIdBarang){
			$resNoSeri = $row['no_seri'];
		}
	}
	return $resNoSeri;
}