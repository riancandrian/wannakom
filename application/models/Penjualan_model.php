<?php
class Penjualan_model extends CI_Model {
	public $rows_per_page = 50;
	public function __construct()
	{
		parent::__construct();
		@define('_PASSWORD_KEY_', 'vEU9fTiboZaoWj3IU1WWMesNTfgWGvTQ1hbKK7bqvU7VxLmOqGEYv1qM'); // BARIS INI HARUS ADA DI admin_model.php DAN site_model.php
	}
	public function get_id_group_customer_from_mobile_order($id_order = NULL){
		$sql = "SELECT b.id_group_customer FROM `tbl_mobile_order` a
				INNER JOIN tbl_customer b ON b.id = a.id_customer
				WHERE a.id = '".$id_order."' ;";
		$query = $this->db->query($sql);
		$res = $query->row_array();
		return $res['id_group_customer'];
	}
	public function get_count_data_mobile_order(){
		$this->db->where('a.delete', '0');
		$this->db->where('a.status_apply', '0');
		$this->db->select('count(a.id) as count');
		$this->db->from('tbl_mobile_order a');
		$this->db->join('tbl_customer b','b.id = a.id_customer','inner');
		$this->db->order_by('a.id','DESC');
		$query = $this->db->get();
		$res['count'] = $query->result_array()[0]['count'];
		return $res;
	}
	public function get_data_mobile_order($id = NULL,$request = NULL){

		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('b.nama_customer', $request['tx_cari'], 'both');
			$this->db->or_like('a.alamat_tujuan', $request['tx_cari'], 'both');
			$this->db->or_like('b.nama_toko', $request['tx_cari'], 'both');
			$this->db->group_end();
		}

		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		if(isset($request['id_customer'])){
			$this->db->where('a.id_customer', $request['id_customer']);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*,b.nama_customer,b.id_group_customer,b.email,b.telepon,b.nama_toko');
		$this->db->from('tbl_mobile_order a');
		$this->db->join('tbl_customer b','b.id = a.id_customer','inner');
		$this->db->order_by('a.id','DESC');
		$query = $this->db->get();
		$resDataOrder = $query->result_array();

		$query = $this->db->query("SELECT * FROM `tbl_mobile_order_detail` ;");
		$resDataOrderDetail = $query->result_array();

		$tmpData = array();
		foreach($resDataOrder as $row){
			$id_order = $row['id'];
			$tmpDataDetail = array();
			foreach($resDataOrderDetail as $rowSub){
				if($id_order == $rowSub['id_mobile_order']){
					$tmpDataDetail[] = $rowSub;
				}
			}
			$row['data_detail_order'] = base64_encode(json_encode($tmpDataDetail));
			$tmpData[] = $row;
		}
		$objTmpData = json_decode(json_encode($tmpData));

		return $objTmpData;
	}
	public function apply_request_dari_app($id = NULL, $request = NULL)
	{
		$query = $this->db->query("
									SELECT
										a.id_customer, a.tgl_input as tanggal,b.email,a.catatan as pesan,
										a.total_bayar as total,0 as pemotongan, 'persen' as pemotongan_tipe, a.total_bayar as grand_total,
										a.total_bayar as pembayaran, 0 as kembali
									FROM `tbl_mobile_order` as a
									INNER JOIN `tbl_customer` AS b ON b.id = a.id_customer
									WHERE a.id = '".$id."'
								;");
		$resDataOrder = $query->row_array();
		$resDataOrder['id_mobile_order'] = $id;
		$resDataOrder['jatuh_tempo'] = (isset($request['jatuh_tempo']))?$request['jatuh_tempo']:"";

		$query = $this->db->query("
									SELECT
										id_barang,qty,harga_jual as harga_satuan,sub_total
									FROM `tbl_mobile_order_detail`
									WHERE id_mobile_order = '".$id."'
									;
								;");
		$resDataOrderDetail = $query->result_array();

		foreach($resDataOrderDetail as $keyRow => $row){
			foreach($row as $key => $val){
				$resDataOrder[$key][$keyRow] = $val;
				if($key == "id_barang"){
					$resDataOrder['expire_date'][$keyRow] = (isset($request['expire_date'][$val]))?$request['expire_date'][$val]:"";
				}
			}
		}
		//$resDataOrder['expire_date'] = @$request['expire_date'];

		//-- $set_detail_no_seri set no seri otomatis
		$set_detail_no_seri = array();
		$TTotal = 0;
		foreach($resDataOrder['id_barang'] as $key => $val){
			$id_barang = $resDataOrder['id_barang'][$key];
			if(isset($request['accept_qty'][$id_barang])){
				$resDataOrder['qty'][$key] = $request['accept_qty'][$id_barang];
				$resDataOrder['sub_total'][$key] = ($request['accept_qty'][$id_barang]*$request['harga_jual'][$id_barang]);
				$TTotal += $resDataOrder['sub_total'][$key];
			}
			$qty = $resDataOrder['qty'][$key];
			$query = $this->db->query("SELECT no_seri, qty FROM `tbl_data_barang_stok` WHERE id_barang = '".$id_barang."' AND qty > 0 ORDER BY tgl_input ASC;");
			$resDataNoSeri = $query->result_array();

			$setArNoSeriBarang = array();
			$setQty = 0;
			foreach($resDataNoSeri as $valNoSeri){
				$no_seriD = $valNoSeri['no_seri'];
				$qtyD = $valNoSeri['qty'];
				if($setQty < $qty and ($qty-$setQty) > $qtyD){
					$setArNoSeriBarang['no_seri'][] = $no_seriD;
					$setArNoSeriBarang['qty'][] = $qtyD;
					$setQty += ($qtyD);
				}else if($setQty < $qty and ($qty-$setQty) <= $qtyD){
					$setArNoSeriBarang['no_seri'][] = $no_seriD;
					$setArNoSeriBarang['qty'][] = ($qty-$setQty);
					$setQty += ($qty-$setQty);
				}
			}
			$set_detail_no_seri[] = json_encode($setArNoSeriBarang);
		}
		if(isset($request['accept_qty'])){
			$resDataOrder['total'] = $TTotal;
			$resDataOrder['grand_total'] = $TTotal;
			$resDataOrder['pembayaran'] = $TTotal;
		}
		$tmp1 = array();
		foreach($set_detail_no_seri as $k1 => $row){
			$data = json_decode($row);
			$tmp2 = array();
			foreach($data as $k2 => $v){
				$tmp2[$k2]= (object)$v;
			}
			$tmp1[$k1] = json_encode((object)$tmp2);
		}
		$set_detail_no_seri = $tmp1;


		//-- $set_detail_no_seri reset no seri berdasarkan request karena ada perubahan
		$set_detail_no_seri = array();
		foreach($request['detail_no_seri'] as $key => $val){
			$set_detail_no_seri[] = $val;
		}
		$resDataOrder['detail_no_seri'] = $set_detail_no_seri;
		//d($request);
		//dd($resDataOrder);
		//return "Berhasil Apply Data";


		$this->penjualan_model->add_transaksi_penjualan($resDataOrder);

		$updateData = array(
			'status_apply' => '1',
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("tbl_mobile_order", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Apply Data";
		}else{
			$message = "Gagal Apply Data!";
		}

		return $message;
	}
	public function apply_request_dari_app2($id = NULL, $request = NULL)
	{
		$json = json_encode($request);
		$dataEnc = base64_encode($json);
		//d($request); d($json); dd($dataEnc);
		$this->db->update("tbl_mobile_order", ['status_apply' => '2', 'data_confirm_by_admin' => $dataEnc], ['id' => $request['id_order']]);
		$this->mobile_service_model->push_notification_to_mobile(['id_order' => $request['id_order'], 'title' => 'Konfirmasi Pembelian', 'text' => 'Silahkan konfirmasi pesanan anda!']);
		$message = "Menunggu konfirmasi customer";
		return $message;
	}
	public function get_transaksi_penjualan_detail_barang($id = NULL,$request = NULL){
		/*
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.tanggal', $request['tx_cari'], 'both');
			$this->db->or_like('a.nomor', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		*/
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		if(isset($request['id_transaksi_penjualan'])){
			$this->db->where('a.id_transaksi_penjualan', $request['id_transaksi_penjualan']);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*,b.id as id_barang,b.nama_barang,b.deskripsi,c.nama_satuan');
		$this->db->from('tbl_transaksi_penjualan_detail_barang a');
		$this->db->join('tbl_data_barang b','b.id = a.id_barang','inner');
		$this->db->join('satuan c','c.id = b.id_satuan','inner');
		$this->db->order_by('a.id','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_transaksi_penjualan_detail_barang($request = NULL, $id_transaksi_penjualan){
		$insertData = array(
			'id_transaksi_penjualan' => $id_transaksi_penjualan,
			'id_barang' => $request['id_barang'],
			//'expire_date' => @$request['expire_date'],
			'qty' => $request['qty'],
			'no_seri' => $request['detail_no_seri'],
			'harga_satuan' => $request['harga_satuan'],
			'sub_total' => $request['sub_total']
		);
		$this->db->insert("tbl_transaksi_penjualan_detail_barang", $insertData);
		$insert_id = $this->db->insert_id();

		if(is_numeric(@$insert_id)){
			$message = "Berhasil Input Barang Faktur Pembelian";
		}else{
			$message = "Gagal Input Barang Faktur Pembelian!";
		}
		return $message;
	}
	public function get_laporan_cepat_transaksi_penjualan(){
		$ret = array();
		$query = $this->db->query("SELECT SUM(sisa_tagihan) belum_dibayar FROM tbl_transaksi_penjualan WHERE `delete` = '0';");
		$ret['belum_dibayar'] = $query->row_array()['belum_dibayar'];

		$query = $this->db->query("SELECT SUM(sisa_tagihan) belum_dibayar_jatuh_tempo FROM tbl_transaksi_penjualan WHERE `delete` = '0' AND jatuh_tempo <= NOW();");
		$ret['belum_dibayar_jatuh_tempo'] = $query->row_array()['belum_dibayar_jatuh_tempo'];

		$query = $this->db->query("
									SELECT SUM(a.grand_total) dibayar_30_hari FROM tbl_transaksi_penjualan a
									WHERE a.`delete` = '0'
									AND (a.tgl_input BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW());
		");
		$ret['dibayar_30_hari'] = $query->row_array()['dibayar_30_hari'];

		return $ret;
	}
	public function get_laporan_transaksi_penjualan($id = NULL,$request = NULL)
	{
		/*
			SELECT
				a.tanggal,a.nomor,b.nama_customer,a.jatuh_tempo,d.nama_barang,c.no_seri,c.qty,e.nama_satuan,c.harga_satuan,(c.qty*c.harga_satuan) as sub_total,a.pesan
			FROM
			tbl_transaksi_penjualan a
			INNER JOIN tbl_customer b on b.id = a.id_customer
			INNER JOIN tbl_transaksi_penjualan_detail_barang c on c.id_transaksi_penjualan = a.id
			INNER JOIN tbl_data_barang d on d.id = c.id_barang
			INNER JOIN satuan e on e.id = d.id_satuan
			ORDER BY a.nomor ASC, b.nama_customer ASC, d.nama_barang ASC;
		*/
		if(!isset($request['bt_rentang']) and !isset($request['bt_periode'])){
			$this->db->where('a.tanggal', date('Y-m-d'));
		}
		if(isset($request['bt_rentang'])){
			$this->db->group_start();
			$this->db->where('a.tanggal >=', $request['tanggal_dari']);
			$this->db->where('a.tanggal <=', $request['tanggal_ke']);
			$this->db->group_end();
		}
		$this->db->select("a.tanggal,a.nomor,b.nama_customer,a.jatuh_tempo,d.nama_barang,c.no_seri,c.qty,e.nama_satuan,c.harga_satuan,(c.qty*c.harga_satuan) as sub_total,a.pesan");
		$this->db->from('tbl_transaksi_penjualan  a');
		$this->db->join("tbl_customer b", "b.id = a.id_customer", "inner");
		$this->db->join("tbl_transaksi_penjualan_detail_barang c", "c.id_transaksi_penjualan = a.id", "inner");
		$this->db->join("tbl_data_barang d", "d.id = c.id_barang", "inner");
		$this->db->join("satuan e", "e.id = d.id_satuan", "inner");

		$this->db->order_by("a.nomor","ASC");
		$this->db->order_by("b.nama_customer","ASC");
		$this->db->order_by("d.nama_barang","ASC");
		$query = $this->db->get();
		return $query->result();
	}
	public function get_transaksi_penjualan($id = NULL,$request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.tanggal', $request['tx_cari'], 'both');
			$this->db->or_like('a.nomor', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		if(isset($request['id_customer']) and isset($request['jatuh_tempo'])){
			$this->db->where('a.id_customer', $request['id_customer']);
			$this->db->where('a.jatuh_tempo <=', $request['jatuh_tempo']);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*,b.nama_customer,b.alamat as alamat_customer,b.jatuh_tempo as top, g.nama_gudang');
		$this->db->from('tbl_transaksi_penjualan  a');
		$this->db->join('tbl_customer  b','a.id_customer = b.id','inner');
		$this->db->join('tbl_gudang g', 'g.id = a.id_gudang', 'left');

		if(isset($request['bisa_retur']) and $request['bisa_retur'] == 1){
			$this->db->join('tbl_transaksi_penjualan_detail_barang  c','c.id_transaksi_penjualan = a.id','inner');
			$this->db->where('c.qty > c.qty_cetak_retur');
			$this->db->group_by('a.nomor');
		}
		$this->db->order_by('a.tanggal','DESC');
		$this->db->order_by('a.nomor','DESC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_transaksi_penjualan($request = NULL)
	{
		$query = $this->db->query("SELECT nomor_urut FROM tbl_transaksi_penjualan WHERE EXTRACT(YEAR_MONTH FROM tgl_input) = '".date('Ym')."' ORDER BY nomor_urut DESC limit 1;");
		$row = $query->row();
		if(isset($row->nomor_urut) and $row->nomor_urut > 0)
			$newNomorUrut = $row->nomor_urut+1;
		else
			$newNomorUrut = 1;
		$newNomor = numLength($newNomorUrut,4)."/FJ/".numberToRoman(date('m'))."/".date('y');

		$insertData = array(
			'nomor' => $newNomor,
			'nomor_urut' => $newNomorUrut,
			'id_mobile_order' => @$request['id_mobile_order'],
			'id_customer' => $request['id_customer'],
			'id_gudang' => $request['id_gudang'],
			'jatuh_tempo' => $request['jatuh_tempo'],
			'tanggal' => $request['tanggal'],
			'email' => $request['email'],
			'pesan' => $request['pesan'],
			'total' => $request['total'],
			'pemotongan' => $request['pemotongan'],
			'pemotongan_tipe' => $request['pemotongan_tipe'],
			'grand_total' => $request['grand_total'],
			'pembayaran' => $request['pembayaran'],
			'kembali' => $request['kembali']
		);
		$this->db->insert("tbl_transaksi_penjualan", $insertData);
		$insert_id = $this->db->insert_id();
		$data = array();
		for($i = 0;$i<count($request['id_barang']);$i++){
			$subdata = array();
			foreach($request as $key => $val){
				if(is_array($val)){
					$subdata[$key] = $request[$key][$i];
				}
			}
			$data[] = $subdata;
		}

		foreach($data as $req){
			if($req['detail_no_seri']!=""){
				$objNS = json_decode($req['detail_no_seri']);
				$obNoSeri = $objNS->no_seri;
				$obQty = $objNS->qty;
				foreach($obNoSeri as $key => $val){
					$no_seri = $obNoSeri->$key;
					$qty = $obQty->$key;

					$this->db->query("update tbl_data_barang_stok set qty = (qty-".$qty.") WHERE id_barang = '".$req['id_barang']."' and no_seri = '".$no_seri."'; ");
				}
				$add_transaksi_penjualan_detail_barang = $this->add_transaksi_penjualan_detail_barang($req, $insert_id);
			}
		}

		if(is_numeric(@$insert_id)){
			$message = @$add_transaksi_penjualan_detail_barang."<br/>Berhasil Input Data";
		}else{
			$message = @$add_transaksi_penjualan_detail_barang."<br/>Gagal Input Data!";
		}
		//return $message;
		return $insert_id;
	}
	public function edit_transaksi_penjualan($request = NULL)
	{
		$updateData = array(
			'id_customer' => $request['id_customer'],
			'id_gudang' => $request['id_gudang'],
			'jatuh_tempo' => $request['jatuh_tempo'],
			'tanggal' => $request['tanggal'],
			'email' => $request['email'],
			'pesan' => $request['pesan'],
			'total' => $request['total'],
			'pemotongan' => $request['pemotongan'],
			'pemotongan_tipe' => $request['pemotongan_tipe'],
			'grand_total' => $request['grand_total'],
			'pembayaran' => $request['pembayaran'],
			'kembali' => $request['kembali']
		);
		$whereEdit = array(
			'id' => $request['id_transaksi_penjualan']
		);
		$res = $this->db->update("tbl_transaksi_penjualan", $updateData, $whereEdit);

		$data = array();
		for($i = 0;$i<count($request['id_barang']);$i++){
			$subdata = array();
			foreach($request as $key => $val){
				if(is_array($val)){
					$subdata[$key] = $request[$key][$i];
				}
			}
			$data[] = $subdata;
		}
		//----------------------------------
			$query = $this->db->query("select id_barang,no_seri from tbl_transaksi_penjualan_detail_barang where id_transaksi_penjualan = '".$request['id_transaksi_penjualan']."' ;");
			foreach($query->result_array() as $row){
				$id_barang = $row['id_barang'];
				$objNS = json_decode($row['no_seri']);
				$obNoSeri = $objNS->no_seri;
				$obQty = $objNS->qty;
				foreach($obNoSeri as $key => $val){
					$no_seri = $obNoSeri->$key;
					$qty = $obQty->$key;
					$this->db->query("update tbl_data_barang_stok set qty=(qty+".$qty.") where id_barang='".$id_barang."' and no_seri = '".$no_seri."'; ");
				}
			}
			$this->db->delete('tbl_transaksi_penjualan_detail_barang', array('id_transaksi_penjualan' => $request['id_transaksi_penjualan']));
		//------------------------------------
		foreach($data as $req){
			if($req['detail_no_seri']!=""){
				$objNS = json_decode($req['detail_no_seri']);
				$obNoSeri = $objNS->no_seri;
				$obQty = $objNS->qty;
				foreach($obNoSeri as $key => $val){
					$no_seri = $obNoSeri->$key;
					$qty = $obQty->$key;
					$this->db->query("update tbl_data_barang_stok set qty=(qty-".$qty.") where id_barang='".$req['id_barang']."' and no_seri = '".$no_seri."'; ");
				}
				$add_transaksi_penjualan_detail_barang = $this->add_transaksi_penjualan_detail_barang($req, $request['id_transaksi_penjualan']);
			}
		}
		/*
		foreach($data as $req){
			if($req['detail_no_seri']!=""){
				$query = $this->db->query("select no_seri from tbl_transaksi_penjualan_detail_barang where id_transaksi_penjualan = '".$request['id_transaksi_penjualan']."' and  id_barang='".$req['id_barang']."' ; ");
				$rowOld = $query->row_array();
				$rowOldNoSeri = changeArray($rowOld['no_seri']);
				$objNS = json_decode($req['detail_no_seri']);
				$obNoSeri = $objNS->no_seri;
				$obQty = $objNS->qty;
				foreach($obNoSeri as $key => $val){
					$no_seri = $obNoSeri->$key;
					$qty = $obQty->$key;
					$rowOldNoSeriQty = (isset($rowOldNoSeri[$no_seri]))?$rowOldNoSeri[$no_seri]:0;
					$this->db->query("update tbl_data_barang_stok set qty=(qty+".$rowOldNoSeriQty."-".$qty.") where id_barang='".$req['id_barang']."' and no_seri = '".$no_seri."'; ");
				}
				$this->db->delete('tbl_transaksi_penjualan_detail_barang', array('id_transaksi_penjualan' => $request['id_transaksi_penjualan'], 'id_barang' => $req['id_barang']));
				$add_transaksi_penjualan_detail_barang = $this->add_transaksi_penjualan_detail_barang($req, $request['id_transaksi_penjualan']);
			}
		}
		*/
		if($res == 1){
			$message = @$add_transaksi_penjualan_detail_barang."<br/>Berhasil Edit Data";
		}else{
			$message = @$add_transaksi_penjualan_detail_barang."<br/>Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_transaksi_penjualan($id = NULL)
	{
		$updateData = array(
			'delete' => '1',
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("tbl_transaksi_penjualan", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}




		$query = $this->db->query("select id_barang,no_seri from tbl_transaksi_penjualan_detail_barang where id_transaksi_penjualan = '".$id."' ;");
		foreach($query->result_array() as $row){
			$id_barang = $row['id_barang'];
			$objNS = json_decode($row['no_seri']);
			$obNoSeri = $objNS->no_seri;
			$obQty = $objNS->qty;
			foreach($obNoSeri as $key => $val){
				$no_seri = $obNoSeri->$key;
				$qty = $obQty->$key;
				$this->db->query("update tbl_data_barang_stok set qty=(qty+".$qty.") where id_barang='".$id_barang."' and no_seri = '".$no_seri."'; ");
			}
		}


		return $message;
	}
	public function get_retur_penjualan_detail_barang($id = NULL,$request = NULL){
		/*
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.tanggal', $request['tx_cari'], 'both');
			$this->db->or_like('a.nomor', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		*/
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		if(isset($request['id_transaksi_penjualan'])){
			$this->db->where('a.id_transaksi_penjualan', $request['id_transaksi_penjualan']);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*,b.id as id_barang,b.nama_barang,b.deskripsi,c.nama_satuan,d.qty as retur_qty,d.no_seri as retur_no_seri,d.keterangan as retur_keterangan');
		$this->db->from('tbl_transaksi_penjualan_detail_barang a');
		$this->db->join('tbl_data_barang b','b.id = a.id_barang','inner');
		$this->db->join('satuan c','c.id = b.id_satuan','inner');
		$this->db->join('tbl_retur_penjualan_detail_barang d','d.id_transaksi_penjualan_detail_barang = a.id and d.id_retur_penjualan = "'.$request['id_retur_penjualan'].'"','left');
		$this->db->order_by('a.id','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_retur_penjualan_detail_barang($request = NULL, $id_retur_penjualan){
		$insertData = array(
			'id_retur_penjualan' => $id_retur_penjualan,
			'id_transaksi_penjualan' => $request['id_transaksi_penjualan'],
			'id_transaksi_penjualan_detail_barang' => $request['id_transaksi_penjualan_detail_barang'],
			'id_barang' => $request['id_barang'],
			'qty' => $request['qty_retur'],
			'no_seri' => $request['detail_no_seri'],
			'keterangan' => $request['keterangan']
		);
		$this->db->insert("tbl_retur_penjualan_detail_barang", $insertData);
		$insert_id = $this->db->insert_id();

		$query = $this->db->query("SELECT sum(qty) qty_cetak_retur FROM tbl_retur_penjualan_detail_barang WHERE id_transaksi_penjualan_detail_barang = '".$request['id_transaksi_penjualan_detail_barang']."';");
		$res = $query->row_array();
		$this->db->query("update tbl_transaksi_penjualan_detail_barang set qty_cetak_retur = '".$res['qty_cetak_retur']."' WHERE id = '".$request['id_transaksi_penjualan_detail_barang']."'; ");

		$query = $this->db->query("SELECT no_seri FROM tbl_retur_penjualan_detail_barang WHERE id_transaksi_penjualan_detail_barang = '".$request['id_transaksi_penjualan_detail_barang']."';");
		$no_seri_cetak_retur = "";
		foreach ($query->result_array() as $row){
			$no_seri_cetak_retur = joinJSONSTRNoSeri($no_seri_cetak_retur, $row['no_seri']);
		}
		$this->db->query("update tbl_transaksi_penjualan_detail_barang set no_seri_cetak_retur = '".$no_seri_cetak_retur."' WHERE id = '".$request['id_transaksi_penjualan_detail_barang']."'; ");


		if(is_numeric(@$insert_id)){
			$message = "Berhasil Input Barang Faktur Pembelian";
		}else{
			$message = "Gagal Input Barang Faktur Pembelian!";
		}
		return $message;
	}
	public function get_laporan_retur_penjualan($id = NULL,$request = NULL)
	{
		$where = "";
		if(!isset($request['bt_rentang']) and !isset($request['bt_periode'])){
			$where .= " and a.tanggal = '".date('Y-m-d')."' ";
		}
		if(isset($request['bt_rentang'])){
			$where .= " and (a.tanggal >= '".$request['tanggal_dari']."' and a.tanggal <= '".$request['tanggal_ke']."') ";
		}
		$sql = "
					SELECT
						a.tanggal,a.nomor as no_retur_penjualan,c.nama_customer,b.nomor as no_transaksi_penjualan,b.jatuh_tempo,
						f.nama_barang,d.no_seri,d.qty as qty_retur,g.nama_satuan,e.harga_satuan,(d.qty*e.harga_satuan) as sub_total,a.pesan
					FROM
					tbl_retur_penjualan a
					INNER JOIN tbl_transaksi_penjualan b ON b.id = a.id_transaksi_penjualan
					INNER JOIN tbl_customer c ON c.id = b.id_customer
					INNER JOIN tbl_retur_penjualan_detail_barang d ON d.id_retur_penjualan = a.id
					INNER JOIN tbl_transaksi_penjualan_detail_barang e ON e.id = d.id_transaksi_penjualan_detail_barang
					INNER JOIN tbl_data_barang f ON f.id = e.id_barang
					INNER JOIN satuan g ON g.id = f.id_satuan
					WHERE 1 ".$where."
					ORDER BY a.nomor ASC, c.nama_customer ASC, f.nama_barang ASC;
		";
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function get_retur_penjualan($id = NULL,$request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.tanggal', $request['tx_cari'], 'both');
			$this->db->or_like('a.nomor', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*,b.nomor as no_transaksi_penjualan,c.nama_customer,c.alamat as alamat_customer,c.jatuh_tempo as top');
		$this->db->from('tbl_retur_penjualan a');
		$this->db->join('tbl_transaksi_penjualan b','b.id = a.id_transaksi_penjualan','inner');
		$this->db->join('tbl_customer  c','c.id = b.id_customer','inner');
		$this->db->order_by('a.tanggal','DESC');
		$this->db->order_by('a.nomor','DESC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_retur_penjualan($request = NULL)
	{
		$query = $this->db->query("SELECT nomor_urut FROM tbl_retur_penjualan WHERE EXTRACT(YEAR_MONTH FROM tgl_input) = '".date('Ym')."' ORDER BY nomor_urut DESC limit 1;");
		$row = $query->row();
		if(isset($row->nomor_urut) and $row->nomor_urut > 0)
			$newNomorUrut = $row->nomor_urut+1;
		else
			$newNomorUrut = 1;
		$newNomor = numLength($newNomorUrut,4)."/RP/".numberToRoman(date('m'))."/".date('y');

		$insertData = array(
			'nomor' => $newNomor,
			'nomor_urut' => $newNomorUrut,
			'id_transaksi_penjualan' => $request['id_transaksi_penjualan'],
			'tanggal' => $request['tanggal'],
			'pesan' => $request['pesan'],
			'total' => $request['total_retur']
		);
		$this->db->insert("tbl_retur_penjualan", $insertData);
		$insert_id = $this->db->insert_id();

		$data = array();
		for($i = 0;$i<count($request['id_transaksi_penjualan_detail_barang']);$i++){
			$subdata = array();
			foreach($request as $key => $val){
				if(is_array($val)){
					$subdata[$key] = $request[$key][$i];
				}
			}
			$data[] = $subdata;
		}

		foreach($data as $req){
			if($req['detail_no_seri']!=""){
				$objNS = json_decode($req['detail_no_seri']);
				$obNoSeri = $objNS->no_seri;
				$obQty = $objNS->qty;
				foreach($obNoSeri as $key => $val){
					$no_seri = $obNoSeri->$key;
					$qty = $obQty->$key;
					$this->db->query("update tbl_data_barang_stok set qty = (qty+".$qty.") WHERE id_barang = '".$req['id_barang']."' and no_seri = '".$no_seri."'; ");
				}
				$req['id_transaksi_penjualan'] = $request['id_transaksi_penjualan'];
				$add_retur_penjualan_detail_barang = $this->add_retur_penjualan_detail_barang($req, $insert_id);
			}
		}
		$this->db->query("update tbl_transaksi_penjualan set status_retur = '1' WHERE id = '".$request['id_transaksi_penjualan']."' ; ");
		if(is_numeric(@$insert_id)){
			$message = @$add_retur_penjualan_detail_barang."<br/>Berhasil Input Data";
		}else{
			$message = @$add_retur_penjualan_detail_barang."<br/>Gagal Input Data!";
		}
		return $message;
	}
	public function edit_retur_penjualan($request = NULL)
	{
		$updateData = array(
			'id_transaksi_penjualan' => $request['id_transaksi_penjualan'],
			'tanggal' => $request['tanggal'],
			'pesan' => $request['pesan'],
			'total' => $request['total_retur']
		);
		$whereEdit = array(
			'id' => $request['id_retur_penjualan']
		);
		$res = $this->db->update("tbl_retur_penjualan", $updateData, $whereEdit);

		$data = array();
		for($i = 0;$i<count($request['id_transaksi_penjualan_detail_barang']);$i++){
			$subdata = array();
			foreach($request as $key => $val){
				if(is_array($val)){
					$subdata[$key] = $request[$key][$i];
				}
			}
			$data[] = $subdata;
		}
		//----------------------------------
			$query = $this->db->query("select id_barang,no_seri from tbl_retur_penjualan_detail_barang where id_retur_penjualan = '".$request['id_retur_penjualan']."' ;");
			foreach($query->result_array() as $row){
				$id_barang = $row['id_barang'];
				$objNS = json_decode($row['no_seri']);
				$obNoSeri = $objNS->no_seri;
				$obQty = $objNS->qty;
				foreach($obNoSeri as $key => $val){
					$no_seri = $obNoSeri->$key;
					$qty = $obQty->$key;
					$this->db->query("update tbl_data_barang_stok set qty=(qty-".$qty.") where id_barang='".$id_barang."' and no_seri = '".$no_seri."'; ");
				}
			}
			$this->db->delete('tbl_retur_penjualan_detail_barang', array('id_retur_penjualan' => $request['id_retur_penjualan']));
		//------------------------------------
		foreach($data as $req){
			if($req['detail_no_seri']!=""){
				$objNS = json_decode($req['detail_no_seri']);
				$obNoSeri = $objNS->no_seri;
				$obQty = $objNS->qty;
				foreach($obNoSeri as $key => $val){
					$no_seri = $obNoSeri->$key;
					$qty = $obQty->$key;
					$this->db->query("update tbl_data_barang_stok set qty = (qty+".$qty.") WHERE id_barang = '".$req['id_barang']."' and no_seri = '".$no_seri."'; ");
				}
				$req['id_transaksi_penjualan'] = $request['id_transaksi_penjualan'];
				$add_retur_penjualan_detail_barang = $this->add_retur_penjualan_detail_barang($req, $request['id_retur_penjualan']);
			}
		}

		if($res == 1){
			$message = @$add_retur_penjualan_detail_barang."<br/>Berhasil Edit Data";
		}else{
			$message = @$add_retur_penjualan_detail_barang."<br/>Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_retur_penjualan($id = NULL)
	{
		$updateData = array(
			'delete' => '1',
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("tbl_retur_penjualan", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}


		$query = $this->db->query("select id_barang,no_seri from tbl_retur_penjualan_detail_barang where id_retur_penjualan = '".$id."' ;");
		foreach($query->result_array() as $row){
			$id_barang = $row['id_barang'];
			$objNS = json_decode($row['no_seri']);
			$obNoSeri = $objNS->no_seri;
			$obQty = $objNS->qty;
			foreach($obNoSeri as $key => $val){
				$no_seri = $obNoSeri->$key;
				$qty = $obQty->$key;
				$this->db->query("update tbl_data_barang_stok set qty=(qty-".$qty.") where id_barang='".$id_barang."' and no_seri = '".$no_seri."'; ");
			}
		}


		return $message;
	}
	public function get_laporan_barang_stok_opname($id = NULL, $request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.kode_barang', $request['tx_cari'], 'both');
			$this->db->or_like('a.nama_barang', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}

		$this->db->where('a.delete', '0');
		$this->db->select('a.*, b.nama_satuan, c.nama_group, SUM(d.qty) as stok_qty');
		$this->db->from('tbl_data_barang a');
		$this->db->join('satuan b','b.id = a.id_satuan','inner');
		$this->db->join('group_barang c','c.id = a.id_group','inner');
		$this->db->join('tbl_data_barang_stok d','d.id_barang = a.id','inner');
		$this->db->group_by('a.id');
		$this->db->order_by('a.nama_barang','ASC');

		$query = $this->db->get();
		print_r($this->db->last_query());die;
		return $query->result();
	}
	public function get_laporan_stok_opname_jenis()
	{
		$row = array();
		$query = $this->db->query("
			SELECT count(id_barang) as stok_tersedia
				FROM (
				SELECT a.id as id_barang, a.nama_barang, a.min_stok, SUM(b.qty) as qty FROM
							tbl_data_barang a
							INNER JOIN tbl_data_barang_stok b ON b.id_barang = a.id
							GROUP BY a.id
				) as tbl_tmp
			WHERE
				qty > min_stok
			;
		");
		$row['stok_tersedia'] = $query->row_array()['stok_tersedia'];

		$query = $this->db->query("
			SELECT count(id_barang) as stok_segera_habis
				FROM (
				SELECT a.id as id_barang, a.nama_barang, a.min_stok, SUM(b.qty) as qty FROM
							tbl_data_barang a
							INNER JOIN tbl_data_barang_stok b ON b.id_barang = a.id
							GROUP BY a.id
				) as tbl_tmp
			WHERE
				qty <= min_stok and qty > 0
			;
		");
		$row['stok_segera_habis'] = $query->row_array()['stok_segera_habis'];

		$query = $this->db->query("
			SELECT count(id_barang) as stok_habis
				FROM (
				SELECT a.id as id_barang, a.nama_barang, a.min_stok, SUM(b.qty) as qty FROM
							tbl_data_barang a
							INNER JOIN tbl_data_barang_stok b ON b.id_barang = a.id
							GROUP BY a.id
				) as tbl_tmp
			WHERE
				qty <= 0
			;
		");
		$row['stok_habis'] = $query->row_array()['stok_habis'];
		return $row;
	}
	public function get_laporan_stok_opname_transaksi_barang($id_barang)
	{
		$query = $this->db->query("
				select * from
				((SELECT a.id id_barang,a.nama_barang,'pembelian' jenis_transaksi,date(c.tanggal) as tanggal,b.sub_total as transaksi,b.qty FROM
				tbl_data_barang a
				INNER JOIN tbl_faktur_pembelian_detail_barang b ON b.id_barang = a.id
				INNER JOIN tbl_faktur_pembelian c ON c.id = b.id_faktur_pembelian)
				UNION ALL
				(SELECT a.id id_barang,a.nama_barang,'retur_pembelian' jenis_transaksi,date(d.tanggal) as tanggal,(b.harga_satuan*e.qty) as transaksi,e.qty FROM
				tbl_data_barang a
				INNER JOIN tbl_faktur_pembelian_detail_barang b ON b.id_barang = a.id
				INNER JOIN tbl_faktur_pembelian c ON c.id = b.id_faktur_pembelian
				INNER JOIN tbl_retur_pembelian d ON d.id_faktur_pembelian = c.id
				INNER JOIN tbl_retur_pembelian_detail_barang e ON e.id_retur_pembelian = d.id)
				UNION ALL
				(SELECT a.id id_barang,a.nama_barang,'penjualan' jenis_transaksi,date(c.tanggal) as tanggal,b.sub_total as transaksi,b.qty FROM
				tbl_data_barang a
				INNER JOIN tbl_transaksi_penjualan_detail_barang b ON b.id_barang = a.id
				INNER JOIN tbl_transaksi_penjualan c ON c.id = b.id_transaksi_penjualan)
				UNION ALL
				(SELECT a.id id_barang,a.nama_barang,'retur_penjualan' jenis_transaksi,date(d.tanggal) as tanggal,(b.harga_satuan*e.qty) as transaksi,e.qty FROM
				tbl_data_barang a
				INNER JOIN tbl_transaksi_penjualan_detail_barang b ON b.id_barang = a.id
				INNER JOIN tbl_transaksi_penjualan c ON c.id = b.id_transaksi_penjualan
				INNER JOIN tbl_retur_penjualan d ON d.id_transaksi_penjualan = c.id
				INNER JOIN tbl_retur_penjualan_detail_barang e ON e.id_retur_penjualan = d.id))
				AS tbl_union
				WHERE tbl_union.id_barang = '".$id_barang."'
		");
		return $query->result();
	}
	public function get_real_stock()
	{
		$query = $this->db->query("
									SELECT id_barang, SUM(qty) qty
									FROM `tbl_data_barang_stok`
									WHERE `status` = '1' AND `delete` = '0'
									GROUP BY id_barang;
								");
		$res = $query->result_array();
		return $res;
	}
}
