<?php
class Admin_model extends CI_Model {
	public $rows_per_page = 50;
	public function __construct()
	{
		parent::__construct();
		@define('_PASSWORD_KEY_', 'vEU9fTiboZaoWj3IU1WWMesNTfgWGvTQ1hbKK7bqvU7VxLmOqGEYv1qM'); // BARIS INI HARUS ADA DI admin_model.php DAN site_model.php
	}
	public function admin_log_add($description = NULL)
	{
		$arrdata = array(
			'id_user' => @$_SESSION['admin_id'],
			'ip' => $_SERVER['REMOTE_ADDR'],
			'host' => $_SERVER['PHP_SELF'],
			'description' => $description
		);
		$this->db->insert("admin_log", $arrdata);
	}
	function antiinjection($data)
	{
	  /*
	  $filter_sql = mysql_real_escape_string(stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
	  return $filter_sql;
	  */
	  return $data;
	}
	public function submit_login($data)
	{
		$data['email'] = $this->antiinjection($data['email']);
		$data['password'] = $this->antiinjection(md5($data['password']));
		$query = $this->db->query("
									SELECT a.* FROM admin_user a
									INNER JOIN admin_menu_hak b ON b.id = a.id_hak
									WHERE
									a.email='".$data['email']."' AND
									a.password='".$data['password']."' AND
									a.status='1' AND
									b.status='1' AND
									b.delete='0'
									;
								");
		$row = $query->row();
		if (isset($row)){
			$newdata = array(
				'admin_id'  => $row->id,
				'admin_fullname' => $row->full_name,
				'admin_email' => $row->email,
				'admin_username'  => $row->user_name,
				'admin_password'     => $row->password,
				'admin_id_hak_akses' => $row->id_hak,
				'admin_login' => 1
			);
			$this->session->set_userdata($newdata);
			redirect('/');
		}else{
			return "<b>Gagal login</b>, User Name atau Password salah!";
		}
	}
	public function update_table_database($request){
		$res = $this->db->update($request['data_table'], $request['data_update'], $request['data_where']);
		if($res == 1){
			$message = "Berhasil Update Data";
		}else{
			$message = "Gagal Update Data!";
		}
		return $message;
	}
	public function if_exist_data_table($request){
		$this->db->where($request['data_where']);
		$this->db->from($request['data_table']);
		$query = $this->db->get();
		return $query->num_rows();
	}
	public function upload_ktp_customer($file_image_name,$file)
	{
		$config['upload_path']          = './assets/upload_ktp_customer/original/';
		$config['allowed_types']        = 'jpg|png';
		$config['file_name']        	= $file_image_name;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$do_upload = $this->upload->do_upload($file);

		if (!$do_upload){
			$message = 'Simpan gambar gagal: '.$this->upload->display_errors();
		}else{
			$this->upload_ktp_customer_display($config,$file);
			$this->load->library('image_lib');
			$message = 'Simpan gambar berhasil';
		}
		unset($config);
		return $message;

	}
	public function upload_ktp_customer_display($data_config,$file)
	{
		$input_file = $data_config['upload_path'].$data_config['file_name'];
		$output_file = str_replace("original/","display/",$data_config['upload_path']).$data_config['file_name'];

		if($_FILES[$file]['type'] == "image/jpeg") $im = imagecreatefromjpeg($input_file);
		if($_FILES[$file]['type'] == "image/png") $im = imagecreatefrompng($input_file);

		$cropped = imagecropauto($im, IMG_CROP_DEFAULT);
		if($cropped !== false){
			imagedestroy($im);
			$im = $cropped;
		}
		if($_FILES[$file]['type'] == "image/jpeg") imagejpeg($im, $output_file);
		if($_FILES[$file]['type'] == "image/png") imagepng($im, $output_file);
		imagedestroy($im);
	}
	public function product_upload_file($data,$file)
	{
		$config['upload_path']          = './assets/upload_product/original/';
		$config['allowed_types']        = 'jpg|png';
		$config['file_name']        = $data['file_image_name'];

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$do_upload = $this->upload->do_upload('image');

		if (!$do_upload){
			$message = 'Simpan gambar gagal: '.$this->upload->display_errors();
		}else{
			$this->product_upload_file_crop($config['upload_path'].$config['file_name']);
			$this->product_upload_file_display($config);
			$this->load->library('image_lib');
			$message = $this->product_upload_file_resize($config, "300");
			$message .= $this->product_upload_file_resize($config, "200");
			$message .= $this->product_upload_file_resize($config, "100");
			$message .= $this->product_upload_file_resize($config, "50");
			$message .= 'Simpan gambar berhasil';
		}
		unset($config);
		return $message;

	}
	public function product_upload_file_crop($file_to_change)
	{
		if(!isset($_FILES['image'])) $_FILES['image'] = $_FILES['picture'];

		if($_FILES['image']['type'] == "image/jpeg") $im = imagecreatefromjpeg($file_to_change);
		if($_FILES['image']['type'] == "image/png") $im = imagecreatefrompng($file_to_change);
		$cropped = imagecropauto($im, IMG_CROP_DEFAULT);
		if($cropped !== false){
			imagedestroy($im);
			unlink($file_to_change);
			$im = $cropped;
		}
		if($_FILES['image']['type'] == "image/jpeg") imagejpeg($im, $file_to_change);
		if($_FILES['image']['type'] == "image/png") imagepng($im, $file_to_change);
		imagedestroy($im);
	}
	public function product_upload_file_display($data_config)
	{
		$input_file = $data_config['upload_path'].$data_config['file_name'];
		$output_file = str_replace("original/","display/",$data_config['upload_path']).$data_config['file_name'];
		if(!isset($_FILES['image'])) $_FILES['image'] = $_FILES['picture'];

		if(isset($_REQUEST['image-data'])){
			$base64_image_string = base64_decode($_REQUEST['image-data']);
			// split the string on commas
			// $data[ 0 ] == "data:image/png;base64"
			// $data[ 1 ] == <actual base64 string>
			$data = explode( ',', $base64_image_string );
			$image_decode = base64_decode($data[1]);

			$im = imagecreatefromstring($image_decode);
		}else{
			if($_FILES['image']['type'] == "image/jpeg") $im = imagecreatefromjpeg($input_file);
			if($_FILES['image']['type'] == "image/png") $im = imagecreatefrompng($input_file);
		}

		$cropped = imagecropauto($im, IMG_CROP_DEFAULT);
		if($cropped !== false){
			imagedestroy($im);
			$im = $cropped;
		}
		if($_FILES['image']['type'] == "image/jpeg") imagejpeg($im, $output_file);
		if($_FILES['image']['type'] == "image/png") imagepng($im, $output_file);
		imagedestroy($im);
	}
	public function product_upload_file_resize($data_config, $new_size)
	{
		$res = "";
		$config['image_library'] = 'gd2';
		$config['source_image'] = str_replace("original/","display/",$data_config['upload_path']).$data_config['file_name'];
		$config['new_image'] = str_replace("original/",$new_size."/",$data_config['upload_path']).$data_config['file_name'];
		$config['maintain_ratio'] = TRUE;
		$config['width']         = $new_size;
		$this->image_lib->clear();
		$this->image_lib->initialize($config);
		$resize = $this->image_lib->resize();
		if (!$resize)
		{
			$res = $this->image_lib->display_errors()."<br>";
		}else{
			$this->product_upload_file_crop($config['new_image']);
		}
		return $res;
	}
	public function get_setting_app($id = NULL,$request = NULL)
	{
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		$this->db->select('a.*');
		$this->db->from('tbl_setting_app a');
		$query = $this->db->get();
		return $query->result();
	}
	public function edit_setting_app($request = NULL)
	{
		$updateData = array(
			'bagi_hasil_perusahaan' => $request['bagi_hasil_perusahaan'],
			'bagi_hasil_investor' => $request['bagi_hasil_investor'],
			'pajak' => $request['pajak'],
			'tgl_update' => date("Y-m-d H:i:s")
		);
		$whereEdit = array(
			'id' => $request['id_setting_app']
		);
		$res = $this->db->update("tbl_setting_app", $updateData, $whereEdit);

		if($res == 1){
			$message = "Berhasil Edit Data";
		}else{
			$message = "Gagal Edit Data!";
		}
		return $message;
	}
	public function get_admin_menu($id_admin_menu = NULL)
	{
		if($id_admin_menu <> NULL){
			$this->db->where('id', $id_admin_menu);
		}
		$this->db->from('admin_menu');
		$this->db->order_by('parent','ASC');
		$this->db->order_by('urut','ASC');
		$this->db->order_by('id','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function get_admin_menu_hak($id_admin_menu_hak = NULL,$request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->like('name', $request['tx_cari_hak_akses'], 'both');
		}
		if($id_admin_menu_hak <> NULL){
			$this->db->where('id', $id_admin_menu_hak);
		}
		$this->db->where('delete', '0');
		$this->db->from('admin_menu_hak');
		$this->db->order_by('name','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function get_admin_menu_hak_akses($id_hak = NULL)
	{
		if($id_hak <> NULL){
			$this->db->where('a.id_hak', $id_hak);
		}
		$this->db->select('a.*, b.name menu_name, b.url menu_url');
		$this->db->from('admin_menu_hak_akses a');
		$this->db->join('admin_menu b','b.id = a.id_menu','inner');
		$this->db->order_by('a.id','ASC');
		$query = $this->db->get();
		// print_r($this->db->last_query());die;
		return $query->result();
	}
	public function add_hak_akses($request = NULL)
	{
		$insertData = array(
			'name' => $request['name'],
			'description' => $request['description'],
			'status' => $request['status']
		);
		$this->db->insert("admin_menu_hak", $insertData);
		$insert_id = $this->db->insert_id();

		$ar_hak_akses = array();
		$id_menu = $request['id_menu'];
		foreach($id_menu as $id){
			$data = array();
			$data['id_hak'] = $insert_id;
			$data['id_menu'] = $id;
			$data['lihat'] = @$request['lihat_'.$id];
			$data['tambah'] = @$request['tambah_'.$id];
			$data['ubah'] = @$request['ubah_'.$id];
			$data['hapus'] = @$request['hapus_'.$id];
				if($data['lihat'] !="" or $data['tambah'] !="" or $data['ubah'] !="" or $data['hapus'] !="")
					$ar_hak_akses[] = $data;
		}
		$this->db->insert_batch('admin_menu_hak_akses', $ar_hak_akses);

		if(is_numeric($insert_id)){
			$message = "Berhasil Input Data";
		}else{
			$message = "Gagal Input Data!";
		}
		return $message;
	}
	public function edit_hak_akses($request = NULL)
	{
		$this->db->delete('admin_menu_hak_akses', array('id_hak' => $request['id_hak_akses']));
		$updateData = array(
			'name' => $request['name'],
			'description' => $request['description'],
			'status' => $request['status']
		);
		$whereEdit = array(
			'id' => $request['id_hak_akses']
		);
		$res = $this->db->update("admin_menu_hak", $updateData, $whereEdit);

		$ar_hak_akses = array();
		$id_menu = $request['id_menu'];
		foreach($id_menu as $id){
			$data = array();
			$data['id_hak'] = $request['id_hak_akses'];
			$data['id_menu'] = $id;
			$data['lihat'] = @$request['lihat_'.$id];
			$data['tambah'] = @$request['tambah_'.$id];
			$data['ubah'] = @$request['ubah_'.$id];
			$data['hapus'] = @$request['hapus_'.$id];
				if($data['lihat'] !="" or $data['tambah'] !="" or $data['ubah'] !="" or $data['hapus'] !="")
					$ar_hak_akses[] = $data;
		}
		$this->db->insert_batch('admin_menu_hak_akses', $ar_hak_akses);

		if($res == 1){
			$message = "Berhasil Edit Data";
		}else{
			$message = "Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_hak_akses($id_hak_akses = NULL)
	{
		$updateData = array(
			'delete' => '1',
		);
		$whereEdit = array(
			'id' => $id_hak_akses
		);
		$res = $this->db->update("admin_menu_hak", $updateData, $whereEdit);
		$this->db->delete('admin_menu_hak_akses', array('id_hak' => $id_hak_akses));
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}
		return $message;
	}
	public function get_admin_user($id = NULL,$request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.full_name', $request['tx_cari'], 'both');
			$this->db->or_like('a.user_name', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*, b.name as hak_akses_name');
		$this->db->from('admin_user a');
		$this->db->join('admin_menu_hak b','b.id = a.id_hak','left');

		$this->db->order_by('a.full_name','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_admin_user($request = NULL)
	{
		$insertData = array(
			'full_name' => $request['full_name'],
			'email' => $request['email'],
			'user_name' => $request['user_name'],
			'password' => md5($request['password']),
			'id_hak' => $request['id_hak'],
			'status' => $request['status']
		);
		$this->db->insert("admin_user", $insertData);
		$insert_id = $this->db->insert_id();

		if(is_numeric($insert_id)){
			$message = "Berhasil Input Data";
		}else{
			$message = "Gagal Input Data!";
		}
		return $message;
	}
	public function edit_admin_user($request = NULL)
	{
		$updateData = array(
			'full_name' => $request['full_name'],
			'email' => $request['email'],
			'user_name' => $request['user_name'],
			'id_hak' => $request['id_hak'],
			'status' => $request['status']
		);
		if(isset($request['password']) and $request['password']<>""){
			$updateData['password'] = md5($request['password']);
		}
		$whereEdit = array(
			'id' => $request['id_admin_user']
		);
		$res = $this->db->update("admin_user", $updateData, $whereEdit);

		if($res == 1){
			$message = "Berhasil Edit Data";
		}else{
			$message = "Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_admin_user($id = NULL)
	{
		$updateData = array(
			'delete' => '1',
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("admin_user", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}
		return $message;
	}
	public function get_app_hak_akses(){
		$result = NULL;
		if(isset($_SESSION['admin_id_hak_akses'])){
			$get_admin_menu_hak_akses = $this->get_admin_menu_hak_akses($_SESSION['admin_id_hak_akses']);
			$arrIdMenuOpen = array();
			foreach($get_admin_menu_hak_akses as $row){
				$id_menu = $row->id_menu;
				$arrIdMenuOpen[$id_menu]['lihat'] = $row->lihat;
				$arrIdMenuOpen[$id_menu]['tambah'] = $row->tambah;
				$arrIdMenuOpen[$id_menu]['ubah'] = $row->ubah;
				$arrIdMenuOpen[$id_menu]['hapus'] = $row->hapus;

				$url_menu = $row->menu_url;
				$arrIdMenuOpen[$url_menu]['lihat'] = $row->lihat;
				$arrIdMenuOpen[$url_menu]['tambah'] = $row->tambah;
				$arrIdMenuOpen[$url_menu]['ubah'] = $row->ubah;
				$arrIdMenuOpen[$url_menu]['hapus'] = $row->hapus;
			}

				$arrIdMenuOpen['admin/profile']['lihat'] = "on";
				$arrIdMenuOpen['admin/profile']['tambah'] = "on";
				$arrIdMenuOpen['admin/profile']['ubah'] = "on";
				$arrIdMenuOpen['admin/profile']['hapus'] = "on";

			$result = $arrIdMenuOpen;
		}
		return $result;
	}
	public function get_group_barang($id = NULL,$request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.kode_group', $request['tx_cari'], 'both');
			$this->db->or_like('a.nama_group', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*');
		$this->db->from('group_barang a');

		$this->db->order_by('a.kode_group','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_group_barang($request = NULL)
	{
		$insertData = array(
			'kode_group' => $request['kode_group'],
			'nama_group' => $request['nama_group'],
			'status' => $request['status']
		);
		$this->db->insert("group_barang", $insertData);
		$insert_id = $this->db->insert_id();

		if(is_numeric($insert_id)){
			$message = "Berhasil Input Data";
		}else{
			$message = "Gagal Input Data!";
		}
		return $message;
	}
	public function edit_group_barang($request = NULL)
	{
		$updateData = array(
			'kode_group' => $request['kode_group'],
			'nama_group' => $request['nama_group'],
			'status' => $request['status']
		);
		$whereEdit = array(
			'id' => $request['id_group_barang']
		);
		$res = $this->db->update("group_barang", $updateData, $whereEdit);

		if($res == 1){
			$message = "Berhasil Edit Data";
		}else{
			$message = "Gagal Edit Data!";
		}
		return $message;
	}
	public function edit_group_barang_setting($request = NULL)
	{
		$updateData = array(
			'stat_kode_group' => @$request['stat_kode_group'],
			'stat_nama' => @$request['stat_nama'],
			'stat_no_urut' => @$request['stat_no_urut'],
			'pemisah' => $request['pemisah'],
			'panjang_no_urut' => $request['panjang_no_urut']
		);
		$whereEdit = array(
			'id' => $request['id_group_barang']
		);
		$res = $this->db->update("group_barang", $updateData, $whereEdit);

		if($res == 1){
			$message = "Berhasil Edit Data";
		}else{
			$message = "Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_group_barang($id = NULL)
	{
		$updateData = array(
			'delete' => '1',
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("group_barang", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}
		return $message;
	}
	public function get_satuan($id = NULL,$request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.kode_satuan', $request['tx_cari'], 'both');
			$this->db->or_like('a.nama_satuan', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*');
		$this->db->from('satuan a');

		$this->db->order_by('a.kode_satuan','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_satuan($request = NULL)
	{
		$insertData = array(
			'kode_satuan' => $request['kode_satuan'],
			'nama_satuan' => $request['nama_satuan'],
			'status' => $request['status']
		);
		$this->db->insert("satuan", $insertData);
		$insert_id = $this->db->insert_id();

		if(is_numeric($insert_id)){
			$message = "Berhasil Input Data";
		}else{
			$message = "Gagal Input Data!";
		}
		return $message;
	}
	public function edit_satuan($request = NULL)
	{
		$updateData = array(
			'kode_satuan' => $request['kode_satuan'],
			'nama_satuan' => $request['nama_satuan'],
			'status' => $request['status']
		);
		$whereEdit = array(
			'id' => $request['id_satuan']
		);
		$res = $this->db->update("satuan", $updateData, $whereEdit);

		if($res == 1){
			$message = "Berhasil Edit Data";
		}else{
			$message = "Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_satuan($id = NULL)
	{
		$updateData = array(
			'delete' => '1',
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("satuan", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}
		return $message;
	}
	public function get_no_urut_by_group_barang($id_group = NULL, $id_data_barang = NULL)
	{
		$sql = "SELECT last_no_urut FROM tbl_no_urut_by_group_barang where id_group_barang = '".$id_group."' ;";
		$query = $this->db->query($sql);
		$last_no_urut = (isset($query->row()->last_no_urut))?(($query->row()->last_no_urut)+1):1;
		if($last_no_urut > 1)
			$this->db->query("UPDATE tbl_no_urut_by_group_barang SET last_no_urut = '".$last_no_urut."' WHERE  id_group_barang = '".$id_group."' ;");
		else
			$this->db->query("INSERT INTO tbl_no_urut_by_group_barang (id_group_barang, last_no_urut) values ('".$id_group."', '".$last_no_urut."');");
		return $last_no_urut;
	}
	public function get_kode_barang($id_group = NULL, $id_data_barang = NULL, $stat_apiweb = NULL)
	{
		$res_kode_barang = "";
		$get_group_barang = $this->get_group_barang($id_group);
			$kode_group = $get_group_barang[0]->kode_group;
			$nama_group = $get_group_barang[0]->nama_group;
			$stat_kode_group = $get_group_barang[0]->stat_kode_group;
			$stat_nama = $get_group_barang[0]->stat_nama;
			$stat_no_urut = $get_group_barang[0]->stat_no_urut;
			$pemisah = $get_group_barang[0]->pemisah;
			$panjang_no_urut = $get_group_barang[0]->panjang_no_urut;
		if($stat_kode_group == "1"){
			$res_kode_barang .= $kode_group.$pemisah;
		}
		if($stat_nama == "1"){
			$res_kode_barang .= $nama_group.$pemisah;
		}
		if($stat_apiweb <> 1){
			if($stat_no_urut == "1"){
				$res_no_urut = $this->get_no_urut_by_group_barang($id_group, $id_data_barang);
				$res_kode_barang .= numLength($res_no_urut, $panjang_no_urut);
			}
		}

		return $res_kode_barang;
	}
	public function get_no_seri_barang($id = NULL, $request = NULL)
	{
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}

		if(isset($request['id_barang'])){
			$this->db->where('a.id_barang', $request['id_barang']);
		}

		$this->db->where('a.delete', '0');
		$this->db->where('a.qty >', '0');
		$this->db->select('no_seri');
		$this->db->from('tbl_data_barang_stok a');
		$this->db->order_by('a.tgl_input','ASC');
		$this->db->order_by('a.id','ASC');
		$query = $this->db->get();
		$result = $query->result_array();
		$len = (count($result)-1);
		$ret['first_no_seri'] = @$result[0]['no_seri'];
		$ret['last_no_seri'] = @$result[$len]['no_seri'];
		return $ret;
	}
	public function get_data_barang_stok($id = NULL, $request = NULL)
	{
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}

		if(isset($request['id_barang'])){
			$this->db->where('a.id_barang', $request['id_barang']);
		}

		$this->db->where('a.delete', '0');
		$this->db->select('a.*, b.nama_barang, c.nama_satuan, d.nama_group');
		$this->db->from('tbl_data_barang_stok a');
		$this->db->join('tbl_data_barang b','b.id = a.id_barang','inner');
		$this->db->join('satuan c','c.id = b.id_satuan','inner');
		$this->db->join('group_barang d','d.id = b.id_group','inner');

		$this->db->order_by('a.no_seri','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function get_data_barang($id = NULL, $request = NULL, $not_in_id_barang = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.kode_barang', $request['tx_cari'], 'both');
			$this->db->or_like('a.nama_barang', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		if($not_in_id_barang <> NULL and is_array($not_in_id_barang)){
			foreach($not_in_id_barang as $row){
				$this->db->where('a.id <>', $row->id_barang);
			}
		}
		$this->db->where('a.delete', '0');
		$this->db->select("
							a.*,
							b.nama_satuan,
							c.nama_group,
							IFNULL(d.harga_jual_normal, 0) as harga_jual,
							IFNULL((
								SELECT
									sb.harga
								FROM `tbl_data_promo` sa
								INNER JOIN `tbl_data_promo_barang` sb ON sb.id_data_promo = sa.id
								WHERE
									(NOW() >= sa.tanggal AND NOW() <= sb.periode_sampai_dengan)
									AND sb.id_barang = a.id
									AND sa.`status` = '1'
									AND sa.`delete` = '0'
								ORDER BY sb.harga ASC
								LIMIT 1
							),0) as harga_jual_promo
			");
		$this->db->from('tbl_data_barang a');
		$this->db->join('satuan b','b.id = a.id_satuan','inner');
		$this->db->join('group_barang c','c.id = a.id_group','inner');
		$this->db->join('tbl_harga_jual d','d.id_barang = a.id','left');

		$this->db->order_by('a.kode_barang','ASC');
		$query = $this->db->get();
		$res = $query->result_array();
		foreach($res as $row => $d){
			if($d['harga_jual_promo'] > 0 and $d['harga_jual_promo'] < $d['harga_jual']){
				$d['harga_jual'] = $d['harga_jual_promo'];
			}
			$res[$row] = $d;
		}
		//debug($this->db->last_query());
		return json_decode(json_encode($res));
	}
	public function add_data_barang($request = NULL, $file = NULL)
	{
// 		$request['kode_barang'] = $this->get_kode_barang($request['id_group'], 0, 0);
		$insertData = array(
			'kode_barang' => $request['kode_barang'],
			'nama_barang' => $request['nama_barang'],
			'id_satuan' => $request['id_satuan'],
			'id_group' => $request['id_group'],
			'min_stok' => $request['min_stok'],
			'deskripsi' => $request['deskripsi'],
			'status' => $request['status']
		);

		if(isset($file['image']['name']) and $file['image']['name'] != "" and $file['image']['name'] != null){
			$request['file_image_name'] = date("Ymdhisa")."-".md5($request['nama_barang'])."-".str_replace(" ","-",$file['image']['name']);
			$insertData['image'] = $request['file_image_name'];
			$message_upload_image = $this->product_upload_file($request,$file);
		}
		$this->db->insert("tbl_data_barang", $insertData);
		$insert_id = $this->db->insert_id();

		if(is_numeric($insert_id)){
			$message = @$message_upload_image."<br/>Berhasil Input Data";
		}else{
			$message = "Gagal Input Data!";
		}
		return $message;
	}
	public function edit_data_barang($request = NULL, $file = NULL)
	{
		$get_data_barang = $this->get_data_barang($request['id_data_barang']);
		$id_group_old = $get_data_barang[0]->id_group;
		$id_group_new = $request['id_group'];

		$updateData = array(
		    'kode_barang' => $request['kode_barang'],
			'nama_barang' => $request['nama_barang'],
			'id_satuan' => $request['id_satuan'],
			'id_group' => $request['id_group'],
			'min_stok' => $request['min_stok'],
			'deskripsi' => $request['deskripsi'],
			'status' => $request['status']
		);
// 		if($id_group_new != $id_group_old){
// 			$new_kode_barang = $this->get_kode_barang($request['id_group'], $request['id_data_barang'], 0);
// 			$updateData['kode_barang'] = $new_kode_barang;
// 		}

		if(isset($file['image']['name']) and $file['image']['name']<>""){
			$file_image = $request['file_image_name'];
			@unlink("./assets/upload_product/original/".$file_image);
			@unlink("./assets/upload_product/display/".$file_image);
			@unlink("./assets/upload_product/300/".$file_image);
			@unlink("./assets/upload_product/200/".$file_image);
			@unlink("./assets/upload_product/100/".$file_image);
			@unlink("./assets/upload_product/50/".$file_image);
			$request['file_image_name'] = date("Ymdhisa")."-".md5($request['nama_barang'])."-".str_replace(" ","-",$file['image']['name']);

			$updateData['image'] = $request['file_image_name'];

			$message_upload_image = $this->product_upload_file($request,$file)."<br>";
		}
		$whereEdit = array(
			'id' => $request['id_data_barang']
		);
		$res = $this->db->update("tbl_data_barang", $updateData, $whereEdit);

		if($res == 1){
			$message = @$message_upload_image."<br/>Berhasil Edit Data";
		}else{
			$message = @$message_upload_image."<br/>Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_data_barang($id = NULL)
	{
		$get_data_barang = $this->get_data_barang($id);
		$file_image = $get_data_barang[0]->image;
		@unlink("./assets/upload_product/original/".$file_image);
		@unlink("./assets/upload_product/display/".$file_image);
		@unlink("./assets/upload_product/300/".$file_image);
		@unlink("./assets/upload_product/200/".$file_image);
		@unlink("./assets/upload_product/100/".$file_image);
		@unlink("./assets/upload_product/50/".$file_image);

		$updateData = array(
			'delete' => '1',
			'image' => ''
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("tbl_data_barang", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}
		return $message;
	}
	public function get_harga_jual($id = NULL,$request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.harga_jual_normal', $request['tx_cari'], 'both');
			$this->db->or_like('b.nama_barang', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*, b.nama_barang, c.nama_satuan, d.nama_group nama_group_barang');
		$this->db->from('tbl_harga_jual  a');
		$this->db->join('tbl_data_barang b','b.id = a.id_barang','inner');
		$this->db->join('satuan c','c.id = b.id_satuan','inner');
		$this->db->join('group_barang d','d.id = b.id_group','inner');

		$this->db->order_by('b.nama_barang','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_harga_jual($request = NULL)
	{
		$insertData = array(
			'id_barang' => $request['id_barang'],
			'harga_beli' => $request['harga_beli'],
			'harga_jual_minimal' => $request['harga_jual_minimal'],
			'harga_jual_normal' => $request['harga_jual_normal'],
			'harga_jual_outlet' => $request['harga_jual_outlet'],
			'status' => $request['status']
		);
		$this->db->insert("tbl_harga_jual", $insertData);
		$insert_id = $this->db->insert_id();

		if(is_numeric($insert_id)){
			$message = "Berhasil Input Data";
		}else{
			$message = "Gagal Input Data!";
		}
		return $message;
	}
	public function edit_harga_jual($request = NULL)
	{
		$updateData = array(
			'id_barang' => $request['id_barang'],
			'harga_beli' => $request['harga_beli'],
			'harga_jual_minimal' => $request['harga_jual_minimal'],
			'harga_jual_normal' => $request['harga_jual_normal'],
			'harga_jual_outlet' => $request['harga_jual_outlet'],
			'status' => $request['status']
		);
		$whereEdit = array(
			'id' => $request['id_harga_jual']
		);
		$res = $this->db->update("tbl_harga_jual", $updateData, $whereEdit);

		if($res == 1){
			$message = "Berhasil Edit Data";
		}else{
			$message = "Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_harga_jual($id = NULL)
	{
		$updateData = array(
			'delete' => '1',
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("tbl_harga_jual", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}
		return $message;
	}
	public function get_kota($id = NULL,$request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.kode_kota', $request['tx_cari'], 'both');
			$this->db->or_like('a.nama_kota', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*');
		$this->db->from('tbl_kota a');

		$this->db->order_by('a.kode_kota','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_kota($request = NULL)
	{
		$insertData = array(
			'kode_kota' => $request['kode_kota'],
			'nama_kota' => $request['nama_kota'],
			'wilayah' => $request['wilayah'],
			'status' => $request['status']
		);
		$this->db->insert("tbl_kota", $insertData);
		$insert_id = $this->db->insert_id();

		if(is_numeric($insert_id)){
			$message = "Berhasil Input Data";
		}else{
			$message = "Gagal Input Data!";
		}
		return $message;
	}
	public function edit_kota($request = NULL)
	{
		$updateData = array(
			'kode_kota' => $request['kode_kota'],
			'nama_kota' => $request['nama_kota'],
			'wilayah' => $request['wilayah'],
			'status' => $request['status']
		);
		$whereEdit = array(
			'id' => $request['id_kota']
		);
		$res = $this->db->update("tbl_kota", $updateData, $whereEdit);

		if($res == 1){
			$message = "Berhasil Edit Data";
		}else{
			$message = "Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_kota($id = NULL)
	{
		$updateData = array(
			'delete' => '1',
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("tbl_kota", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}
		return $message;
	}

	# GUDANG
	public function get_gudang($id = NULL,$request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.kode_gudang', $request['tx_cari'], 'both');
			$this->db->or_like('a.nama_gudang', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*');
		$this->db->from('tbl_gudang a');

		$this->db->order_by('a.kode_gudang','ASC');
		$query = $this->db->get();
		// print_r($this->db->last_query());die;
		return $query->result();
	}
	public function add_gudang($request = NULL)
	{
		$insertData = array(
			'kode_gudang' => $request['kode_gudang'],
			'nama_gudang' => $request['nama_gudang'],
			'wilayah' => $request['wilayah'],
			'status' => $request['status']
		);
		$this->db->insert("tbl_gudang", $insertData);
		$insert_id = $this->db->insert_id();

		if(is_numeric($insert_id)){
			$message = "Berhasil Input Data";
		}else{
			$message = "Gagal Input Data!";
		}
		return $message;
	}
	public function edit_gudang($request = NULL)
	{
		$updateData = array(
			'kode_gudang' => $request['kode_gudang'],
			'nama_gudang' => $request['nama_gudang'],
			'wilayah' => $request['wilayah'],
			'status' => $request['status']
		);
		$whereEdit = array(
			'id' => $request['id_gudang']
		);
		$res = $this->db->update("tbl_gudang", $updateData, $whereEdit);

		if($res == 1){
			$message = "Berhasil Edit Data";
		}else{
			$message = "Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_gudang($id = NULL)
	{
		$updateData = array(
			'delete' => '1',
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("tbl_gudang", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}
		return $message;
	}

	# DESIGN
	public function get_design($id = NULL,$request = NULL){
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.customer', $request['tx_cari'], 'both');
			// $this->db->or_like('a.nama_gudang', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*');
		$this->db->from('tbl_design a');

		$this->db->order_by('a.tanggal','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_design($request = NULL, $file = NULL){
		$message_upload_image = '';
		$insertData = array(
			'tanggal' 	=> $request['tanggal'],
			'customer' 	=> $request['customer'],
			'status' 		=> $request['status'],
			'warna1' 		=> $request['warna1'],
			'warna2' 		=> $request['warna2']
		);

		if(isset($file['gambar1']['name']) and $file['gambar1']['name']<>""){
			$request['gambar1_name'] = date("Ymdhisa")."-".md5($request['customer'].'Logo')."-".str_replace(" ","-",$file['gambar1']['name']);
			$message_upload_image .= $this->upload_design($request['gambar1_name'],'gambar1');
			$insertData['gambar1'] = $request['gambar1_name'];
		}

		if(isset($file['gambar2']['name']) and $file['gambar2']['name']<>""){
			$request['gambar2_name'] = date("Ymdhisa")."-".md5($request['customer'].'Gambar')."-".str_replace(" ","-",$file['gambar2']['name']);
			$message_upload_image .= $this->upload_design($request['gambar2_name'],'gambar2');
			$insertData['gambar2'] = $request['gambar2_name'];
		}

		$this->db->insert("tbl_design", $insertData);
		$insert_id = $this->db->insert_id();

		if(is_numeric($insert_id)){
			$message = "Berhasil Input Data";
		}else{
			$message = "Gagal Input Data!";
		}
		return $message;
	}
	public function upload_design($file_image_name,$file){
		$config['upload_path']     = './assets/upload_design/original/';
		$config['allowed_types']   = 'jpg|png';
		$config['file_name']       = $file_image_name;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$do_upload = $this->upload->do_upload($file);

		if (!$do_upload){
			$message = 'Simpan gambar gagal: '.$this->upload->display_errors();
		}else{
			$this->upload_design_display($config,$file);
			$this->load->library('image_lib');
			$message = 'Simpan gambar berhasil';
		}
		unset($config);
		return $message;
	}
	public function upload_design_display($data_config,$file){
		$input_file = $data_config['upload_path'].$data_config['file_name'];
		$output_file = str_replace("original/","display/",$data_config['upload_path']).$data_config['file_name'];

		if($_FILES[$file]['type'] == "image/jpg") $im = imagecreatefromjpeg($input_file);
		if($_FILES[$file]['type'] == "image/png") $im = imagecreatefrompng($input_file);

		$cropped = imagecropauto($im, IMG_CROP_DEFAULT);
		if($cropped !== false){
			imagedestroy($im);
			$im = $cropped;
		}
		if($_FILES[$file]['type'] == "image/jpg") imagejpeg($im, $output_file);
		if($_FILES[$file]['type'] == "image/png") imagepng($im, $output_file);
		imagedestroy($im);
	}


	public function get_group_suplier($id = NULL,$request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.id', $request['tx_cari'], 'both');
			$this->db->or_like('a.nama_group_suplier', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*');
		$this->db->from('tbl_group_suplier a');

		$this->db->order_by('a.id','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_group_suplier($request = NULL)
	{
		$insertData = array(
			'nama_group_suplier' => $request['nama_group_suplier'],
			'status' => $request['status']
		);
		$this->db->insert("tbl_group_suplier", $insertData);
		$insert_id = $this->db->insert_id();

		if(is_numeric($insert_id)){
			$message = "Berhasil Input Data";
		}else{
			$message = "Gagal Input Data!";
		}
		return $message;
	}
	public function edit_group_suplier($request = NULL)
	{
		$updateData = array(
			'nama_group_suplier' => $request['nama_group_suplier'],
			'status' => $request['status']
		);
		$whereEdit = array(
			'id' => $request['id_group_suplier']
		);
		$res = $this->db->update("tbl_group_suplier", $updateData, $whereEdit);

		if($res == 1){
			$message = "Berhasil Edit Data";
		}else{
			$message = "Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_group_suplier($id = NULL)
	{
		$updateData = array(
			'delete' => '1',
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("tbl_group_suplier", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}
		return $message;
	}
	public function get_no_urut_by_group_suplier($id_group = NULL)
	{
		$sql = "SELECT last_no_urut FROM tbl_no_urut_by_group_suplier where id_group_suplier = '".$id_group."' ;";
		$query = $this->db->query($sql);
		$last_no_urut = (isset($query->row()->last_no_urut))?(($query->row()->last_no_urut)+1):1;
		if($last_no_urut > 1)
			$this->db->query("UPDATE tbl_no_urut_by_group_suplier SET last_no_urut = '".$last_no_urut."' WHERE  id_group_suplier = '".$id_group."' ;");
		else
			$this->db->query("INSERT INTO tbl_no_urut_by_group_suplier (id_group_suplier, last_no_urut) values ('".$id_group."', '".$last_no_urut."');");
		return $last_no_urut;
	}
	public function get_data_suplier($id = NULL,$request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.kode_suplier', $request['tx_cari'], 'both');
			$this->db->or_like('a.nama_suplier', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*, b.nama_group_suplier');
		$this->db->from('tbl_suplier a');
		$this->db->join('tbl_group_suplier b','b.id = a.id_group_suplier','inner');

		$this->db->order_by('a.kode_suplier','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_data_suplier($request = NULL)
	{
		$new_kode_suplier = $request['id_group_suplier'].$this->get_no_urut_by_group_suplier($request['id_group_suplier']);
		$insertData = array(
			'id_group_suplier' => $request['id_group_suplier'],
			'kode_suplier' => $new_kode_suplier,
			'nama_suplier' => $request['nama_suplier'],
			'email' => $request['email'],
			'alamat' => $request['alamat'],
			'nama_kota' => $request['nama_kota'],
			'telepon' => $request['telepon'],
			'jatuh_tempo' => $request['jatuh_tempo'],
			'status' => $request['status']
		);
		$this->db->insert("tbl_suplier", $insertData);
		$insert_id = $this->db->insert_id();

		if(is_numeric($insert_id)){
			$message = "Berhasil Input Data";
		}else{
			$message = "Gagal Input Data!";
		}
		return $message;
	}
	public function edit_data_suplier($request = NULL)
	{
		$get_data_suplier = $this->get_data_suplier($request['id_suplier']);
		$id_group_old = $get_data_suplier[0]->id_group_suplier;
		$id_group_new = $request['id_group_suplier'];
		$updateData = array(
			'id_group_suplier' => $request['id_group_suplier'],
			'nama_suplier' => $request['nama_suplier'],
			'email' => $request['email'],
			'alamat' => $request['alamat'],
			'nama_kota' => $request['nama_kota'],
			'telepon' => $request['telepon'],
			'jatuh_tempo' => $request['jatuh_tempo'],
			'status' => $request['status']
		);
		if($id_group_new != $id_group_old){
			$new_kode_suplier = $request['id_group_suplier'].$this->get_no_urut_by_group_suplier($request['id_group_suplier']);
			$updateData['kode_suplier'] = $new_kode_suplier;
		}
		$whereEdit = array(
			'id' => $request['id_suplier']
		);
		$res = $this->db->update("tbl_suplier", $updateData, $whereEdit);

		if($res == 1){
			$message = "Berhasil Edit Data";
		}else{
			$message = "Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_data_suplier($id = NULL)
	{
		$updateData = array(
			'delete' => '1',
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("tbl_suplier", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}
		return $message;
	}
	public function get_group_customer($id = NULL,$request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.kode_group_customer', $request['tx_cari'], 'both');
			$this->db->or_like('a.nama_group_customer', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*');
		$this->db->from('tbl_group_customer a');

		$this->db->order_by('a.kode_group_customer','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_group_customer($request = NULL)
	{
		$insertData = array(
			'kode_group_customer' => $request['kode_group_customer'],
			'nama_group_customer' => $request['nama_group_customer'],
			'status' => $request['status']
		);
		$this->db->insert("tbl_group_customer", $insertData);
		$insert_id = $this->db->insert_id();

		if(is_numeric($insert_id)){
			$message = "Berhasil Input Data";
		}else{
			$message = "Gagal Input Data!";
		}
		return $message;
	}
	public function edit_group_customer($request = NULL)
	{
		$updateData = array(
			'kode_group_customer' => $request['kode_group_customer'],
			'nama_group_customer' => $request['nama_group_customer'],
			'status' => $request['status']
		);
		$whereEdit = array(
			'id' => $request['id_group_customer']
		);
		$res = $this->db->update("tbl_group_customer", $updateData, $whereEdit);

		if($res == 1){
			$message = "Berhasil Edit Data";
		}else{
			$message = "Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_group_customer($id = NULL)
	{
		$updateData = array(
			'delete' => '1',
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("tbl_group_customer", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}
		return $message;
	}
	public function get_data_customer($id = NULL,$request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.kode_customer', $request['tx_cari'], 'both');
			$this->db->or_like('a.nama_customer', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*, b.nama_group_customer');
		$this->db->from('tbl_customer a');
		$this->db->join('tbl_group_customer b','b.id = a.id_group_customer','left');

		$this->db->order_by('a.kode_customer','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function get_no_urut_by_group_customer($id_group = NULL)
	{
		$sql = "SELECT last_no_urut FROM tbl_no_urut_by_group_customer where id_group_customer = '".$id_group."' ;";
		$query = $this->db->query($sql);
		$last_no_urut = (isset($query->row()->last_no_urut))?(($query->row()->last_no_urut)+1):1;
		if($last_no_urut > 1)
			$this->db->query("UPDATE tbl_no_urut_by_group_customer SET last_no_urut = '".$last_no_urut."' WHERE  id_group_customer = '".$id_group."' ;");
		else
			$this->db->query("INSERT INTO tbl_no_urut_by_group_customer (id_group_customer, last_no_urut) values ('".$id_group."', '".$last_no_urut."');");
		return $last_no_urut;
	}
	public function add_data_customer($request = NULL, $file = NULL)
	{
		$message_upload_image = "";
		$new_kode_customer = $request['id_group_customer'].$this->get_no_urut_by_group_customer($request['id_group_customer']);
		$insertData = array(
			'id_group_customer' => $request['id_group_customer'],
			'kode_customer' => $new_kode_customer,
			'nama_customer' => $request['nama_customer'],
			'email' => $request['email'],
			'alamat' => $request['alamat'],
			'no_rekening' => $request['no_rekening'],
			'atas_nama_rekening' => $request['atas_nama_rekening'],
			'id_kota' => $request['id_kota'],
			'kode_kota' => $request['kode_kota'],
			'nama_kota' => $request['nama_kota'],
			'telepon' => $request['telepon'],
			'jatuh_tempo' => $request['jatuh_tempo'],
			'nama_toko' => $request['nama_toko'],
			'alamat_toko' => $request['alamat_toko'],

			'nama_istri' => $request['nama_istri'],
			'email_istri' => $request['email_istri'],
			'telepon_istri' => $request['telepon_istri'],
			'alamat_istri' => $request['alamat_istri'],
			'jumlah_anak' => $request['jumlah_anak'],
			'nama_anak' => $request['nama_anak'],

			'status' => $request['status']
		);

		if(isset($file['image_ktp']['name']) and $file['image_ktp']['name']<>""){
			$request['image_ktp_name'] = date("Ymdhisa")."-".md5($request['nama_customer'])."-".str_replace(" ","-",$file['image_ktp']['name']);
			$message_upload_image .= $this->upload_ktp_customer($request['image_ktp_name'],'image_ktp');
			$insertData['image_ktp'] = $request['image_ktp_name'];
		}

		if(isset($file['image_ktp_istri']['name']) and $file['image_ktp_istri']['name']<>""){
			$request['image_ktp_istri_name'] = date("Ymdhisa")."-".md5($request['nama_istri'])."-".str_replace(" ","-",$file['image_ktp_istri']['name']);
			$message_upload_image .= $this->upload_ktp_customer($request['image_ktp_istri_name'],'image_ktp_istri');
			$insertData['image_ktp_istri'] = $request['image_ktp_istri_name'];
		}
		$this->db->insert("tbl_customer", $insertData);
		$insert_id = $this->db->insert_id();

		if(is_numeric($insert_id)){
			$message = @$message_upload_image."<br/>Berhasil Input Data";
		}else{
			$message = "Gagal Input Data!";
		}
		return $message;
	}

	public function edit_data_customer($request = NULL, $file = NULL)
	{
		$get_data_customer = $this->get_data_customer($request['id_customer']);
		$id_group_old = $get_data_customer[0]->id_group_customer;
		$id_group_new = $request['id_group_customer'];
		$updateData = array(
			'id_group_customer' => $request['id_group_customer'],
			'nama_customer' => $request['nama_customer'],
			'email' => $request['email'],
			'alamat' => $request['alamat'],
			'id_kota' => $request['id_kota'],
			'kode_kota' => $request['kode_kota'],
			'nama_kota' => $request['nama_kota'],
			'telepon' => $request['telepon'],
			'no_rekening' => $request['no_rekening'],
			'atas_nama_rekening' => $request['atas_nama_rekening'],
			'jatuh_tempo' => $request['jatuh_tempo'],
			'nama_toko' => $request['nama_toko'],
			'alamat_toko' => $request['alamat_toko'],
			'nama_istri' => $request['nama_istri'],
			'email_istri' => $request['email_istri'],
			'telepon_istri' => $request['telepon_istri'],
			'alamat_istri' => $request['alamat_istri'],
			'jumlah_anak' => $request['jumlah_anak'],
			'nama_anak' => $request['nama_anak'],
			'status' => $request['status']
		);
		if($id_group_new != $id_group_old){
			$new_kode_customer = $request['id_group_customer'].$this->get_no_urut_by_group_customer($request['id_group_customer']);
			$updateData['kode_customer'] = $new_kode_customer;
		}
		if(isset($file['image_ktp']['name']) and $file['image_ktp']['name']<>""){
			$file_image = $request['file_image_ktp_name'];
			@unlink("./assets/upload_ktp_customer/original/".$file_image);
			@unlink("./assets/upload_ktp_customer/display/".$file_image);
			$request['image_ktp_name'] = date("Ymdhisa")."-".md5($request['nama_customer'])."-".str_replace(" ","-",$file['image_ktp']['name']);
			$updateData['image_ktp'] = $request['image_ktp_name'];
			$message_upload_image = $this->upload_ktp_customer($request['image_ktp_name'],'image_ktp');
		}
		if(isset($file['image_ktp_istri']['name']) and $file['image_ktp_istri']['name']<>""){
			$file_image = $request['file_image_ktp_istri_name'];
			@unlink("./assets/upload_ktp_customer/original/".$file_image);
			@unlink("./assets/upload_ktp_customer/display/".$file_image);
			$request['image_ktp_istri_name'] = date("Ymdhisa")."-".md5($request['nama_istri'])."-".str_replace(" ","-",$file['image_ktp_istri']['name']);
			$updateData['image_ktp_istri'] = $request['image_ktp_istri_name'];
			$message_upload_image = $this->upload_ktp_customer($request['image_ktp_istri_name'],'image_ktp_istri');
		}
		$whereEdit = array(
			'id' => $request['id_customer']
		);
		$res = $this->db->update("tbl_customer", $updateData, $whereEdit);

		if($res == 1){
			$message = "Berhasil Edit Data";
		}else{
			$message = "Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_data_customer($id = NULL)
	{
		$updateData = array(
			'delete' => '1',
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("tbl_customer", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}
		return $message;
	}
	public function get_id_barang_data_promo_barang()
	{
		$this->db->select('id_barang');
		$this->db->from('tbl_data_promo_barang');
		$this->db->group_by('id_barang');
		$query = $this->db->get();
		return $query->result();
	}
	public function get_data_promo_barang($id = NULL,$request = NULL)
	{
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		if(isset($request['id_data_promo']) and $request['id_data_promo'] <> NULL){
			$this->db->where('a.id_data_promo', $request['id_data_promo']);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*,b.nama_barang,b.deskripsi,c.nama_satuan');
		$this->db->from('tbl_data_promo_barang a');
		$this->db->join('tbl_data_barang b','b.id = a.id_barang','inner');
		$this->db->join('satuan c','c.id = b.id_satuan','inner');

		$this->db->order_by('a.id','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_data_promo_barang($request = NULL, $file = NULL, $id_data_promo)
	{
		$insertData = array(
			'id_barang' => $request['id_barang'],
			'id_data_promo' => $id_data_promo,
			'harga' => $request['harga'],
			'periode_sampai_dengan' => $request['periode_sampai_dengan']
		);



		if(isset($file['image']['name']) and $file['image']['name']<>""){
			$file_image = $request['file_image_name'];
			@unlink("./assets/upload_product/original/".$file_image);
			@unlink("./assets/upload_product/display/".$file_image);
			@unlink("./assets/upload_product/300/".$file_image);
			@unlink("./assets/upload_product/200/".$file_image);
			@unlink("./assets/upload_product/100/".$file_image);
			@unlink("./assets/upload_product/50/".$file_image);
			$request['file_image_name'] = date("Ymdhisa")."-".md5($request['id_barang'])."-".str_replace(" ","-",$file['image']['name']);

			$insertData['gambar'] = $request['file_image_name'];

			$message_upload_image = $this->product_upload_file($request,$file)."<br>";
		}

		$this->db->insert("tbl_data_promo_barang", $insertData);
		$insert_id = $this->db->insert_id();

		if(is_numeric($insert_id)){
			$message = @$message_upload_image."<br/>Berhasil Input Data Promo Barang";
		}else{
			$message = @$message_upload_image."<br/>Gagal Input Data Promo Barang!";
		}
		return $message;
	}
	public function edit_data_promo_barang($request = NULL, $file = NULL)
	{
		$updateData = array(
			'id_barang' => $request['id_barang'],
			'harga' => $request['harga'],
			'periode_sampai_dengan' => $request['periode_sampai_dengan']
		);

		if(isset($file['image']['name']) and $file['image']['name']<>""){
			$file_image = $request['file_image_name'];
			@unlink("./assets/upload_product/original/".$file_image);
			@unlink("./assets/upload_product/display/".$file_image);
			@unlink("./assets/upload_product/300/".$file_image);
			@unlink("./assets/upload_product/200/".$file_image);
			@unlink("./assets/upload_product/100/".$file_image);
			@unlink("./assets/upload_product/50/".$file_image);
			$request['file_image_name'] = date("Ymdhisa")."-".md5($request['id_barang'])."-".str_replace(" ","-",$file['image']['name']);

			$updateData['gambar'] = $request['file_image_name'];

			$message_upload_image = $this->product_upload_file($request,$file)."<br>";
		}
		$whereEdit = array(
			'id' => $request['id_data_detail_promo_barang']
		);
		$res = $this->db->update("tbl_data_promo_barang", $updateData, $whereEdit);

		if($res == 1){
			$message = @$message_upload_image."<br/>Berhasil Edit Data";
		}else{
			$message = @$message_upload_image."<br/>Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_data_promo_barang($id = NULL, $file_image = NULL)
	{
		@unlink("./assets/upload_product/original/".$file_image);
		@unlink("./assets/upload_product/display/".$file_image);
		@unlink("./assets/upload_product/300/".$file_image);
		@unlink("./assets/upload_product/200/".$file_image);
		@unlink("./assets/upload_product/100/".$file_image);
		@unlink("./assets/upload_product/50/".$file_image);

		$updateData = array(
			'delete' => '1',
			'gambar' => ''
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("tbl_data_promo_barang", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}
		return $message;
	}
	public function get_data_promo($id = NULL,$request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.tanggal', $request['tx_cari'], 'both');
			$this->db->or_like('a.nomor', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*');
		$this->db->from('tbl_data_promo  a');

		$this->db->order_by('a.tanggal','DESC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_data_promo($request = NULL)
	{
		$query = $this->db->query("SELECT nomor_urut FROM tbl_data_promo WHERE EXTRACT(YEAR_MONTH FROM tgl_input) = '".date('Ym')."' ORDER BY nomor_urut DESC limit 1;");
		$row = $query->row();
		if(isset($row->nomor_urut) and $row->nomor_urut > 0)
			$newNomorUrut = $row->nomor_urut+1;
		else
			$newNomorUrut = 1;
		// 0001/PRM/III/19
		$newNomor = numLength($newNomorUrut,4)."/PRM/".numberToRoman(date('m'))."/".date('y');
		$insertData = array(
			'tanggal' => $request['tanggal'],
			'nomor' => $newNomor,
			'nomor_urut' => $newNomorUrut,
			'keterangan' => $request['keterangan'],
			'status' => $request['status']
		);
		$this->db->insert("tbl_data_promo", $insertData);
		$insert_id = $this->db->insert_id();

		$data = array();
		for($i = 0;$i<count($request['id_barang']);$i++){
			$subdata = array();
			foreach($request as $key => $val){
				if(is_array($val)){
					$subdata[$key] = $request[$key][$i];
				}
			}
			foreach($_FILES as $name => $val_name){
				foreach($val_name as $name_key => $key){
					$subdata['FILES']['image'][$name_key] = $key[$i];
				}
			}
			$data[] = $subdata;
		}

		foreach($data as $req){
			$_FILES = $req['FILES'];
			$message_add_data_promo_barang = $this->admin_model->add_data_promo_barang($req, $_FILES, $insert_id);
		}


		if(is_numeric(@$insert_id)){
			$message = @$message_add_data_promo_barang."<br/>Berhasil Input Data";
		}else{
			$message = @$message_add_data_promo_barang."<br/>Gagal Input Data!";
		}
		return $message;
	}
	public function edit_data_promo($request = NULL)
	{
		$updateData = array(
			'tanggal' => $request['tanggal'],
			'nomor' => $request['nomor'],
			'keterangan' => $request['keterangan'],
			'status' => $request['status']
		);
		$whereEdit = array(
			'id' => $request['id_data_promo']
		);
		$res = $this->db->update("tbl_data_promo", $updateData, $whereEdit);

		if($res == 1){
			$message = "Berhasil Edit Data";
		}else{
			$message = "Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_data_promo($id = NULL)
	{
		$updateData = array(
			'delete' => '1',
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("tbl_data_promo", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}
		return $message;
	}
	public function get_faktur_pembelian_detail_barang($id = NULL,$request = NULL){
		/*
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.tanggal', $request['tx_cari'], 'both');
			$this->db->or_like('a.nomor', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		*/
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		if(isset($request['id_faktur_pembelian'])){
			$this->db->where('a.id_faktur_pembelian', $request['id_faktur_pembelian']);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*,b.id as id_barang,b.nama_barang,b.deskripsi,c.nama_satuan');
		$this->db->from('tbl_faktur_pembelian_detail_barang a');
		$this->db->join('tbl_data_barang b','b.id = a.id_barang','inner');
		$this->db->join('satuan c','c.id = b.id_satuan','inner');
		$this->db->order_by('a.id','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_faktur_pembelian_detail_barang($request = NULL, $id_faktur_pembelian){
		$insertData = array(
			'id_faktur_pembelian' => $id_faktur_pembelian,
			'id_barang' => $request['id_barang'],
			'expire_date' => $request['expire_date'],
			'qty' => $request['qty'],
			'no_seri' => $request['detail_no_seri'],
			'harga_satuan' => $request['harga_satuan'],
			'sub_total' => $request['sub_total']
		);
		$this->db->insert("tbl_faktur_pembelian_detail_barang", $insertData);
		$insert_id = $this->db->insert_id();
		//---------------insert ke tbl_harga_jual------
		$query = $this->db->query("SELECT id_barang, harga_beli FROM tbl_harga_jual where `delete` = '0' and id_barang = '".$request['id_barang']."' ;");
		$num_rows = $query->num_rows();
		$row_array = $query->row_array();
		if($num_rows == 0){
			$this->db->query("INSERT INTO tbl_harga_jual (id_barang, harga_beli) values ('".$request['id_barang']."', '".$request['harga_satuan']."') ;");
		}else{
			//if($request['harga_satuan'] > $row_array['harga_beli']){
				$this->db->query("UPDATE tbl_harga_jual SET harga_beli = '".$request['harga_satuan']."' WHERE id_barang = '".$request['id_barang']."' ;");
			//}
		}
		//---------------------------------------------
		if(is_numeric(@$insert_id)){
			$message = "Berhasil Input Barang Faktur Pembelian";
		}else{
			$message = "Gagal Input Barang Faktur Pembelian!";
		}
		return $message;
	}
	public function get_laporan_cepat_faktur_pembelian(){
		$ret = array();
		$query = $this->db->query("SELECT SUM(sisa_tagihan) belum_dibayar FROM tbl_faktur_pembelian WHERE `delete` = '0';");
		$ret['belum_dibayar'] = $query->row_array()['belum_dibayar'];

		$query = $this->db->query("SELECT SUM(sisa_tagihan) belum_dibayar_jatuh_tempo FROM tbl_faktur_pembelian WHERE `delete` = '0' AND jatuh_tempo <= NOW();");
		$ret['belum_dibayar_jatuh_tempo'] = $query->row_array()['belum_dibayar_jatuh_tempo'];

		$query = $this->db->query("
									SELECT SUM(b.pembayaran) dibayar_30_hari FROM tbl_faktur_pembelian a
									INNER JOIN tbl_pembayaran_hutang_detail_faktur b ON b.id_faktur_pembelian = a.id
									WHERE a.`delete` = '0'
									AND (b.tgl_input BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW());
		");
		$ret['dibayar_30_hari'] = $query->row_array()['dibayar_30_hari'];
		return $ret;
	}
	public function get_laporan_faktur_pembelian($id = NULL,$request = NULL)
	{
		/*
			SELECT
				a.tanggal,a.nomor,b.nama_suplier,a.no_sj_suplier,a.jatuh_tempo,d.nama_barang,c.no_seri,c.qty,e.nama_satuan,c.harga_satuan,(c.qty*c.harga_satuan) as sub_total,a.pesan
			FROM
			tbl_faktur_pembelian a
			INNER JOIN tbl_suplier b on b.id = a.id_suplier
			INNER JOIN tbl_faktur_pembelian_detail_barang c on c.id_faktur_pembelian = a.id
			INNER JOIN tbl_data_barang d on d.id = c.id_barang
			INNER JOIN satuan e on e.id = d.id_satuan
			ORDER BY a.nomor ASC, b.nama_suplier ASC, d.nama_barang ASC;
		*/
		if(!isset($request['bt_rentang']) and !isset($request['bt_periode'])){
			$this->db->where('a.tanggal', date('Y-m-d'));
		}
		if(isset($request['bt_rentang'])){
			$this->db->group_start();
			$this->db->where('a.tanggal >=', $request['tanggal_dari']);
			$this->db->where('a.tanggal <=', $request['tanggal_ke']);
			$this->db->group_end();
		}
		$this->db->where('a.delete', '0');
		$this->db->select("a.tanggal,a.nomor,b.nama_suplier,a.no_sj_suplier,a.jatuh_tempo,d.nama_barang,c.no_seri,c.qty,e.nama_satuan,c.harga_satuan,(c.qty*c.harga_satuan) as sub_total,a.pesan");
		$this->db->from('tbl_faktur_pembelian  a');
		$this->db->join("tbl_suplier b", "b.id = a.id_suplier", "inner");
		$this->db->join("tbl_faktur_pembelian_detail_barang c", "c.id_faktur_pembelian = a.id", "inner");
		$this->db->join("tbl_data_barang d", "d.id = c.id_barang", "inner");
		$this->db->join("satuan e", "e.id = d.id_satuan", "inner");

		$this->db->order_by("a.nomor","ASC");
		$this->db->order_by("b.nama_suplier","ASC");
		$this->db->order_by("d.nama_barang","ASC");
		$query = $this->db->get();
		return $query->result();
	}
	public function get_faktur_pembelian($id = NULL,$request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.tanggal', $request['tx_cari'], 'both');
			$this->db->or_like('a.nomor', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		if(isset($request['id_suplier']) and isset($request['jatuh_tempo'])){
			$this->db->where('a.id_suplier', $request['id_suplier']);
			$this->db->where('a.jatuh_tempo <=', $request['jatuh_tempo']);
		}
		if(isset($request['id_pembayaran_hutang'])){
			$this->db->where('a.sisa_tagihan > ', '0');
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*,b.nama_suplier,b.alamat as alamat_suplier,b.jatuh_tempo as top, g.nama_gudang');
		$this->db->from('tbl_faktur_pembelian  a');
		$this->db->join('tbl_suplier  b','a.id_suplier = b.id','inner');
		$this->db->join('tbl_gudang g', 'g.id = a.id_gudang', 'left');

		if(isset($request['bisa_retur']) and $request['bisa_retur'] == 1){
			$this->db->join('tbl_faktur_pembelian_detail_barang  c','c.id_faktur_pembelian = a.id','inner');
			$this->db->where('c.qty > c.qty_cetak_retur');
			$this->db->group_by('a.nomor');
		}
		$this->db->order_by('a.tanggal','DESC');
		$this->db->order_by('a.nomor','DESC');
		$query = $this->db->get();

		return $query->result();
	}
	public function add_faktur_pembelian($request = NULL)
	{
		$query = $this->db->query("SELECT nomor_urut FROM tbl_faktur_pembelian WHERE EXTRACT(YEAR_MONTH FROM tgl_input) = '".date('Ym')."' ORDER BY nomor_urut DESC limit 1;");
		$row = $query->row();
		if(isset($row->nomor_urut) and $row->nomor_urut > 0)
			$newNomorUrut = $row->nomor_urut+1;
		else
			$newNomorUrut = 1;
		$newNomor = numLength($newNomorUrut,4)."/FB/".numberToRoman(date('m'))."/".date('y');

		$insertData = array(
			'nomor' => $newNomor,
			'nomor_urut' => $newNomorUrut,
			'id_suplier' => $request['id_suplier'],
			'id_gudang' => $request['id_gudang'],
			'jatuh_tempo' => $request['jatuh_tempo'],
			'tanggal' => $request['tanggal'],
			'email' => $request['email'],
			'no_sj_suplier' => $request['no_sj_suplier'],
			'pesan' => $request['pesan'],
			'total' => $request['total'],
			'pemotongan' => $request['pemotongan'],
			'pemotongan_tipe' => $request['pemotongan_tipe'],
			'grand_total' => $request['grand_total'],
			'sisa_tagihan' => $request['grand_total']
		);
		$this->db->insert("tbl_faktur_pembelian", $insertData);
		$insert_id = $this->db->insert_id();
		$data = array();
		for($i = 0;$i<count($request['id_barang']);$i++){
			$subdata = array();
			foreach($request as $key => $val){
				if(is_array($val)){
					$subdata[$key] = $request[$key][$i];
				}
			}
			$data[] = $subdata;
		}

		foreach($data as $req){
			if($req['detail_no_seri']!=""){
				$objNS = json_decode($req['detail_no_seri']);
				$obNoSeri = $objNS->no_seri;
				$obQty = $objNS->qty;
				foreach($obNoSeri as $key => $val){
					$no_seri = $obNoSeri->$key;
					$qty = $obQty->$key;

					$query = $this->db->query("SELECT * FROM tbl_data_barang_stok where id_barang = '".$req['id_barang']."' and no_seri = '".$no_seri."' ; ");
					$num_rows = $query->num_rows();
					if($num_rows == 0){
						$insertData = array(
							'id_barang' => $req['id_barang'],
							'no_seri' => $no_seri,
							'qty' => $qty,
							'harga_satuan_seri' => $req['harga_satuan']
						);
						$this->db->insert("tbl_data_barang_stok", $insertData);
					}else{
						$this->db->query("update tbl_data_barang_stok set qty=(qty+".$qty.") where id_barang='".$req['id_barang']."' and no_seri = '".$no_seri."'; ");
					}
				}
				$add_faktur_pembelian_detail_barang = $this->admin_model->add_faktur_pembelian_detail_barang($req, $insert_id);
			}
		}

		if(is_numeric(@$insert_id)){
			$message = @$add_faktur_pembelian_detail_barang."<br/>Berhasil Input Data";
		}else{
			$message = @$add_faktur_pembelian_detail_barang."<br/>Gagal Input Data!";
		}
		//return $message;
		return $insert_id;
	}
	public function edit_faktur_pembelian($request = NULL)
	{
		$updateData = array(
			'id_suplier' => $request['id_suplier'],
			'id_gudang' => $request['id_gudang'],
			'jatuh_tempo' => $request['jatuh_tempo'],
			'tanggal' => $request['tanggal'],
			'email' => $request['email'],
			'no_sj_suplier' => $request['no_sj_suplier'],
			'pesan' => $request['pesan'],
			'total' => $request['total'],
			'pemotongan' => $request['pemotongan'],
			'pemotongan_tipe' => $request['pemotongan_tipe'],
			'grand_total' => $request['grand_total'],
			'sisa_tagihan' => $request['grand_total']
		);


		$whereEdit = array(
			'id' => $request['id_faktur_pembelian']
		);
		$res = $this->db->update("tbl_faktur_pembelian", $updateData, $whereEdit);
		$data = array();
		for($i = 0;$i<count($request['id_barang']);$i++){
			$subdata = array();
			foreach($request as $key => $val){
				if(is_array($val)){
					$subdata[$key] = $request[$key][$i];
				}
			}
			$data[] = $subdata;
		}
		//----------------------------------
			$query = $this->db->query("select id_barang,no_seri from tbl_faktur_pembelian_detail_barang where id_faktur_pembelian = '".$request['id_faktur_pembelian']."' ;");
			foreach($query->result_array() as $row){
				$id_barang = $row['id_barang'];
				$objNS = json_decode($row['no_seri']);
				$obNoSeri = $objNS->no_seri;
				$obQty = $objNS->qty;
				foreach($obNoSeri as $key => $val){
					$no_seri = $obNoSeri->$key;
					$qty = $obQty->$key;
					$this->db->query("update tbl_data_barang_stok set qty=(qty-".$qty.") where id_barang='".$id_barang."' and no_seri = '".$no_seri."'; ");
				}
			}
			$this->db->delete('tbl_faktur_pembelian_detail_barang', array('id_faktur_pembelian' => $request['id_faktur_pembelian']));
		//------------------------------------
		foreach($data as $req){
			if($req['detail_no_seri']!=""){
				$objNS = json_decode($req['detail_no_seri']);
				$obNoSeri = $objNS->no_seri;
				$obQty = $objNS->qty;
				foreach($obNoSeri as $key => $val){
					$no_seri = $obNoSeri->$key;
					$qty = $obQty->$key;
					$this->db->query("update tbl_data_barang_stok set qty=(qty+".$qty.") where id_barang='".$req['id_barang']."' and no_seri = '".$no_seri."'; ");
				}
				$add_faktur_pembelian_detail_barang = $this->admin_model->add_faktur_pembelian_detail_barang($req, $request['id_faktur_pembelian']);
			}
		}
		//----------------------------------
		/*
		foreach($data as $req){
			if($req['detail_no_seri']!=""){
				$query = $this->db->query("select no_seri from tbl_faktur_pembelian_detail_barang where id_faktur_pembelian = '".$request['id_faktur_pembelian']."' and  id_barang='".$req['id_barang']."' ;");
				$rowOld = $query->row_array();
				$rowOldNoSeri = changeArray($rowOld['no_seri']);
				$objNS = json_decode($req['detail_no_seri']);
				$obNoSeri = $objNS->no_seri;
				$obQty = $objNS->qty;
				foreach($obNoSeri as $key => $val){
					$no_seri = $obNoSeri->$key;
					$qty = $obQty->$key;
					$rowOldNoSeriQty = (isset($rowOldNoSeri[$no_seri]))?$rowOldNoSeri[$no_seri]:0;
					$this->db->query("update tbl_data_barang_stok set qty=(qty-".$rowOldNoSeriQty."+".$qty.") where id_barang='".$req['id_barang']."' and no_seri = '".$no_seri."'; ");
				}
				$this->db->delete('tbl_faktur_pembelian_detail_barang', array('id_faktur_pembelian' => $request['id_faktur_pembelian'], 'id_barang' => $req['id_barang']));
				$add_faktur_pembelian_detail_barang = $this->admin_model->add_faktur_pembelian_detail_barang($req, $request['id_faktur_pembelian']);
			}
		}
		*/
		if($res == 1){
			$message = @$add_faktur_pembelian_detail_barang."<br/>Berhasil Edit Data";
		}else{
			$message = @$add_faktur_pembelian_detail_barang."<br/>Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_faktur_pembelian($id = NULL)
	{
		$updateData = array(
			'delete' => '1',
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("tbl_faktur_pembelian", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}

		$query = $this->db->query("select id_barang,no_seri from tbl_faktur_pembelian_detail_barang where id_faktur_pembelian = '".$id."' ;");
		foreach($query->result_array() as $row){
			$id_barang = $row['id_barang'];
			$objNS = json_decode($row['no_seri']);
			$obNoSeri = $objNS->no_seri;
			$obQty = $objNS->qty;
			foreach($obNoSeri as $key => $val){
				$no_seri = $obNoSeri->$key;
				$qty = $obQty->$key;
				$this->db->query("update tbl_data_barang_stok set qty=(qty-".$qty.") where id_barang='".$id_barang."' and no_seri = '".$no_seri."'; ");
			}
		}

		return $message;
	}
	public function get_retur_pembelian_detail_barang($id = NULL,$request = NULL){
		/*
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.tanggal', $request['tx_cari'], 'both');
			$this->db->or_like('a.nomor', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		*/
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		if(isset($request['id_faktur_pembelian'])){
			$this->db->where('a.id_faktur_pembelian', $request['id_faktur_pembelian']);
		}
		$this->db->where('a.delete', '0');

		$this->db->select('a.*,b.id as id_barang,b.nama_barang,b.deskripsi,c.nama_satuan,d.qty as retur_qty,d.no_seri as retur_no_seri,d.keterangan as retur_keterangan');

		$this->db->from('tbl_faktur_pembelian_detail_barang a');
		$this->db->join('tbl_data_barang b','b.id = a.id_barang','inner');
		$this->db->join('satuan c','c.id = b.id_satuan','inner');
		$this->db->join('tbl_retur_pembelian_detail_barang d','d.id_faktur_pembelian_detail_barang = a.id and d.id_retur_pembelian = "'.$request['id_retur_pembelian'].'"','left');
		$this->db->order_by('a.id','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_retur_pembelian_detail_barang($request = NULL, $id_retur_pembelian){
		$insertData = array(
			'id_retur_pembelian' => $id_retur_pembelian,
			'id_faktur_pembelian' => $request['id_faktur_pembelian'],
			'id_faktur_pembelian_detail_barang' => $request['id_faktur_pembelian_detail_barang'],
			'id_barang' => $request['id_barang'],
			'qty' => $request['qty_retur'],
			'no_seri' => $request['detail_no_seri'],
			'keterangan' => $request['keterangan']
		);
		$this->db->insert("tbl_retur_pembelian_detail_barang", $insertData);
		$insert_id = $this->db->insert_id();

		$query = $this->db->query("SELECT sum(qty) qty_cetak_retur FROM tbl_retur_pembelian_detail_barang WHERE id_faktur_pembelian_detail_barang = '".$request['id_faktur_pembelian_detail_barang']."';");
		$res = $query->row_array();
		$this->db->query("update tbl_faktur_pembelian_detail_barang set qty_cetak_retur = '".$res['qty_cetak_retur']."' WHERE id = '".$request['id_faktur_pembelian_detail_barang']."'; ");

		$query = $this->db->query("SELECT no_seri FROM tbl_retur_pembelian_detail_barang WHERE id_faktur_pembelian_detail_barang = '".$request['id_faktur_pembelian_detail_barang']."';");
		$no_seri_cetak_retur = "";
		foreach ($query->result_array() as $row){
			$no_seri_cetak_retur = joinJSONSTRNoSeri($no_seri_cetak_retur, $row['no_seri']);
		}
		$this->db->query("update tbl_faktur_pembelian_detail_barang set no_seri_cetak_retur = '".$no_seri_cetak_retur."' WHERE id = '".$request['id_faktur_pembelian_detail_barang']."'; ");

		if(is_numeric(@$insert_id)){
			$message = "Berhasil Input Barang Faktur Pembelian";
		}else{
			$message = "Gagal Input Barang Faktur Pembelian!";
		}
		return $message;
	}
	public function get_laporan_retur_pembelian($id = NULL,$request = NULL)
	{
		$where = "";
		if(!isset($request['bt_rentang']) and !isset($request['bt_periode'])){
			$where .= " and a.tanggal = '".date('Y-m-d')."' ";
		}
		if(isset($request['bt_rentang'])){
			$where .= " and (a.tanggal >= '".$request['tanggal_dari']."' and a.tanggal <= '".$request['tanggal_ke']."') ";
		}
		$sql = "
					SELECT
						a.tanggal,a.nomor as no_retur_pembelian,c.nama_suplier,b.nomor as no_faktur_pembelian,b.jatuh_tempo,
						f.nama_barang,d.no_seri,d.qty as qty_retur,g.nama_satuan,e.harga_satuan,(d.qty*e.harga_satuan) as sub_total,a.pesan
					FROM
					tbl_retur_pembelian a
					INNER JOIN tbl_faktur_pembelian b ON b.id = a.id_faktur_pembelian
					INNER JOIN tbl_suplier c ON c.id = b.id_suplier
					INNER JOIN tbl_retur_pembelian_detail_barang d ON d.id_retur_pembelian = a.id
					INNER JOIN tbl_faktur_pembelian_detail_barang e ON e.id = d.id_faktur_pembelian_detail_barang
					INNER JOIN tbl_data_barang f ON f.id = e.id_barang
					INNER JOIN satuan g ON g.id = f.id_satuan
					WHERE 1
					and a.delete = '0'
					".$where."
					ORDER BY a.nomor ASC, c.nama_suplier ASC, f.nama_barang ASC;
		";
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function get_retur_pembelian($id = NULL,$request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.tanggal', $request['tx_cari'], 'both');
			$this->db->or_like('a.nomor', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*,b.nomor as no_faktur_pembelian,c.nama_suplier,c.alamat as alamat_suplier,c.jatuh_tempo as top');
		$this->db->from('tbl_retur_pembelian a');
		$this->db->join('tbl_faktur_pembelian b','b.id = a.id_faktur_pembelian','inner');
		$this->db->join('tbl_suplier  c','c.id = b.id_suplier','inner');
		$this->db->order_by('a.tanggal','DESC');
		$this->db->order_by('a.nomor','DESC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_retur_pembelian($request = NULL)
	{
		$query = $this->db->query("SELECT nomor_urut FROM tbl_retur_pembelian WHERE EXTRACT(YEAR_MONTH FROM tgl_input) = '".date('Ym')."' ORDER BY nomor_urut DESC limit 1;");
		$row = $query->row();
		if(isset($row->nomor_urut) and $row->nomor_urut > 0)
			$newNomorUrut = $row->nomor_urut+1;
		else
			$newNomorUrut = 1;
		$newNomor = numLength($newNomorUrut,4)."/RB/".numberToRoman(date('m'))."/".date('y');

		$insertData = array(
			'nomor' => $newNomor,
			'nomor_urut' => $newNomorUrut,
			'id_faktur_pembelian' => $request['id_faktur_pembelian'],
			'tanggal' => $request['tanggal'],
			'pesan' => $request['pesan'],
			'total' => $request['total_retur']
		);
		$this->db->insert("tbl_retur_pembelian", $insertData);
		$insert_id = $this->db->insert_id();

		$data = array();
		for($i = 0;$i<count($request['id_faktur_pembelian_detail_barang']);$i++){
			$subdata = array();
			foreach($request as $key => $val){
				if(is_array($val)){
					$subdata[$key] = $request[$key][$i];
				}
			}
			$data[] = $subdata;
		}

		foreach($data as $req){
			if($req['detail_no_seri']!=""){
				$objNS = json_decode($req['detail_no_seri']);
				$obNoSeri = $objNS->no_seri;
				$obQty = $objNS->qty;
				foreach($obNoSeri as $key => $val){
					$no_seri = $obNoSeri->$key;
					$qty = $obQty->$key;
					$this->db->query("update tbl_data_barang_stok set qty = (qty-".$qty.") WHERE id_barang = '".$req['id_barang']."' and no_seri = '".$no_seri."'; ");
				}
				$req['id_faktur_pembelian'] = $request['id_faktur_pembelian'];
				$add_retur_pembelian_detail_barang = $this->admin_model->add_retur_pembelian_detail_barang($req, $insert_id);
			}
		}
		$this->db->query("update tbl_faktur_pembelian set total_retur = (total_retur+".$request['total_retur']."), sisa_tagihan = (sisa_tagihan-".$request['total_retur']."), status_retur = '1' WHERE id = '".$request['id_faktur_pembelian']."' ; ");
		if(is_numeric(@$insert_id)){
			$message = @$add_retur_pembelian_detail_barang."<br/>Berhasil Input Data";
		}else{
			$message = @$add_retur_pembelian_detail_barang."<br/>Gagal Input Data!";
		}
		return $message;
	}
	public function edit_retur_pembelian($request = NULL)
	{
		$row_retur_beli = $this->db->query("SELECT total AS total_retur FROM tbl_retur_pembelian WHERE id = '".$request['id_retur_pembelian']."' ;")->row();

		$updateData = array(
			'id_faktur_pembelian' => $request['id_faktur_pembelian'],
			'tanggal' => $request['tanggal'],
			'pesan' => $request['pesan'],
			'total' => $request['total_retur']
		);
		$whereEdit = array(
			'id' => $request['id_retur_pembelian']
		);
		$res = $this->db->update("tbl_retur_pembelian", $updateData, $whereEdit);

		$data = array();
		for($i = 0;$i<count($request['id_faktur_pembelian_detail_barang']);$i++){
			$subdata = array();
			foreach($request as $key => $val){
				if(is_array($val)){
					$subdata[$key] = $request[$key][$i];
				}
			}
			$data[] = $subdata;
		}
		//----------------------------------
			$query = $this->db->query("select id_barang,no_seri from tbl_retur_pembelian_detail_barang where id_retur_pembelian = '".$request['id_retur_pembelian']."' ;");
			foreach($query->result_array() as $row){
				$id_barang = $row['id_barang'];
				$objNS = json_decode($row['no_seri']);
				$obNoSeri = $objNS->no_seri;
				$obQty = $objNS->qty;
				foreach($obNoSeri as $key => $val){
					$no_seri = $obNoSeri->$key;
					$qty = $obQty->$key;
					$this->db->query("update tbl_data_barang_stok set qty=(qty+".$qty.") where id_barang='".$id_barang."' and no_seri = '".$no_seri."'; ");
				}
			}
			$this->db->delete('tbl_retur_pembelian_detail_barang', array('id_retur_pembelian' => $request['id_retur_pembelian']));
		//------------------------------------
		foreach($data as $req){
			if($req['detail_no_seri']!=""){
				$objNS = json_decode($req['detail_no_seri']);
				$obNoSeri = $objNS->no_seri;
				$obQty = $objNS->qty;
				foreach($obNoSeri as $key => $val){
					$no_seri = $obNoSeri->$key;
					$qty = $obQty->$key;
					$this->db->query("update tbl_data_barang_stok set qty = (qty-".$qty.") WHERE id_barang = '".$req['id_barang']."' and no_seri = '".$no_seri."'; ");
				}
				$req['id_faktur_pembelian'] = $request['id_faktur_pembelian'];
				$add_retur_pembelian_detail_barang = $this->admin_model->add_retur_pembelian_detail_barang($req, $request['id_retur_pembelian']);
			}
		}
		$this->db->query("update tbl_faktur_pembelian set total_retur = ((total_retur-".$row_retur_beli->total_retur.")+".$request['total_retur']."), sisa_tagihan = ((sisa_tagihan+".$row_retur_beli->total_retur.")-".$request['total_retur']."), status_retur = '1' WHERE id = '".$request['id_faktur_pembelian']."' ; ");
		if($res == 1){
			$message = @$add_retur_pembelian_detail_barang."<br/>Berhasil Edit Data";
		}else{
			$message = @$add_retur_pembelian_detail_barang."<br/>Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_retur_pembelian($id = NULL)
	{
		$updateData = array(
			'delete' => '1',
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("tbl_retur_pembelian", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}

		$query = $this->db->query("select id_barang,no_seri from tbl_retur_pembelian_detail_barang where id_retur_pembelian = '".$id."' ;");
		foreach($query->result_array() as $row){
			$id_barang = $row['id_barang'];
			$objNS = json_decode($row['no_seri']);
			$obNoSeri = $objNS->no_seri;
			$obQty = $objNS->qty;
			foreach($obNoSeri as $key => $val){
				$no_seri = $obNoSeri->$key;
				$qty = $obQty->$key;
				$this->db->query("update tbl_data_barang_stok set qty=(qty+".$qty.") where id_barang='".$id_barang."' and no_seri = '".$no_seri."'; ");
			}
		}

		return $message;
	}



































	public function get_pembayaran_hutang_detail_faktur($id = NULL,$request = NULL){
		/*
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.tanggal', $request['tx_cari'], 'both');
			$this->db->or_like('a.nomor', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		*/
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		if(isset($request['id_suplier']) and isset($request['jatuh_tempo'])){
			$this->db->where('a.id_suplier', $request['id_suplier']);
			$this->db->where('a.jatuh_tempo <=', $request['jatuh_tempo']);
		}
		if(isset($request['id_pembayaran_hutang'])){
			//$this->db->where('a.sisa_tagihan > ', '0');
		}
		$this->db->where('a.delete', '0');
		$this->db->select('a.*,b.nama_suplier,b.alamat as alamat_suplier,b.jatuh_tempo as top,d.pembayaran');
		$this->db->from('tbl_faktur_pembelian  a');
		$this->db->join('tbl_suplier  b','a.id_suplier = b.id','inner');
		$this->db->join('tbl_pembayaran_hutang_detail_faktur d','d.id_faktur_pembelian = a.id and d.id_pembayaran_hutang = "'.$request['id_pembayaran_hutang'].'"','left');
		$this->db->order_by('a.tanggal','DESC');
		$this->db->order_by('a.nomor','DESC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_pembayaran_hutang_detail_faktur($request = NULL, $id_pembayaran_hutang){
		$insertData = array(
			'id_pembayaran_hutang' => $id_pembayaran_hutang,
			'id_faktur_pembelian' => $request['id_faktur_pembelian'],
			'pembayaran' => $request['pembayaran']
		);
		$this->db->insert("tbl_pembayaran_hutang_detail_faktur", $insertData);
		$insert_id = $this->db->insert_id();
		$this->db->query("update tbl_faktur_pembelian set sisa_tagihan = (grand_total-(".$request['pembayaran']."+(grand_total-sisa_tagihan))) where id = '".$request['id_faktur_pembelian']."'; ");
		if(is_numeric(@$insert_id)){
			$message = "Berhasil Input Barang Faktur Pembelian";
		}else{
			$message = "Gagal Input Barang Faktur Pembelian!";
		}
		return $message;
	}
	public function get_laporan_pembayaran_hutang($id = NULL,$request = NULL)
	{
		$where = "";
		if(!isset($request['bt_rentang']) and !isset($request['bt_periode'])){
			$where .= " and a.tanggal = '".date('Y-m-d')."' ";
		}
		if(isset($request['bt_rentang'])){
			$where .= " and (a.tanggal >= '".$request['tanggal_dari']."' and a.tanggal <= '".$request['tanggal_ke']."') ";
		}
		/*
		$sql = "
			SELECT
				a.tanggal,a.nomor as no_pembayaran_hutang,a.jatuh_tempo,c.nomor as no_faktur_pembelian,
				c.total as nilai_hutang,IFNULL(d.total,0) as nilai_retur,b.pembayaran,c.sisa_tagihan as sisa_hutang,a.pesan
			FROM
			tbl_pembayaran_hutang a
			INNER JOIN tbl_pembayaran_hutang_detail_faktur b on b.id_pembayaran_hutang = a.id
			INNER JOIN tbl_faktur_pembelian c on c.id = b.id_faktur_pembelian
			LEFT JOIN tbl_retur_pembelian d on d.id_faktur_pembelian = c.id
			WHERE 1
			AND a.delete = '0'
			".$where."
			AND (d.total > 0 OR b.pembayaran > 0)
			ORDER BY a.tanggal ASC, a.nomor ASC, c.nomor ASC;
		";
		*/
		$sql = "
			SELECT
				a.id as id_pembayaran_hutang,a.tanggal,a.nomor as no_pembayaran_hutang,a.jatuh_tempo,c.nomor as no_faktur_pembelian,
				c.total as nilai_hutang,
				IFNULL(d.total,0) as nilai_retur,
				(c.total-IFNULL(d.total,0)) as total_hutang,
						(
							SELECT IFNULL(sum(aa.pembayaran),0) FROM `tbl_pembayaran_hutang_detail_faktur` aa
							where
								aa.`status` = '1'
								and aa.`delete` = '0'
								and aa.id_faktur_pembelian = c.id
								and aa.id_pembayaran_hutang < a.id
						) as telah_dibayar,
				b.pembayaran,
				a.pesan
			FROM
			tbl_pembayaran_hutang a
			INNER JOIN tbl_pembayaran_hutang_detail_faktur b on b.id_pembayaran_hutang = a.id
			INNER JOIN tbl_faktur_pembelian c on c.id = b.id_faktur_pembelian
			LEFT JOIN tbl_retur_pembelian d on d.id_faktur_pembelian = c.id
			WHERE 1
			AND a.delete = '0'
			".$where."
			AND (d.total > 0 OR b.pembayaran > 0)
			ORDER BY a.tanggal ASC, a.nomor ASC, c.nomor ASC;
		";
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function get_pembayaran_hutang($id = NULL,$request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.tanggal', $request['tx_cari'], 'both');
			$this->db->or_like('a.nomor', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		$this->db->where('a.delete', '0');
		/*
		$this->db->select("
							a.*,
							b.nama_suplier,
							b.alamat as alamat_suplier,
							b.jatuh_tempo as top,
							(SELECT GROUP_CONCAT(DISTINCT nomor order by nomor asc SEPARATOR ', ') FROM tbl_faktur_pembelian WHERE id_suplier = a.id_suplier group by id_suplier) no_faktur_pembelian,
							(SELECT SUM(cb.pembayaran) FROM tbl_faktur_pembelian ca left join tbl_pembayaran_hutang_detail_faktur cb on cb.id_faktur_pembelian = ca.id WHERE ca.id_suplier = a.id_suplier group by id_suplier) pembayaran
						");
		*/
		$this->db->select("
							a.*,
							b.nama_suplier,
							b.alamat as alamat_suplier,
							b.jatuh_tempo as top,
							(
								SELECT GROUP_CONCAT(DISTINCT an.nomor order by nomor asc SEPARATOR ', ') FROM tbl_faktur_pembelian an
								INNER JOIN tbl_pembayaran_hutang_detail_faktur bn ON bn.id_faktur_pembelian = an.id
								WHERE bn.pembayaran > 0 and bn.id_pembayaran_hutang = a.id
							) no_faktur_pembelian,
							a.total as pembayaran
						");
		$this->db->from('tbl_pembayaran_hutang a');
		$this->db->join('tbl_suplier b','b.id = a.id_suplier','inner');

		$this->db->order_by('a.tanggal','DESC');
		$this->db->order_by('a.nomor','DESC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_pembayaran_hutang($request = NULL)
	{
		//debug($request);

		$query = $this->db->query("SELECT nomor_urut FROM tbl_pembayaran_hutang WHERE EXTRACT(YEAR_MONTH FROM tgl_input) = '".date('Ym')."' ORDER BY nomor_urut DESC limit 1;");
		$row = $query->row();
		if(isset($row->nomor_urut) and $row->nomor_urut > 0)
			$newNomorUrut = $row->nomor_urut+1;
		else
			$newNomorUrut = 1;
		$newNomor = numLength($newNomorUrut,4)."/PH/".numberToRoman(date('m'))."/".date('y');

		$insertData = array(
			'nomor' => $newNomor,
			'nomor_urut' => $newNomorUrut,
			'id_suplier' => $request['id_suplier'],
			'tanggal' => $request['tanggal'],
			'jatuh_tempo' => $request['jatuh_tempo'],
			'cara_bayar' => $request['cara_bayar'],
			'pemotongan' => $request['pemotongan'],
			'pemotongan_tipe' => $request['pemotongan_tipe'],
			'total' => $request['total'],
			'pesan' => $request['pesan']
		);

		$this->db->insert("tbl_pembayaran_hutang", $insertData);
		$insert_id = $this->db->insert_id();

		$data = array();
		for($i = 0;$i<count($request['id_faktur_pembelian']);$i++){
			$subdata = array();
			foreach($request as $key => $val){
				if(is_array($val)){
					$subdata[$key] = $request[$key][$i];
				}
			}
			$data[] = $subdata;
		}

		foreach($data as $req){
			$add_pembayaran_hutang_detail_faktur = $this->admin_model->add_pembayaran_hutang_detail_faktur($req, $insert_id);
		}

		if(is_numeric(@$insert_id)){
			$message = @$add_pembayaran_hutang_detail_faktur."<br/>Berhasil Input Data";
		}else{
			$message = @$add_pembayaran_hutang_detail_faktur."<br/>Gagal Input Data!";
		}
		return $message;

	}
	public function edit_pembayaran_hutang($request = NULL)
	{
		$updateData = array(
			'id_suplier' => $request['id_suplier'],
			'tanggal' => $request['tanggal'],
			'jatuh_tempo' => $request['jatuh_tempo'],
			'cara_bayar' => $request['cara_bayar'],
			'pemotongan' => $request['pemotongan'],
			'pemotongan_tipe' => $request['pemotongan_tipe'],
			'total' => $request['total'],
			'pesan' => $request['pesan']
		);
		$whereEdit = array(
			'id' => $request['id_pembayaran_hutang']
		);
		$res = $this->db->update("tbl_pembayaran_hutang", $updateData, $whereEdit);


		$result_pembayaran_hutang = $this->db->query("SELECT * FROM tbl_pembayaran_hutang_detail_faktur WHERE id_pembayaran_hutang = '".$request['id_pembayaran_hutang']."' ;")->result();
		foreach($result_pembayaran_hutang as $row_ph){
			$this->db->query("update tbl_faktur_pembelian set sisa_tagihan = (sisa_tagihan+".$row_ph->pembayaran.") where id = '".$row_ph->id_faktur_pembelian."'; ");
		}


		$this->db->delete('tbl_pembayaran_hutang_detail_faktur', array('id_pembayaran_hutang' => $request['id_pembayaran_hutang']));

		$data = array();
		for($i = 0;$i<count($request['id_faktur_pembelian']);$i++){
			$subdata = array();
			foreach($request as $key => $val){
				if(is_array($val)){
					$subdata[$key] = $request[$key][$i];
				}
			}
			$data[] = $subdata;
		}

		foreach($data as $req){
			$add_pembayaran_hutang_detail_faktur = $this->admin_model->add_pembayaran_hutang_detail_faktur($req, $request['id_pembayaran_hutang']);
		}

		if($res == 1){
			$message = @$add_pembayaran_hutang_detail_faktur."<br/>Berhasil Edit Data";
		}else{
			$message = @$add_pembayaran_hutang_detail_faktur."<br/>Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_pembayaran_hutang($id = NULL)
	{
		$updateData = array(
			'delete' => '1',
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("tbl_pembayaran_hutang", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}
		return $message;
	}
	public function post_download_data($request){
		$res = $this->db->update("tbl_download_data", array('data'=>$request['data']), array('type'=>$request['type']));
		return $res;
	}
	public function get_download_data($type){
		$this->db->select("data");
		$this->db->from("tbl_download_data");
		$this->db->where("type", $type);
		$query = $this->db->get();
		$res = $query->result();
		return $res;
	}
	public function clear_download_data($type){
		$this->db->update("tbl_download_data", array('data'=>''), array('type'=>$type));
	}
	public function get_nilai_pendapatan_perusahaan($request)
	{
		$where = "";
		if(isset($request['bulan_ini'])){
			$where = " AND EXTRACT(YEAR_MONTH FROM tanggal) = '".date("Ym")."' ";
		}
		if(isset($request['tahun_ini'])){
			$where = " AND EXTRACT(YEAR FROM tanggal) = '".date("Y")."' ";
		}
		if(isset($request['tanggal'])){
			$where = " AND EXTRACT(YEAR_MONTH FROM tanggal) = '".date_format(date_create($request['tanggal']),"Ym")."' ";
		}
		$sql_penjualan = "SELECT SUM(grand_total) penjualan FROM `tbl_transaksi_penjualan` WHERE `delete` = '0' ".$where." ;";
		$sql_pembelian = "SELECT SUM(grand_total) pembelian FROM `tbl_faktur_pembelian` WHERE `delete` = '0' ".$where." ;";

		$query_penjualan = $this->db->query($sql_penjualan);
		$penjualan = $query_penjualan->row()->penjualan;

		$query_pembelian = $this->db->query($sql_pembelian);
		$pembelian = $query_pembelian->row()->pembelian;

		return ($penjualan-$pembelian);
	}
	public function get_laporan_dashboard($request)
	{
		$ret = array();
		$where = "";
		if(!isset($request['bt_rentang']) and !isset($request['bt_periode'])){
			//$where .= " and EXTRACT(YEAR_MONTH FROM a.tanggal) = EXTRACT(YEAR_MONTH FROM NOW()) ";
			$where .= " and a.tanggal = '".date("Y-m-d")."' ";
		}
		if(isset($request['bt_rentang'])){
			$where .= " and (a.tanggal >= '".$request['tanggal_dari']."' and a.tanggal <= '".$request['tanggal_ke']."') ";
		}

		$query = $this->db->query("
				select SUM(a.transaksi) as nilai_pengeluaran from
				((SELECT a.id id_barang,a.nama_barang,'pembelian' jenis_transaksi,date(c.tanggal) as tanggal,b.sub_total as transaksi,b.qty FROM
				tbl_data_barang a
				INNER JOIN tbl_faktur_pembelian_detail_barang b ON b.id_barang = a.id
				INNER JOIN tbl_faktur_pembelian c ON c.id = b.id_faktur_pembelian)
				UNION ALL
				(SELECT a.id id_barang,a.nama_barang,'retur_penjualan' jenis_transaksi,date(d.tanggal) as tanggal,(b.harga_satuan*e.qty) as transaksi,e.qty FROM
				tbl_data_barang a
				INNER JOIN tbl_transaksi_penjualan_detail_barang b ON b.id_barang = a.id
				INNER JOIN tbl_transaksi_penjualan c ON c.id = b.id_transaksi_penjualan
				INNER JOIN tbl_retur_penjualan d ON d.id_transaksi_penjualan = c.id
				INNER JOIN tbl_retur_penjualan_detail_barang e ON e.id_retur_penjualan = d.id))
				AS a
				WHERE 1 ".$where."
		");
		$ret['pengeluaran_hari_ini'] = $query->row_array()['nilai_pengeluaran'];

		$query = $this->db->query("
				select SUM(a.transaksi) as nilai_pendapatan from
				((SELECT a.id id_barang,a.nama_barang,'retur_pembelian' jenis_transaksi,date(d.tanggal) as tanggal,(b.harga_satuan*e.qty) as transaksi,e.qty FROM
				tbl_data_barang a
				INNER JOIN tbl_faktur_pembelian_detail_barang b ON b.id_barang = a.id
				INNER JOIN tbl_faktur_pembelian c ON c.id = b.id_faktur_pembelian
				INNER JOIN tbl_retur_pembelian d ON d.id_faktur_pembelian = c.id
				INNER JOIN tbl_retur_pembelian_detail_barang e ON e.id_retur_pembelian = d.id)
				UNION ALL
				(SELECT a.id id_barang,a.nama_barang,'penjualan' jenis_transaksi,date(c.tanggal) as tanggal,b.sub_total as transaksi,b.qty FROM
				tbl_data_barang a
				INNER JOIN tbl_transaksi_penjualan_detail_barang b ON b.id_barang = a.id
				INNER JOIN tbl_transaksi_penjualan c ON c.id = b.id_transaksi_penjualan))
				AS a
				WHERE 1 ".$where."
		");

		$ret['pendapatan_hari_ini'] = $query->row_array()['nilai_pendapatan'];

		$query = $this->db->query("
				select * from
				((SELECT a.id id_barang,a.nama_barang,'penjualan' jenis_transaksi,date(c.tanggal) as tanggal,b.sub_total as transaksi,b.qty FROM
				tbl_data_barang a
				INNER JOIN tbl_transaksi_penjualan_detail_barang b ON b.id_barang = a.id
				INNER JOIN tbl_transaksi_penjualan c ON c.id = b.id_transaksi_penjualan))
				AS a
				WHERE 1 ".$where."
		");
		$ret['transaksi_hari_ini'] =  $query->num_rows();


		$query = $this->db->query("
									SELECT
										SUM(a.grand_total) as penjualan,
										b.nama_kota,
										CONCAT(EXTRACT(YEAR FROM a.tanggal),'/',EXTRACT(MONTH FROM a.tanggal)) as tahun_bulan,
										EXTRACT(YEAR_MONTH FROM a.tanggal) as tahunbulan
									FROM tbl_transaksi_penjualan as a
									INNER JOIN tbl_customer as b on b.id = a.id_customer
									WHERE EXTRACT(YEAR FROM a.tanggal) = EXTRACT(YEAR FROM NOW())
									GROUP BY b.id_kota, tahunbulan
									ORDER BY tahunbulan ASC;
								");
		$result_array = $query->result_array();

		$list_nama_kota = array();
		foreach($result_array as $row){
			if(trim($row['nama_kota']) != "" and !in_array($row['nama_kota'],$list_nama_kota))
			$list_nama_kota[] = $row['nama_kota'];
		}

		$list_tahun_bulan = array();
		foreach($result_array as $row){
			if(trim($row['tahun_bulan']) != "" and !in_array($row['tahun_bulan'],$list_tahun_bulan))
			$list_tahun_bulan[] = $row['tahun_bulan'];
		}

		$resDataTB = "";
		foreach($list_tahun_bulan as $tahun_bulan){
			$resStr = "'".$tahun_bulan."'";
			foreach($result_array as $arRow){
				if($arRow['tahun_bulan'] == $tahun_bulan){
					$resStr .= ", ".$arRow['penjualan'];
				}
			}
			$resDataTB .= "[".$resStr."],";
		}

		$ret['chart_per_wilayah']['nama_kota'] = implode("', '",$list_nama_kota);
		$ret['chart_per_wilayah']['tahun_bulan_penjualan'] = rtrim($resDataTB,",");

		$query = $this->db->query("
									SELECT
									date(a.tanggal) as tanggal,
									c.id id_barang,
									c.nama_barang,
									SUM(b.qty) qty,
									SUM(b.sub_total) as transaksi
									FROM tbl_transaksi_penjualan a
									INNER JOIN tbl_transaksi_penjualan_detail_barang b ON b.id_transaksi_penjualan = a.id
									INNER JOIN tbl_data_barang c ON c.id = b.id_barang
									WHERE 1 ".$where."
									GROUP BY id_barang
		");
		$result_array = $query->result_array();

		$resDataBarang = "";
		foreach($result_array as $row){
			$resDataBarang .= "['".$row['nama_barang']."', ".$row['qty']."],";
		}

		$ret['chart_pergerakan_penjualan'] = rtrim($resDataBarang,",");
		return $ret;
	}
}
