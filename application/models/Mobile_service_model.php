<?php
class Mobile_service_model extends CI_Model {
	public $rows_per_page = 50;
	public function __construct()
	{
		parent::__construct();
		@define('_PASSWORD_KEY_', 'vEU9fTiboZaoWj3IU1WWMesNTfgWGvTQ1hbKK7bqvU7VxLmOqGEYv1qM'); // BARIS INI HARUS ADA DI admin_model.php DAN site_model.php
	}
	public function user_register($request = NULL)
	{
		$query = $this->db->query("SELECT * FROM tbl_customer
											WHERE
												`delete` = '0' 
												and email = '".$request['email']."' 
												LIMIT 1
								;");
		$num_rows = $query->num_rows();
		$row_array = $query->row_array();
		if($num_rows == 0){
			$status = "0";
			if($request['id_group_customer'] == "1" or $request['id_group_customer'] == "2" or $request['id_group_customer'] == "3"){
				$status = "1";
			}
			$new_kode_customer = $request['id_group_customer'].$this->admin_model->get_no_urut_by_group_customer($request['id_group_customer']);
			$insertData = array(
				'nama_customer' => $request['full_name'],
				'id_group_customer' => $request['id_group_customer'],
				'kode_customer' => $new_kode_customer,
				'telepon' => $request['telepon'],
				'email' => $request['email'],
				'password' => md5($request['password']),
				'status' => $status
			);
			$this->db->insert("tbl_customer", $insertData);
			$res['insert_id'] = $this->db->insert_id();
			$res['message'] = "Anda berhasil mendaftar, silahkan login!";
		}else{
			$res['insert_id'] = 0;
			if($row_array['status'] == "0"){
				$res['message'] = "email yang anda masukan sudah terdaftar, tetapi belum di konfirmasi oleh admin.\nSilahkan melakukan konfirmasi, untuk bisa login!";
			}else{
				$res['message'] = "email yang anda masukan sudah terdaftar";
			}
		}
		return json_encode($res);
	}
	public function user_login($request = NULL)
	{
		$query = $this->db->query("SELECT * FROM tbl_customer 
											WHERE
												`delete` = '0' 
												and 
												(
													(email = '".$request['email']."' and password = '".md5(@$request['password'])."')
													or
													(email = '".$request['email']."' and password = '".@$request['password_enc']."')
												)
												LIMIT 1
								;");
		$num_rows = $query->num_rows();
		$row_array = $query->row_array();
		
		$res['num_rows'] = $num_rows;
		if($num_rows > 0){
			$res['data_length'] = $num_rows;
			$res['data'] = $row_array;
		}
		return json_encode($res);
	}
	public function user_update_token($request = NULL)
	{
		$token = @$request['token'];
		$id_user = $request['id_user'];
		$result = $this->db->update("tbl_customer", ["token" => $token], ["id" => $id_user]);
		
		if($result === true){
			$res['result'] = $result;
			$res['message'] = "Token berhasil diupdate!";
		}else{
			$res['result'] = $result;
			$res['message'] = "Token gagal diupdate!";
		}
		return json_encode($res);
	}
	public function forget_password($request = NULL)
	{
		$query = $this->db->query("SELECT * FROM tbl_customer 
											WHERE
												`delete` = '0' 
												and email = '".$request['email']."' 
												LIMIT 1
								;");
		$num_rows = $query->num_rows();
		$row_array = $query->row_array();
		
		$res['num_rows'] = $num_rows;
		if($num_rows > 0){
			$res['data_length'] = $num_rows;
			$res['data'] = $row_array;
			if($row_array['status'] == "1"){
				//kirim email reset password;
			}
		}
		return json_encode($res);
	}
	public function upload_image_ktp($data_image, $new_image_name, $old_image_name){
		$UPLOAD_DIR_ORI = './assets/upload_ktp_customer/original/';
		$UPLOAD_DIR_DISP = './assets/upload_ktp_customer/display/';
		@unlink($UPLOAD_DIR_ORI . $old_image_name);
		@unlink($UPLOAD_DIR_DISP . $old_image_name);
		$image_parts = explode(";base64,", $data_image);
		$image_type_aux = explode("image/", $image_parts[0]);
		$image_type = $image_type_aux[1];
		$image_base64 = base64_decode($image_parts[1]);
		if($image_type == "png"){
			$file_ori = $UPLOAD_DIR_ORI . $new_image_name . '.png';
			$file_disp = $UPLOAD_DIR_DISP . $new_image_name . '.png';
			$ext = ".png";
		}else if($image_type == "jpeg"){
			$file_ori = $UPLOAD_DIR_ORI . $new_image_name . '.jpg';
			$file_disp = $UPLOAD_DIR_DISP . $new_image_name . '.jpg';
			$ext = ".jpg";
		}
		file_put_contents($file_ori, $image_base64);
		file_put_contents($file_disp, $image_base64);
		
		return $ext;
	}
	public function update_profile($request = NULL)
	{
		$dataSet = array();
		foreach($request as $key => $val){
			if($key != 'id' and $key != 'password' and $key != 'file_image_ktp' and $key != 'file_image_ktp_istri' and $key != 'image_ktp' and $key != 'image_ktp_istri'){
				$dataSet[$key] = $val;
			}
			if($key == 'file_image_ktp' or $key == 'file_image_ktp_istri'){
				$keyn = str_replace("file_","",$key);
				$new_image_name = date("Ymdhisa")."-".md5(uniqid())."-".str_replace(" ","-",$request['nama_customer']);
				$old_image_name = $request[$keyn];
				$ext = $this->mobile_service_model->upload_image_ktp($val,$new_image_name,$old_image_name);
				$dataSet[$keyn] = $new_image_name.$ext;
			}
		}
		$dataWhere = array(
			'id' => $request['id']
		);
		$result = $this->db->update("tbl_customer", $dataSet, $dataWhere);
		
		if($result === true){
			$res['result'] = $result;
			$res['message'] = "Profile anda berhasil diupdate!";
		}else{
			$res['result'] = $result;
			$res['message'] = "Profile anda gagal diupdate!";
		}
		return json_encode($res);
	}
	public function get_promo_slider($request = NULL)
	{
		$tanggal_sekarang = date("Y-m-d");
		$query = $this->db->query("
									SELECT CONCAT(a.id,b.id) id,a.tanggal as dari_tanggal,b.periode_sampai_dengan as sampai_dengan_tanggal,b.harga,b.gambar,a.keterangan FROM `tbl_data_promo` a
									INNER JOIN `tbl_data_promo_barang` b ON b.id_data_promo = a.id
									WHERE
									('".$tanggal_sekarang."' >= a.tanggal AND '".$tanggal_sekarang."' <= b.periode_sampai_dengan)
									AND a.`status` = '1'
									AND a.`delete` = '0'
									;
								  ");
		$res = $query->result_array();
		return json_encode($res);
	}
	public function get_data_barang_for_sale($request = NULL)
	{
		$query = $this->db->query("
									SELECT 
										a.id_barang,
										b.nama_barang,
										SUM(a.qty) qty,
										c.harga_jual_normal as harga_jual,
										IFNULL((
											SELECT 
												sb.harga
											FROM `tbl_data_promo` sa
											INNER JOIN `tbl_data_promo_barang` sb ON sb.id_data_promo = sa.id
											WHERE
												(NOW() >= sa.tanggal AND NOW() <= sb.periode_sampai_dengan)
												AND sb.id_barang = a.id_barang
												AND sa.`status` = '1'
												AND sa.`delete` = '0'
											ORDER BY sb.harga ASC
											LIMIT 1
										),0) as harga_jual_promo
									FROM `tbl_data_barang_stok` a
									INNER JOIN `tbl_data_barang` b ON b.id = a.id_barang
									INNER JOIN `tbl_harga_jual` c ON c.id_barang = a.id_barang
									WHERE
										a.`status` = '1'
										AND b.`status` = '1'
										AND c.`status` = '1'
										AND a.`delete` = '0'
										AND b.`delete` = '0'
										AND c.`delete` = '0'
										AND a.qty > 0
									GROUP BY a.id_barang
									ORDER BY b.nama_barang ASC
									;
								  ");
		$res = $query->result_array();
		foreach($res as $row => $d){
			if($d['harga_jual_promo'] > 0 and $d['harga_jual_promo'] < $d['harga_jual']){
				$d['harga_jual'] = $d['harga_jual_promo'];
			}
			$res[$row] = $d;
		}
		return json_encode($res);
	}
	public function get_histori_transaksi($request = NULL)
	{
		$where = "";
		if(isset($request['id_customer'])){
			$where .= " AND a.id_customer = '".$request['id_customer']."' ";
		}
		if(isset($request['id_order'])){
			$where .= " AND a.id = '".$request['id_order']."' ";
		}
		$query = $this->db->query("
									SELECT 
										a.id,
										a.id_customer,
										a.tgl_input as tgl_request,
										IFNULL(b.tanggal,'') as tgl_approve,
										b.nomor,
										a.total_qty as qty,
										a.total_bayar as total,
										a.status_apply,
										a.data_confirm_by_admin
									FROM `tbl_mobile_order` a 
									LEFT JOIN `tbl_transaksi_penjualan` b ON b.id_mobile_order = a.id
									WHERE 1 
									".$where." 
									AND a.`status` = '1' 
									AND a.`delete` = '0' 
									ORDER BY a.id DESC 
								  ");
		$res = $query->result_array();
		return json_encode($res);
	}
	public function order_app($request = NULL)
	{
		$this->db->trans_begin();
		
		$insertOrder = array(
			'id_customer' => $request['data_customer']['id'],
			'alamat_tujuan' => $request['alamat_tujuan'],
			'catatan' => $request['catatan'],
			'pembayaran' => $request['pembayaran'],
			'total_qty' => $request['total_qty'],
			'total_bayar' => $request['total_bayar'],
			'ongkir' => $request['ongkir'],
			'grand_total' => $request['grand_total']
		);
		$this->db->insert("tbl_mobile_order", $insertOrder);
		$id_mobile_order = $this->db->insert_id();
		
		$cart = $request['cart'];
		$insertCart = [];
		foreach($cart as $row){
			$rowData = array(
				'id_mobile_order' => $id_mobile_order,
				'id_barang' => $row['id_barang'],
				'nama_barang' => $row['nama_barang'],
				'qty' => $row['qty_order'],
				'harga_jual' => $row['harga_jual'],
				'sub_total' => $row['sub_total']
			);
			$insertCart[] = $rowData;
		}
		$this->db->insert_batch('tbl_mobile_order_detail', $insertCart);		

		if($this->db->trans_status() === FALSE){
			$result = FALSE;
			$this->db->trans_rollback();
		}else{
			$result = TRUE;
			$this->db->trans_commit();
		}

		if($result === TRUE){
			$res['status_order'] = 1;
			$res['message'] = "Anda berhasil melakukan transaksi!";
		}else{
			$res['status_order'] = 0;
			$res['message'] = "Anda gagal melakukan transaksi!";
		}
		return json_encode($res);
	}
	public function confirm_order_by_customer($request = NULL){
		$data_update = [
							'pembayaran' => $request['pembayaran'],
							'rekening_pengirim' => $request['data_rekening']
						];
		$this->db->update("tbl_mobile_order", $data_update, ["id" => $request['id_order']]);
		$row = json_decode($this->mobile_service_model->get_histori_transaksi($request),true)[0];
		$data_confirm_by_admin = json_decode(base64_decode($row['data_confirm_by_admin']),true);
		$data_input = $data_confirm_by_admin;
		$res['message'] = $this->penjualan_model->apply_request_dari_app($data_input['id_order'], $data_input);
		return json_encode($res);
	}
	public function cancel_order_app($request = NULL)
	{
		$id_order = $request['id_order'];
		$this->db->update('tbl_mobile_order', ['delete'=>'1'], ['id' => $id_order]);
		$this->db->update('tbl_mobile_order_detail', ['delete'=>'1'], ['id_mobile_order' => $id_order]);
		$res['status_order'] = 1;
		$res['message'] = "Anda berhasil membatalkan transaksi!";
		return json_encode($res);
	}
	public function push_notification_to_mobile($request = NULL)
	{
		$id_order = $request['id_order'];
		$sql = "SELECT b.token FROM `tbl_mobile_order` a
				INNER JOIN tbl_customer b ON b.id = a.id_customer
				WHERE
				a.id = '".$id_order."' ; ";
		$query = $this->db->query($sql);
		$data_customer = $query->row_array();
		
		if($data_customer){
			$token = $data_customer['token'];
			$title = $request['title'];
			$text = $request['text'];
			$url = "https://fcm.googleapis.com/fcm/send";
			$serverKey = 'AAAAEKM8zkg:APA91bHTO7UjT30XpOuekqaQ-zOnq139iaxkz8lr48aB3EkMJUDrLfA7p_Iwejgtt94AXumsMr0DoghlFZfMHh8UPA3y_g-1VIUB75yvWNJkXyI-G7VJEDBkNrDSrcy-dCH5HPEhkDPD';
			$notification = array('title' =>$title , 'text' => $text, 'sound' => 'default');
			$arrayToSend = array('to' => $token, 'notification' => $notification);
			$json = json_encode($arrayToSend);
			
			$headers = array();
			$headers[] = 'Content-Type: application/json';
			$headers[] = 'Authorization: key='. $serverKey;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
			curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);

			//Send the request
			$response = @curl_exec($ch);
			curl_close($ch);
			return $response;
		}
	}
}