<?php
class Saham_model extends CI_Model {
	public $rows_per_page = 50;
	public function __construct()
	{
		parent::__construct();
		@define('_PASSWORD_KEY_', 'vEU9fTiboZaoWj3IU1WWMesNTfgWGvTQ1hbKK7bqvU7VxLmOqGEYv1qM'); // BARIS INI HARUS ADA DI admin_model.php DAN site_model.php
	}
	public function get_data_investor($id = NULL,$request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.tanggal', $request['tx_cari'], 'both');
			$this->db->or_like('a.nomor', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		
		$this->db->where('a.`delete`', '0');
		$this->db->select('a.*');
		$this->db->from('tbl_data_investor a');
		
		$this->db->order_by('a.tanggal','DESC');
		$this->db->order_by('a.nomor','DESC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_data_investor($request = NULL, $file = NULL)
	{
		$query = $this->db->query("SELECT nomor_urut FROM tbl_data_investor WHERE EXTRACT(YEAR_MONTH FROM tgl_input) = '".date('Ym')."' ORDER BY nomor_urut DESC limit 1;");
		$row = $query->row();
		if(isset($row->nomor_urut) and $row->nomor_urut > 0)
			$newNomorUrut = $row->nomor_urut+1;
		else
			$newNomorUrut = 1;
		// 0001/IVT/I/19
		$newNomor = numLength($newNomorUrut,4)."/IVT/".numberToRoman(date('m'))."/".date('y');
		
		$insertData = array(
			'tanggal' => $request['tanggal'],
			'nomor' => $newNomor,
			'nomor_urut' => $newNomorUrut,
			'id_customer' => $request['id_customer'],
			'kode_investor' => "",
			'nama_investor' => $request['nama_investor'],
			'email' => $request['email'],
			'alamat' => $request['alamat'],
			'no_npwp' => $request['no_npwp'],
			'foto_copy_ktp' => $request['foto_copy_ktp'],
			'nilai_investasi' => $request['nilai_investasi'],
			'metode_bayar' => $request['metode_bayar']
		);
		$this->db->insert("tbl_data_investor", $insertData);
		$insert_id = $this->db->insert_id();
		
		$this->db->query("update tbl_data_investor set kode_investor='".$insert_id."' where id = '".$insert_id."' ;");
		
		if(is_numeric($insert_id)){
			$message = "Berhasil Input Data";
		}else{
			$message = "Gagal Input Data!";
		}
		return $message;
	}
	public function edit_data_investor($request = NULL, $file = NULL)
	{
		$updateData = array(
			'tanggal' => $request['tanggal'],
			'id_customer' => $request['id_customer'],
			'nama_investor' => $request['nama_investor'],
			'email' => $request['email'],
			'alamat' => $request['alamat'],
			'no_npwp' => $request['no_npwp'],
			'foto_copy_ktp' => $request['foto_copy_ktp'],
			'nilai_investasi' => $request['nilai_investasi'],
			'metode_bayar' => $request['metode_bayar']
		);
		$whereEdit = array(
			'id' => $request['id_investor']
		);
		$res = $this->db->update("tbl_data_investor", $updateData, $whereEdit);
		
		if($res == 1){
			$message = "Berhasil Edit Data";
		}else{
			$message = "Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_data_investor($id = NULL)
	{
		$updateData = array(
			'delete' => '1'
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("tbl_data_investor", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}
		return $message;
	}
	public function product_upload_file($data,$file)
	{
		$config['upload_path']          = './assets/upload_ktp_investor/original/';
		$config['allowed_types']        = 'jpg|png';
		$config['file_name']        = $data['file_image_name'];
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$do_upload = $this->upload->do_upload('foto_copy_ktp');
		
		if (!$do_upload){
			$message = 'Simpan gambar gagal: '.$this->upload->display_errors();
		}else{
			//$this->product_upload_file_crop($config['upload_path'].$config['file_name']);
			$this->product_upload_file_display($config);
			$message = 'Simpan gambar berhasil';
		}
		unset($config);
		return $message;
		
	}
	public function product_upload_file_crop($file_to_change)
	{
		if(!isset($_FILES['image'])) $_FILES['image'] = $_FILES['foto_copy_ktp'];
		
		if($_FILES['image']['type'] == "image/jpeg") $im = imagecreatefromjpeg($file_to_change);
		if($_FILES['image']['type'] == "image/png") $im = imagecreatefrompng($file_to_change);
		$cropped = imagecropauto($im, IMG_CROP_DEFAULT);
		if($cropped !== false){
			imagedestroy($im);
			unlink($file_to_change);
			$im = $cropped;
		}
		if($_FILES['image']['type'] == "image/jpeg") imagejpeg($im, $file_to_change);
		if($_FILES['image']['type'] == "image/png") imagepng($im, $file_to_change);
		imagedestroy($im);
	}
	public function product_upload_file_display($data_config)
	{
		$output_file = './assets/upload_ktp_investor/display/'.$data_config['file_name'];
		if(isset($_REQUEST['image-data'])){
			$base64_image_string = base64_decode($_REQUEST['image-data']);
			// split the string on commas
			// $data[ 0 ] == "data:image/png;base64"
			// $data[ 1 ] == <actual base64 string>
			$data = explode( ',', $base64_image_string );
			$image_data = base64_decode($data[1]);
			file_put_contents($output_file, $image_data);
		}
	}
	public function get_pembagian_saham_detail_investor($id = NULL,$request = NULL)
	{
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		if(isset($request['id_pembagian_saham'])){
			$this->db->where('a.id_pembagian_saham', $request['id_pembagian_saham']);
		}
		$this->db->where('a.`delete`', '0');
		$this->db->select('a.*');
		$this->db->from('tbl_pembagian_saham_detail_investor a');
		
		$this->db->order_by('a.nama_investor','ASC');
		$this->db->order_by('a.email','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_pembagian_saham_detail_investor($request = NULL, $id_pembagian_saham)
	{
		$insertData = array(
			'id_pembagian_saham' => $id_pembagian_saham,
			'tanggal' => $request['tanggal'],
			'id_investor' => $request['id_investor'],
			'nama_investor' => $request['nama_investor'],
			'email' => $request['email'],
			'nilai_investasi' => $request['nilai_investasi'],
			'persentase' => $request['persentase'],
			'pendapatan_investasi' => $request['pendapatan_investasi']
		);
		$this->db->insert("tbl_pembagian_saham_detail_investor", $insertData);
		$insert_id = $this->db->insert_id();
		
		if(is_numeric(@$insert_id)){
			$message = "Berhasil Input Investor";
		}else{
			$message = "Gagal Input Investor!";
		}
		return $message;
	}
	public function get_laporan_cepat_pembagian_saham()
	{
		$ret = array();
		$ret['pendapatan_bulan_ini'] = $this->admin_model->get_nilai_pendapatan_perusahaan(array('bulan_ini'=>'1'));
		
		$ret['pendapatan_per_tahun'] = $this->admin_model->get_nilai_pendapatan_perusahaan(array('tahun_ini'=>'1'));
				
		$query = $this->db->query("SELECT SUM(total_pembagian_hasil) AS pembagian_saham FROM tbl_pembagian_saham WHERE `delete` = '0' AND EXTRACT(YEAR_MONTH FROM tanggal) = '".date("Ym")."' ;");
		$ret['pembagian_saham_bulan_ini'] = $query->row_array()['pembagian_saham'];

		return $ret;
	}
	public function get_pembagian_saham($id = NULL,$request = NULL)
	{
		if(isset($request['bt_cari'])){
			$this->db->group_start();
			$this->db->like('a.tanggal', $request['tx_cari'], 'both');
			$this->db->or_like('a.nomor', $request['tx_cari'], 'both');
			$this->db->group_end();
		}
		if($id <> NULL){
			$this->db->where('a.id', $id);
		}
		
		$this->db->where('a.`delete`', '0');
		$this->db->select('a.*');
		$this->db->from('tbl_pembagian_saham a');
		
		$this->db->order_by('a.tanggal','DESC');
		$this->db->order_by('a.nomor','DESC');
		$query = $this->db->get();
		return $query->result();
	}
	public function add_pembagian_saham($request = NULL, $file = NULL)
	{
		$query = $this->db->query("SELECT nomor_urut FROM tbl_pembagian_saham WHERE EXTRACT(YEAR_MONTH FROM tgl_input) = '".date('Ym')."' ORDER BY nomor_urut DESC limit 1;");
		$row = $query->row();
		if(isset($row->nomor_urut) and $row->nomor_urut > 0)
			$newNomorUrut = $row->nomor_urut+1;
		else
			$newNomorUrut = 1;
		// 0001/IVT/I/19
		$newNomor = numLength($newNomorUrut,4)."/IVT/".numberToRoman(date('m'))."/".date('y');
		
		$insertData = array(
			'tanggal' => $request['tanggal'],
			'nomor' => $newNomor,
			'nomor_urut' => $newNomorUrut,
			'nilai_pendapatan_perusahaan' => $request['nilai_pendapatan_perusahaan'],
			'zakat' => $request['zakat'],
			'persentase_pajak' => $request['persentase_pajak'],
			'nilai_pajak' => $request['nilai_pajak'],
			'biaya_gaji' => $request['biaya_gaji'],
			'biaya_atk' => $request['biaya_atk'],
			'biaya_lain_lain' => $request['biaya_lain_lain'],
			'total_pendapatan_bersih' => $request['total_pendapatan_bersih'],
			'bagi_hasil_perusahaan' => $request['bagi_hasil_perusahaan'],
			'bagi_hasil_investor' => $request['bagi_hasil_investor'],
			'total_investasi' => $request['total_investasi'],
			'total_pembagian_hasil' => $request['total_pembagian_hasil']
		);
		$this->db->insert("tbl_pembagian_saham", $insertData);
		$insert_id = $this->db->insert_id();
		
		$data = array();
		for($i = 0;$i<count($request['id_investor']);$i++){
			$subdata = array();
			foreach($request as $key => $val){
				if(is_array($val)){
					$subdata[$key] = $request[$key][$i];
				}
			}
			$data[] = $subdata;
		}
		
		foreach($data as $req){
			if($req['id_investor']!=""){
				$req['tanggal'] = $request['tanggal'];
				$add_pembagian_saham_detail_investor = $this->add_pembagian_saham_detail_investor($req, $insert_id);
			}
		}
		
		if(is_numeric($insert_id)){
			$message = @$add_pembagian_saham_detail_investor."<br/>Berhasil Input Data";
		}else{
			$message = @$add_pembagian_saham_detail_investor."<br/>Gagal Input Data!";
		}
		//return $message;
		return $insert_id;
	}
	public function edit_pembagian_saham($request = NULL)
	{
		$updateData = array(
			'tanggal' => $request['tanggal'],
			'nilai_pendapatan_perusahaan' => $request['nilai_pendapatan_perusahaan'],
			'zakat' => $request['zakat'],
			'persentase_pajak' => $request['persentase_pajak'],
			'nilai_pajak' => $request['nilai_pajak'],
			'biaya_gaji' => $request['biaya_gaji'],
			'biaya_atk' => $request['biaya_atk'],
			'biaya_lain_lain' => $request['biaya_lain_lain'],
			'total_pendapatan_bersih' => $request['total_pendapatan_bersih'],
			'bagi_hasil_perusahaan' => $request['bagi_hasil_perusahaan'],
			'bagi_hasil_investor' => $request['bagi_hasil_investor'],
			'total_investasi' => $request['total_investasi'],
			'total_pembagian_hasil' => $request['total_pembagian_hasil']
		);
		$whereEdit = array(
			'id' => $request['id_pembagian_saham']
		);
		$res = $this->db->update("tbl_pembagian_saham", $updateData, $whereEdit);

		$data = array();
		for($i = 0;$i<count(@$request['id_investor']);$i++){
			$subdata = array();
			foreach($request as $key => $val){
				if(is_array($val)){
					$subdata[$key] = $request[$key][$i];
				}
			}
			$data[] = $subdata;
		}
		//----------------------------------
			$this->db->delete('tbl_pembagian_saham_detail_investor', array('id_pembagian_saham' => $request['id_pembagian_saham']));
		//------------------------------------
		foreach($data as $req){
			if($req['id_investor']!=""){
				$req['tanggal'] = $request['tanggal'];
				$add_pembagian_saham_detail_investor = $this->add_pembagian_saham_detail_investor($req, $request['id_pembagian_saham']);
			}
		}
		if($res == 1){
			$message = @$add_pembagian_saham_detail_investor."<br/>Berhasil Edit Data";
		}else{
			$message = @$add_pembagian_saham_detail_investor."<br/>Gagal Edit Data!";
		}
		return $message;
	}
	public function hapus_pembagian_saham($id = NULL)
	{
		$updateData = array(
			'delete' => '1',
		);
		$whereEdit = array(
			'id' => $id
		);
		$res = $this->db->update("tbl_pembagian_saham", $updateData, $whereEdit);
		if($res == 1){
			$message = "Berhasil Hapus Data";
		}else{
			$message = "Gagal Hapus Data!";
		}
		
		return $message;
	}
	public function get_laporan_cepat_wilayah_penjualan($request)
	{
		$ret = array();
		/*
			$query = $this->db->query("SELECT SUM(total_pembagian_hasil) AS pembagian_saham FROM tbl_pembagian_saham WHERE `delete` = '0' AND EXTRACT(YEAR_MONTH FROM tanggal) = '".date("Ym")."' ;");
			$ret['pembagian_saham_bulan_ini'] = $query->row_array()['pembagian_saham'];
		*/
		$query = $this->db->query("
									SELECT c.wilayah, SUM(a.grand_total) as penjualan FROM tbl_transaksi_penjualan as a
									INNER JOIN tbl_customer as b on b.id = a.id_customer
									INNER JOIN tbl_kota as c on c.id = b.id_kota
									WHERE a.`delete` = '0'
									GROUP BY b.id_kota
									ORDER BY penjualan ASC;
								");
		$result_array = $query->result_array();
		$ret['penjualan_terendah']['wilayah'] = current($result_array)['wilayah'];
		$ret['penjualan_terendah']['penjualan'] = current($result_array)['penjualan'];
		$ret['penjualan_tertinggi']['wilayah'] = end($result_array)['wilayah'];
		$ret['penjualan_tertinggi']['penjualan'] = end($result_array)['penjualan'];
			
		$query = $this->db->query("
									SELECT 
										SUM(a.grand_total) as penjualan, 
										c.wilayah, 
										CONCAT(EXTRACT(YEAR FROM a.tanggal),'/',EXTRACT(MONTH FROM a.tanggal)) as tahun_bulan,
										EXTRACT(YEAR_MONTH FROM a.tanggal) as tahunbulan
									FROM tbl_transaksi_penjualan as a
									INNER JOIN tbl_customer as b on b.id = a.id_customer
									INNER JOIN tbl_kota as c on c.id = b.id_kota
									WHERE a.`delete` = '0'
									AND EXTRACT(YEAR FROM a.tanggal) = EXTRACT(YEAR FROM NOW())
									GROUP BY b.id_kota, tahunbulan
									ORDER BY tahunbulan ASC;
								");
		$result_array = $query->result_array();
		
		$list_wilayah = array();
		foreach($result_array as $row){
			if(trim($row['wilayah']) != "" and !in_array($row['wilayah'],$list_wilayah))
			$list_wilayah[] = $row['wilayah'];
		}
		
		$list_tahun_bulan = array();
		foreach($result_array as $row){
			if(trim($row['tahun_bulan']) != "" and !in_array($row['tahun_bulan'],$list_tahun_bulan))
			$list_tahun_bulan[] = $row['tahun_bulan'];
		}
		
		$resDataTB = "";
		foreach($list_tahun_bulan as $tahun_bulan){
			$resStr = "'".$tahun_bulan."'";
			foreach($result_array as $arRow){
				if($arRow['tahun_bulan'] == $tahun_bulan){
					$resStr .= ", ".$arRow['penjualan'];
				}
			}
			$resDataTB .= "[".$resStr."],";
		}
		
		$ret['chart']['wilayah'] = implode("', '",$list_wilayah);
		$ret['chart']['tahun_bulan_penjualan'] = rtrim($resDataTB,",");
		
		if(isset($request['bt_cari_customer_tertinggi'])){
			$whereCT = " AND b.nama_customer like '%".$request['tx_cari']."%' ";
		}
		$query = $this->db->query("
									SELECT 
									b.id,
									b.nama_customer,
									c.wilayah,
									a.grand_total as penjualan
									FROM tbl_transaksi_penjualan as a
									INNER JOIN tbl_customer as b on b.id = a.id_customer
									INNER JOIN tbl_kota as c on c.id = b.id_kota
									WHERE a.`delete` = '0'
									AND EXTRACT(YEAR_MONTH FROM a.tanggal) = EXTRACT(YEAR_MONTH FROM NOW())
									".@$whereCT."
									ORDER BY penjualan DESC
									LIMIT 1;
								");
		$ret['pembelian_customer_paling_tinggi'] = $query->row_array();
		
		$query = $this->db->query("
				select * from
				((SELECT d.id id_customer,d.nama_customer,a.id id_barang,a.nama_barang,c.nomor,'penjualan' jenis_transaksi,date(c.tanggal) as tanggal,b.qty,b.sub_total as transaksi FROM 
				tbl_data_barang a
				INNER JOIN tbl_transaksi_penjualan_detail_barang b ON b.id_barang = a.id
				INNER JOIN tbl_transaksi_penjualan c ON c.id = b.id_transaksi_penjualan
				INNER JOIN tbl_customer d ON d.id = c.id_customer
				WHERE c.`delete` = '0')
				UNION ALL
				(SELECT f.id id_customer,f.nama_customer,a.id id_barang,a.nama_barang,d.nomor,'retur_penjualan' jenis_transaksi,date(d.tanggal) as tanggal,e.qty,(b.harga_satuan*e.qty) as transaksi FROM 
				tbl_data_barang a
				INNER JOIN tbl_transaksi_penjualan_detail_barang b ON b.id_barang = a.id
				INNER JOIN tbl_transaksi_penjualan c ON c.id = b.id_transaksi_penjualan
				INNER JOIN tbl_retur_penjualan d ON d.id_transaksi_penjualan = c.id
				INNER JOIN tbl_retur_penjualan_detail_barang e ON e.id_retur_penjualan = d.id
				INNER JOIN tbl_customer f ON f.id = c.id_customer
				WHERE c.`delete` = '0'))
				AS tbl_union
				WHERE 
				EXTRACT(YEAR_MONTH FROM tbl_union.tanggal) = EXTRACT(YEAR_MONTH FROM NOW()) 
				AND tbl_union.id_customer = '".$ret['pembelian_customer_paling_tinggi']['id']."'
				order by tbl_union.tanggal desc ;
		");
		$ret['transaksi_barang_customer_paling_tinggi'] = $query->result_array();
		return $ret;
	}
	public function get_laporan_profit($request)
	{
		$ret = array();
		$where = "";
		if(!isset($request['bt_rentang']) and !isset($request['bt_periode'])){
			$where .= " and EXTRACT(YEAR_MONTH FROM a.tanggal) = EXTRACT(YEAR_MONTH FROM NOW()) ";
			$request['tanggal_dari'] = date("Y-m-d");
		}
		if(isset($request['bt_rentang'])){
			$where .= " and (a.tanggal >= '".$request['tanggal_dari']."' and a.tanggal <= '".$request['tanggal_ke']."') ";
		}
		$query = $this->db->query("
				select SUM(a.transaksi) as nilai_pengeluaran from
				((SELECT a.id id_barang,a.nama_barang,'pembelian' jenis_transaksi,date(c.tanggal) as tanggal,b.sub_total as transaksi,b.qty FROM 
				tbl_data_barang a
				INNER JOIN tbl_faktur_pembelian_detail_barang b ON b.id_barang = a.id
				INNER JOIN tbl_faktur_pembelian c ON c.id = b.id_faktur_pembelian
				WHERE c.`delete` = '0')
				UNION ALL
				(SELECT a.id id_barang,a.nama_barang,'retur_penjualan' jenis_transaksi,date(d.tanggal) as tanggal,(b.harga_satuan*e.qty) as transaksi,e.qty FROM 
				tbl_data_barang a
				INNER JOIN tbl_transaksi_penjualan_detail_barang b ON b.id_barang = a.id
				INNER JOIN tbl_transaksi_penjualan c ON c.id = b.id_transaksi_penjualan
				INNER JOIN tbl_retur_penjualan d ON d.id_transaksi_penjualan = c.id
				INNER JOIN tbl_retur_penjualan_detail_barang e ON e.id_retur_penjualan = d.id
				WHERE d.`delete` = '0'))
				AS a
				WHERE 1 ".$where."
		");
		$ret['nilai_pengeluaran'] = $query->row_array()['nilai_pengeluaran'];
		
		$query = $this->db->query("
				select SUM(a.transaksi) as nilai_pendapatan from
				((SELECT a.id id_barang,a.nama_barang,'retur_pembelian' jenis_transaksi,date(d.tanggal) as tanggal,(b.harga_satuan*e.qty) as transaksi,e.qty FROM 
				tbl_data_barang a
				INNER JOIN tbl_faktur_pembelian_detail_barang b ON b.id_barang = a.id
				INNER JOIN tbl_faktur_pembelian c ON c.id = b.id_faktur_pembelian
				INNER JOIN tbl_retur_pembelian d ON d.id_faktur_pembelian = c.id
				INNER JOIN tbl_retur_pembelian_detail_barang e ON e.id_retur_pembelian = d.id
				WHERE d.`delete` = '0')
				UNION ALL
				(SELECT a.id id_barang,a.nama_barang,'penjualan' jenis_transaksi,date(c.tanggal) as tanggal,b.sub_total as transaksi,b.qty FROM 
				tbl_data_barang a
				INNER JOIN tbl_transaksi_penjualan_detail_barang b ON b.id_barang = a.id
				INNER JOIN tbl_transaksi_penjualan c ON c.id = b.id_transaksi_penjualan
				WHERE c.`delete` = '0'))
				AS a
				WHERE 1 ".$where."
		");
		$ret['nilai_pendapatan'] = $query->row_array()['nilai_pendapatan'];
		$ret['saldo_kas'] = 0;
		
		
		
		
		
		//-------------------------------- PROFIT 
		
		$query = $this->db->query("
			SELECT
			a.tanggal,
			a.nomor,
			c.id id_barang,
			c.nama_barang,
			d.harga_jual_normal harga_jual,
			b.no_seri
			FROM `tbl_transaksi_penjualan` a
			INNER JOIN tbl_transaksi_penjualan_detail_barang b ON b.id_transaksi_penjualan = a.id
			INNER JOIN tbl_data_barang c ON c.id = b.id_barang
			INNER JOIN tbl_harga_jual d ON d.id_barang = b.id_barang
			WHERE 1 
			AND a.`delete` = '0'
			".$where."
		");		
		$resProfit = $query->result_array();
		$arr_data_profit = array();
		foreach($resProfit as $row){
			$arr_harga_satuan_seri = array();
			$res_harga_satuan_seri = $this->db->query("SELECT no_seri, harga_satuan_seri FROM `tbl_data_barang_stok` WHERE id_barang = '".$row['id_barang']."' ;")->result_array();
			foreach($res_harga_satuan_seri as $row_hss){
				$no_seri = $row_hss['no_seri'];
				$harga_satuan_seri = $row_hss['harga_satuan_seri'];
				$arr_harga_satuan_seri[$no_seri] = $harga_satuan_seri;
			}
			
			$obj_no_seri = json_decode($row['no_seri'],true);
			$no_seri = $obj_no_seri['no_seri'];
			$qty = $obj_no_seri['qty'];
			foreach($no_seri as $k => $val){
				$sub_data = array();
				$sub_data['tanggal'] = $tanggal = $row['tanggal'];
				$sub_data['nomor'] = $nomor = $row['nomor'];
				$sub_data['nama_barang'] = $nama_barang = $row['nama_barang'];
				$sub_data['harga_jual'] = $harga_jual = $row['harga_jual'];
					$sub_data['id_barang'] = $id_barang = $row['id_barang'];
					$sub_data['no_seri'] = $r_no_seri = $no_seri[$k];
					$sub_data['qty'] = $r_qty = $qty[$k];
				$sub_data['harga_beli'] = $harga_beli = $arr_harga_satuan_seri[$r_no_seri];
				$sub_data['profit'] = $profit = (($harga_jual-$harga_beli)*$r_qty);
				
				$arr_data_profit[] = $sub_data;
			}
		}
		
		//debug($arr_data_profit);
		//die();
		//-------------------------------- END PROFIT 
		/*
		$query = $this->db->query("
			SELECT a.tanggal,a.nomor,c.nama_barang,b.no_seri,b.qty,d.harga_beli,d.harga_jual_normal harga_jual,((d.harga_jual_normal-d.harga_beli)*b.qty) as profit FROM `tbl_transaksi_penjualan` a
			INNER JOIN tbl_transaksi_penjualan_detail_barang b ON b.id_transaksi_penjualan = a.id
			INNER JOIN tbl_data_barang c ON c.id = b.id_barang
			INNER JOIN tbl_harga_jual d ON d.id_barang = b.id_barang
			WHERE 1 
			AND a.`delete` = '0'
			".$where."
		");
		$ret['get_table_transaksi_penjualan'] = $query->result_array();
		*/
		$ret['get_table_transaksi_penjualan'] = $arr_data_profit;
		
		
		
		
		
		
		
		
		
		$query = $this->db->query("
									select 
												a.tanggal,
												CONCAT(EXTRACT(YEAR FROM a.tanggal),'/',EXTRACT(MONTH FROM a.tanggal)) as tahun_bulan,
												EXTRACT(YEAR_MONTH FROM a.tanggal) as tahunbulan,
												a.jenis_transaksi,
												a.transaksi, 
												SUM(IF(a.jenis_transaksi = 'penjualan' OR a.jenis_transaksi = 'retur_pembelian',a.transaksi,0)) as pemasukan,
												SUM(IF(a.jenis_transaksi = 'pembelian' OR a.jenis_transaksi = 'retur_penjualan',a.transaksi,0)) as pengeluaran
									from
									((SELECT a.id id_barang,a.nama_barang,'pembelian' jenis_transaksi,date(c.tanggal) as tanggal,b.sub_total as transaksi,b.qty FROM 
									tbl_data_barang a
									INNER JOIN tbl_faktur_pembelian_detail_barang b ON b.id_barang = a.id
									INNER JOIN tbl_faktur_pembelian c ON c.id = b.id_faktur_pembelian
									WHERE c.`delete` = '0')
									UNION ALL
									(SELECT a.id id_barang,a.nama_barang,'retur_pembelian' jenis_transaksi,date(d.tanggal) as tanggal,(b.harga_satuan*e.qty) as transaksi,e.qty FROM 
									tbl_data_barang a
									INNER JOIN tbl_faktur_pembelian_detail_barang b ON b.id_barang = a.id
									INNER JOIN tbl_faktur_pembelian c ON c.id = b.id_faktur_pembelian
									INNER JOIN tbl_retur_pembelian d ON d.id_faktur_pembelian = c.id
									INNER JOIN tbl_retur_pembelian_detail_barang e ON e.id_retur_pembelian = d.id
									WHERE d.`delete` = '0')
									UNION ALL
									(SELECT a.id id_barang,a.nama_barang,'penjualan' jenis_transaksi,date(c.tanggal) as tanggal,b.sub_total as transaksi,b.qty FROM 
									tbl_data_barang a
									INNER JOIN tbl_transaksi_penjualan_detail_barang b ON b.id_barang = a.id
									INNER JOIN tbl_transaksi_penjualan c ON c.id = b.id_transaksi_penjualan
									WHERE c.`delete` = '0')
									UNION ALL
									(SELECT a.id id_barang,a.nama_barang,'retur_penjualan' jenis_transaksi,date(d.tanggal) as tanggal,(b.harga_satuan*e.qty) as transaksi,e.qty FROM 
									tbl_data_barang a
									INNER JOIN tbl_transaksi_penjualan_detail_barang b ON b.id_barang = a.id
									INNER JOIN tbl_transaksi_penjualan c ON c.id = b.id_transaksi_penjualan
									INNER JOIN tbl_retur_penjualan d ON d.id_transaksi_penjualan = c.id
									INNER JOIN tbl_retur_penjualan_detail_barang e ON e.id_retur_penjualan = d.id
									WHERE d.`delete` = '0'))
									AS a
									WHERE EXTRACT(YEAR FROM a.tanggal) = EXTRACT(YEAR FROM '".$request['tanggal_dari']."')
									GROUP BY tahunbulan
									ORDER BY tahunbulan ASC
								");
		$result_array = $query->result_array();
				
		$list_tahun_bulan = array();
		foreach($result_array as $row){
			if(trim($row['tahun_bulan']) != "" and !in_array($row['tahun_bulan'],$list_tahun_bulan))
			$list_tahun_bulan[] = $row['tahun_bulan'];
		}
		
		$resDataTB = "";
		foreach($list_tahun_bulan as $tahun_bulan){
			$resStr = "'".$tahun_bulan."'";
			foreach($result_array as $arRow){
				if($arRow['tahun_bulan'] == $tahun_bulan){
					$resStr .= ", ".($arRow['pemasukan']-$arRow['pengeluaran']);
				}
			}
			$resDataTB .= "[".$resStr."],";
		}
		
		$ret['chart_pendapatan_tahun_ini'] = rtrim($resDataTB,",");
		
		
		return $ret;
	}
	public function get_laporan_bagi_hasil_saham($request)
	{
		$where = "";
		if(!isset($request['bt_rentang'])){
			//$where .= " and EXTRACT(YEAR_MONTH FROM a.tanggal) = EXTRACT(YEAR_MONTH FROM NOW()) ";
			$where .= " and a.tanggal = '".date("Y-m-d")."' ";
		}
		if(isset($request['bt_rentang'])){
			$where .= " and (a.tanggal >= '".$request['tanggal_dari']."' and a.tanggal <= '".$request['tanggal_ke']."') ";
		}

		$query = $this->db->query("
				SELECT 
				SUM(nilai_pendapatan_perusahaan) as nilai_pendapatan_perusahaan,
				SUM(zakat) as zakat,
				SUM((nilai_pajak+biaya_gaji+biaya_atk+biaya_lain_lain)) as biaya_biaya,
				SUM(total_pendapatan_bersih) as total_pendapatan_bersih
				FROM `tbl_pembagian_saham` as a
				WHERE 1 ".$where."
		");
		$ret['get_laporan_pendapatan'] = $query->row_array();
		
		$query = $this->db->query("
				SELECT 
				COUNT(*) as get_laporan_pendapatan_length
				FROM `tbl_pembagian_saham` as a
				WHERE 1 ".$where."
		");
		$ret['get_laporan_pendapatan_length'] = $get_laporan_pendapatan_length = $query->row_array()['get_laporan_pendapatan_length'];
		//echo $this->db->last_query();
		//echo $query->num_rows();
/*
nomor
nama_investor
email
nilai_investasi
persentase
pendapatan_investasi
*/
		if($get_laporan_pendapatan_length>0){
			$query = $this->db->query("
					SELECT 
					*
					FROM `tbl_pembagian_saham_detail_investor` as a
					WHERE 1 ".$where."
					order by a.tanggal asc
			");
			$get_data_investor = $query->result();
			$arDataInvestor = array();
			$tNilaiInvestasi = 0;
			foreach($get_data_investor as $row){
				$tNilaiInvestasi += $row->nilai_investasi;
			}
			foreach($get_data_investor as $row){
				$sub['id'] = $row->id;
				$sub['id_investor'] = "IV".numLength($row->id_investor,4);
				$sub['nama_investor'] = $row->nama_investor;
				$sub['email'] = $row->email;
				$sub['nilai_investasi'] = format_rupiah($row->nilai_investasi);
				$sub['persentase'] = $row->persentase;
				$sub['pendapatan_investasi'] = format_rupiah($row->pendapatan_investasi);
				$sub['tanggal'] = date_format(date_create($row->tanggal),"Y-m-d");
				$arDataInvestor[] = $sub;
			}
			$ret['get_data_investor'] = $arDataInvestor;
		}
		
		return $ret;
	}
}