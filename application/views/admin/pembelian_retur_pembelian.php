<?php 
$aksesKey = $this->router->fetch_class()."/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug();
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}
	
	$no=0;
	$htm_table_retur_pembelian = "";
	foreach($get_retur_pembelian as $row){
		$htm_table_retur_pembelian.="
										<tr data-id=\"".$row->id."\">
											<th scope=\"row\">".($no+=1)."</th>
											<td>".repair_date($row->tanggal)."</td>
											<td>".$row->nomor."</td>
											<td>".$row->nama_suplier."</td>
											<td>".$row->no_faktur_pembelian."</td>
											<td>".format_rupiah($row->total)."</td>
											<td>".$row->pesan."</td>
											<td>".btnStatLabel($row->status)."</td>
											<td>";
												if(isset($aksesUbah)) $htm_table_retur_pembelian.=" <a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/edit/".$row->id."\" class=\"btn btn-outline-success btn-sm\"><i class=\"fa fa-edit\"></i>&nbsp; Edit</a> ";
												if(isset($aksesHapus)) $htm_table_retur_pembelian.=" <a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/hapus/".$row->id."\" class=\"btn btn-outline-danger btn-sm\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";
						  $htm_table_retur_pembelian.="</td>
										</tr>
									";
	}
	
	if($htm_table_retur_pembelian == ""){
		$htm_table_retur_pembelian .= "<tr><th colspan='10' class=\"text-center\">. : Data Kosong : .</th></tr>";
	}
		$htm_table_retur_pembelian .= "<tr><th colspan='10'>Menampilkan ".(($no>0)?1:0)." .. ".$no." dari ".$no." Baris</th></tr>";

	$htm_option_faktur_pembelian ="";
	foreach($get_faktur_pembelian as $row){
		$htm_option_faktur_pembelian.="<option value=\"".$row->id."\" data-faktur-pembelian='".json_encode($row)."' ".((isset($get_edit_retur_pembelian[0]->id_faktur_pembelian) and $get_edit_retur_pembelian[0]->id_faktur_pembelian == $row->id)?"selected=selected":"")." >".$row->nomor."</option>";
	}
		
	if($action == "tambah"){
		$tambahRequired = " required='required' ";
	}else if($action == "edit"){
		$editRequired = " required='required' ";
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method(); ?>">Transaksi Retur Pembelian <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-6">
		<form action="" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari" placeholder="Pencarian..." class="form-control form-control-sm" required="required" />
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari"><i class="fa fa-search"></i></button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<?php if(isset($aksesTambah)){ ?>
		<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/tambah"; ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil"></i>&nbsp; Tambah</a>
		<?php } ?>
		<button type="button" class="btn btn-outline-warning btn-sm" id="bt_print"><i class="fa fa-print"></i>&nbsp; Cetak</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="bg-info">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Tanggal</th>
					<th scope="col">Nomor</th>
					<th scope="col">Supplier</th>
					<th scope="col">No FB</th>
					<th scope="col">Total Retur</th>
					<th scope="col">Pesan</th>
					<th scope="col">Status</th>
					<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
					<th scope="col">Aksi</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php echo $htm_table_retur_pembelian; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>
<?php if($action == "tambah" or $action == "edit"){ ?>
<div class="card">
	<div class="card-body">
		<form method="post" action="" enctype="multipart/form-data" name="form_retur_pembelian" class="image-editor">
			<input type="hidden" name="form_action" value="<?php echo $action; ?>">
			<input type="hidden" name="id_retur_pembelian" value="<?php echo @$get_edit_retur_pembelian[0]->id; ?>">
			<div class="row">
				<div class="col-4">
					<div class="form-group">
						<label>No. Retur Pembelian</label>
						<input type="text" class="form-control" name="nomor" value="<?php echo (isset($get_edit_retur_pembelian[0]->nomor))?$get_edit_retur_pembelian[0]->nomor:"[Auto Generate After Submit]"; ?>" readonly="readonly">
					</div>
					<div class="form-group">
						<label>Nama Supplier</label>
						<input type="text" class="form-control" name="nama_suplier" value="<?php echo @$get_edit_retur_pembelian[0]->nama_suplier; ?>" readonly="readonly">
					</div>
					<div class="form-group">
						<label>Tanggal Faktur</label>
						<input type="date" class="form-control" name="tgl_faktur" value="<?php echo @explode(" ",@$get_edit_retur_pembelian[0]->tgl_faktur)[0]; ?>" readonly="readonly">
					</div>
					<div class="form-group">
						<label>Nomor FB</label>
						<div class="input-group">
							<input type="text" class="form-control" name="no_faktur_pembelian" placeholder="No FB" value="<?php echo @$get_edit_faktur_pembelian[0]->nomor; ?>" required="required">
							<input type="hidden" name="id_faktur_pembelian"  value="<?php echo @$get_edit_retur_pembelian[0]->id_faktur_pembelian; ?>" required="required">
							<div class="input-group-append">
								<span class="input-group-text"><i class="fa fa-search"></i></span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Gudang</label>
						<input type="text" class="form-control" name="nama_gudang" value="<?php echo @$get_edit_retur_pembelian[0]->nama_gudang; ?>" readonly="readonly">
					</div>					
				</div>
				<div class="col-4">
					<div class="form-group">
						<label>Alamat Suplier</label>
						<textarea class="form-control" name="alamat_suplier" readonly="readonly" rows="1"><?php echo @$get_edit_retur_pembelian[0]->alamat_suplier; ?></textarea>
					</div>
					<div class="form-group">
						<label>Email Suplier</label>
						<input type="email" class="form-control" name="email_suplier" value="<?php echo @$get_edit_retur_pembelian[0]->email; ?>" readonly="readonly">
					</div>
					<div class="form-group">
						<label>Jatuh Tempo</label>
						<input type="date" class="form-control" name="jatuh_tempo" value="<?php echo @$get_edit_retur_pembelian[0]->jatuh_tempo; ?>" readonly="readonly">
					</div>
					<div class="form-group">
						<label>Tanggal Retur</label>
						<input type="date" class="form-control" name="tanggal" value="<?php echo (isset($get_edit_retur_pembelian[0]->tanggal))?explode(" ",$get_edit_retur_pembelian[0]->tanggal)[0]:date('Y-m-d'); ?>" required="required">
					</div>					
					
				</div>
				<div class="col-4">
					<div class="form-group">
						<h3 class="font-weight-bold">Total Retur</h3>
						<h2 class="text-total-retur text-primary font-weight-bold">Rp. 0,-</h2>
					</div>
				</div>
			</div>
			<table class="table table-form">
				<thead class="bg-info">
					<tr>
						<th scope="col">#</th>
						<th scope="col" style="width:150px;">Nama Barang</th>
						<th scope="col">Qty Faktur</th>
						<th scope="col">Yg Bisa di Retur</th>
						<th scope="col">Qty Retur</th>
						<th scope="col">Satuan</th>
						<th scope="col">No Seri</th>
						<th scope="col">Harga Satuan</th>
						<th scope="col">Sub Total</th>
						<th scope="col">Keterangan</th>
						<th scope="col">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<tr><th colspan="12" class="text-center">. : Data Belum Tersedia : .</th></tr>
					<tr><th colspan="12">&nbsp;</th></tr>
				</tbody>
			</table>
			<div class="row">
				<div class="col-6">
					<div class="form-group">
						<label>Pesan</label>
						<textarea class="form-control" name="pesan"><?php echo @$get_edit_retur_pembelian[0]->pesan; ?></textarea>
					</div>
				</div>
				<div class="col-6">
					<div class="row">
						<div class="col-6">
							Total Faktur
							<input type="hidden" name="total_faktur">
						</div>
						<div class="col-6 text-right text-total-faktur">
							Rp. 0,-
						</div>
						<div class="col-6">
							Total Retur
							<input type="hidden" name="total_retur">
						</div>
						<div class="col-6 text-right text-total-retur">
							Rp. 0,-
						</div>
						<div class="col-6 font-weight-bold">
							Sisa Tagihan
							<input type="hidden" name="sisa_tagihan">
						</div>
						<div class="col-6 text-right font-weight-bold text-sisa-tagihan">
							Rp. 0,-
						</div>
					</div>
				</div>
			</div>
			<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method(); ?>" class="btn btn-outline-danger btn-sm btn-flat">Kembali</a>
			<button type="submit" name="submit_retur_pembelian" class="btn btn-success btn-sm btn-flat">Submit</button>
		</form>
	</div>
</div>
<?php } ?>
<div class="modal" id="modalDetailFakturPembelian" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Pilih Faktur Pembelian</span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<table class="table table-sm">
		  <thead>
			<tr>
			  <th class="border-top-0" scope="col">Nomor</th>
			  <th class="border-top-0" scope="col">Nama Suplier</th>
			  <th class="border-top-0" scope="col">Tanggal</th>
			  <th class="border-top-0" scope="col">Jatuh Tempo</th>
			  <th class="border-top-0" scope="col">Email</th>
			  <th class="border-top-0" scope="col">No SJ</th>
			  <th class="border-top-0" scope="col">Status</th>
			</tr>
		  </thead>
		  <tbody>
		  </tbody>
		</table>
      </div>
    </div>
  </div>
</div>
<div class="modal" id="modalDetailNoSeri" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Detail No Seri</span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<table class="table table-sm">
		  <thead>
			<tr>
			  <th class="border-top-0" scope="col">No Seri</th>
			  <th class="border-top-0 scope="col">Qty</th>
			  <th class="border-top-0 scope="col">&nbsp;</th>
			</tr>
		  </thead>
		  <tbody>
			&nbsp;
		  </tbody>
		</table>
      </div>
      <div class="modal-footer">
		<!--<button type="button" class="btn btn-sm btn-flat btn-success btn-add-no-seri-detail"><i class="fa fa-plus"></i></button>-->
		<button type="button" class="btn btn-sm btn-flat btn-secondary" data-dismiss="modal">Cancel</button>
		<button type="button" class="btn btn-sm btn-flat btn-primary btn-save-no-seri-detail">Save</button>
      </div>
    </div>
  </div>
</div>
<script>
	var VG_onpage_data_table = "tbl_retur_pembelian";
	var BASE_URL = "<?php echo base_url(); ?>";
	var aksesHapus = "<?php echo @$aksesHapus; ?>";
	var action = "<?php echo $action; ?>";
	var action_id = "<?php echo $action_id; ?>";
	var btnDetailNoSeri = null;
	function grandTotal(){
		var form = $j("form[name='form_retur_pembelian']");
		var total_faktur = 0;
		var total_retur = 0;
		form.find("table.table-form tbody").find("tr").each(function(){
			let row = $j(this);
			let harga_satuan = row.find("input#harga_satuan").val();
			let qty_faktur = row.find("input#qty_faktur").val();
			let qty_retur = row.find("input#qty_retur").val();
				if(qty_retur == "") qty_retur = 0;
			total_faktur += (parseInt(harga_satuan) * parseInt(qty_faktur));
			total_retur += (parseInt(harga_satuan) * parseInt(qty_retur));
		});
		var sisa_tagihan = total_faktur - total_retur;
		
		form.find("input[name='total_faktur']").val(total_faktur);
		form.find(".text-total-faktur").html(formatRupiah(total_faktur));
		form.find("input[name='total_retur']").val(total_retur);
		form.find(".text-total-retur").html(formatRupiah(total_retur));
		form.find("input[name='sisa_tagihan']").val(sisa_tagihan);
		form.find(".text-sisa-tagihan").html(formatRupiah(sisa_tagihan));
	}
	function setDataDetailBrangFakturPembeliantoTable(dataJSON){
		var htm_table_form_detail_faktur_pembelian = "";
		var get_detail_barang_faktur_pembelian = JSON.parse(dataJSON);
		var no=0;
		for(var x in get_detail_barang_faktur_pembelian){
			no++;
			var row = get_detail_barang_faktur_pembelian[x];
			var qty_retur = 0;
			var no_seri_retur = "";
			var keterangan = "";
			if(typeof(row['retur_qty']) !== "undefined" && row['retur_qty']!=null) var qty_retur = row['retur_qty'];
			if(typeof(row['retur_no_seri']) !== "undefined" && row['retur_no_seri']!=null) var no_seri_retur = row['retur_no_seri'];
			if(typeof(row['retur_keterangan']) !== "undefined" && row['retur_keterangan']!=null) var keterangan = row['retur_keterangan'];
			htm_table_form_detail_faktur_pembelian +=" <tr>"+
															"<input type=\"hidden\" name=\"id_faktur_pembelian_detail_barang[]\" id=\"id_faktur_pembelian_detail_barang\" value=\""+row['id']+"\" />"+
															"<input type=\"hidden\" name=\"id_barang[]\" id=\"id_barang\" value=\""+row['id_barang']+"\" />"+
															"<input type=\"hidden\" name=\"no[]\" id=\"no\" value=\""+no+"\" disabled='disabled' />"+
															"<th scope=\"row\">"+no+"</th>"+
															"<td><input type=\"text\" name=\"nama_barang[]\" id=\"nama_barang\" placeholder=\"Nama Barang\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['nama_barang']+"\" /></td>"+
															"<td><input type=\"number\" name=\"qty_faktur[]\" id=\"qty_faktur\" placeholder=\"Qty Faktur\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['qty']+"\" /></td>"+
															"<td><input type=\"number\" name=\"qty_bisa_retur[]\" id=\"qty_bisa_retur\" placeholder=\"Qty Bisa Retur\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+((parseInt(row['qty'])-parseInt(row['qty_cetak_retur']))+parseInt(qty_retur))+"\" /></td>"+
															"<td><input type=\"number\" name=\"qty_retur[]\" id=\"qty_retur\" placeholder=\"Qty Retur\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+qty_retur+"\" /></td>"+
															"<td><input type=\"text\" name=\"satuan[]\" id=\"satuan\" placeholder=\"Satuan\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['nama_satuan']+"\" /></td>"+
															"<td><button type=\"button\" class=\"btn btn-outline-secondary btn-sm btn-flat btnDetailNoSeri\" data-toggle=\"modal\" >Detail</button>"+
															"<input type=\"hidden\" name=\"detail_no_seri[]\" id=\"detail_no_seri\" data-no-seri='"+row['no_seri']+"' data-no-seri-cetak-retur='"+row['no_seri_cetak_retur']+"' value='"+no_seri_retur+"' /></td>"+
															"<td><input type=\"number\" name=\"harga_satuan[]\" id=\"harga_satuan\" placeholder=\"Harga Satuan\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['harga_satuan']+"\" /></td>"+
															"<td><input type=\"number\" name=\"sub_total[]\" id=\"sub_total\" placeholder=\"Sub Total\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['sub_total']+"\" /></td>"+
															"<td><input type=\"text\" name=\"keterangan[]\" id=\"keterangan\" placeholder=\"Keterangan\" class=\"form-control form-control-sm\" value=\""+keterangan+"\" /></td>"+
															"<td><button type=\"button\" class=\"btn btn-sm btn-danger\" onclick=\"deleteRow(this);\">Hapus</button></td>"+
														"</tr>";
		}
		$j(".table-form tbody").html(htm_table_form_detail_faktur_pembelian);
		grandTotal();
	}
	function loadSelectIdFakturPembelian(data_faktur){
		/*
		var selected = obj.find(":selected");
		var data_faktur = selected.data('faktur-pembelian');
		*/
		var id_faktur_pembelian = data_faktur['id'];
		var tglFaktur = (data_faktur['tanggal']).toString().split(" ");
		$j("form[name='form_retur_pembelian']").find("input[name='id_faktur_pembelian']").val(data_faktur['id']);
		$j("form[name='form_retur_pembelian']").find("input[name='no_faktur_pembelian']").val(data_faktur['nomor']);
		$j("form[name='form_retur_pembelian']").find("input[name='tgl_faktur']").val(tglFaktur[0]);
		$j("form[name='form_retur_pembelian']").find("input[name='jatuh_tempo']").val(data_faktur['jatuh_tempo']);
		$j("form[name='form_retur_pembelian']").find("input[name='nama_suplier']").val(data_faktur['nama_suplier']);
		$j("form[name='form_retur_pembelian']").find("input[name='email_suplier']").val(data_faktur['email']);
		$j("form[name='form_retur_pembelian']").find("input[name='nama_gudang']").val(data_faktur['nama_gudang']);
		$j("form[name='form_retur_pembelian']").find("textarea[name='alamat_suplier']").val(data_faktur['alamat_suplier']);
		
		var act = "";
		if(action == "tambah")
			act = "get_faktur_pembelian_detail_barang";
		else if(action == "edit")
			act = "get_retur_pembelian_detail_barang";
		$j.ajax({
			type:"POST",
			url:"<?php echo base_url()."admin/apiweb"; ?>",
			data:{action:act,id_faktur_pembelian:id_faktur_pembelian,id_retur_pembelian:"<?php echo @$get_edit_retur_pembelian[0]->id; ?>"},
			success:function(dataJSON){
				setDataDetailBrangFakturPembeliantoTable(dataJSON);
			}
		});
	}
	function deleteRow(obj){
		$j(obj).parents('tr').hide();
		$j(obj).parents('tr').find("input[name='qty']").val('0');
	}
	$j(document).on("click","#bt_print",function(){
		printTableData();
	});
	$j(".standardSelect").chosen({
		disable_search_threshold: 10,
		no_results_text: "Oops, nothing found!",
		width: "100%"
	});
	$j("select[name='id_faktur_pembelian']").change(function(){
		loadSelectIdFakturPembelian($j(this));
	});
	$j("form[name='form_retur_pembelian']").on("focus keyup change blur ","input[name='qty_retur[]']",function(){
		var row = $j(this).parents("tr");
		var thisVal = parseInt($j(this).val());
		var qty_bisa_retur = parseInt(row.find("input[name='qty_bisa_retur[]']").val());
		if(thisVal > qty_bisa_retur){
			alert("Qty Retur tidak bisa melebihi qty Faktur");
			$j(this).val(qty_bisa_retur);
		}
		grandTotal();
	});
	$j("form[name='form_retur_pembelian']").submit(function(er){
		var ret = true;
		var id_faktur_pembelian = $j(this).find("input[name='id_faktur_pembelian']").val();
		if(id_faktur_pembelian == ""){
			alert("Kolom No Faktur belum di isi!");
			ret = false;
		}
		var cek_no_seri = false;
		$j(this).find("input[name='detail_no_seri[]']").each(function(){
			var val = $j(this).val();
			if(val != ""){
				cek_no_seri = true;
			}
		});
		if(cek_no_seri == false){
			alert("Anda belum input no seri yang akan di retur, minimal 1 no seri!");
			ret = false;
		}
		return ret;
	});
	$j("form[name='form_retur_pembelian']").on("keypress","input[name='no_faktur_pembelian']",function(event){
		var value = $j(this).val();
		var keycode = (event.keyCode ? event.keyCode : event.which);
		var ret = true;
		if(keycode == '13'){
			$j.ajax({
				type:"POST",
				url:"<?php echo base_url()."admin/apiweb"; ?>",
				data:{action:"get_faktur_pembelian",bt_cari:1,tx_cari:value,bisa_retur:1},
				dataType:"json",
				success: function(obj){
					var htmTblData = "";
					for(x in obj){
						var row = obj[x];
						htmTblData += "<tr>"+
										"<td><button type=\"button\" class=\"btn btn-link btnSetNoFB\" data-data-faktur='"+JSON.stringify(row)+"' >"+row['nomor']+"</button></td>"+
										"<td>"+row['nama_suplier']+"</td>"+
										"<td>"+repair_date(row['tanggal'])+"</td>"+
										"<td>"+repair_date(row['jatuh_tempo'])+"</td>"+
										"<td>"+row['email']+"</td>"+
										"<td>"+row['no_sj_suplier']+"</td>"+
										"<td>"+row['status_faktur']+"</td>"+
									"</tr>";
					}
					$j('.modal#modalDetailFakturPembelian').find("table tbody").html(htmTblData);
					$j('.modal#modalDetailFakturPembelian').modal('show');
				}
			});
			ret = false;
		}
		return ret;
	});
	$j('.modal#modalDetailFakturPembelian').on('hidden.bs.modal', function (e){
		$j('.modal#modalDetailFakturPembelian').find("table tbody").html("");
	})
	$j(".modal#modalDetailFakturPembelian").on("click","button.btnSetNoFB",function(){
		var df = $j(this).data('data-faktur');
		loadSelectIdFakturPembelian(df);
		$j('.modal#modalDetailFakturPembelian').modal('hide');
	})
	$j(".table-form").on("click",".btnDetailNoSeri",function(){
		btnDetailNoSeri = $j(this);
		var dataRow = btnDetailNoSeri.parents("tr");
		var id_barang = dataRow.find("select#id_barang").val();
		if(id_barang != ""){
			var detail_no_seri = dataRow.find("input#detail_no_seri").data("no-seri");
			var detail_no_seri_cetak_retur = dataRow.find("input#detail_no_seri").data("no-seri-cetak-retur");
			var detail_no_seri_value = dataRow.find("input#detail_no_seri").val();
			if(detail_no_seri != ""){
				$j('#modalDetailNoSeri').modal('show');
				var obj = detail_no_seri;
				var objNoSeri = obj['no_seri'];
				var objQty = obj['qty'];
				
				if(detail_no_seri_cetak_retur != "" && action == "tambah"){
					var objCtr = detail_no_seri_cetak_retur;
					var objCtrNoSeri = objCtr['no_seri'];
					var objCtrQty = objCtr['qty'];
				}
				
				if(detail_no_seri_value != ""){
					var objVal = JSON.parse(detail_no_seri_value);
					var objValNoSeri = objVal['no_seri'];
					var objValQty = objVal['qty'];
				}
				var rowData = "";
				for(x in objNoSeri){
					var style = "";
					if(typeof(objValQty) != "undefined" && typeof(objValQty[x]) != "undefined" && objValQty[x] == "0"){
						var style = "display: none;";
					}
					rowData += "<tr style='"+style+"'>"+
								  "<td>"+
									"<input type=\"text\" class=\"form-control form-control-sm\" name=\"no_seri\" type=\"text\" placeholder=\"No Seri\" value=\""+objNoSeri[x]+"\" readonly=\"readonly\" >"+
								  "</td>"+
								  "<td>"+
									"<input type=\"number\" class=\"form-control form-control-sm\" name=\"qty\" type=\"text\" placeholder=\"Qty\" ";

										if(typeof(objCtrQty) != "undefined" && typeof(objCtrQty[x]) != "undefined"){
											rowData += " data-max-retur=\""+(parseInt(objQty[x])-parseInt(objCtrQty[x]))+"\" ";
										}else{
											rowData += " data-max-retur=\""+(parseInt(objQty[x]))+"\" ";
										}

										if(typeof(objValQty) != "undefined" && typeof(objValQty[x]) != "undefined"){
											rowData += " value=\""+objValQty[x]+"\" ";
										}else{
											rowData += " value=\"0\" ";
										}
											
						rowData +="</td>"+
								  "<td>"+
									"<button type=\"button\" class=\"btn btn-sm btn-flat btn-danger btn-del-no-seri-detail\"><i class=\"fa fa-minus\"></i></button>"+
								  "</td>"+
								"</tr>";
				}
				$j(".modal#modalDetailNoSeri").find("tbody").html(rowData);
			}else{
				alert("Barang ini tidak memiliki No Seri!");
			}
		}else{
			alert("Barang harus dipilih terlebih dahulu!");
		}
	});
	$j(".modal#modalDetailNoSeri").on("click",".btn-add-no-seri-detail",function(){
		var rowData = " <tr>"+
						  "<td>"+
							"<input class=\"form-control form-control-sm\" name=\"no_seri\" type=\"text\" placeholder=\"No Seri\">"+
						  "</td>"+
						  "<td>"+
							"<input class=\"form-control form-control-sm\" name=\"qty\" type=\"text\" placeholder=\"Qty\">"+
						  "</td>"+
						  "<td>"+
							"<button type=\"button\" class=\"btn btn-sm btn-flat btn-danger btn-del-no-seri-detail\"><i class=\"fa fa-minus\"></i></button>"+
						  "</td>"+
						"</tr>";
		$j(".modal#modalDetailNoSeri").find("tbody").append(rowData);
	});
	$j(".modal#modalDetailNoSeri").on("click",".btn-del-no-seri-detail",function(){
		//$j(this).parents("tr").remove();
		deleteRow($j(this));
		grandTotal();
	});
	$j(".modal#modalDetailNoSeri").on("click",".btn-save-no-seri-detail",function(){
		var modal = $j(".modal#modalDetailNoSeri");
		var tbody = modal.find("tbody>tr");
		var data = {};
		var dNoSeri = {};
		var dQty = {};
		var tQty = 0;
		var n = 0;
		var validate = true;
		tbody.each(function(){
			var no_seri = $j(this).find("input[name='no_seri']").val();
			var qty     = $j(this).find("input[name='qty']").val();
			if(no_seri != "" && parseInt(qty) > 0){
				if(no_seri != "" && qty != ""){
					dNoSeri[n] = no_seri;
					dQty[n] = qty;
					data['no_seri'] = dNoSeri;
					data['qty'] = dQty;
					tQty += parseInt(qty);
					n++;
				}
			}else{
				validate = false;
			}
		});
		if(validate){
			var xParent = btnDetailNoSeri.parents("tr");
			xParent.find("input#detail_no_seri").val(JSON.stringify(data));
			xParent.find("input#qty_retur").val(tQty);
			grandTotal();
			$j('#modalDetailNoSeri').modal('hide');
		}else{
			alert("Minimal input 1 qty retur!");
		}
	});
	$j('.modal#modalDetailNoSeri').on('hidden.bs.modal', function (e) {
		btnDetailNoSeri = null;
		$j(".modal#modalDetailNoSeri").find("tbody").html("");
	})
	$j(".modal#modalDetailNoSeri").on("focus keyup change blur ","input[name='qty']",function(){
		var val = parseInt($j(this).val());
		var max_retur = parseInt($j(this).data("max-retur"));
		if(val > max_retur){
			$j(this).val(max_retur);
			alert("max retur qty no seri ini: "+max_retur);
		}
	});
	$j(document).ready(function(){
		if(action == "edit"){
			loadSelectIdFakturPembelian(<?php echo json_encode(@$get_edit_faktur_pembelian[0]);?>);
		}else if(action == "tambah" && action_id != ""){
			loadSelectIdFakturPembelian(<?php echo json_encode(@$get_edit_faktur_pembelian[0]);?>);
		}
	});
</script>
<?php } ?>