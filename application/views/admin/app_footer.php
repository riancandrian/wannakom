                    </div><!-- /# column -->
                </div>
                <!--  /Dashboard -->
                <div class="clearfix"></div>
            </div>
            <!-- .animated -->
        </div>
        <!-- /.content -->
        <div class="clearfix"></div>
        <!-- Footer -->
        <footer class="site-footer">
            <div class="footer-inner bg-white">
                <div class="row">
                    <div class="col-sm-6">
                        Copyright &copy; 2019
                    </div>
                    <div class="col-sm-6 text-right">
                        Designed by <a href="http://www.wannakomunika.com" target="_blank">Wanna Komunika</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- /.site-footer -->
    </div>
    <!-- /#right-panel -->

    <!--Local Stuff-->
    <script>
		$j(document).ready(function($) {
			$j("table.table-data").on("click",".btn-update-status",function(){
				var data_id = $j(this).parents("tr").data("id");
				var data_status = $j(this).data("status");
				var c = confirm('Anda akan update status data ini?');
				var data_table = VG_onpage_data_table;
				var data_update = {status:data_status};
				var data_where = {id:data_id};
				if(c){
					$j.ajax({
						type:"POST",
						url:"<?php echo base_url()."admin/apiweb"; ?>",
						data:{action:"update_table",data_table:data_table,data_update:data_update,data_where:data_where},
						success: function(res){
							window.location.reload();
						}
					});
				}
			});
        });
    </script>
</body>
</html>
