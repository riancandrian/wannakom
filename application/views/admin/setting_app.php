<?php
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}
?>
<?php echo $notif_message; ?>
<form method="post" action="" name="form_setting_app" enctype="multipart/form-data">
<input type="hidden" name="form_action" value="<?php echo $action; ?>">
<input type="hidden" name="id_setting_app" value="<?php echo @$get_edit_setting_app[0]->id; ?>">
<div class="row">
	<div class="col-6">
		<div class="card">
			<div class="card-header p-2">
				<strong class="card-title">Setting Pembagian Hasil</strong>
			</div>
			<div class="card-body p-2">
				<div class="row">
					<div class="col-6">
						Perusahaan
					</div>
					<div class="col-6">
						Investor
					</div>
					<div class="w-100"></div>
					<div class="col-6 form-group">
						<input type="number" class="form-control" name="bagi_hasil_perusahaan" value="<?php echo @$get_edit_setting_app[0]->bagi_hasil_perusahaan; ?>" required="required">
					</div>
					<div class="col-6 form-group">
						<input type="number" class="form-control" name="bagi_hasil_investor" value="<?php echo @$get_edit_setting_app[0]->bagi_hasil_investor; ?>" required="required">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-6">
		<div class="card">
			<div class="card-header p-2">
				<strong class="card-title">Setting Pajak</strong>
			</div>
			<div class="card-body p-2">
				<div class="row">
					<div class="col-12">
						Pajak
					</div>
					<div class="w-100"></div>
					<div class="col-12 form-group input-group">
						<input type="number" class="form-control" name="pajak" value="<?php echo @$get_edit_setting_app[0]->pajak; ?>" required="required">
						<div class="input-group-append">
							<span class="input-group-text">%</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body p-2">
				<div class="row">
					<div class="col-12 text-right">
						<button type="submit" name="submit_setting_app" class="btn btn-primary btn-flat">Submit</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
<script>
$j(document).on("focus keyup change blur","input[name='bagi_hasil_perusahaan'], input[name='bagi_hasil_investor']",function(){
	var name = $j(this).attr("name");
	var value = parseInt($j(this).val());
	if(name == "bagi_hasil_perusahaan"){
		$j("input[name='bagi_hasil_investor']").val(100-value);
	}else{
		$j("input[name='bagi_hasil_perusahaan']").val(100-value);
	}
});
</script>