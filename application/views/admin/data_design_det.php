<div class="row">
  <div class="col-6">
    <div class="card">
      <div class="card-body">
        <form class="image-editor">
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <label>Logo</label>
              </div>

              <div class="form-group">
                <img src="<?=base_url('assets/upload_design/original/').$data['gambar1'];?>" alt="logo">
              </div>
            </div>

            <div class="col-6">
              <div class="form-group">
                <label>Gambar</label>
              </div>

              <div class="form-group">
                <img src="<?=base_url('assets/upload_design/original/').$data['gambar2'];?>" alt="gambar">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-3">
              <div class="form-group">
                <label>Warna 1</label>
              </div>

              <div class="form-group">
                <label>Gradient</label>
              </div>
            </div>

            <div class="col-1">
              <div class="form-group">
                <label>:</label>
              </div>

              <div class="form-group">
                <label>:</label>
              </div>
            </div>

            <div class="col-4">
              <div class="form-group">
                <input type="color" value="<?=$data['warna1']?>" class="form-control" disabled>
              </div>

              <div class="form-group">
                <input type="color" value="<?=$data['warna2']?>" class="form-control" disabled>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="col-6">
    <div class="card">
      <div class="card-body">
        <form class="image-editor" action="<?=base_url('design/simpan_det')?>" method="post" enctype="multipart/form-data">
          <div class="row">
            <div class="col-8">
              <div class="row" id="design_det">
                <?php foreach ($detail as $x): ?>
                  <div class="col-4">
                    <img src="<?=base_url('assets/upload_design/detail/original/').$x->gambar;?>" alt="logo">
                  </div>
                <?php endforeach; ?>
              </div>
            </div>

            <div class="col-4">
              <div class="form-group">
                <input type="hidden" name="idparent" value="<?=$parent?>">
                <input type="file" class="form-control input-sm" name="filenya" value="">
              </div>

              <div class="form-group">
                <?php if (!$data['status'] == '1'): ?>
                  <button type="submit" class="btn btn-sm btn-outline-secondary btn-block" name="button">Simpan Design</button>
                <?php endif; ?>
              </div>

              <div class="form-group">
                <?php if (!$data['status'] == '1'): ?>
                  <button type="button" class="btn btn-sm btn-outline-success btn-block" onclick="approve()">Approve Design</button>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script type="text/javascript">
  function approve(){
    var id_design = $('input[name="idparent"]').val();

    $.ajax({
      url : "<?=base_url()?>" + "design/approve",
      data: {id_design: id_design},
      type: 'POST',
      success: function(result){
        var jsonData = JSON.parse(result);

        if(jsonData.success){
          location.href = "<?=base_url('admin/data_design')?>";
        }
      }
    })
  }
</script>
