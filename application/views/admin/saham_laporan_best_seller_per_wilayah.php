<?php 
$aksesKey = $this->router->fetch_class()."/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug();
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}
	
	$no=0;
	$htm_table_transaksi_barang_customer_paling_tinggi = "";
	foreach($get_laporan_cepat_wilayah_penjualan['transaksi_barang_customer_paling_tinggi'] as $row){
		$htm_table_transaksi_barang_customer_paling_tinggi.="
										<tr>
											<td scope=\"row\">".($no+=1)."</td>
											<td>".repair_date($row['tanggal'])."</td>
											<td>".$row['nomor']."</td>
											<td>".$row['qty']."</td>
											<td>".format_rupiah($row['transaksi'])."</td>
										</tr>
									";
	}
	if($htm_table_transaksi_barang_customer_paling_tinggi == ""){
		$htm_table_transaksi_barang_customer_paling_tinggi .= "<tr><th colspan='20' class=\"text-center\">. : Data Kosong : .</th></tr>";
	}
		$htm_table_transaksi_barang_customer_paling_tinggi .= "<tr><th colspan='20'>Menampilkan ".(($no>0)?1:0)." .. ".$no." dari ".$no." Baris</th></tr>";
	
	if($action == "tambah"){
		$tambahRequired = " required='required' ";
	}else if($action == "edit"){
		$editRequired = " required='required' ";
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method(); ?>">Laporan Best Seller per Wilayah <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-6">
		<div class="card">
			<div class="card-body p-2">
				<div class="row align-items-center">
					<div class="col-3 text-center">
						<i class="fa fa-check-square fa-5x"></i>
					</div>
					<div class="col-9">
						<div style="font-size:10px;">Wilayah Penjualan Tertinggi</div>
						<div class="font-weight-bold" style="font-size:25px;"><?php echo @$get_laporan_cepat_wilayah_penjualan['penjualan_tertinggi']['wilayah']; ?></div>
						<div style="font-size:10px;">Total Penjualan</div>
						<div class="font-weight-bold" style="font-size:25px;"><?php echo format_rupiah(@$get_laporan_cepat_wilayah_penjualan['penjualan_tertinggi']['penjualan']); ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-6">
		<div class="card">
			<div class="card-body p-2">
				<div class="row align-items-center">
					<div class="col-3 text-center">
						<i class="fa fa-minus-square fa-5x"></i>
					</div>
					<div class="col-9">
						<div style="font-size:10px;">Wilayah Penjualan Terendah</div>
						<div class="font-weight-bold" style="font-size:25px;"><?php echo @$get_laporan_cepat_wilayah_penjualan['penjualan_terendah']['wilayah']; ?></div>
						<div style="font-size:10px;">Total Penjualan</div>
						<div class="font-weight-bold" style="font-size:25px;"><?php echo format_rupiah(@$get_laporan_cepat_wilayah_penjualan['penjualan_terendah']['penjualan']); ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="mb-3 font-weight-bold">Penjualan per Wilayah</h4>
				<div id="curve_chart" style="width:100%;height:400px;"></div>
			</div>
		</div>
	</div>
</div>
<div class="row" id="info_customer_tertinggi">
	<div class="col-6">
		<form action="#info_customer_tertinggi" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari" placeholder="Cari Customer..." class="form-control form-control-sm" required="required" />
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari_customer_tertinggi"><i class="fa fa-search"></i></button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<button type="button" class="btn btn-outline-warning btn-sm btn-print" ><i class="fa fa-print"></i>&nbsp; Cetak</button>
	</div>
</div>
<div class="row">
	<div class="col-6">
		<div class="card">
			<div class="card-body">
				<div class="row pb-3 border-bottom border-dark">
					<div class="col font-weight-bold" style="font-size:17px;">
						<i class="fa fa-dropbox fa-lg"></i>&nbsp;&nbsp;
						Informasi Customer Paling Tinggi
					</div>
				</div>
				<div class="row py-1 border-bottom border-dark">
					<div class="col-4">
						Customer
					</div>
					<div class="col-8">
						: <?php echo @$get_laporan_cepat_wilayah_penjualan['pembelian_customer_paling_tinggi']['nama_customer']; ?>
					</div>
				</div>
				<div class="row py-1 border-bottom border-dark">
					<div class="col-4">
						Wilayah
					</div>
					<div class="col-8">
						: <?php echo @$get_laporan_cepat_wilayah_penjualan['pembelian_customer_paling_tinggi']['wilayah']; ?>
					</div>
				</div>
				<div class="row py-1 border-bottom border-dark">
					<div class="col-4">
						Penjualan
					</div>
					<div class="col-8">
						: <?php echo format_rupiah(@$get_laporan_cepat_wilayah_penjualan['pembelian_customer_paling_tinggi']['penjualan']); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-6">
		<div class="card">
			<div class="card-body">
				<table class="table table-data">
					<thead class="bg-info">
						<tr>
							<th scope="col">#</th>
							<th scope="col">Tanggal</th>
							<th scope="col">Transaksi</th>
							<th scope="col">Qty</th>
							<th scope="col">Total</th>
						</tr>
					</thead>
					<tbody>
						<?php echo @$htm_table_transaksi_barang_customer_paling_tinggi; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['line']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
	var data = google.visualization.arrayToDataTable([
	  [<?php echo "'Bulan', '".@$get_laporan_cepat_wilayah_penjualan['chart']['wilayah']."'"; ?>],
	  <?php echo @$get_laporan_cepat_wilayah_penjualan['chart']['tahun_bulan_penjualan']; ?>
	]);

	var options = {
	  //title: 'Penjualan per Wilayah',
	  curveType: 'function',
	  legend: { position: 'bottom' }
	};

	var chart = new google.charts.Line(document.getElementById('curve_chart'));

	chart.draw(data, options);
  }
</script>
<script>
	var VG_onpage_data_table = "tbl_transaksi_penjualan";
	var BASE_URL = "<?php echo base_url(); ?>";
	var aksesHapus = "<?php echo @$aksesHapus; ?>";
	var action = "<?php echo $action; ?>";
	var btnDetailNoSeri = null;
	var no=0;
	$j(document).on("click",".btn-print",function(){
		var data = $j(this).parents("div.col-lg-12").html();
		printScreen("<div class='row'><div class='col-lg-12'>"+data+"</div></div>");
	});
	$j("button.btn-export").click(function(){
		var data_type = $j(this).data("type");
		var dtH = {};
		var i = 0;
		$j("table.table-data").find("thead tr th").each(function(){
			var dt = $j(this).html();
			dtH[i] = dt;
			i++;
		});
		
		var dtB = {};
		var i = 0;
		$j("table.table-data").find("tbody tr").each(function(){
			var dtR = {};
			var ir = 0;
			$j(this).find("td").each(function(){
				var dt = $j(this).html();
				if(String(dt).trim() != ""){
					dtR[ir] = dt;
					ir++;		
				}				
			});
			dtB[i] = dtR;
			i++;
		});
		var dataFD = {'header':dtH,'body':dtB};
		var encData = btoa(JSON.stringify(dataFD));
		if(data_type == "to_excel"){
			var export_type = "file_excel";
		}else if(data_type == "to_pdf"){
			var export_type = "file_pdf";
		}
		$j.ajax({
			type:"POST",
			url:"<?php echo base_url()."admin/apiweb"; ?>",
			data:{action:"post_download_data",type:export_type,data:encData},
			dataType:"json",
			success: function(obj){
				if(obj == true){
					window.open("<?php echo base_url()."download/"; ?>"+export_type,"_blank");
				}else{
					alert("error download, code: "+obj);
				}
			}
		});
	});
	$j(document).ready(function(){
		$j('table#transaksi_barang').DataTable({
			"pageLength": 100
		});
	});
</script>
<?php } ?>