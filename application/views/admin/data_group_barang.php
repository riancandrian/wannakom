<?php 
$aksesKey = "admin/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug();
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}

	$no=0;
	$htm_table_group_barang = "";
	foreach($get_group_barang as $row){
		$htm_table_group_barang.="
						<tr data-id=\"".$row->id."\">
							<th scope=\"row\">".($no+=1)."</th>
							<td>".$row->kode_group."</td>
							<td>".$row->nama_group."</td>
							<td>".btnStatLabel($row->status)."</td>
							<td>";
								// if(isset($aksesUbah)) $htm_table_group_barang.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/setting/".$row->id."\" class=\"btn btn-outline-info btn-sm\"><i class=\"fa fa-gear\"></i>&nbsp; Setting</a> ";
								if(isset($aksesUbah)) $htm_table_group_barang.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/edit/".$row->id."\" class=\"btn btn-outline-success btn-sm\"><i class=\"fa fa-edit\"></i>&nbsp; Edit</a> ";
								if(isset($aksesHapus)) $htm_table_group_barang.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/hapus/".$row->id."\" class=\"btn btn-outline-danger btn-sm\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";
		  $htm_table_group_barang.="</td>
						</tr>
					";
	}
	if($htm_table_group_barang == ""){
		$htm_table_group_barang .= "<tr><th colspan='7' class=\"text-center\">. : Data Kosong : .</th></tr>";
		$htm_table_group_barang .= "<tr><th colspan='7' class=\"text-center\">&nbsp;</th></tr>";
	}
	
	if($action == "tambah"){
		$passRequired = " required='required' ";
	}else if($action == "edit"){
		$hintPassword = "<span class=\"help-block text-warning\">Kosongkan kolom Password jika tidak akan mengganti Password.</span>";
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>">Group Barang <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-6">
		<form action="" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari" placeholder="Cari Group Barang" class="form-control form-control-sm" required="required" />
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<?php if(isset($aksesTambah)){ ?>
		<a href="<?php echo base_url()."admin/".$this->router->fetch_method()."/tambah"; ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil"></i>&nbsp; Tambah</a>
		<?php } ?>
		<button type="button" class="btn btn-outline-warning btn-sm" id="bt_print"><i class="fa fa-print"></i>&nbsp; Cetak</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="thead-dark">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Kode Group</th>
					<th scope="col">Nama Group</th>
					<th scope="col">Status</th>
					<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
					<th scope="col">Aksi</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php echo $htm_table_group_barang; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>
<?php if($action == "tambah" or $action == "edit"){ ?>
<div class="card">
	<div class="card-body">
		<form name="form_group_barang" method="post" action="">
			<input type="hidden" name="form_action" value="<?php echo $action; ?>">
			<input type="hidden" name="id_group_barang" value="<?php echo @$get_edit_group_barang[0]->id; ?>">
			<div class="form-group">
				<label>Kode Group</label>
				<input type="text" class="form-control" name="kode_group" value="<?php echo @$get_edit_group_barang[0]->kode_group; ?>" required="required">
				<span class="help-block color-red"></span>
			</div>
			<div class="form-group">
				<label>Nama Group</label>
				<input type="text" class="form-control" name="nama_group" value="<?php echo @$get_edit_group_barang[0]->nama_group; ?>" required="required">
			</div>
			<div class="form-group">
				<label>Status</label>
				<select class="form-control" name="status" required="required">
					<option value='1' <?php echo(isset($get_edit_group_barang[0]->status) and $get_edit_group_barang[0]->status == "1")?"selected=selected":""; ?>><?php echo statLabel("1"); ?></option>
					<option value='0' <?php echo(isset($get_edit_group_barang[0]->status) and $get_edit_group_barang[0]->status == "0")?"selected=selected":""; ?>><?php echo statLabel("0"); ?></option>
				</select>
			</div>
			<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>" class="btn btn-secondaray btn-flat">Back</a>
			<button type="submit" name="submit_group_barang" class="btn btn-primary btn-flat">Submit</button>
		</form>
	</div>
</div>
<?php } ?>
<?php if($action == "setting"){ ?>
<div class="card">
	<div class="card-body">
		<form method="post" action="">
			<input type="hidden" name="form_action" value="<?php echo $action; ?>">
			<input type="hidden" name="id_group_barang" value="<?php echo @$get_edit_group_barang[0]->id; ?>">
			<div class="row">
				<div class="col-4">
					<div class="form-check">
						<div class="checkbox">
							<label for="stat_kode_group" class="form-check-label ">
								<input type="checkbox" id="stat_kode_group" name="stat_kode_group" value="1" class="form-check-input" <?php echo (isset($get_edit_group_barang[0]->stat_kode_group) and $get_edit_group_barang[0]->stat_kode_group == '1')?'checked=checked':'';?> /> Kode Group
							</label>
						</div>
						<div class="checkbox">
							<label for="stat_nama" class="form-check-label ">
								<input type="checkbox" id="stat_nama" name="stat_nama" value="1" class="form-check-input" <?php echo (isset($get_edit_group_barang[0]->stat_nama) and $get_edit_group_barang[0]->stat_nama == '1')?'checked=checked':'';?> /> Nama
							</label>
						</div>
						<div class="checkbox">
							<label for="stat_no_urut" class="form-check-label ">
								<input type="checkbox" id="stat_no_urut" name="stat_no_urut" value="1" class="form-check-input" <?php echo (isset($get_edit_group_barang[0]->stat_no_urut) and $get_edit_group_barang[0]->stat_no_urut == '1')?'checked=checked':'';?> /> Nomor Urut
							</label>
						</div>
					</div>
				</div>
				<div class="col-8">
					<div class="form-group">
						<label>Pemisah</label>
						<input type="text" class="form-control d-inline w-auto" name="pemisah" value="<?php echo @$get_edit_group_barang[0]->pemisah; ?>">
						<label>Default (tanpa spasi)</label>
					</div>
					<div class="form-group">
						<label>Panjang No Urut</label>
						<input type="text" class="form-control d-inline w-auto" name="panjang_no_urut" value="<?php echo @$get_edit_group_barang[0]->panjang_no_urut; ?>">
						<label>Digit</label>
					</div>
				</div>
			</div>
			<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>" class="btn btn-secondaray btn-flat">Back</a>
			<button type="submit" name="submit_group_barang" class="btn btn-primary btn-flat">Submit</button>
		</form>
	</div>
</div>
<?php } ?>
<script>
	var action = "<?php echo $action; ?>";
	var VG_onpage_data_table = "group_barang";
	var submit_form_group_barang = true;
	var msg_alert = "";
	$j(document).on("click","#bt_print",function(){
		printTableData();
	});
	
	$j("form[name='form_group_barang']").on("keyup","input[name='kode_group']",function(){
		var kode_group_def = "<?php echo @$get_edit_group_barang[0]->kode_group; ?>";
		var obj_kode_group = $j(this);
		var kode_group = obj_kode_group.val();
		var data_table = VG_onpage_data_table;
		var data_where = {kode_group:kode_group,delete:0};
		if(action == "edit") data_where['kode_group !='] = kode_group_def;
		$j.ajax({
			type:"POST",
			url:"<?php echo base_url()."admin/apiweb"; ?>",
			data:{action:"if_exist_data_table",data_table:data_table,data_where:data_where},
			success: function(res){
				if(res > 0){
					submit_form_group_barang = false;
					msg_alert = "Kode Group sudah ada!";
					obj_kode_group.next().html(msg_alert);
				}else{
					submit_form_group_barang = true;
					obj_kode_group.next().html("");
				}
			}
		});
	});
	
	$j(document).on("submit","form[name='form_group_barang']",function(){
		if(submit_form_group_barang == false && msg_alert !="") alert(msg_alert);
		return submit_form_group_barang;
	});
</script>
<?php } ?>