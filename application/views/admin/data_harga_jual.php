<?php 
$aksesKey = "admin/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug();
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}

	$no=0;
	$htm_table_harga_jual = "";
	foreach($get_harga_jual as $row){
		$htm_table_harga_jual.="
						<tr data-id=\"".$row->id."\">
							<th scope=\"row\">".($no+=1)."</th>
							<td>".$row->nama_barang."</td>
							<td>".$row->nama_satuan."</td>
							<td>".format_rupiah($row->harga_beli)."</td>
							<td>".format_rupiah($row->harga_jual_normal-$row->harga_beli)."</td>
							<td>".@number_format((($row->harga_jual_normal-$row->harga_beli)/$row->harga_jual_normal*100),1)."%</td>
							<td>".format_rupiah($row->harga_jual_minimal)."</td>
							<td>".format_rupiah($row->harga_jual_normal)."</td>
							<td>".format_rupiah($row->harga_jual_outlet)."</td>
							<td>".btnStatLabel($row->status)."</td>
							<td>";
								if(isset($aksesUbah)) $htm_table_harga_jual.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/edit/".$row->id."\" class=\"btn btn-outline-success btn-sm\"><i class=\"fa fa-edit\"></i>&nbsp; Edit</a> ";
								if(isset($aksesHapus)) $htm_table_harga_jual.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/hapus/".$row->id."\" class=\"btn btn-outline-danger btn-sm\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";
		  $htm_table_harga_jual.="</td>
						</tr>
					";
	}
	if($htm_table_harga_jual == ""){
		$htm_table_harga_jual .= "<tr><th colspan='12' class=\"text-center\">. : Data Kosong : .</th></tr>";
		$htm_table_harga_jual .= "<tr><th colspan='12' class=\"text-center\">&nbsp;</th></tr>";
	}
	
	$htm_option_data_barang = "";
	foreach($get_data_barang as $row){
		$htm_option_data_barang.="<option value=\"".$row->id."\" data-nama-group=\"".$row->nama_group."\" ".((isset($get_edit_harga_jual[0]->id_barang) and $get_edit_harga_jual[0]->id_barang == $row->id)?"selected=selected":"")." >".$row->nama_barang."</option>";
	}
	
	if($action == "tambah"){
		$passRequired = " required='required' ";
	}else if($action == "edit"){
		$hintPassword = "<span class=\"help-block text-warning\">Kosongkan kolom Password jika tidak akan mengganti Password.</span>";
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>">Harga Jual <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-6">
		<form action="" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari" placeholder="Cari Harga Jual" class="form-control form-control-sm" required="required" />
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<?php if(isset($aksesTambah)){ ?>
		<a href="<?php echo base_url()."admin/".$this->router->fetch_method()."/tambah"; ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil"></i>&nbsp; Tambah</a>
		<?php } ?>
		<button type="button" class="btn btn-outline-warning btn-sm" id="bt_print"><i class="fa fa-print"></i>&nbsp; Cetak</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="thead-dark">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Nama Barang</th>
					<th scope="col">Satuan</th>
					<th scope="col">Harga Beli</th>
					<th scope="col">Profit</th>
					<th scope="col">Profit %</th>
					<th scope="col">Harga Jual Minimal</th>
					<th scope="col">Harga Jual Normal</th>
					<th scope="col">Harga Jual Outlet</th>
					<th scope="col">Status</th>
					<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
					<th scope="col">Aksi</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php echo $htm_table_harga_jual; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>
<?php if($action == "tambah" or $action == "edit"){ ?>
<div class="card">
	<div class="card-body">
		<form method="post" action="">
			<input type="hidden" name="form_action" value="<?php echo $action; ?>">
			<input type="hidden" name="id_harga_jual" value="<?php echo @$get_edit_harga_jual[0]->id; ?>">
			<div class="form-group">
				<label>Nama Barang</label>
				<select data-placeholder="Pilih..." class="form-control standardSelect" name="id_barang" required="required" tabindex="1">
					<option value="" label="Pilih..."></option>
					<?php echo $htm_option_data_barang; ?>
				</select>
			</div>
			<div class="form-group">
				<label>Group Barang</label>
				<input type="text" class="form-control" name="nama_group_barang" value="<?php echo @$get_edit_harga_jual[0]->nama_group_barang; ?>" readonly="readonly">
			</div>
			<div class="form-group">
				<label>Harga Beli</label>
				<input type="text" class="form-control" name="harga_beli" value="<?php echo @$get_edit_harga_jual[0]->harga_beli; ?>" required="required">
			</div>
			<div class="form-group">
				<label>Harga Jual Minimal</label>
				<input type="text" class="form-control" name="harga_jual_minimal" value="<?php echo @$get_edit_harga_jual[0]->harga_jual_minimal; ?>" required="required">
			</div>
			<div class="form-group">
				<label>Harga Jual Normal</label>
				<input type="text" class="form-control" name="harga_jual_normal" value="<?php echo @$get_edit_harga_jual[0]->harga_jual_normal; ?>" required="required">
			</div>
			<div class="form-group">
				<label>Harga Jual Outlet</label>
				<input type="text" class="form-control" name="harga_jual_outlet" value="<?php echo @$get_edit_harga_jual[0]->harga_jual_outlet; ?>" required="required">
			</div>
			<div class="form-group">
				<label>Profit</label>
				<input type="text" class="form-control" name="profit" value="<?php echo (@$get_edit_harga_jual[0]->harga_jual_normal-@$get_edit_harga_jual[0]->harga_beli); ?>" readonly="readonly">
			</div>
			<div class="form-group">
				<label>Status</label>
				<select class="form-control" name="status" required="required">
					<option value='1' <?php echo(isset($get_edit_harga_jual[0]->status) and $get_edit_harga_jual[0]->status == "1")?"selected=selected":""; ?>><?php echo statLabel("1"); ?></option>
					<option value='0' <?php echo(isset($get_edit_harga_jual[0]->status) and $get_edit_harga_jual[0]->status == "0")?"selected=selected":""; ?>><?php echo statLabel("0"); ?></option>
				</select>
			</div>
			<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>" class="btn btn-secondaray btn-flat">Back</a>
			<button type="submit" name="submit_harga_jual" class="btn btn-primary btn-flat">Submit</button>
		</form>
	</div>
</div>
<?php } ?>
<script>
	var VG_onpage_data_table = "tbl_harga_jual";
	$j(document).on("click","#bt_print",function(){
		printTableData();
	});
	$j(".standardSelect").chosen({
		disable_search_threshold: 10,
		no_results_text: "Oops, nothing found!",
		width: "100%"
	});
	$j("select[name='id_barang']").change(function(){
		var nama_group = $j(this).find(':selected').data('nama-group');
		$j("input[name='nama_group_barang']").val(nama_group);
	});
	$j("input[name='harga_beli'], input[name='harga_jual_normal']").keyup(function(){
		var harga_beli = parseInt($j("input[name='harga_beli']").val());
		var harga_jual_normal = parseInt($j("input[name='harga_jual_normal']").val());
		$j("input[name='profit']").val(harga_jual_normal-harga_beli);
	});
</script>
<?php } ?>