<?php
$aksesKey = "admin/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug();
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}

	$no=0;
	$htm_table_design = "";
	foreach($get_design as $row){

		if($row->status == '1'){
			$status = 'Approved';
		}else{
			$status = 'Proses Review';
		}

		$htm_table_design.="
						<tr data-id=\"".$row->id."\">
							<th scope=\"row\">".($no+=1)."</th>
							<td>".$row->tanggal."</td>
							<td>".$row->customer."</td>
							<td>".$status."</td>
							<td>";
								if(isset($aksesUbah)) $htm_table_design.=" <a href=\"".base_url()."design/data_design_det/".$row->id."\" class=\"btn btn-outline-success btn-sm\"><i class=\"fa fa-eye\"></i>&nbsp; Detail</a> ";
								if(isset($aksesHapus)) $htm_table_design.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/hapus/".$row->id."\" class=\"btn btn-outline-danger btn-sm\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";
		  $htm_table_design.="</td>
						</tr>
					";
	}
	if($htm_table_design == ""){
		$htm_table_design .= "<tr><th colspan='7' class=\"text-center\">. : Data Kosong : .</th></tr>";
		$htm_table_design .= "<tr><th colspan='7' class=\"text-center\">&nbsp;</th></tr>";
	}

	if($action == "tambah"){
		$passRequired = " required='required' ";
	}else if($action == "edit"){
		$hintPassword = "<span class=\"help-block text-warning\">Kosongkan kolom Password jika tidak akan mengganti Password.</span>";
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>">Data Design Customer <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-6">
		<form action="" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari" placeholder="Cari Customer" class="form-control form-control-sm" required="required" />
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<?php if(isset($aksesTambah)){ ?>
		<a href="<?php echo base_url()."admin/".$this->router->fetch_method()."/tambah"; ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil"></i>&nbsp; Tambah</a>
		<?php } ?>
		<!-- <button type="button" class="btn btn-outline-warning btn-sm" id="bt_print"><i class="fa fa-print"></i>&nbsp; Cetak</button> -->
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="thead-dark">
				<tr>
					<th scope="col" width="5%">#</th>
					<th scope="col" width="10%">Tanggal</th>
					<th scope="col" width="60%">Customer</th>
					<th scope="col">Status</th>
					<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
					<th scope="col">Aksi</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php echo $htm_table_design; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>
<?php if($action == "tambah" or $action == "edit"){ ?>

<div class="card">
	<div class="card-body">
		<form name="form_crud" method="post" action="" enctype="multipart/form-data">
			<input type="hidden" name="form_action" value="<?php echo $action; ?>">
			<input type="hidden" name="id_design" value="<?php echo @$get_edit_design[0]->id; ?>">
			<div class="form-group">
				<label>Tanggal</label>
				<input type="date" class="form-control" name="tanggal" value="<?php echo @$get_edit_design[0]->tanggal; ?>" required="required">
				<span class="help-block color-red"></span>
			</div>
			<div class="form-group">
				<label>Customer</label>
				<input type="text" class="form-control" name="customer" value="<?php echo @$get_edit_design[0]->customer; ?>" required="required">
				<span class="help-block color-red"></span>
			</div>
			<div class="form-group">
				<label>Status</label>
				<select class="form-control" name="status" required="required">
					<option value='1' <?php echo(isset($get_edit_design[0]->status) and $get_edit_design[0]->status == "1")?"selected=selected":""; ?>><?php echo "Approve"; ?></option>
					<option value='0' <?php echo(isset($get_edit_design[0]->status) and $get_edit_design[0]->status == "0")?"selected=selected":""; ?>><?php echo "Proses"; ?></option>
				</select>
			</div>
			<div class="form-group">
				<label>Logo</label>
				<input type="file" class="form-control" name="gambar1" accept="image/x-png,image/jpg" required>
				<span class="help-block color-red"></span>

				<input type="hidden" name="file_gambar1_name" value="<?php echo @$get_edit_design[0]->gambar1; ?>"/>
			</div>
			<div class="form-group">
				<label>Gambar</label>
				<input type="file" class="form-control" name="gambar2" accept="image/x-png,image/jpg" required>
				<span class="help-block color-red"></span>

				<input type="hidden" name="file_gambar2_name" value="<?php echo @$get_edit_design[0]->gambar2; ?>"/>
			</div>
			<div class="form-group">
				<label>Background Warna</label>
				<input type="color" class="form-control" name="warna1" value="">
			</div>
			<div class="form-group">
				<label>Background Warna Gradient</label>
				<input type="color" class="form-control" name="warna2" value="">
			</div>
			<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>" class="btn btn-secondaray btn-flat">Back</a>
			<button type="submit" name="submit_design" class="btn btn-primary btn-flat">Submit</button>
		</form>
	</div>
</div>
<?php } ?>

<!-- MODAL LIHAT -->
<div class="modal fade" id="modal-default" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form class="" action="index.html" method="post">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h3 class="modal-title" id="judul">Purchase Order</h3>
				</div>

				<div class="modal-body">
					<div class="col-4">

					</div>
				</div>

				<div class="modal-footer">

				</div>
			</form>
		</div>
	</div>
</div>


<script>
	var action = "<?php echo $action; ?>";
	var VG_onpage_data_table = "tbl_design";
	var submit_kode_design = true;
	var submit_nama_design = true;
	var msg_alert_kode_design = "";
	var msg_alert_nama_design = "";

	$j(document).on("submit","form[name='form_crud']",function(){
		if(submit_kode_design == false && msg_alert_kode_design !="")  alert(msg_alert_kode_design);
		else if(submit_nama_design == false && msg_alert_nama_design !="") alert(msg_alert_nama_design);

		if(submit_kode_design === true && submit_nama_design === true)
			return true;
		else
			return false;
	});
</script>
<?php } ?>
