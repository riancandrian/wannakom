<?php
$aksesKey = $this->router->fetch_class()."/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug();
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}

	$no=0;
	$htm_table_transaksi_penjualan = "";
	foreach($get_transaksi_penjualan as $row){
		$htm_table_transaksi_penjualan.="
										<tr data-id=\"".$row->id."\">
											<th scope=\"row\">".($no+=1)."</th>
											<td>".repair_date2($row->tanggal)."</td>";

			if(isset($aksesUbah))
				$htm_table_transaksi_penjualan.="<td><a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/preview/".$row->id."\" class=\"text-primary\">".$row->nomor."</a></td>";
			else
				$htm_table_transaksi_penjualan.="<td>".$row->nomor."</td>";
					$htm_table_transaksi_penjualan.="<td>".@$row->nama_customer."</td>
											<td>".repair_date($row->jatuh_tempo)."</td>
											<td>".$row->nama_gudang."</td>
											<td>".$row->status_penjualan."</td>
											<!--<td>".$row->sisa_tagihan."</td>-->
											<td>".format_rupiah($row->total)."</td>
											<td>";
												//if(isset($aksesUbah)) $htm_table_transaksi_penjualan.=" <a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/edit/".$row->id."\" class=\"btn btn-outline-success btn-sm\"><i class=\"fa fa-edit\"></i>&nbsp; Edit</a> ";
												if(isset($aksesHapus)) $htm_table_transaksi_penjualan.=" <a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/hapus/".$row->id."\" class=\"btn btn-outline-danger btn-sm\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";
						  $htm_table_transaksi_penjualan.="</td>
										</tr>
									";
	}

	if($htm_table_transaksi_penjualan == ""){
		$htm_table_transaksi_penjualan .= "<tr><th colspan='10' class=\"text-center\">. : Data Kosong : .</th></tr>";
	}
		$htm_table_transaksi_penjualan .= "<tr><th colspan='10'>Menampilkan ".(($no>0)?1:0)." .. ".$no." dari ".$no." Baris</th></tr>";
	if(isset($get_transaksi_penjualan_detail_barang) and is_array($get_transaksi_penjualan_detail_barang)){
		$no=0;
		$htm_table_transaksi_penjualan_detail_barang = "";
		foreach($get_transaksi_penjualan_detail_barang as $row){
			$htm_table_transaksi_penjualan_detail_barang.="
											<tr data-id=\"".$row->id."\">
												<th scope=\"row\">".($no+=1)."</th>
												<td>".$row->nama_barang."</td>
												<td>".$row->deskripsi."</td>
											<!--<td>".$row->expire_date."</td>-->
												<td>".$row->qty."</td>
												<td>".$row->nama_satuan."</td>
												<td>".format_rupiah($row->harga_satuan)."</td>
												<td>".format_rupiah($row->sub_total)."</td>
											</tr>
										";
		}

		if($htm_table_transaksi_penjualan_detail_barang == ""){
			$htm_table_transaksi_penjualan_detail_barang .= "<tr><th colspan='10' class=\"text-center\">. : Data Kosong : .</th></tr>";
		}
			$htm_table_transaksi_penjualan_detail_barang .= "<tr><th colspan='10'>&nbsp;</th></tr>";
			//$htm_table_transaksi_penjualan_detail_barang .= "<tr><th colspan='10'>Menampilkan ".(($no>0)?1:0)." .. ".$no." dari ".$no." Baris</th></tr>";
	}
	$htm_option_customer ="<option value=\"\" data-alamat=\"\" >Pilih...</option>";
	foreach($get_data_customer as $row){
		$htm_option_customer.="<option value=\"".$row->id."\" data-alamat=\"".$row->alamat."\" data-jatuh-tempo=\"".$row->jatuh_tempo."\" ".((isset($get_edit_transaksi_penjualan[0]->id_customer) and $get_edit_transaksi_penjualan[0]->id_customer == $row->id)?"selected=selected":"")." >".$row->nama_customer."</option>";
	}

	if($action == "tambah"){
		$tambahRequired = " required='required' ";
	}else if($action == "edit"){
		$editRequired = " required='required' ";
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method(); ?>">Transaksi Penjualan <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-4">
		<div class="card">
			<div class="card-header bg-warning p-2">
				<strong class="card-title">Penjualan Belum Dibayar</strong>
			</div>
			<div class="card-body p-2">
				<div style="font-size:12px;">Total</div>
				<div><h2 class="card-text font-weight-bold"><?php echo format_rupiah(@$get_laporan_cepat_transaksi_penjualan['belum_dibayar']); ?></h2></div>
			</div>
		</div>
	</div>
	<div class="col-4">
		<div class="card">
			<div class="card-header bg-danger p-2">
				<strong class="card-title">Penjualan Jatuh Tempo</strong>
			</div>
			<div class="card-body p-2">
				<div style="font-size:12px;">Total</div>
				<div><h2 class="card-text font-weight-bold"><?php echo format_rupiah(@$get_laporan_cepat_transaksi_penjualan['belum_dibayar_jatuh_tempo']); ?></h2></div>
			</div>
		</div>
	</div>
	<div class="col-4">
		<div class="card">
			<div class="card-header bg-primary p-2">
				<strong class="card-title">Pelunasan Diterima 30 Hari Terakhir</strong>
			</div>
			<div class="card-body p-2">
				<div style="font-size:12px;">Total</div>
				<div><h2 class="card-text font-weight-bold"><?php echo format_rupiah(@$get_laporan_cepat_transaksi_penjualan['dibayar_30_hari']); ?></h2></div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-6">
		<form action="" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari" placeholder="Pencarian..." class="form-control form-control-sm" required="required" />
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari"><i class="fa fa-search"></i></button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<?php if(isset($aksesTambah)){ ?>
		<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/tambah"; ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil"></i>&nbsp; Tambah</a>
		<?php } ?>
		<button type="button" class="btn btn-outline-warning btn-sm" id="bt_print"><i class="fa fa-print"></i>&nbsp; Cetak</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="bg-info">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Tanggal</th>
					<th scope="col">Nomor</th>
					<th scope="col">Customer</th>
					<th scope="col">Tgl JT</th>
					<th scope="col">Gudang</th>
					<th scope="col">Status</th>
					<!--<th scope="col">Sisa Tagihan</th>-->
					<th scope="col">Total</th>
					<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
					<th scope="col">Aksi</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php echo $htm_table_transaksi_penjualan; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>
<?php if($action == "preview"){ ?>
<div class="card">
	<div class="card-body">
		<div class="row bg-light pb-3 pt-3">
			<div class="col-2 font-weight-bold">
				Customer
			</div>
			<div class="col-2 text-primary">
				<?php echo @$get_edit_transaksi_penjualan[0]->nama_customer; ?>
			</div>
			<div class="col-2 font-weight-bold">
				No Transaksi
			</div>
			<div class="col-2 text-primary">
				<?php echo @$get_edit_transaksi_penjualan[0]->nomor; ?>
			</div>
			<div class="col-2">
				&nbsp;
			</div>
			<div class="col-2">
				&nbsp;
			</div>
		</div>
		<div class="row">
			<div class="col-2 font-weight-bold">
				Alamat Customer
			</div>
			<div class="col-2">
				<?php echo @$get_edit_transaksi_penjualan[0]->alamat_customer; ?>
			</div>
			<div class="col-2 font-weight-bold">
				Tgl Transaksi
			</div>
			<div class="col-2">
				<?php echo @repair_date2(@$get_edit_transaksi_penjualan[0]->tanggal); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-2 font-weight-bold">
				Gudang
			</div>
			<div class="col-2">
				<?php echo @$get_edit_transaksi_penjualan[0]->nama_gudang; ?>
			</div>
			<div class="col-2 font-weight-bold">
				Tgl JT
			</div>
			<div class="col-2">
				<?php echo @repair_date(@$get_edit_transaksi_penjualan[0]->jatuh_tempo); ?>
			</div>
			<div class="col-2 font-weight-bold">
				&nbsp;
			</div>
			<div class="col-2">
				&nbsp;
			</div>
		</div>
		<div class="row">
			<div class="col-2 font-weight-bold">
				&nbsp;
			</div>
			<div class="col-2">
				&nbsp;
			</div>
			<div class="col-2 font-weight-bold">
				TOP
			</div>
			<div class="col-2">
				<?php echo @$get_edit_transaksi_penjualan[0]->top; ?>
			</div>
			<div class="col-2 font-weight-bold">
				&nbsp;
			</div>
			<div class="col-2">
				&nbsp;
			</div>
		</div>
		<table class="table table-form">
			<thead class="bg-info">
				<tr>
					<th scope="col">#</th>
					<th scope="col" style="width:150px;">Nama Barang</th>
					<th scope="col">Deskripsi</th>
					<!--<th scope="col">Expire Date</th>-->
					<th scope="col">Qty</th>
					<th scope="col">Satuan</th>
					<th scope="col">Harga Satuan</th>
					<th scope="col">Sub Total</th>
				</tr>
			</thead>
			<tbody>
				<?php echo $htm_table_transaksi_penjualan_detail_barang; ?>
			</tbody>
		</table>
		<div class="row">
			<div class="col-6">
				<div class="form-group">
					<label>Pesan</label><br/>
					<?php echo @$get_edit_transaksi_penjualan[0]->pesan; ?>
				</div>
			</div>
			<div class="col-6">
				<div class="row">
					<div class="col-6">
						Total
					</div>
					<div class="col-6 text-right">
						<?php echo @format_rupiah(@$get_edit_transaksi_penjualan[0]->total); ?>
					</div>
					<div class="col-6">
						Pemotongan <?php echo @$get_edit_transaksi_penjualan[0]->pemotongan; ?> %
					</div>
					<div class="col-6 text-right">
						<?php echo @format_rupiah((@$get_edit_transaksi_penjualan[0]->total*@$get_edit_transaksi_penjualan[0]->pemotongan/100)); ?>
					</div>
					<div class="col-6 font-weight-bold">
						Grand Total
					</div>
					<div class="col-6 text-right font-weight-bold">
						<?php echo @format_rupiah(@$get_edit_transaksi_penjualan[0]->grand_total); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt-5">
			<div class="col-4">
				<?php echo " <a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/hapus/".@$get_edit_transaksi_penjualan[0]->id."\" class=\"btn btn-sm btn-flat btn-secondary\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";?>
			</div>
			<div class="col-4 text-center">
				<button type="button" class="btn btn-sm btn-flat btn-info" id="bt_print_preview"><i class="fa fa-print"></i>&nbsp; Cetak</button>
				<div class="btn-group">
				  <button type="button" class="btn btn-sm btn-flat btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Tindakan
				  </button>
				  <div class="dropdown-menu">
					<a class="dropdown-item" href="<?php echo base_url()."penjualan/retur_penjualan/tambah/".@$get_edit_transaksi_penjualan[0]->id; ?>">Retur Penjualan</a>
					<!--<a class="dropdown-item" href="<?php echo base_url()."penjualan/pembayaran_hutang/tambah/".@$get_edit_transaksi_penjualan[0]->id_customer; ?>">Pembayaran Hutang</a>-->
				  </div>
				</div>
			</div>
			<div class="col-4 text-right">
				<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method(); ?>" class="btn btn-sm btn-flat btn-danger">Kembali</a>
				<?php
					echo " <a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/edit/".@$get_edit_transaksi_penjualan[0]->id."\" class=\"btn btn-sm btn-flat btn-success ";
					if(isset($get_edit_transaksi_penjualan[0]->status_retur) and @$get_edit_transaksi_penjualan[0]->status_retur == '1') echo " disabled ";
					echo " \">Ubah</a> ";
				?>
			</div>
		</div>
		<div class="row mt-5">
			<div class="col">
				<p class="text-danger">*Jika telah diretur data ini tidak bisa di edit</p>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<?php if($action == "tambah" or $action == "edit"){ ?>
<div class="card">
	<div class="card-body">
		<form method="post" action="" enctype="multipart/form-data" name="form_transaksi_penjualan" class="image-editor">
			<input type="hidden" name="form_action" value="<?php echo $action; ?>">
			<input type="hidden" name="id_transaksi_penjualan" value="<?php echo @$get_edit_transaksi_penjualan[0]->id; ?>">
			<div class="row">
				<div class="col-4">
					<div class="form-group">
						<label>No Transaksi</label>
						<input type="text" class="form-control" name="nomor" value="<?php echo (isset($get_edit_transaksi_penjualan[0]->nomor))?$get_edit_transaksi_penjualan[0]->nomor:"[Auto Generate After Submit]"; ?>" readonly="readonly">
					</div>
					<div class="form-group">
						<label>Customer</label>
						<!--
						<select data-placeholder="Pilih..." class="form-control standardSelect" name="id_customer" tabindex="1">
							<option value="" label="Pilih..."></option>
							<?php echo $htm_option_customer; ?>
						</select>
						-->
						<div class="input-group">
							<input type="text" class="form-control" name="nama_customer" placeholder="Nama Customer" value="<?php echo @$get_edit_transaksi_penjualan[0]->nama_customer; ?>" required="required">
							<input type="hidden" name="id_customer"  value="<?php echo @$get_edit_transaksi_penjualan[0]->id_customer; ?>" required="required">
							<div class="input-group-append">
								<span class="input-group-text"><i class="fa fa-search"></i></span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Jatuh Tempo</label>
						<input type="date" class="form-control" name="jatuh_tempo" value="<?php echo (isset($get_edit_transaksi_penjualan[0]->jatuh_tempo))?$get_edit_transaksi_penjualan[0]->jatuh_tempo:date('Y-m-d'); ?>" required="required">
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label>Tanggal</label>
						<input type="date" class="form-control" name="tanggal" value="<?php echo (isset($get_edit_transaksi_penjualan[0]->tanggal))?date_format(date_create($get_edit_transaksi_penjualan[0]->tanggal),'Y-m-d'):date('Y-m-d'); ?>" required="required">
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="email" class="form-control" name="email" value="<?php echo @$get_edit_transaksi_penjualan[0]->email; ?>" readonly="readonly">
					</div>
					<div class="form-group">
						<label>Top</label>
						<input type="text" class="form-control" name="top" value="<?php echo @$get_edit_data_customer[0]->jatuh_tempo; ?>" readonly="readonly">
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label>Alamat Customer</label>
						<textarea class="form-control" name="alamat_customer" readonly="readonly" rows="1"> <?php echo @$get_edit_data_customer[0]->alamat;?> </textarea>
					</div>

					<div class="form-group">
						<label>Gudang</label>
						<select class="form-control" name="id_gudang">
							<?php

								foreach ($get_data_gudang as $row) {

									if(isset($get_edit_transaksi_penjualan[0]->id_gudang)){
										if($get_edit_transaksi_penjualan[0]->id_gudang == $row->id){
											echo "<option value='".$row->id."' selected>".$row->nama_gudang."</option>";
										}else{
											echo "<option value='".$row->id."'>".$row->nama_gudang."</option>";
										}
									}else{
										echo "<option value='".$row->id."'>".$row->nama_gudang."</option>";
									}

								}

							?>
						</select>
					</div>
				</div>
			</div>
			<table class="table table-form">
				<thead class="bg-info">
					<tr>
						<th scope="col">#</th>
						<th scope="col" style="width:150px;">Nama Barang</th>
						<th scope="col">Deskripsi</th>
						<!--<th scope="col">Expire Date</th>-->
						<th scope="col">Qty</th>
						<th scope="col">Satuan</th>
						<th scope="col">No Seri</th>
						<th scope="col">Harga Satuan</th>
						<th scope="col">Sub Total</th>
						<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
						<th scope="col" class="text-center">Aksi</th>
						<?php } ?>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
			<div class="row">
				<div class="col-6">
					<div class="form-group">
						<label>Pesan</label>
						<textarea class="form-control" name="pesan"><?php echo @$get_edit_transaksi_penjualan[0]->pesan; ?></textarea>
					</div>
				</div>
				<div class="col-6">
					<div class="row">
						<div class="col-6">
							Total
							<input type="hidden" name="total" value="<?php echo @$get_edit_transaksi_penjualan[0]->total; ?>">
						</div>
						<div class="col-6 text-right text-total">
							Rp. 0,-
						</div>
						<div class="col-6">
							Pemotongan
						</div>
						<div class="col-6">
							&nbsp;
						</div>
						<div class="col-6">
							<div class="input-group input-group-sm">
							  <input type="number" class="form-control" name="pemotongan" placeholder="0" value="<?php echo (isset($get_edit_transaksi_penjualan[0]->pemotongan))?$get_edit_transaksi_penjualan[0]->pemotongan:'0'; ?>">
							  <input type="hidden" name="pemotongan_tipe" value="<?php echo (isset($get_edit_transaksi_penjualan[0]->pemotongan_tipe))?$get_edit_transaksi_penjualan[0]->pemotongan_tipe:'persen'; ?>">
							  <div class="input-group-append">
								<button class="btn btn-outline-secondary btn-pemotongan-type <?php echo (isset($get_edit_transaksi_penjualan[0]->pemotongan_tipe) and $get_edit_transaksi_penjualan[0]->pemotongan_tipe == 'persen')?'active':(!isset($get_edit_transaksi_penjualan[0]->pemotongan_tipe))?'active':''; ?>" type="button" data-type="persen">%</button>
								<button class="btn btn-outline-secondary btn-pemotongan-type <?php echo (isset($get_edit_transaksi_penjualan[0]->pemotongan_tipe) and $get_edit_transaksi_penjualan[0]->pemotongan_tipe == 'nominal')?'active':''; ?>" type="button" data-type="nominal">Rp</button>
							  </div>
							</div>
						</div>
						<div class="col-6 text-right text-pemotongan">
							Rp. 0,-
						</div>
						<div class="col-6 font-weight-bold">
							Grand Total
							<input type="hidden" name="grand_total" value="<?php echo @$get_edit_transaksi_penjualan[0]->grand_total; ?>">
						</div>
						<div class="col-6 text-right font-weight-bold text-grand-total">
							Rp. 0,-
						</div>
						<div class="col-6 font-weight-bold">
							Pembayaran
						</div>
						<div class="col-6 text-right font-weight-bold">
							<input type="number" name="pembayaran" value="<?php echo @$get_edit_transaksi_penjualan[0]->pembayaran; ?>" required="required">
						</div>
						<div class="col-6 font-weight-bold">
							Kembali
							<input type="hidden" name="kembali" value="<?php echo @$get_edit_transaksi_penjualan[0]->kembali; ?>">
						</div>
						<div class="col-6 text-right font-weight-bold text-kembali">
							Rp. 0,-
						</div>
					</div>
				</div>
			</div>
			<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/preview/".@$get_edit_transaksi_penjualan[0]->id; ?>" class="btn btn-outline-danger btn-sm btn-flat">Kembali</a>
			<button type="submit" name="submit_transaksi_penjualan" class="btn btn-success btn-sm btn-flat">Submit</button>
		</form>
	</div>
</div>
<!-- Modal -->
<div class="modal" id="modalDetailNoSeri" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Detail No Seri</span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<div class="row mb-2 text-center">
			<div class="col first_no_seri">-</div>
			<div class="col">s/d</div>
			<div class="col last_no_seri">-</div>
		</div>
		<div class="row mb-2 text-center">
			<div class="col">
				<input type="text" class="form-control form-control-sm w-100 mb-3" name="no_seri_awal" placeholder="Input no seri awal..." >
			</div>
			<div class="col">
				<input type="text" class="form-control form-control-sm w-100 mb-3" name="no_seri_akhir" placeholder="Input no seri akhir..." >
			</div>
		</div>
		<table class="table table-sm">
		  <thead>
			<tr>
			  <th class="border-top-0" scope="col" colspan="3">
				<input type="text" class="form-control form-control-sm w-100 mb-3" name="search_no_seri" placeholder="Input no seri..." autofocus >
			   </th>
			</tr>
		  </thead>
		  <thead>
			<tr>
			  <th class="border-top-0" scope="col">No Seri</th>
			  <th class="border-top-0" scope="col" style="width:25%;">Qty</th>
			  <th class="border-top-0" scope="col">&nbsp;</th>
			</tr>
		  </thead>
		  <tbody>
			<tr>
			  <td>
				<input type="text" class="form-control form-control-sm" name="no_seri" type="text" placeholder="No Seri">
			  </td>
			  <td>
				<input type="number" class="form-control form-control-sm" name="qty" type="text" placeholder="Qty">
			  </td>
			  <td>
				<button type="button" class="btn btn-sm btn-flat btn-danger btn-del-no-seri-detail"><i class="fa fa-minus"></i></button>
			  </td>
			</tr>
		  </tbody>
		</table>
      </div>
      <div class="modal-footer">
		<!--<button type="button" class="btn btn-sm btn-flat btn-success btn-add-no-seri-detail"><i class="fa fa-plus"></i></button>-->
		<button type="button" class="btn btn-sm btn-flat btn-secondary" data-dismiss="modal">Cancel</button>
		<button type="button" class="btn btn-sm btn-flat btn-primary btn-save-no-seri-detail">Save</button>
      </div>
    </div>
  </div>
</div>
<?php } ?>

<div class="modal" id="modalDetailCustomer" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Pilih Customer</span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<table class="table table-sm">
		  <thead>
			<tr>
			  <th class="border-top-0" scope="col">Kode</th>
			  <th class="border-top-0" scope="col">Nama Customer</th>
			  <th class="border-top-0" scope="col">Alamat</th>
			  <th class="border-top-0" scope="col">Kota</th>
			  <th class="border-top-0" scope="col">Telepon</th>
			  <th class="border-top-0" scope="col">Jatuh Tempo</th>
			</tr>
		  </thead>
		  <tbody>
		  </tbody>
		</table>
      </div>
    </div>
  </div>
</div>
<script>
	var VG_skip = false;
	var VG_onpage_data_table = "tbl_transaksi_penjualan";
	var BASE_URL = "<?php echo base_url(); ?>";
	var aksesHapus = "<?php echo @$aksesHapus; ?>";
	var action = "<?php echo $action; ?>";
	var PG_TmpNoSeriStok = {};
	var PG_TmpNoSeriOnTable = {};
	var btnDetailNoSeri = null;
	var no=0;
	function setAddButtonDataBarangTransaksiPenjualan(){
		$j(".btn-add-detail-barang").parents("tr").remove();
		var htm_table_form_detail_barang = "";
		htm_table_form_detail_barang+= "<tr><td colspan='10'><button type=\"button\" class=\"btn btn-outline-success btn-sm btn-add-detail-barang\" >+ Tambah Barang</button></td></tr>";
		$j(".table-form tbody").append(htm_table_form_detail_barang);
		$j(".standardSelect").chosen({disable_search_threshold: 10,no_results_text: "Oops, nothing found!",width: "100%"});
	}
	function setAddDataBarangTransaksiPenjualan(){
		var htm_option_barang = "";
		var get_data_barang = JSON.parse(`<?php echo json_encode($get_data_barang); ?>`);
		for(var i in get_data_barang){
			var row_barang = get_data_barang[i];
			htm_option_barang+="<option value=\""+row_barang['id']+"\" data-deskripsi=\""+row_barang['deskripsi']+"\" data-nama-satuan=\""+row_barang['nama_satuan']+"\" data-harga-jual=\""+row_barang['harga_jual']+"\" >"+row_barang['nama_barang']+"</option>";
		}
		no++;
		var htm_table_form_detail_barang = "";
		htm_table_form_detail_barang+="<tr>"+
											"<input type=\"hidden\" name=\"no[]\" id=\"no\" value=\""+no+"\" disabled='disabled' />"+
											"<th scope=\"row\">"+no+"</th>"+
											"<td>"+
												"<select data-placeholder=\"Cari Barang\" class=\"form-control standardSelect\" name=\"id_barang[]\" id=\"id_barang\" >"+
													//"<option value=\"\" data-deskripsi=\"\" data-nama-satuan=\"\" data-harga-jual=\"\">Cari Barang</option>"+
													"<option value=\"\" label=\"Pilih...\"></option>"+
													htm_option_barang+
												"</select>"+
											"</td>"+
											"<td><input type=\"text\" name=\"deskripsi[]\" id=\"deskripsi\" placeholder=\"Deskripsi\" class=\"form-control form-control-sm\" readonly=\"readonly\" /></td>"+
											/*"<td><input type=\"date\" name=\"expire_date[]\" id=\"expire_date\" placeholder=\"Expire Date\" class=\"form-control form-control-sm\" style=\"width:150px;\" required=\"required\" /></td>"+*/
											"<td><input type=\"number\" name=\"qty[]\" id=\"qty\" placeholder=\"Qty\" class=\"form-control form-control-sm\" readonly=\"readonly\" /></td>"+
											"<td><input type=\"text\" name=\"satuan[]\" id=\"satuan\" placeholder=\"Satuan\" class=\"form-control form-control-sm\" readonly=\"readonly\" /></td>"+
											"<td><button type=\"button\" class=\"btn btn-outline-secondary btn-sm btn-flat btnDetailNoSeri\" data-toggle=\"modal\" >Detail</button>"+
												"<input type=\"hidden\" name=\"detail_no_seri[]\" id=\"detail_no_seri\" /></td>"+
											"<td><input type=\"number\" name=\"harga_satuan[]\" id=\"harga_satuan\" placeholder=\"Harga Satuan\" class=\"form-control form-control-sm\" readonly=\"readonly\" /></td>"+
											"<td><input type=\"number\" name=\"sub_total[]\" id=\"sub_total\" placeholder=\"Sub Total\" class=\"form-control form-control-sm\" readonly=\"readonly\" /></td>"+
											"<td class=\"text-center\">";
						htm_table_form_detail_barang+=" <button type=\"button\" class=\"btn btn-outline-danger btn-sm btn-hapus-add-detail-barang\" ><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</button> ";
											htm_table_form_detail_barang+="</td></tr>";
		$j(".table-form tbody").append(htm_table_form_detail_barang);
		setAddButtonDataBarangTransaksiPenjualan();
	}
	function subTotal(xParent){
		var qty = parseInt(xParent.find("input#qty").val());
		var harga_satuan = parseInt(xParent.find("input#harga_satuan").val());
		xParent.find("input#sub_total").val(qty*harga_satuan);
		grandTotal();
	}
	function grandTotal(){
		var form_transaksi = $j("form[name='form_transaksi_penjualan']");
		var total = 0;
		var pembayaran = parseInt(form_transaksi.find("input[name='pembayaran']").val());
		var pemotongan = parseInt(form_transaksi.find("input[name='pemotongan']").val());
		var pemotongan_tipe = $j(".btn-pemotongan-type.active").data("type");
		var grand_total = 0;
		form_transaksi.find("input[name='sub_total[]']").each(function(){
			total += parseInt($j(this).val());
		});
		form_transaksi.find("input[name='total']").val(total);
		form_transaksi.find(".text-total").html("Rp. "+numberWithCommas(total)+",-");

		if(pemotongan_tipe == "persen")
			var val_pemotongan = (total*pemotongan/100);
		else if(pemotongan_tipe == "nominal")
			var val_pemotongan = (pemotongan);

		form_transaksi.find(".text-pemotongan").html("Rp. "+numberWithCommas(val_pemotongan)+",-");

		form_transaksi.find("input[name='grand_total']").val(total-val_pemotongan);
		form_transaksi.find(".text-grand-total").html("Rp. "+numberWithCommas(total-val_pemotongan)+",-");

		form_transaksi.find("input[name='kembali']").val(pembayaran-(total-val_pemotongan));
		form_transaksi.find(".text-kembali").html("Rp. "+numberWithCommas(pembayaran-(total-val_pemotongan))+",-");

	}
	function setDataDetailBrangTransaksiPenjualantoTable(dataJSON){
		var htm_table_form_detail_transaksi_penjualan = "";
		var get_detail_barang_transaksi_penjualan = JSON.parse(dataJSON);

		for(var x in get_detail_barang_transaksi_penjualan){
			var row = get_detail_barang_transaksi_penjualan[x];

			var htm_option_barang = "";
			var get_data_barang = JSON.parse(`<?php echo json_encode($get_data_barang); ?>`);
			for(var i in get_data_barang){
				var row_barang = get_data_barang[i];
				htm_option_barang +="<option value=\""+row_barang['id']+"\" data-deskripsi=\""+row_barang['deskripsi']+"\" data-nama-satuan=\""+row_barang['nama_satuan']+"\" data-harga-jual=\""+row_barang['harga_jual']+"\" ";
				if(typeof(row['id_barang']) !== "undefined" && row['id_barang'] == row_barang['id']) htm_option_barang += " selected=selected ";
				htm_option_barang +=" >"+row_barang['nama_barang']+"</option>";
			}

			no++;
			htm_table_form_detail_transaksi_penjualan +=" <tr>"+
											"<input type=\"hidden\" name=\"no[]\" id=\"no\" value=\""+no+"\" disabled='disabled' />"+
											"<th scope=\"row\">"+no+"</th>"+
											"<td>"+
												"<select data-placeholder=\"Cari Barang\" class=\"form-control standardSelect\" name=\"id_barang[]\" id=\"id_barang\" >"+
													//"<option value=\"\" data-deskripsi=\"\" data-nama-satuan=\"\" data-harga-jual=\"\">Cari Barang</option>"+
													"<option value=\"\" label=\"Pilih...\"></option>"+
													htm_option_barang+
												"</select>"+
											"</td>"+
											"<td><input type=\"text\" name=\"deskripsi[]\" id=\"deskripsi\" placeholder=\"Deskripsi\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['deskripsi']+"\" /></td>"+
											/*"<td><input type=\"date\" name=\"expire_date[]\" id=\"expire_date\" placeholder=\"Expire Date\" class=\"form-control form-control-sm\" style=\"width:150px;\" required=\"required\" value=\""+row['expire_date']+"\" /></td>"+*/
											"<td><input type=\"number\" name=\"qty[]\" id=\"qty\" placeholder=\"Qty\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['qty']+"\" /></td>"+
											"<td><input type=\"text\" name=\"satuan[]\" id=\"satuan\" placeholder=\"Satuan\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['nama_satuan']+"\" /></td>"+
											"<td><button type=\"button\" class=\"btn btn-outline-secondary btn-sm btn-flat btnDetailNoSeri\" data-toggle=\"modal\" >Detail</button>"+
												"<input type=\"hidden\" name=\"detail_no_seri[]\" id=\"detail_no_seri\" value='"+row['no_seri']+"' /></td>"+
											"<td><input type=\"number\" name=\"harga_satuan[]\" id=\"harga_satuan\" placeholder=\"Harga Satuan\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['harga_satuan']+"\" /></td>"+
											"<td><input type=\"number\" name=\"sub_total[]\" id=\"sub_total\" placeholder=\"Sub Total\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['sub_total']+"\" /></td>";
			htm_table_form_detail_transaksi_penjualan+=
												"<td class=\"text-center\">";
													if(aksesHapus == "1")
														htm_table_form_detail_transaksi_penjualan+=" <button type=\"button\" class=\"btn btn-outline-danger btn-sm btn-hapus-add-detail-barang\" ><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</button> ";
													htm_table_form_detail_transaksi_penjualan+="</td></tr>";
		}

		$j(".table-form tbody").html(htm_table_form_detail_transaksi_penjualan);
		setAddButtonDataBarangTransaksiPenjualan();
		grandTotal();
	}
	function loadDataDetailTransaksiPenjualan(){
		$j.ajax({
			type:"POST",
			url:"<?php echo base_url()."admin/apiweb"; ?>",
			data:{action:"get_transaksi_penjualan_detail_barang",id_transaksi_penjualan:"<?php echo @$get_edit_transaksi_penjualan[0]->id; ?>"},
			success: function(dataJSON){
				setDataDetailBrangTransaksiPenjualantoTable(dataJSON);
			}
		});
	}
	function createHtmlBarcodeNoSeriOnEnter(){
		var rowData = "";
		//console.log('createHtmlBarcodeNoSeriOnEnter');
		//console.log(PG_TmpNoSeriOnTable);
		for(i in PG_TmpNoSeriOnTable){
			var noseri = i
			var qty = parseInt(PG_TmpNoSeriOnTable[i]['qty']);
			var qty_max = parseInt(PG_TmpNoSeriOnTable[i]['qty_max']);
			rowData += "<tr>"+
						  "<td>"+
							"<input type=\"text\" class=\"form-control form-control-sm\" name=\"no_seri\" type=\"text\" placeholder=\"No Seri\" value=\""+noseri+"\" readonly=\"readonly\" >"+
						  "</td>"+
						  "<td>"+
							"<input type=\"number\" class=\"form-control form-control-sm\" name=\"qty\" type=\"text\" placeholder=\"Qty\" ";
			rowData	+= " data-max-qty=\""+qty_max+"\" value=\""+qty+"\" ";
			rowData += " >"+
						  "</td>"+
						  "<td>"+
							"<button type=\"button\" class=\"btn btn-sm btn-flat btn-danger btn-del-no-seri-detail\"><i class=\"fa fa-minus\"></i></button>"+
						  "</td>"+
						"</tr>";
		}
		//console.log(rowData);
		$j(".modal#modalDetailNoSeri").find("tbody").html(rowData);
		$j(".modal#modalDetailNoSeri").find("input[name='search_no_seri']").trigger('focus');
	}
	function barcodeNoSeriOnEnter(pval = null){
		//console.log('barcodeNoSeriOnEnter');
		//console.log(PG_TmpNoSeriStok);
		var str_alert = "";
		var stat_alert = true;
		for(i in PG_TmpNoSeriStok){
			var noseri = i
			var qty_stok = parseInt(PG_TmpNoSeriStok[i]);
			//console.log(noseri+" || "+qty);
			if(pval == noseri && qty_stok > 0){
				if(typeof(PG_TmpNoSeriOnTable[noseri]) == "undefined"){
					let tmp = {};
						tmp['qty'] = 1;
						tmp['qty_max'] = qty_stok;
					PG_TmpNoSeriOnTable[noseri] = tmp;
				}else{
					if( qty_stok > parseInt(PG_TmpNoSeriOnTable[noseri]['qty'])){
						let tmp = {};
							tmp['qty'] = (parseInt(PG_TmpNoSeriOnTable[noseri]['qty'])+1);
							tmp['qty_max'] = qty_stok;
						PG_TmpNoSeriOnTable[noseri] = tmp;
					}else{
						alert("max qty no seri ini: "+qty_stok);

					}
				}
				stat_alert = false;
			}else{
				str_alert = "No seri \""+pval+"\" tidak tersedia";
			}
		}
		if(stat_alert){
			if(VG_skip === false)
			alert(str_alert);
		}
		//console.log(PG_TmpNoSeriOnTable);
		createHtmlBarcodeNoSeriOnEnter();
	}
	$j("input[name='search_no_seri']").keyup(function(e){
		if(e.which == 13) {
			VG_skip = false;
			var val = $j(this).val();
			barcodeNoSeriOnEnter(val);
		}
	});
	$j("input[name='no_seri_awal']").keyup(function(e){
		if(e.which == 13) {
			$j("input[name='no_seri_akhir']").focus();
		}
	});
	$j("input[name='no_seri_akhir']").keyup(function(e){
		if(e.which == 13){
			var no_seri_awal = $j("input[name='no_seri_awal']").val();
			var no_seri_akhir = $j("input[name='no_seri_akhir']").val();
			for(var i = no_seri_awal; i<=no_seri_akhir; i++ ){
				VG_skip = true;
				barcodeNoSeriOnEnter(i);
			}
		}
	});
	$j(document).on("click","#bt_print",function(){
		printTableData();
	});
	$j(document).on("click","#bt_print_preview",function(){
		var data = $j(this).parents("div.card").html();
		printTableData("<div class='card'>"+data+"</div>");
	});
	$j(".standardSelect").chosen({
		disable_search_threshold: 10,
		no_results_text: "Oops, nothing found!",
		width: "100%"
	});
	$j("select[name='id_customer']").change(function(){
		var selected = $j(this).find(":selected");
		var alamat_customer = selected.data('alamat');
		var top = selected.data('jatuh-tempo');
		$j("form[name='form_transaksi_penjualan']").find("textarea[name='alamat_customer']").val(alamat_customer);
		$j("form[name='form_transaksi_penjualan']").find("input[name='top']").val(top);
	});
	$j("form[name='form_transaksi_penjualan']").submit(function(er){
		var ret = true;
		var id_customer = $j(this).find("input[name='id_customer']").val();
		if(id_customer == ""){
			alert("Kolom customer belum di isi!");
			ret = false;
		}
		$j(this).find("select[name='id_barang[]']").each(function(){
			var val = $j(this).val();
			if(val == ""){
				alert("Pastikan Nama Barang di isi dengan benar!");
				ret = false;
			}
		});
		$j(this).find("input[name='detail_no_seri[]']").each(function(){
			var val = $j(this).val();
			if(val == ""){
				alert("Pastikan No Seri di isi dengan benar!");
				ret = false;
			}
		});
		var kembali = parseInt($j(this).find("input[name='kembali']").val());
		if(kembali < 0){
			alert("Uang pembayaran kurang: "+kembali);
			ret = false;
		}
		return ret;
	});
	$j("form[name='form_transaksi_penjualan']").on("keyup","input[name='pemotongan'], input[name='pembayaran']",function(){
		grandTotal();
	});
	$j("form[name='form_transaksi_penjualan']").on("keypress","input[name='nama_customer']",function(event){
		var value = $j(this).val();
		var keycode = (event.keyCode ? event.keyCode : event.which);
		var ret = true;
		if(keycode == '13'){
			$j.ajax({
				type:"POST",
				url:"<?php echo base_url()."admin/apiweb"; ?>",
				data:{action:"get_data_customer",bt_cari:1,tx_cari:value},
				dataType:"json",
				success: function(obj){
					var htmTblData = "";
					for(x in obj){
						var row = obj[x];
						htmTblData += "<tr>"+
										"<td>"+row['kode_customer']+"</td>"+
										"<td><button type=\"button\" class=\"btn btn-link btnSetIdCustomer\" data-id=\""+row['id']+"\" data-nama=\""+row['nama_customer']+"\" data-email=\""+row['email']+"\" data-alamat=\""+row['alamat']+"\" data-jatuh-tempo=\""+row['jatuh_tempo']+"\" >"+row['nama_customer']+"</button></td>"+
										"<td>"+row['alamat']+"</td>"+
										"<td>"+row['nama_kota']+"</td>"+
										"<td>"+row['telepon']+"</td>"+
										"<td>"+row['jatuh_tempo']+"</td>"+
									"</tr>";
					}
					$j('.modal#modalDetailCustomer').find("table tbody").html(htmTblData);
					$j('.modal#modalDetailCustomer').modal('show');
				}
			});
			ret = false;
		}
		return ret;
	});
	$j("form[name='form_transaksi_penjualan']").on("click",".btn-pemotongan-type",function(){
		$j(".btn-pemotongan-type").removeClass("active");
		$j(this).addClass("active");
		$j("input[name='pemotongan_tipe']").val($j(this).data("type"));
		grandTotal();
	});
	$j('.modal#modalDetailCustomer').on('hidden.bs.modal', function (e){
		$j('.modal#modalDetailCustomer').find("table tbody").html("");
	})
	$j(".modal#modalDetailCustomer").on("click","button.btnSetIdCustomer",function(){
		var id = $j(this).data('id');
		var nama = $j(this).data('nama');
		var email = $j(this).data('email');
		var alamat = $j(this).data('alamat');
		var top = $j(this).data('jatuh-tempo');
		$j("form[name='form_transaksi_penjualan']").find("input[name='id_customer']").val(id);
		$j("form[name='form_transaksi_penjualan']").find("input[name='nama_customer']").val(nama);
		$j("form[name='form_transaksi_penjualan']").find("input[name='email']").val(email);
		$j("form[name='form_transaksi_penjualan']").find("textarea[name='alamat_customer']").val(alamat);
		$j("form[name='form_transaksi_penjualan']").find("input[name='top']").val(top);
		$j('.modal#modalDetailCustomer').modal('hide');
	})
	$j(".table-form").on("click",".btn-add-detail-barang",function(){
		setAddDataBarangTransaksiPenjualan();
	});
	$j(".table-form").on("click",".btn-hapus-add-detail-barang",function(){
		$j(this).parents("tr").remove();
		grandTotal();
	});
	$j(".table-form").on("change","select[name='id_barang[]']",function(){
		var row = $j(this).parents("tr");
		var deskripsi = $j(this).find(":selected").data("deskripsi");
		var satuan = $j(this).find(":selected").data("nama-satuan");
		var harga_jual = $j(this).find(":selected").data("harga-jual");
		if(harga_jual == "0"){
			$j(this).val("");
			$j(this).trigger("chosen:updated");
			row.find("input[name='deskripsi[]']").val("");
			row.find("input[name='satuan[]']").val("");
			row.find("input[name='harga_satuan[]']").val("");
			alert("Harga Jual untuk produk ini belum ditentukan, silahkan input di menu Master Data -> Harga Jual.");
		}else{
			row.find("input[name='deskripsi[]']").val(deskripsi);
			row.find("input[name='satuan[]']").val(satuan);
			row.find("input[name='harga_satuan[]']").val(harga_jual);
		}
	});
	$j(".table-form").on("keyup","input#qty, input#harga_satuan",function(){
		var xParent = $j(this).parents("tr");
		subTotal(xParent);
	});
	$j(".modal#modalDetailNoSeri").on("click",".btn-add-no-seri-detail",function(){
		var rowData = " <tr>"+
						  "<td>"+
							"<input class=\"form-control form-control-sm\" name=\"no_seri\" type=\"text\" placeholder=\"No Seri\">"+
						  "</td>"+
						  "<td>"+
							"<input class=\"form-control form-control-sm\" name=\"qty\" type=\"text\" placeholder=\"Qty\">"+
						  "</td>"+
						  "<td>"+
							"<button type=\"button\" class=\"btn btn-sm btn-flat btn-danger btn-del-no-seri-detail\"><i class=\"fa fa-minus\"></i></button>"+
						  "</td>"+
						"</tr>";
		$j(".modal#modalDetailNoSeri").find("tbody").append(rowData);
	});
	$j(".modal#modalDetailNoSeri").on("click",".btn-del-no-seri-detail",function(){
		var noseri = $j(this).parents("tr").find("input[name='no_seri']").val();
		var tmp = {};
		for(i in PG_TmpNoSeriOnTable){
			if(i != noseri){
				tmp[i] = PG_TmpNoSeriOnTable[i];
			}
		}
		PG_TmpNoSeriOnTable = tmp;
		$j(this).parents("tr").remove();
	});
	$j(".modal#modalDetailNoSeri").on("click",".btn-save-no-seri-detail",function(){
		PG_TmpNoSeriStok = {};
		PG_TmpNoSeriOnTable = {};
		var modal = $j(".modal#modalDetailNoSeri");
		var tbody = modal.find("tbody>tr");
		var data = {};
		var dNoSeri = {};
		var dQty = {};
		var tQty = 0;
		var n = 0;
		tbody.each(function(){
			var no_seri = $j(this).find("input[name='no_seri']").val();
			var qty     = $j(this).find("input[name='qty']").val();
			if(no_seri != "" || qty != ""){
				dNoSeri[n] = no_seri;
				dQty[n] = qty;
				data['no_seri'] = dNoSeri;
				data['qty'] = dQty;
				tQty += parseInt(qty);
				n++;
			}
		});
		var xParent = btnDetailNoSeri.parents("tr");
		xParent.find("input#detail_no_seri").val(JSON.stringify(data));
		xParent.find("input#qty").val(tQty);
		subTotal(xParent);
		$j('#modalDetailNoSeri').modal('hide');
	});
	$j(".table-form").on("click",".btnDetailNoSeri",function(){
		btnDetailNoSeri = $j(this);
		var dataRow = btnDetailNoSeri.parents("tr");
		var id_barang = dataRow.find("select#id_barang").val();
		if(id_barang != ""){
			//ambil data no seri dari stok
			$j.ajax({
				type:"POST",
				url:"<?php echo base_url()."admin/apiweb"; ?>",
				data:{action:"get_data_barang_stok",id_barang:id_barang},
				dataType:"json",
				success: function(obj){
					$j('#modalDetailNoSeri').modal('show');
					var detail_no_seri = dataRow.find("input#detail_no_seri").val();
					if(detail_no_seri != ""){
						var no_seri_set = [];
						var objDetail = JSON.parse(detail_no_seri);
						var objNoSeri = objDetail['no_seri'];
						var objQty = objDetail['qty'];
						for(x in objNoSeri){
							var n = objNoSeri[x];
							var q = objQty[x];
							no_seri_set[n] = q;
						}
					}

					var rowData = "";
					var objTmpStok = {};
					var objTmpValInput = {};
					//console.log(obj);
					for(x in obj){
						var no_seri = obj[x]['no_seri'];
						var qty = obj[x]['qty'];

						rowData += "<tr>"+
									  "<td>"+
										"<input type=\"text\" class=\"form-control form-control-sm\" name=\"no_seri\" type=\"text\" placeholder=\"No Seri\" value=\""+no_seri+"\" readonly=\"readonly\" >"+
									  "</td>"+
									  "<td>"+
										"<input type=\"number\" class=\"form-control form-control-sm\" name=\"qty\" type=\"text\" placeholder=\"Qty\" ";
						if(typeof(no_seri_set) != "undefined" && typeof(no_seri_set[no_seri]) != "undefined"){
							rowData	+= " data-max-qty=\""+(parseInt(qty)+parseInt(no_seri_set[no_seri]))+"\" value=\""+no_seri_set[no_seri]+"\" ";
							objTmpValInput[no_seri] = no_seri_set[no_seri];
							objTmpStok[no_seri] = parseInt(qty)+parseInt(no_seri_set[no_seri]);
						}else{
							rowData	+= " data-max-qty=\""+qty+"\" value=\"0\" ";
							objTmpStok[no_seri] = qty;
						}
						rowData += " >"+
									  "</td>"+
									  "<td>"+
										"<button type=\"button\" class=\"btn btn-sm btn-flat btn-danger btn-del-no-seri-detail\"><i class=\"fa fa-minus\"></i></button>"+
									  "</td>"+
									"</tr>";
					}
					//console.log(objTmpStok);
					var objTmpStokLength = Object.keys(objTmpStok).length;
					if(objTmpStok){
						var first_no_seri = Object.keys(objTmpStok)[0];
						var last_no_seri = Object.keys(objTmpStok)[(objTmpStokLength-1)];
					}else{
						var first_no_seri = "-";
						var last_no_seri = "-";
					}

					$j('#modalDetailNoSeri .first_no_seri').text(first_no_seri);
					$j('#modalDetailNoSeri .last_no_seri').text(last_no_seri);

					//alert(rowData);
					if(rowData == ""){
						rowData = "<tr><td colspan='5' class='text-center pt-4'>Stok barang tidak tersedia</td></tr>";
					}else{ //if(action == "tambah")
						rowData = "<tr><td colspan='5' class='text-center pt-4'>Silahkan masukan No Seri!</td></tr>";
					}
					$j(".modal#modalDetailNoSeri").find("tbody").html(rowData);
					PG_TmpNoSeriStok = objTmpStok;
					//console.log(objTmpStok);
					//console.log(objTmpValInput);
					if(detail_no_seri != ""){
						for(var i in objTmpValInput){
							var noseri = i;
							var qty = parseInt(objTmpValInput[i]);
							//console.log(noseri+" | "+qty);
							for(xi = 0; xi < qty; xi++){
								//console.log(noseri);
								barcodeNoSeriOnEnter(noseri);
							}
						}
					}else if(last_no_seri != "-"){
						barcodeNoSeriOnEnter(last_no_seri);
					}
				}
			});
			$j(".modal#modalDetailNoSeri").find("input[name='search_no_seri']").trigger('focus');
		}else{
			alert("Barang harus dipilih terlebih dahulu!");
		}
	});
	$j('.modal#modalDetailNoSeri').on('hidden.bs.modal', function (e) {
		btnDetailNoSeri = null;
		VG_skip = false;
		PG_TmpNoSeriStok = {};
		PG_TmpNoSeriOnTable = {};
		$j(".modal#modalDetailNoSeri").find("tbody").html("");
		$j(".modal#modalDetailNoSeri").find("input[name='search_no_seri']").val("");
	})
	$j(".modal#modalDetailNoSeri").on("focus keyup change blur ","input[name='qty']",function(){
		var noseri = $j(this).parents("tr").find("input[name='no_seri']").val();
		var val = parseInt($j(this).val());
		var max_qty = parseInt($j(this).data("max-qty"));
		if(val > max_qty){
			$j(this).val(max_qty);
			PG_TmpNoSeriOnTable[noseri]['qty'] = max_qty;
			alert("max qty no seri ini: "+max_qty);
		}else{
			PG_TmpNoSeriOnTable[noseri]['qty'] = val;
		}
	});
	$j(document).ready(function(){
		/*
		if(action == "edit"){
			var selCustomer = $j("form[name='form_transaksi_penjualan']").find("select[name='id_customer']").find(":selected");
			var alamat_customer = selCustomer.data('alamat');
			var top = selCustomer.data('jatuh-tempo');
			$j("form[name='form_transaksi_penjualan']").find("textarea[name='alamat_customer']").val(alamat_customer);
			$j("form[name='form_transaksi_penjualan']").find("input[name='top']").val(top);
		}
		*/
		<?php
			if($action == "tambah"){
				echo "setAddDataBarangTransaksiPenjualan();";
			}else if($action == "edit"){
				echo "loadDataDetailTransaksiPenjualan();";
			}
		?>
	});
</script>
<?php } ?>
