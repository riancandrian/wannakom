<?php
include './assets/pdf-php/src/Cezpdf.php';

$data = json_decode(base64_decode($get_download_data[0]->data));
$dataHead = $data->header;
$dataBody = $data->body;

$pdf = new Cezpdf('a4','landscape');
$pdf->ezSetMargins(20,20,20,20);
$pdf->selectFont('Times-Roman');

$cols = array();
foreach($dataHead as $key => $val){
	$cols[] = $val;
}

$data = array();
foreach($dataBody as $keyRow => $valRow){
	$data_sub = array();
	foreach($valRow as $key => $val){
		$data_sub[] = $val;
	}
	$data[] = $data_sub;
}
$tbconfig = array('width' => '800');
$pdf->ezTable($data, $cols, '', $tbconfig);

$pdf->ezStream(array('compress'=>0));
?>