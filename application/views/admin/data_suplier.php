<?php
$aksesKey = "admin/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug();
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}

	$no=0;
	$htm_table_data_suplier = "";
	foreach($get_data_suplier as $row){
		$htm_table_data_suplier.="
						<tr data-id=\"".$row->id."\">
							<th scope=\"row\">".($no+=1)."</th>
							<td>".$row->kode_suplier."</td>
							<td>".$row->nama_suplier."</td>
							<td>".$row->nama_group_suplier."</td>
							<td>".$row->nama_kota."</td>
							<td>".$row->telepon."</td>
							<td>".$row->jatuh_tempo."</td>
							<td>".btnStatLabel($row->status)."</td>
							<td>";
								if(isset($aksesUbah)) $htm_table_data_suplier.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/edit/".$row->id."\" class=\"btn btn-outline-success btn-sm\"><i class=\"fa fa-edit\"></i>&nbsp; Edit</a> ";
								if(isset($aksesHapus)) $htm_table_data_suplier.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/hapus/".$row->id."\" class=\"btn btn-outline-danger btn-sm\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";
		  $htm_table_data_suplier.="</td>
						</tr>
					";
	}
	if($htm_table_data_suplier == ""){
		$htm_table_data_suplier .= "<tr><th colspan='10' class=\"text-center\">. : Data Kosong : .</th></tr>";
		$htm_table_data_suplier .= "<tr><th colspan='10' class=\"text-center\">&nbsp;</th></tr>";
	}
	
	$htm_option_group_suplier = "";
	foreach($get_group_suplier as $row){
		$htm_option_group_suplier.="<option value=\"".$row->id."\" ".((isset($get_edit_data_suplier[0]->id_group_suplier) and $get_edit_data_suplier[0]->id_group_suplier == $row->id)?"selected=selected":"")." >".$row->nama_group_suplier."</option>";
	}
	
	if($action == "tambah"){
		$passRequired = " required='required' ";
	}else if($action == "edit"){
		$hintPassword = "<span class=\"help-block text-warning\">Kosongkan kolom Password jika tidak akan mengganti Password.</span>";
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>">Data Suplier <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-6">
		<form action="" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari" placeholder="Cari Data Suplier" class="form-control form-control-sm" required="required" />
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<?php if(isset($aksesTambah)){ ?>
		<a href="<?php echo base_url()."admin/".$this->router->fetch_method()."/tambah"; ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil"></i>&nbsp; Tambah</a>
		<?php } ?>
		<button type="button" class="btn btn-outline-warning btn-sm" id="bt_print"><i class="fa fa-print"></i>&nbsp; Cetak</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="thead-dark">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Kode Suplier</th>
					<th scope="col">Nama Suplier</th>
					<th scope="col">Group Suplier</th>
					<th scope="col">Kota</th>
					<th scope="col">No Telp</th>
					<th scope="col">JT</th>
					<th scope="col">Status</th>
					<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
					<th scope="col">Aksi</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php echo $htm_table_data_suplier; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>
<?php if($action == "tambah" or $action == "edit"){ ?>
<div class="card">
	<div class="card-body">
		<form method="post" action="">
			<input type="hidden" name="form_action" value="<?php echo $action; ?>">
			<input type="hidden" name="id_suplier" value="<?php echo @$get_edit_data_suplier[0]->id; ?>">
			<div class="form-group">
				<label>Group Suplier</label>
				<select data-placeholder="Pilih..." class="form-control standardSelect" name="id_group_suplier" required="required" tabindex="1">
					<option value="" label="Pilih..."></option>
					<?php echo $htm_option_group_suplier; ?>
				</select>
			</div>
			<div class="form-group">
				<label>Kode Suplier</label>
				<input type="text" class="form-control" name="kode_suplier" value="<?php echo @$get_edit_data_suplier[0]->kode_suplier; ?>" required="required" readonly="readonly">
			</div>
			<div class="form-group">
				<label>Nama Suplier</label>
				<input type="text" class="form-control" name="nama_suplier" value="<?php echo @$get_edit_data_suplier[0]->nama_suplier; ?>" required="required">
			</div>
			<div class="form-group">
				<label>Email</label>
				<input type="email" class="form-control" name="email" value="<?php echo @$get_edit_data_suplier[0]->email; ?>">
			</div>
			<div class="form-group">
				<label>Alamat</label>
				<textarea class="form-control" name="alamat" required="required"><?php echo @$get_edit_data_suplier[0]->alamat; ?></textarea>
			</div>
			<div class="form-group">
				<label>Kota</label>
				<input type="text" class="form-control" name="nama_kota" value="<?php echo @$get_edit_data_suplier[0]->nama_kota; ?>" required="required">
			</div>
			<div class="form-group">
				<label>Telepon</label>
				<input type="text" class="form-control" name="telepon" value="<?php echo @$get_edit_data_suplier[0]->telepon; ?>" required="required">
			</div>
			<div class="form-group">
				<label>Jatuh Tempo</label>
				<input type="text" class="form-control" name="jatuh_tempo" value="<?php echo @$get_edit_data_suplier[0]->jatuh_tempo; ?>" required="required">
			</div>
			<div class="form-group">
				<label>Status</label>
				<select class="form-control" name="status" required="required">
					<option value='1' <?php echo(isset($get_edit_data_suplier[0]->status) and $get_edit_data_suplier[0]->status == "1")?"selected=selected":""; ?>><?php echo statLabel("1"); ?></option>
					<option value='0' <?php echo(isset($get_edit_data_suplier[0]->status) and $get_edit_data_suplier[0]->status == "0")?"selected=selected":""; ?>><?php echo statLabel("0"); ?></option>
				</select>
			</div>
			<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>" class="btn btn-secondaray btn-flat">Back</a>
			<button type="submit" name="submit_data_suplier" class="btn btn-primary btn-flat">Submit</button>
		</form>
	</div>
</div>
<?php } ?>
<script>
	var VG_onpage_data_table = "tbl_suplier";
	$j(document).on("click","#bt_print",function(){
		printTableData();
	});
	$j("select[name='id_group_suplier']").change(function(){
		var id_group_suplier = $j(this).val();
		$j("input[name='kode_suplier']").val(id_group_suplier);
	});
	$j(".standardSelect").chosen({
		disable_search_threshold: 10,
		no_results_text: "Oops, nothing found!",
		width: "100%"
	});
</script>
<?php } ?>