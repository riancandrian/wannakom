<?php
$aksesKey = "admin/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug($_REQUEST);
	//debug($_FILES);
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}

	$no=0;
	$htm_table_data_promo = "";
	foreach($get_data_promo as $row){
		$htm_table_data_promo.="
						<tr data-id=\"".$row->id."\">
							<th scope=\"row\">".($no+=1)."</th>
							<td>".$row->tanggal."</td>
							<td>".$row->nomor."</td>
							<td>".$row->keterangan."</td>
							<td>".btnStatLabel($row->status)."</td>
							<td>";
								if(isset($aksesUbah)) $htm_table_data_promo.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/edit/".$row->id."\" class=\"btn btn-outline-success btn-sm\"><i class=\"fa fa-edit\"></i>&nbsp; Edit</a> ";
								if(isset($aksesHapus)) $htm_table_data_promo.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/hapus/".$row->id."\" class=\"btn btn-outline-danger btn-sm\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";
		  $htm_table_data_promo.="</td>
						</tr>
					";
	}
	if($htm_table_data_promo == ""){
		$htm_table_data_promo .= "<tr><th colspan='7' class=\"text-center\">. : Data Kosong : .</th></tr>";
		$htm_table_data_promo .= "<tr><th colspan='7' class=\"text-center\">&nbsp;</th></tr>";
	}
	
	if($action == "tambah"){
		$passRequired = " required='required' ";
	}else if($action == "edit"){
		$hintPassword = "<span class=\"help-block text-warning\">Kosongkan kolom Password jika tidak akan mengganti Password.</span>";
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>">Data Promo <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-6">
		<form action="" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari" placeholder="Cari Promo" class="form-control form-control-sm" required="required" />
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<?php if(isset($aksesTambah)){ ?>
		<a href="<?php echo base_url()."admin/".$this->router->fetch_method()."/tambah"; ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil"></i>&nbsp; Tambah</a>
		<?php } ?>
		<button type="button" class="btn btn-outline-warning btn-sm" id="bt_print"><i class="fa fa-print"></i>&nbsp; Cetak</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="thead-dark">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Tanggal</th>
					<th scope="col">Nomor</th>
					<th scope="col">Keterangan</th>
					<th scope="col">Status</th>
					<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
					<th scope="col">Aksi</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php echo $htm_table_data_promo; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>
<?php if($action == "tambah" or $action == "edit"){ ?>
<div class="card">
	<div class="card-body">
		<form method="post" action="" enctype="multipart/form-data" name="submit_data_promo">
			<input type="hidden" name="form_action" value="<?php echo $action; ?>">
			<input type="hidden" name="id_data_promo" value="<?php echo @$get_edit_data_promo[0]->id; ?>">
			<div class="form-group">
				<label>Tanggal</label>
				<input type="date" class="form-control" name="tanggal" value="<?php echo (isset($get_edit_data_promo[0]->tanggal))?$get_edit_data_promo[0]->tanggal:date('Y-m-d'); ?>" required="required">
				<!-- http://jsfiddle.net/g7mvaosL/ -->
			</div>
			<div class="form-group">
				<label>Nomor</label>
				<input type="text" class="form-control" name="nomor" value="<?php echo (isset($get_edit_data_promo[0]->nomor))?$get_edit_data_promo[0]->nomor:"[Auto Generate After Submit]"; ?>" required="required" readonly="readonly">
			</div>
			<div class="form-group">
				<label>Keterangan</label>
				<input type="text" class="form-control" name="keterangan" value="<?php echo @$get_edit_data_promo[0]->keterangan; ?>">
			</div>
			<div class="form-group">
				<label>Status</label>
				<select class="form-control" name="status" required="required">
					<option value='1' <?php echo(isset($get_edit_data_promo[0]->status) and $get_edit_data_promo[0]->status == "1")?"selected=selected":""; ?>><?php echo statLabel("1"); ?></option>
					<option value='0' <?php echo(isset($get_edit_data_promo[0]->status) and $get_edit_data_promo[0]->status == "0")?"selected=selected":""; ?>><?php echo statLabel("0"); ?></option>
				</select>
			</div>
			<table class="table table-form">
				<thead class="thead-dark">
					<tr>
						<th scope="col">#</th>
						<th scope="col" style="width:150px;">Nama Barang</th>
						<th scope="col">Deskripsi</th>
						<th scope="col">Satuan</th>
						<th scope="col" style="width:150px;">Periode Promo s/d</th>
						<th scope="col">Harga</th>
						<th scope="col">Gambar</th>
						<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
						<th scope="col" style="width:185px;" class="text-center">Aksi</th>
						<?php } ?>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
			<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>" class="btn btn-secondaray btn-flat">Back</a>
			<button type="submit" name="submit_data_promo" class="btn btn-primary btn-flat">Submit</button>
		</form>
	</div>
</div>
<?php } ?>
<script>
	var VG_onpage_data_table = "tbl_data_promo";
	var BASE_URL = "<?php echo base_url(); ?>";
	var event_target_files = [];
	var aksesHapus = "<?php echo @$aksesHapus; ?>";
	var action = "<?php echo $action; ?>";
	var no=0;
	
	function setAddButtonDataDetailPromoBrangtoTable(){
		$j(".btn-add-promo-barang").remove();
		var htm_table_form_detail_promo = "";
		htm_table_form_detail_promo+= "<tr class='btn-add-promo-barang'><th colspan='10'><button type=\"button\" class=\"btn btn-outline-success btn-sm\" >+ Tambah Barang</button></th></tr>";
		$j(".table-form tbody").append(htm_table_form_detail_promo);
		$j(".standardSelect").chosen({disable_search_threshold: 10,no_results_text: "Oops, nothing found!",width: "100%"});
	}
	function setAddDataDetailPromoBrangtoTable(){
		var htm_option_barang = "";
		var get_data_barang = JSON.parse(`<?php echo json_encode($get_data_barang); ?>`);
		for(var i in get_data_barang){
			var row_barang = get_data_barang[i];
			htm_option_barang+="<option value=\""+row_barang['id']+"\" data-deskripsi=\""+row_barang['deskripsi']+"\" data-nama-satuan=\""+row_barang['nama_satuan']+"\" >"+row_barang['nama_barang']+"</option>";
		}
		no++;
		var htm_table_form_detail_promo = "";
		htm_table_form_detail_promo+="<tr>"+
											"<input type=\"hidden\" name=\"no[]\" id=\"no\" value=\""+no+"\" disabled='disabled' />"+
											"<input type=\"hidden\" name=\"file_image_name[]\" id=\"file_image_name\" value=\"\" />"+
											"<th scope=\"row\">"+no+"</th>"+
											"<td>"+
												"<select data-placeholder=\"Cari Barang\" class=\"form-control standardSelect\" name=\"id_barang[]\" id=\"id_barang\" >"+
													"<option value=\"\" label=\"Pilih...\"></option>"+
													htm_option_barang+
												"</select>"+
											"</td>"+
											"<td><input type=\"text\" name=\"deskripsi[]\" id=\"deskripsi\" placeholder=\"Deskripsi\" class=\"form-control form-control-sm\" readonly=\"readonly\" /></td>"+
											"<td><input type=\"text\" name=\"satuan[]\" id=\"satuan\" placeholder=\"Satuan\" class=\"form-control form-control-sm\" readonly=\"readonly\" /></td>"+
											"<td><input type=\"date\" name=\"periode_sampai_dengan[]\" id=\"periode_sampai_dengan\" class=\"form-control form-control-sm\" style=\"width:150px;\" required=\"required\" value=\"<?php echo date("Y-m-d"); ?>\" /></td>"+
											"<td><input type=\"text\" name=\"harga[]\" id=\"harga\" placeholder=\"Harga\" class=\"form-control form-control-sm\" required=\"required\" /></td>"+
											"<td>"+
											"<label for=\"gambar_"+no+"\" class=\"btn btn-outline-secondary btn-sm\">Get Image</label>"+
											"<input type=\"file\" id=\"gambar_"+no+"\" name=\"gambar[]\" id=\"gambar\" style=\"display:none;\" />"+
											"</td>"+
											"<td class=\"text-center\">";
							if(action == "tambah"){
								htm_table_form_detail_promo+=" <button type=\"button\" class=\"btn btn-outline-danger btn-sm btn-hapus-add-detail-promo\" ><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</button> ";
							}else if(action == "edit"){
								htm_table_form_detail_promo+=" <button type=\"button\" class=\"btn btn-outline-danger btn-sm btn-hapus-add-detail-promo\" ><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</button> ";
								htm_table_form_detail_promo+=" <button type=\"button\" class=\"btn btn-outline-success btn-sm btn-add-detail-promo d-none\" >Commit</button> ";
							}
											htm_table_form_detail_promo+="</td></tr>";
		$j(".table-form tbody").append(htm_table_form_detail_promo);
		setAddButtonDataDetailPromoBrangtoTable();
	}
	function setDataDetailPromoBrangtoTable(dataJSON){
		var htm_table_form_detail_promo = "";
		var get_data_detail_promo_barang = JSON.parse(dataJSON);
		for(var x in get_data_detail_promo_barang){
			var row = get_data_detail_promo_barang[x];
			
			var htm_option_barang = "";
			var get_data_barang = JSON.parse(`<?php echo json_encode($get_data_barang); ?>`);
			for(var i in get_data_barang){
				var row_barang = get_data_barang[i];
				htm_option_barang +="<option value=\""+row_barang['id']+"\" data-deskripsi=\""+row_barang['deskripsi']+"\" data-nama-satuan=\""+row_barang['nama_satuan']+"\" ";
				if(typeof(row['id_barang']) !== "undefined" && row['id_barang'] == row_barang['id']) htm_option_barang += " selected=selected ";
				htm_option_barang +=" >"+row_barang['nama_barang']+"</option>";
			}
			
			no++;
			htm_table_form_detail_promo +=" <tr>"+
												"<input type=\"hidden\" name=\"no[]\" id=\"no\" value=\""+no+"\" />"+
												"<input type=\"hidden\" name=\"file_image_name[]\" id=\"file_image_name\" value=\""+row['gambar']+"\" />"+
												"<th scope=\"row\">"+no+"</th>"+
												"<td>"+
													"<select data-placeholder=\"Cari Barang\" class=\"form-control standardSelect\" name=\"id_barang[]\" id=\"id_barang\" required=\"required\" tabindex=\"0\">"+
														"<option value=\"\" label=\"Pilih...\"></option>"+
														htm_option_barang+
													"</select>"+
												"</td>"+
												"<td><input type=\"text\" name=\"deskripsi[]\" id=\"deskripsi\" placeholder=\"Deskripsi\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['deskripsi']+"\" /></td>"+
												"<td><input type=\"text\" name=\"satuan[]\" id=\"satuan\" placeholder=\"Satuan\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['nama_satuan']+"\" /></td>"+
												"<td><input type=\"date\" name=\"periode_sampai_dengan[]\" id=\"periode_sampai_dengan\" class=\"form-control form-control-sm\" style=\"width:150px;\" required=\"required\" value=\""+row['periode_sampai_dengan']+"\" /></td>"+
												"<td><input type=\"text\" name=\"harga[]\" id=\"harga\" placeholder=\"Harga\" class=\"form-control form-control-sm\" required=\"required\" value=\""+row['harga']+"\" /></td>"+
												"<td>";
												if(row['gambar'] == ""){
													htm_table_form_detail_promo+="<label for=\"gambar_"+no+"\" class=\"btn btn-outline-secondary btn-sm\">Get Image</label>";
												}else{
													htm_table_form_detail_promo+="<label for=\"gambar_"+no+"\" style=\"width:100%;height:80px;background: url('"+BASE_URL+"/assets/upload_product/100/"+row['gambar']+"');background-size:contain;background-repeat:no-repeat;background-position:center;\" >&nbsp;</label>";
												}
												htm_table_form_detail_promo+="<input type=\"file\" name=\"gambar[]\" id=\"gambar_"+no+"\" style=\"display:none;\" />"+
												"</td>"+
												"<td class=\"text-center\">";
													if(aksesHapus == "1") htm_table_form_detail_promo+=" <a href=\"javascript:void(0);\" class=\"btn btn-outline-danger btn-sm btn-hapus-detail-promo\" data-id=\""+row['id']+"\" ><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";
													htm_table_form_detail_promo+="<button type=\"button\" class=\"btn btn-outline-primary btn-sm btn-update-detail-promo d-none\" data-id=\""+row['id']+"\">Update</button> ";
													htm_table_form_detail_promo+="</td></tr>";
		}

		$j(".table-form tbody").html(htm_table_form_detail_promo);
		setAddButtonDataDetailPromoBrangtoTable();
	}
	function loadDataDetailPromoBrang(){
		$j.ajax({
			type:"POST",
			url:"<?php echo base_url()."admin/apiweb"; ?>",
			data:{action:"get_data_detail_promo_barang",id_data_promo:"<?php echo @$get_edit_data_promo[0]->id; ?>"},
			success: function(dataJSON){
				setDataDetailPromoBrangtoTable(dataJSON);
			}
		});
	}
	function cekIdBarangPromo(nObj){
		var i = 0;
		var thisSelect = nObj.find(":selected");
		$j("table.table-form").find("select[name='id_barang[]']").each(function(){
			var objSelect = $j(this);
			var value = objSelect.find(":selected").val();
			if(thisSelect.val() == value) i++;
			if(i>1){
				alert("Barang ini sudah di input");
				nObj.val("");
				nObj.parents("tr").find("input[name='deskripsi[]']").val("");
				nObj.parents("tr").find("input[name='satuan[]']").val("");
				nObj.trigger("chosen:updated");
			}
		});
	}
	$j(document).on("click","#bt_print",function(){
		printTableData();
	});
	$j(".table-form").on("change","select[name='id_barang[]']",function(){
		var obj_this = $j(this);
		var deskripsi = obj_this.find(":selected").data("deskripsi");
		var nama_satuan = obj_this.find(":selected").data("nama-satuan");
		var obj_row =  $j(this).parents("tr");
		obj_row.find("input[name='deskripsi[]']").val(deskripsi);
		obj_row.find("input[name='satuan[]']").val(nama_satuan);
		cekIdBarangPromo(obj_this);
	});
	$j(".table-form").on("click",".btn-hapus-detail-promo",function(){
		var obj_row =  $j(this).parents("tr");
		var id_data_detail_promo_barang = $j(this).data("id");
		var file_image_name = obj_row.find("input[name='file_image_name[]']").val();
		var c = confirm('Anda akan menghapus data ini?');
		if(c){
			$j.ajax({
				type:"POST",
				url:"<?php echo base_url()."admin/apiweb"; ?>",
				data:{action:"hapus_data_detail_promo_barang",id_data_detail_promo_barang:id_data_detail_promo_barang,file_image_name:file_image_name},
				success: function(res){
					alert(res);
					loadDataDetailPromoBrang();
				}
			});
		}
	});
	$j(".table-form").on("change","input[type='file']",function(event){
		//btn btn-outline-secondary btn-sm
		var obj_this =  $j(this);
		var obj_row =  obj_this.parents("tr");
		var no = obj_row.find("input[name='no[]']").val();
		event_target_files[no] = event.target.files;
		var file = event.target.files[0];
		//style=\"width:100%;height:80px;background: url('');background-size:contain;background-repeat:no-repeat;background-position:center;\"
		getBase64(file).then(data=>{
			var label = obj_this.prev("label");
			label.attr("class","");
			label.html("");
			label.attr("style","width:100%;height:80px;background: url('"+data+"');background-size:contain;background-repeat:no-repeat;background-position:center;");
			//console.log(label.attr("class"));
			//console.log(data);
		});
	});
	$j(".table-form").on("click",".btn-update-detail-promo",function(){
		var obj_row =  $j(this).parents("tr");
		var formData = new FormData();
		var no = obj_row.find("input[name='no[]']").val();
		var id_data_detail_promo_barang = $j(this).data("id");
		
		var id_barang = obj_row.find("select[name='id_barang[]']").val();
		var harga = obj_row.find("input[name='harga[]']").val();
		var periode_sampai_dengan = obj_row.find("input[name='periode_sampai_dengan[]']").val();
		var file_image_name = obj_row.find("input[name='file_image_name[]']").val();
		var gambar_storage = event_target_files[no];
		
		if(id_barang == "" || harga == "" || periode_sampai_dengan == ""){
			alert("Pastikan semua kolom telah terisi dengan benar!");
		}else{
			if(typeof(gambar_storage)!=="undefined"){
				var gambar = gambar_storage[0];
				formData.append("image", gambar);
			}
			
				formData.append("action", "edit_data_detail_promo_barang");
				formData.append("id_data_detail_promo_barang", id_data_detail_promo_barang);
				formData.append("id_barang", id_barang);
				formData.append("harga", harga);
				formData.append("periode_sampai_dengan", periode_sampai_dengan);
				formData.append("file_image_name", file_image_name);
				
				
			$j.ajax({
				type:"POST",
				url:"<?php echo base_url()."admin/apiweb"; ?>",
				data:formData,
				cache: false,
				contentType: false,
				processData: false,
				success: function(res){
					alert(res.replace(/<br\/>/g, "\n").replace(/<br>/g, "\n"));
					loadDataDetailPromoBrang();
				}
			});
		}
	});
	$j(".table-form").on("click",".btn-add-promo-barang",function(){
		setAddDataDetailPromoBrangtoTable();
	});
	$j(".table-form").on("click",".btn-hapus-add-detail-promo",function(){
		$j(this).parents("tr").remove();
	});
	$j(".table-form").on("click",".btn-add-detail-promo",function(){
		var obj_row =  $j(this).parents("tr");
		var formData = new FormData();
		var no = obj_row.find("input[name='no[]']").val();
		var id_data_detail_promo_barang = $j(this).data("id");
		
		var id_barang = obj_row.find("select[name='id_barang[]']").val();
		var harga = obj_row.find("input[name='harga[]']").val();
		var periode_sampai_dengan = obj_row.find("input[name='periode_sampai_dengan[]']").val();
		var file_image_name = obj_row.find("input[name='file_image_name[]']").val();
		var gambar_storage = event_target_files[no];
		
		if(id_barang == "" || harga == "" || periode_sampai_dengan == ""){
			alert("Pastikan semua kolom telah terisi dengan benar!");
		}else{
			if(typeof(gambar_storage)!=="undefined"){
				var gambar = gambar_storage[0];
				formData.append("image", gambar);
			}
			
				formData.append("action", "add_data_detail_promo_barang");
				formData.append("id_data_promo", "<?php echo @$get_edit_data_promo[0]->id; ?>");
				formData.append("id_data_detail_promo_barang", id_data_detail_promo_barang);
				formData.append("id_barang", id_barang);
				formData.append("harga", harga);
				formData.append("periode_sampai_dengan", periode_sampai_dengan);
				formData.append("file_image_name", file_image_name);
				
				
			$j.ajax({
				type:"POST",
				url:"<?php echo base_url()."admin/apiweb"; ?>",
				data:formData,
				cache: false,
				contentType: false,
				processData: false,
				success: function(res){
					alert(res.replace(/<br\/>/g, "\n").replace(/<br>/g, "\n"));
					loadDataDetailPromoBrang();
				}
			});
		}
	});
	$j("form[name='submit_data_promo']").submit(function(){
		$j(this).find("button.btn-update-detail-promo, button.btn-add-detail-promo").each(function(){
			$j(this).trigger("click");
		});
		return true;
	});
	$j(document).ready(function(){
		<?php
			if($action == "tambah"){
				echo "setAddDataDetailPromoBrangtoTable();";
			}else if($action == "edit"){
				echo "loadDataDetailPromoBrang();";
			}
		?>
	});
</script>
<?php } ?>