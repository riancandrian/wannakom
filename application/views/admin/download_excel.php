<?php
$data = json_decode(base64_decode($get_download_data[0]->data));
$dataHead = $data->header;
$dataBody = $data->body;
//debug($data);
$thead = "<tr>";
foreach($dataHead as $key => $val){
	$thead .= "<th>".$val."</th>";
}
$thead .= "</tr>";

$tbody = "";
foreach($dataBody as $keyRow => $valRow){
	$tbody .= "<tr>";
	foreach($valRow as $key => $val){
		$tbody .= "<td>".$val."</td>";
	}
	$tbody .= "</tr>";
}
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <title>Download File Excel</title>
</head>

<body>
	<style type="text/css">
		body{
			font-family: sans-serif;
		}
		table{
			margin: 20px auto;
			border-collapse: collapse;
		}
		table th,
		table td{
			border: 1px solid #3c3c3c;
			padding: 3px 8px;

		}
		a{
			background: blue;
			color: #fff;
			padding: 8px 10px;
			text-decoration: none;
			border-radius: 2px;
		}
	</style>
	<?php
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=file_excel.xls");
	?>
	<table border="1">
		<thead>
			<?php echo @$thead; ?>
		</thead>
		<tbody>
			<?php echo @$tbody; ?>
		</tbody>
	</table>
</body>
</html>
