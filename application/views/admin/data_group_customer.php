<?php
$aksesKey = "admin/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug();
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}

	$no=0;
	$htm_table_group_customer = "";
	foreach($get_group_customer as $row){
		$htm_table_group_customer.="
						<tr data-id=\"".$row->id."\">
							<th scope=\"row\">".($no+=1)."</th>
							<td>".$row->id."</td>
							<td>".$row->nama_group_customer."</td>
							<td>".btnStatLabel($row->status)."</td>
							<td>";
								if(isset($aksesUbah)) $htm_table_group_customer.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/edit/".$row->id."\" class=\"btn btn-outline-success btn-sm\"><i class=\"fa fa-edit\"></i>&nbsp; Edit</a> ";
								if(isset($aksesHapus) and $row->id > 3) $htm_table_group_customer.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/hapus/".$row->id."\" class=\"btn btn-outline-danger btn-sm\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";
		  $htm_table_group_customer.="</td>
						</tr>
					";
	}
	if($htm_table_group_customer == ""){
		$htm_table_group_customer .= "<tr><th colspan='7' class=\"text-center\">. : Data Kosong : .</th></tr>";
		$htm_table_group_customer .= "<tr><th colspan='7' class=\"text-center\">&nbsp;</th></tr>";
	}
	
	if($action == "tambah"){
		$passRequired = " required='required' ";
	}else if($action == "edit"){
		$hintPassword = "<span class=\"help-block text-warning\">Kosongkan kolom Password jika tidak akan mengganti Password.</span>";
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>">Group Customer <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-6">
		<form action="" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari" placeholder="Cari Group Customer" class="form-control form-control-sm" required="required" />
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<?php if(isset($aksesTambah)){ ?>
		<a href="<?php echo base_url()."admin/".$this->router->fetch_method()."/tambah"; ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil"></i>&nbsp; Tambah</a>
		<?php } ?>
		<button type="button" class="btn btn-outline-warning btn-sm" id="bt_print"><i class="fa fa-print"></i>&nbsp; Cetak</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="thead-dark">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Kode Group Customer</th>
					<th scope="col">Nama Group Customer</th>
					<th scope="col">Status</th>
					<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
					<th scope="col">Aksi</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php echo $htm_table_group_customer; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>
<?php if($action == "tambah" or $action == "edit"){ ?>
<div class="card">
	<div class="card-body">
		<form name="form_crud" method="post" action="">
			<input type="hidden" name="form_action" value="<?php echo $action; ?>">
			<input type="hidden" name="id_group_customer" value="<?php echo @$get_edit_group_customer[0]->id; ?>">
			<div class="form-group">
				<label>Kode Group Customer</label>
				<input type="text" class="form-control" name="kode_group_customer" value="<?php echo (isset($get_edit_group_customer[0]->id))?$get_edit_group_customer[0]->id:"[Auto Generate After Submit]"; ?>" required="required" readonly="readonly">
			</div>
			<div class="form-group">
				<label>Nama Group Customer</label>
				<input type="text" class="form-control" name="nama_group_customer" value="<?php echo @$get_edit_group_customer[0]->nama_group_customer; ?>" required="required">
				<span class="help-block color-red"></span>
			</div>
			<div class="form-group">
				<label>Status</label>
				<select class="form-control" name="status" required="required">
					<option value='1' <?php echo(isset($get_edit_group_customer[0]->status) and $get_edit_group_customer[0]->status == "1")?"selected=selected":""; ?>><?php echo statLabel("1"); ?></option>
					<option value='0' <?php echo(isset($get_edit_group_customer[0]->status) and $get_edit_group_customer[0]->status == "0")?"selected=selected":""; ?>><?php echo statLabel("0"); ?></option>
				</select>
			</div>
			<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>" class="btn btn-secondaray btn-flat">Back</a>
			<button type="submit" name="submit_group_customer" class="btn btn-primary btn-flat">Submit</button>
		</form>
	</div>
</div>
<?php } ?>
<script>
	var action = "<?php echo $action; ?>";
	var VG_onpage_data_table = "tbl_group_customer";
	var submit_nama_group_customer = true;
	var msg_alert_nama_group_customer = "";
	$j(document).on("click","#bt_print",function(){
		printTableData();
	});
	
	$j("form[name='form_crud']").on("keyup","input[name='nama_group_customer']",function(){
		var nama_group_customer_def = "<?php echo @$get_edit_group_customer[0]->nama_group_customer; ?>";
		var obj_nama_group_customer = $j(this);
		var nama_group_customer = obj_nama_group_customer.val();
		var data_table = VG_onpage_data_table;
		var data_where = {nama_group_customer:nama_group_customer,delete:0};
		if(action == "edit") data_where['nama_group_customer !='] = nama_group_customer_def;
		$j.ajax({
			type:"POST",
			url:"<?php echo base_url()."admin/apiweb"; ?>",
			data:{action:"if_exist_data_table",data_table:data_table,data_where:data_where},
			success: function(res){
				if(res > 0){
					submit_nama_group_customer = false;
					msg_alert_nama_group_customer = "Nama Group sudah ada!";
					obj_nama_group_customer.next().html(msg_alert_nama_group_customer);
				}else{
					submit_nama_group_customer = true;
					obj_nama_group_customer.next().html("");
				}
			}
		});
	});
	
	$j(document).on("submit","form[name='form_crud']",function(){
		if(submit_nama_group_customer == false && msg_alert_nama_group_customer !="") alert(msg_alert_nama_group_customer);
		
		if(submit_nama_group_customer === true)
			return true;
		else
			return false;
	});
	
</script>
<?php } ?>