<?php 
$aksesKey = $this->router->fetch_class()."/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug($get_admin_menu_hak);
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}

	$no=0;
	$htm_table_investor = "";
	foreach($get_data_investor as $row){
		$htm_table_investor.="
						<tr data-id=\"".$row->id."\">
							<th scope=\"row\">".($no+=1)."</th>
							<td>".repair_date($row->tanggal)."</td>
							<td>".$row->nomor."</td>
							<td>".$row->nama_investor."</td>
							<td>".$row->email."</td>
							<td>".format_rupiah($row->nilai_investasi)."</td>
							<td>".ucwords(str_replace("_"," ",$row->metode_bayar))."</td>
							<td>";
								if(isset($aksesUbah)) $htm_table_investor.=" <a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/edit/".$row->id."\" class=\"btn btn-outline-success btn-sm\"><i class=\"fa fa-edit\"></i>&nbsp; Edit</a> ";
								if(isset($aksesHapus)) $htm_table_investor.=" <a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/hapus/".$row->id."\" class=\"btn btn-outline-danger btn-sm\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";
		  $htm_table_investor.="</td>
						</tr>
					";
	}
	if($htm_table_investor == ""){
		$htm_table_investor .= "<tr><th colspan='15' class=\"text-center\">. : Data Kosong : .</th></tr>";
		$htm_table_investor .= "<tr><th colspan='15' class=\"text-center\">&nbsp;</th></tr>";
	}
	
	$get_metode_bayar = array('kas_tunai','transfer_bank','kartu_kredit');
	$htm_metode_bayar = "<option value=''>Pilih...</option>";
	foreach($get_metode_bayar as $row){
		$htm_metode_bayar .= "<option value='".$row."' ".((isset($get_edit_investor[0]->metode_bayar) and $get_edit_investor[0]->metode_bayar == $row)?"selected=selected":"").">".ucwords(str_replace("_"," ",$row))."</option>";
	}
	
	if($action == "tambah"){
		$tambahRequired = " required='required' ";
	}else if($action == "edit"){
		$editHintSkip = "<span class=\"help-block text-warning\">Biarkan kolom ini jika tidak akan menggantinya.</span>";
		$editRequired = " required='required' ";
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method(); ?>">Input Data Investor <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-6">
		<form action="" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari" placeholder="Cari Investor" class="form-control form-control-sm" required="required"/>
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<?php if(isset($aksesTambah)){ ?>
		<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/tambah"; ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil"></i>&nbsp; Tambah</a>
		<?php } ?>
		<button type="button" class="btn btn-outline-warning btn-sm" id="bt_print"><i class="fa fa-print"></i>&nbsp; Cetak</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="thead-dark">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Tanggal</th>
					<th scope="col">Nomor</th>
					<th scope="col">Nama Investor</th>
					<th scope="col">Email</th>
					<th scope="col">Nilai Investasi</th>
					<th scope="col">Metode Pembayaran</th>
					<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
					<th scope="col">Aksi</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php echo $htm_table_investor; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>
<?php if($action == "tambah" or $action == "edit"){ ?>
<div class="card">
	<div class="card-body">
		<form method="post" action="" name="form_data_investor" class="image-editor" enctype="multipart/form-data">
			<input type="hidden" name="form_action" value="<?php echo $action; ?>">
			<input type="hidden" name="id_investor" value="<?php echo @$get_edit_investor[0]->id; ?>">
			<div class="row">
				<div class="col-4">
					<div class="form-group">
						<label>No Faktur</label>
						<input type="text" class="form-control" name="nomor" value="<?php echo (isset($get_edit_investor[0]->nomor))?$get_edit_investor[0]->nomor:"[Auto Generate After Submit]"; ?>" readonly="readonly">
					</div>
					<div class="form-group">
						<label>Kode Investor</label>
						<input type="text" class="form-control" name="kode_investor" value="<?php echo (isset($get_edit_investor[0]->kode_investor))?$get_edit_investor[0]->kode_investor:"[Auto Generate After Submit]"; ?>" readonly="readonly">
					</div>
					<div class="form-group">
						<label>Nama Investor</label>
						<div class="input-group">
							<input type="text" class="form-control" name="nama_investor" placeholder="Nama Investor" value="<?php echo @$get_edit_investor[0]->nama_investor; ?>" required="required">
							<input type="hidden" name="id_customer"  value="<?php echo @$get_edit_investor[0]->id_customer; ?>" required="required">
							<div class="input-group-append">
								<span class="input-group-text"><i class="fa fa-search"></i></span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="email" class="form-control" name="email" value="<?php echo @$get_edit_investor[0]->email; ?>" readonly="readonly">
					</div>
					<div class="form-group">
						<label>Alamat</label>
						<textarea class="form-control" name="alamat"  readonly="readonly"><?php echo @$get_edit_investor[0]->alamat; ?></textarea>
					</div>
					<div class="form-group">
						<label>No NPWP</label>
						<input type="text" class="form-control" name="no_npwp" value="<?php echo @$get_edit_investor[0]->no_npwp; ?>" required="required">
					</div>
				</div>
				<div class="col-4">
					<!--
					<div class="form-group">
						<label>Foto Copy KTP</label>
						<input type="file" class="form-control-file cropit-image-input" name="foto_copy_ktp" <?php echo @$tambahRequired; ?>>
						<?php echo @$editHintSkip; ?>
						<?php if(isset($get_edit_investor[0]->foto_copy_ktp)) echo "<br/><img src='".base_url()."assets/upload_ktp_investor/display/".$get_edit_investor[0]->foto_copy_ktp."' style='max-width:100px;margin:10px 0px 0px 0px;' />"; ?>
						<div class="cropit-preview" style="width:200px;height:100px;"></div>
						<div class="image-size-label">Resize image</div>
						<input type="range" class="cropit-image-zoom-input" name="image-zoom"/>
						<input type="hidden" class="hidden-image-data" name="image-data"/>
						<input type="hidden" name="file_image_name" value="<?php echo @$get_edit_investor[0]->foto_copy_ktp; ?>"/>
					</div>
					-->
					<div class="form-group">
						<label>Foto Copy KTP</label>
						<?php 
							if(isset($get_edit_investor[0]->foto_copy_ktp)) 
								echo "<br/><img name='foto_copy_ktp_display' src='".base_url()."assets/upload_ktp_customer/display/".$get_edit_investor[0]->foto_copy_ktp."' style='max-width:100px;margin:10px 0px 0px 0px;' />";
							else
								echo "<br/><img name='foto_copy_ktp_display' src='https://cdn.shopify.com/s/assets/no-image-100-c91dd4bdb56513f2cbf4fc15436ca35e9d4ecd014546c8d421b1aece861dfecf_100x.gif' style='max-width:100px;margin:10px 0px 0px 0px;' />"
						?>
						<input type="hidden" name="foto_copy_ktp" value="<?php echo @$get_edit_investor[0]->foto_copy_ktp; ?>"/>
					</div>
					<div class="form-group">
						<label>Metode Pembayaran</label>
						<select class="form-control" name="metode_bayar" required="required">
							<?php echo $htm_metode_bayar; ?>
						</select>
					</div>
					<div class="form-group">
						<label>Tanggal</label>
						<input type="date" class="form-control" name="tanggal" value="<?php echo (isset($get_edit_investor[0]->tanggal))?date_format(date_create($get_edit_investor[0]->tanggal),"Y-m-d"):date("Y-m-d"); ?>" required="required">
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label class="w-100 text-center text-info">Nilai Investasi</label>
						<hr class="mt-0" style="border-top:3px solid rgba(0,0,0,.1);" />
						<input type="number" class="form-control" name="nilai_investasi" value="<?php echo @$get_edit_investor[0]->nilai_investasi; ?>" required="required">
					</div>
					<!--
					<div class="row">
						<div class="col-12">
							<label class="w-100 text-center text-info">Sistem Bagi Hasil</label>
							<hr class="mt-0" style="border-top:3px solid rgba(0,0,0,.1);" />
						</div>
						<div class="col-6">
							<label>Perusahaan</label>
							<div class="form-group input-group">
								<input type="text" class="form-control" name="bagi_hasil_perusahaan" value=
									"<?php 
										if(isset($get_edit_investor[0]->bagi_hasil_perusahaan)){
											$val = $get_edit_investor[0]->bagi_hasil_perusahaan;
										}else{
											if(isset($get_setting_app[0]->bagi_hasil_perusahaan)){
												$val = $get_setting_app[0]->bagi_hasil_perusahaan;
											}else{
												$val = "[Belum diseting]";
											}
										}
										echo $val; 
									?>" readonly="readonly" min="0" max="100">
								<div class="input-group-append">
									<span class="input-group-text">%</span>
								</div>
							</div>
						</div>
						<div class="col-6">
							<label>Investor</label>
							<div class="form-group input-group">
								<input type="text" class="form-control" name="bagi_hasil_investor" value=
									"<?php 
										if(isset($get_edit_investor[0]->bagi_hasil_investor)){
											$val = $get_edit_investor[0]->bagi_hasil_investor;
										}else{
											if(isset($get_setting_app[0]->bagi_hasil_investor)){
												$val = $get_setting_app[0]->bagi_hasil_investor;
											}else{
												$val = "[Belum diseting]";
											}
										}
										echo $val; 
									?>" readonly="readonly" min="0" max="100">
								<div class="input-group-append">
									<span class="input-group-text">%</span>
								</div>
							</div>
						</div>
					</div>
					-->
				</div>
			</div>
			<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method(); ?>" class="btn btn-secondaray btn-flat">Back</a>
			<button type="submit" name="submit_investor" class="btn btn-primary btn-flat">Submit</button>
		</form>
	</div>
</div>
<?php } ?>
<div class="modal" id="modalDetailCustomer" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Pilih Customer</span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<table class="table table-sm">
		  <thead>
			<tr>
			  <th class="border-top-0" scope="col">Kode</th>
			  <th class="border-top-0" scope="col">Nama Customer</th>
			  <th class="border-top-0" scope="col">Alamat</th>
			  <th class="border-top-0" scope="col">Kota</th>
			  <th class="border-top-0" scope="col">Telepon</th>
			  <th class="border-top-0" scope="col">Jatuh Tempo</th>
			</tr>
		  </thead>
		  <tbody>
		  </tbody>
		</table>
      </div>
    </div>
  </div>
</div>
<script>
	var VG_onpage_data_table = "tbl_data_investor";
	var BASE_URL = "<?php echo base_url(); ?>";
	var aksesHapus = "<?php echo @$aksesHapus; ?>";
	var action = "<?php echo $action; ?>";
	var btnDetailNoSeri = null;
	var no=0;
	$j('.modal#modalDetailCustomer').on('hidden.bs.modal', function (e){
		$j('.modal#modalDetailCustomer').find("table tbody").html("");
	})
	$j(".modal#modalDetailCustomer").on("click","button.btnSetIdCustomer",function(){
		var id = $j(this).data('id');
		var nama = $j(this).data('nama');
		var email = $j(this).data('email');
		var alamat = $j(this).data('alamat');
		var image_ktp = $j(this).data('image-ktp');
		var top = $j(this).data('jatuh-tempo');
		$j("form[name='form_data_investor']").find("input[name='id_customer']").val(id);
		$j("form[name='form_data_investor']").find("input[name='nama_investor']").val(nama);
		$j("form[name='form_data_investor']").find("input[name='email']").val(email);
		$j("form[name='form_data_investor']").find("textarea[name='alamat']").val(alamat);
		
		$j("form[name='form_data_investor']").find("input[name='foto_copy_ktp']").val(image_ktp);
		$j("form[name='form_data_investor']").find("img[name='foto_copy_ktp_display']").attr("src",BASE_URL+"assets/upload_ktp_customer/display/"+image_ktp);
		
		$j('.modal#modalDetailCustomer').modal('hide');
	})
	$j("form[name='form_data_investor']").on("keypress","input[name='nama_investor']",function(event){
		var value = $j(this).val();
		var keycode = (event.keyCode ? event.keyCode : event.which);
		var ret = true;
		if(keycode == '13'){
			$j.ajax({
				type:"POST",
				url:"<?php echo base_url()."admin/apiweb"; ?>",
				data:{action:"get_data_customer",bt_cari:1,tx_cari:value},
				dataType:"json",
				success: function(obj){
					var htmTblData = "";
					for(x in obj){
						var row = obj[x];
						htmTblData += "<tr>"+
										"<td>"+row['kode_customer']+"</td>"+
										"<td><button type=\"button\" class=\"btn btn-link btnSetIdCustomer\" data-id=\""+row['id']+"\" data-nama=\""+row['nama_customer']+"\" data-email=\""+row['email']+"\" data-alamat=\""+row['alamat']+"\" data-image-ktp=\""+row['image_ktp']+"\" data-jatuh-tempo=\""+row['jatuh_tempo']+"\" >"+row['nama_customer']+"</button></td>"+
										"<td>"+row['alamat']+"</td>"+
										"<td>"+row['nama_kota']+"</td>"+
										"<td>"+row['telepon']+"</td>"+
										"<td>"+row['jatuh_tempo']+"</td>"+
									"</tr>";
					}
					$j('.modal#modalDetailCustomer').find("table tbody").html(htmTblData);
					$j('.modal#modalDetailCustomer').modal('show');
				}
			});
			ret = false;
		}
		return ret;
	});
	$j("form[name='form_data_investor']").submit(function(){
		var imageData = $j('.image-editor').cropit('export');
		$j('.hidden-image-data').val(window.btoa(imageData));
	});
	$j(document).on("click","#bt_print",function(){
		printTableData();
	});
	$j(document).ready(function(){
		$j('.image-editor').cropit();
	});
</script>
<?php } ?>