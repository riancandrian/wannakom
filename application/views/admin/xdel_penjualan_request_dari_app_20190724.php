<?php 
$aksesKey = $this->router->fetch_class()."/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug();
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}
	
	$no=0;
	$htm_table_data_mobile_order = "";
	//dd($get_data_mobile_order);
	foreach($get_data_mobile_order as $row){
		$htm_table_data_mobile_order.="
										<tr data-id=\"".$row->id."\">
											<th scope=\"row\">".($no+=1)."</th>
											<td>".str_replace(" ","<br/>",$row->tgl_input)."</td>
											<td>".$row->nama_customer."</td>
											<td>".$row->nama_toko."</td>
											<td>".$row->alamat_tujuan."</td>
											<td>".$row->catatan."</td>
											<td>".$row->pembayaran."</td>
											<td>".format_rupiah($row->total_bayar)."</td>
											<td>".format_rupiah($row->ongkir)."</td>											
											
												<td>";
													if(isset($aksesUbah) and $row->status_apply == "0") 
														$htm_table_data_mobile_order.=" <a href=\"javascript:void(0);\" class=\"btn btn-sm btn-success\" data-id-order=\"".$row->id."\" data-detail-order=\"".@$row->data_detail_order."\" onclick=\"showDataDetailOrderBeforeApply(this,".@$row->id_group_customer.");\" title=\"Menuggu konfirmasi admin\">View Order</a> ";
													else if(isset($aksesUbah) and $row->status_apply == "1") 
														$htm_table_data_mobile_order.=" <a href=\"javascript:void(0);\" class=\"btn btn-sm btn-secondary\" data-id-order=\"".$row->id."\" data-detail-order=\"".@$row->data_detail_order."\" onclick=\"showDataDetailOrderAfterApply(this);\" title=\"Order sukses\">View Order</a> ";
													else if(isset($aksesUbah) and $row->status_apply == "2") 
														$htm_table_data_mobile_order.=" <a href=\"javascript:void(0);\" class=\"btn btn-sm btn-warning\" data-id-order=\"".$row->id."\" data-detail-order=\"".@$row->data_detail_order."\" onclick=\"showDataDetailOrderAfterApply(this);\" title=\"Menunggu konfirmasi customer\">View Order</a>";
							  $htm_table_data_mobile_order.="</td>
											
											
											
												<!--
												<td>";
													if(isset($aksesUbah) and $row->status_apply == "0") 
														$htm_table_data_mobile_order.=" <a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/apply/".$row->id."\" class=\"btn btn-success btn-sm\"  onclick=\"return confirm('Anda akan Apply Order ini?');\"><i class=\"fa fa-edit\"></i>&nbsp; Apply</a> ";
													else
														$htm_table_data_mobile_order.=" <a href=\"javascript:void(0);\" class=\"btn btn-secondary btn-sm disabled\" ><i class=\"fa fa-edit\"></i>&nbsp; Apply</a> ";
													//if(isset($aksesHapus)) $htm_table_data_mobile_order.=" <a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/hapus/".$row->id."\" class=\"btn btn-outline-danger btn-sm\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";
							  $htm_table_data_mobile_order.="</td>
											-->
										</tr>
									";
	}
	
	if($htm_table_data_mobile_order == ""){
		$htm_table_data_mobile_order .= "<tr><th colspan='15' class=\"text-center\">. : Data Kosong : .</th></tr>";
	}
		$htm_table_data_mobile_order .= "<tr><th colspan='15'>Menampilkan ".(($no>0)?1:0)." .. ".$no." dari ".$no." Baris</th></tr>";
		
	if($action == "tambah"){
		$tambahRequired = " required='required' ";
	}else if($action == "edit"){
		$editRequired = " required='required' ";
	}
?>
<style>
	.privatElment{
		display:none;
	}
</style>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method(); ?>">Request Pembelian Dari Mobile App <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-6">
		<form action="" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari" placeholder="Pencarian..." class="form-control form-control-sm" required="required" />
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari"><i class="fa fa-search"></i></button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<!--
		<?php if(isset($aksesTambah)){ ?>
		<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/tambah"; ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil"></i>&nbsp; Tambah</a>
		<?php } ?>
		-->
		<button type="button" class="btn btn-outline-warning btn-sm" id="bt_print"><i class="fa fa-print"></i>&nbsp; Cetak</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="bg-info">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Tanggal</th>
					<th scope="col">Nama Customer</th>
					<th scope="col">Nama Toko</th>
					<th scope="col">Alamat Tujuan</th>
					<th scope="col">Catatan</th>
					<th scope="col">Cara Pembayaran</th>
					<th scope="col">Total Bayar</th>
					<th scope="col">Ongkir</th>
					<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
					<th scope="col">Aksi</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php echo $htm_table_data_mobile_order; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>

<!-- Modal -->
<div class="modal" id="modalDataDetailOrder" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
	  <form action="" method="post" name="formApplyOrder">
		<input type="hidden" name="form_action" value="apply">
		<input type="hidden" name="id_order" value="">
		<div class="modal-content">
		  <div class="modal-header">
			<span class="modal-title">Detail Order</span>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<table class="table table-sm">
			  <thead>
				<tr>
				  <th class="border-top-0" scope="col">Nama Barang</th>
				  <th class="border-top-0" scope="col">Qty Pesanan</th>
				  <th class="border-top-0" scope="col">Real Stock</th>
				  <th class="border-top-0 privatElment" scope="col">Accept Qty</th>
				  <th class="border-top-0" scope="col">Harga Jual</th>
				  <th class="border-top-0" scope="col">Sub Total</th>
				  <!--<th class="border-top-0" scope="col">Expire Date</th>-->
				  
				</tr>
			  </thead>
			  <tbody>
			  </tbody>
			</table>
		  </div>
		  <div class="modal-footer">
			<div class="row">
				<div class="col text-right font-weight-bold">
					Grand Total : <span class="grand-total">Rp 0,-</span>
				</div>
			</div>
		  </div>
		  <div class="modal-footer">
			<div class="row">
				<div class="col form-inline">
					  <!--
					  <div class="form-group mx-sm-3 mb-2">
						<label>Jatuh Tempo</label>
						<input type="date" name="jatuh_tempo" class="form-control mx-sm-3" required="required">
					  </div> 
					  -->
					  <button type="submit" class="btn btn-success mb-2">Apply</button>
				</div>
			</div>
		  </div>
		</div>
	  </form>
  </div>
</div>
<script>
	var VG_onpage_data_table = "tbl_transaksi_penjualan";
	var BASE_URL = "<?php echo base_url(); ?>";
	var aksesHapus = "<?php echo @$aksesHapus; ?>";
	var action = "<?php echo $action; ?>";
	var btnDetailNoSeri = null;
	var no=0;
	function showDataDetailOrderBeforeApply(obj, id_group_customer){
		var privatElment =(id_group_customer == 2 || id_group_customer == 3)?true:false;
		var detail_order = JSON.parse(atob($j(obj).data("detail-order")));
		var htmDetailOrder = "";
		var tSubTotal = 0;
		for(var i in detail_order){
			var row = detail_order[i];
			tSubTotal += parseInt(row['sub_total']);
			htmDetailOrder += "<tr idrow="+row['id_barang']+" >"+
								"<input type=\"hidden\" name=\"id_barang["+row['id_barang']+"]\" value=\""+row['id_barang']+"\">"+
								"<input type=\"hidden\" name=\"nama_barang["+row['id_barang']+"]\" value=\""+row['nama_barang']+"\">"+
								"<input type=\"hidden\" name=\"harga_jual["+row['id_barang']+"]\" value=\""+row['harga_jual']+"\">"+
								 "<td>"+row['nama_barang']+"</td>"+
								 "<td><input type=\"number\" name=\"qty["+row['id_barang']+"]\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['qty']+"\"></td>"+
								 "<td><input type=\"number\" name=\"real_stock["+row['id_barang']+"]\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\"0\"></td>"+
								 "<td class=\"privatElment\"><input type=\"number\" name=\"accept_qty["+row['id_barang']+"]\" class=\"form-control form-control-sm\" value=\""+row['qty']+"\" data-harga-jual=\""+row['harga_jual']+"\"></td>"+
								 "<td>"+format_rupiah(row['harga_jual'])+"</td>"+
								 "<td class=\"sub_total\">"+format_rupiah(row['sub_total'])+"</td>"+
								 //"<td><input type=\"date\" name=\"expire_date["+row['id_barang']+"]\" class=\"form-control form-control-sm\" required=\"required\"></td>"+
							   "</tr>";
		}
		
		$j('.modal#modalDataDetailOrder').modal('show');
		
		$j('.modal#modalDataDetailOrder').find("input[name='id_order']").val($j(obj).data("id-order"));
		$j('.modal#modalDataDetailOrder').find("input[name='jatuh_tempo']").prop("disabled",false);
		$j('.modal#modalDataDetailOrder').find("table tbody").html(htmDetailOrder);
		$j('.modal#modalDataDetailOrder').find(".grand-total").html(format_rupiah(tSubTotal));
		$j('.modal#modalDataDetailOrder').find("button[type='submit']").prop("disabled",true);
		
		if(privatElment){
			$j(".privatElment").show();
		}else{
			$j(".privatElment").hide();
		}
		
		$j.ajax({
			type:"POST",
			url:"<?php echo base_url()."admin/apiweb"; ?>",
			data:{action:"get_real_stock"},
			dataType:"JSON",
			success: function(obj){
				var buttonSubmitDis = false;
				for(var i in obj){
					var r = obj[i];
					var id_barang = r.id_barang;
					var qty_stock = parseInt(r.qty);
					var qty_current = parseInt($j('.modal#modalDataDetailOrder').find("table tbody tr[idrow='"+id_barang+"'] input[name^='qty']").val());
					$j('.modal#modalDataDetailOrder').find("table tbody tr[idrow='"+id_barang+"'] input[name^='real_stock']").val(qty_stock);
					if(qty_stock < qty_current){
						buttonSubmitDis = true;
					}
				}
				if(buttonSubmitDis === true){
					alert("Transaksi tidak dapat dilakukan, \nAda qty barang yang kurang!");
				}
				$j('.modal#modalDataDetailOrder').find("button[type='submit']").prop("disabled",buttonSubmitDis);
			}
		});
	}
	function showDataDetailOrderAfterApply(obj){
		var detail_order = JSON.parse(atob($j(obj).data("detail-order")));
		var htmDetailOrder = "";
		var tSubTotal = 0;
		for(var i in detail_order){
			var row = detail_order[i];
			tSubTotal += parseInt(row['sub_total']);
			htmDetailOrder += "<tr idrow="+row['id_barang']+" >"+
								 "<td>"+row['nama_barang']+"</td>"+
								 "<td><input type=\"number\" name=\"qty\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['qty']+"\"></td>"+
								 "<td><input type=\"number\" name=\"real_stock\" class=\"form-control form-control-sm\" disabled=\"disabled\" value=\"0\"></td>"+
								 "<td>"+format_rupiah(row['harga_jual'])+"</td>"+
								 "<td>"+format_rupiah(row['sub_total'])+"</td>"+
								 //"<td><input type=\"date\" name=\"expire_date["+row['id_barang']+"]\" class=\"form-control form-control-sm\" disabled=\"disabled\"></td>"+
								 
							   "</tr>";
		}
		
		$j('.modal#modalDataDetailOrder').modal('show');
		
		
		$j('.modal#modalDataDetailOrder').find("input[name='id_order']").val($j(obj).data("id-order"));
		$j('.modal#modalDataDetailOrder').find("input[name='jatuh_tempo']").prop("disabled",true);
		$j('.modal#modalDataDetailOrder').find("table tbody").html(htmDetailOrder);
		$j('.modal#modalDataDetailOrder').find(".grand-total").html(format_rupiah(tSubTotal));
		$j('.modal#modalDataDetailOrder').find("button[type='submit']").prop("disabled",true);
		
	}
	$j(document).on("click","#bt_print",function(){
		printTableData();
	});
	$j(document).on("click","#bt_print_preview",function(){
		var data = $j(this).parents("div.card").html();
		printTableData("<div class='card'>"+data+"</div>");
	});
	$j('.modal#modalDataDetailOrder').on('hidden.bs.modal', function (e){
		$j('.modal#modalDataDetailOrder').find("table tbody").html("");
		$j('.modal#modalDataDetailOrder').find(".grand-total").html("Rp 0,-");
	})
	$j('.modal#modalDataDetailOrder').on("change","[name^='accept_qty']", function (e){
		var row = $j(this).parents("tr");
		var value = parseInt($j(this).val());
		var harga_jual = parseInt($j(this).data("harga-jual"));
		var sub_total = (harga_jual*value);
		row.find("td.sub_total").html(format_rupiah(sub_total));
	})
	$j("form[name='formApplyOrder']").submit(function(){
		//return false;
	});
	$j(document).ready(function(){
		firebaseTblInitialize.update({"status_order_baru" : "0"});
	});
</script>
<?php } ?>