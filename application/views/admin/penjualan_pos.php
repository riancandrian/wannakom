<div class="row">
  <div class="col-6">
    <div class="card">
      <div class="card-body">
        <form class="image-editor">
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <label>Nomor Transaksi</label>
                <input type="text" class="form-control" name="" placeholder="Generate Otomatis" readonly="">
              </div>

              <div class="form-group">
                <label>Tanggal</label>
                <input type="date" class="form-control" onkeyup="send()" id="tgl">
              </div>

              <div class="form-group">
                <label>Customer</label>
                <select class="form-control" id="cust" onkeyup="send()" name="">
                  <?php foreach ($customer as $x): ?>
                    <option value="<?=$x->id?>" > <?=$x->nama_customer?> </option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>

            <div class="col-6">
              <div class="form-group">
                <label>Keterangan</label>
                <textarea class="form-control" id="ket" rows="4" onkeyup="send()"></textarea>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-12">
              <hr>
            </div>

            <div class="col-7">
              <div class="form-group">
              <input type="text" class="form-control" id="pencarian" placeholder="Cari Barang">
            </div>
            </div>

            <div class="col-5 ">
              <label class="pull-right">User : Yovi</label>
            </div>
          </div>

          <div class="row">
            <div class="col-12">
              <table class="table table-form" id="tb_data">
                <thead class="bg-info">
                  <th>Kode Barang</th>
                  <th>Nama Barang</th>
                  <th>Satuan</th>
                  <th>Pick</th>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="col-6">
    <div class="card">
      <div class="card-body">
        <form class="image-editor" id="f_data">
          <div class="row">
            <div class="col-12">
              <h2 align="center">Check Out</h2>
              <br>
            </div>
          </div>

          <!-- FORM SEBELAH KIRI -->
          <input type="hidden" name="tanggal_kirim" value="">
          <input type="hidden" name="customer_kirim" value="">
          <input type="hidden" name="keterangan_kirim" value="">

          <div class="row">
            <div class="col-12">
              <table class="table table-form" id="tb_order">
                <thead class="bg-info">
                  <th width="5%">#</th>
                  <th width="15%">Kode Barang</th>
                  <th width="35%">Nama Barang</th>
                  <th width="15%">Qty</th>
                  <th width="15%">Harga Jual</th>
                  <th width="15%">Sub Total</th>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>

          <hr>
          <div class="row">
            <div class="col-3">
              <div class="form-group">
                <label>Total</label>
              </div>
              <div class="form-group">
                <label>Discount (%)</label>
              </div>
            </div>

              <div class="col-4">
              <div class="form-group">
                <input type="text" class="form-control" id="total" name="total" readonly>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" onkeyup="hitungDiskon()" id="diskon" value="0" name="diskon">
              </div>
            </div>
          </div>

          <hr>
          <div class="row">
            <div class="col-3">
              <h3>Grand Total</h3>
            </div>

            <div class="col-4">
              <h3 style="color: green;" id="grandTotal"></h3>
            </div>
          </div>

          <br>
          <div class="row">
            <div class="col-12">
              <button type="button" class="btn bg-info btn-block" id="open_payment" ><b style="color: white" id="grandTotalPay"></b></button>
            </div>
          </div>

          <!-- MODAL PAYMENT -->
          <div class="modal fade" id="payment" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
              <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="smallmodalLabel">Pembayaran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                  <div class="row">
                    <div class="col-4">
                      <div class="form-group">
                        <label>Jumlah Tagihan</label>
                      </div>

                      <div class="form-group">
                        <label>Metode Pembayaran</label>
                      </div>

                      <div class="form-group bank">
                        <label>Bank</label>
                      </div>

                      <div class="form-group">
                        <label>Uang Bayar</label>
                      </div>

                      <div class="form-group">
                        <label>Kembali</label>
                      </div>
                    </div>

                    <div class="col-6">
                      <div class="form-group">
                        <input type="text" class="form-control" name="grand_total" id="modal_tagihan" readonly>
                      </div>

                      <div class="form-group">
                        <select class="form-control" name="" id="metode">
                          <option value="Tunai">Tunai</option>
                          <option value="Debet">Debet</option>
                          <option value="Kredit">Kredit</option>
                        </select>
                      </div>

                      <div class="form-group bank">
                        <select class="form-control" name="">
                          <option value="BCA-021">BCA | 02138955544 | Test 1</option>
                          <option value="MDR-021">MDR | 02138955544 | Test 2</option>
                          <option value="BNI-021">BNI | 02138955544 | Test 3</option>
                        </select>
                      </div>

                      <div class="form-group">
                        <input type="text" class="form-control" name="pembayaran" id="modal_uang" onkeyup="hitungKembalian()">
                      </div>

                      <div class="form-group">
                        <input type="text" class="form-control" name="kembali" id="modal_kembalian" readonly>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                  <button type="submit" class="btn btn-info ">Bayar</button>
                </div>
              </div>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-6 text-right">
    <a href="" class="btn btn-outline-danger"><i class="fa fa-remove"></i>&nbsp; Cancel Order</a>
    <button type="button" class="btn btn-outline-success" id="bt_print"><i class="fa fa-save"></i>&nbsp; Hold Order</button>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script type="text/javascript">
  updateList();

  $(document).ready(function(){
    $('.bank').hide();

    $('#pencarian').on('keyup', function(){
      var value = $(this).val();

      $.ajax({
        url : '<?=base_url('pos/cari_barang')?>',
        data: {value: value},
        type: 'POST',
        success: function(result){
          var jsonData = JSON.parse(result);
          $('#tb_data tbody').empty();

          if(jsonData.success){
            $.each(jsonData.data, function(key, val){
              var tr = "<tr>\
                          <td>"+val.kode_barang+"</td>\
                          <td>"+val.nama_barang+"</td>\
                          <td>"+val.nama_satuan+"</td>\
                          <td>\
                            <button type='button' class='btn btn-outline-secondary btn-sm' onclick={pick('"+val.id+"')}><i class='fa fa-cart-plus'></i></button>\
                          </td>\
                        </tr>";

              $('#tb_data tbody').append(tr);
            });
          }
        }
      });
    });

    $('#open_payment').on('click', function(){
      $('#payment').modal('show');
    });

    $('#metode').on('change', function(){
      var pilihan = $(this).val();

      if(pilihan == 'Debet'){
        $('.bank').show();
      }else{
        $('.bank').hide();
      }
    });

    $('form#f_data').submit(function(e){
      e.preventDefault();
      var formData = new FormData(this);

      $.ajax({
          url: '<?=base_url('pos/simpan')?>',
          type: 'POST',
          data: formData,
          success: function (data) {
              var jsonData = JSON.parse(data);

              if(jsonData.success){
                alert("Data Berhasil disimpan");
                // window.open(url+'invoice/cetakBenang/'+jsonData.invid);
                location.replace('index');
              }else{
                alert("Data Gagal disimpan");
              }
          },
          cache: false,
          contentType: false,
          processData: false
      });
    });

  });

  function send(){

    var tanggal = $('#tgl').val();
    var customer = $('#cust').val();
    var keterangan = $('#ket').val();

    $('input[name="tanggal_kirim"]').val(tanggal);
    $('input[name="customer_kirim"]').val(customer);
    $('input[name="keterangan_kirim"]').val(customer);
  }

  function pick(id){
    $.ajax({
      url : '<?=base_url('pos/insert_tmp')?>',
      data: {idbarang: id},
      type: 'POST',
      success: function(result){
        var jsonData = JSON.parse(result);

        if(jsonData.success){
          updateList();
        }else{
          alert("Barang sudah ada di list");
        }
      }
    });
  }

  function updateList(){
    $.ajax({
      url : '<?=base_url('pos/get_tmp')?>',
      type: 'POST',
      success: function(result){
        var jsonData = JSON.parse(result);
        $('#tb_order tbody').empty();
        var total = 0;

        if(jsonData.success){
          $.each(jsonData.data, function(key, val){
            var sub = val.qty * val.harga;
            total += sub;

            var tr = "<tr>\
                        <td><button type='button' class='btn btn-outline-secondary btn-sm' onclick='hapus("+val.id+")'><i class='fa fa-trash'></i></button></td>\
                        <td>"+val.kode_barang+"\
                          <input type='hidden' name='idbarang[]' value='"+val.idbarang+"'>\
                          <input type='hidden' name='idtemp[]' value='"+val.id+"'></td>\
                        <td>"+val.nama_barang+"</td>\
                        <td><input type='number' value='"+val.qty+"' class='form-control' name='qty[]' id='qty"+val.id+"' onchange='hitung("+val.id+")'></td>\
                        <td><input type='number' value='"+val.harga+"' class='form-control' name='harga[]' id='harga"+val.id+"' onchange='hitung("+val.id+")'></td>\
                        <td><input type='text' value='"+sub+"' class='form-control' name='sub[]' readonly id='nilai"+val.id+"'></td>\
                      </tr>";

            $('#tb_order tbody').append(tr);
          });
        }
        $('#total').val(total);
        hitungDiskon();
      }
    })
  }

  function hitung(id){
    var qty   = $('#qty'+id).val();
    var harga = $('#harga'+id).val();
    var nilai = qty * harga;

    $('#nilai'+id).val(nilai);
    saveNilai(id);
  }

  function saveNilai(id){
    var qty   = $('#qty'+id).val();
    var harga = $('#harga'+id).val();

    $.ajax({
      url : '<?=base_url('pos/update_tmp')?>',
      type: 'POST',
      data: {id: id, qty: qty, harga: harga},
      success: function(result){
        var jsonData = JSON.parse(result);

        if(jsonData.success){
          updateList();
        }
      }
    })
  }

  function hapus(id){
    $.ajax({
      url : '<?=base_url('pos/hapus_tmp')?>',
      type: 'POST',
      data: {id: id},
      success: function(result){
        var jsonData = JSON.parse(result);

        if(jsonData.success){
          updateList();
        }
      }
    })
  }

  function hitungDiskon(){
    var total  = $('#total').val();
    var diskon = $('#diskon').val();
    var nilai  = (diskon/100) * total;

    var hargaAkhir = total - nilai;
    $('#grandTotal').html('Rp. '+hargaAkhir);
    $('#grandTotalPay').html('Pay ( Rp. '+hargaAkhir+' )');
    $('#modal_tagihan').val(hargaAkhir);
  }

  function hitungKembalian(){
    var tagihan = $('#modal_tagihan').val();
    var uang_bayar = $('#modal_uang').val();

    var kembalian = 0;

    if(uang_bayar > tagihan){
      kembalian = uang_bayar - tagihan;
    }else{
      kembalian = 0;
    }

    $('#modal_kembalian').val(kembalian);

  }
</script>
