<?php 
$aksesKey = $this->router->fetch_class()."/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug();
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}
	
	$no=0;
	$htm_table_retur_penjualan = "";
	foreach($get_laporan_retur_penjualan as $row){
		$txtNoSeri = "";
		if($row->no_seri != ""){
			$objNS = json_decode($row->no_seri);
			$obNoSeri = $objNS->no_seri;
			$obQty = $objNS->qty;
			foreach($obNoSeri as $key => $val){
				$no_seri = $obNoSeri->$key;
				$qty = $obQty->$key;
				$txtNoSeri .= $no_seri." : ".$qty."<br/>";
			}
		}
		$htm_table_retur_penjualan.="
										<tr>
											<td scope=\"row\">".($no+=1)."</td>
											<td>".repair_date2($row->tanggal)."</td>
											<td>".$row->no_retur_penjualan."</td>
											<td>".$row->nama_customer."</td>
											<td>".$row->no_transaksi_penjualan."</td>
											<td>".repair_date($row->jatuh_tempo)."</td>
											<td>".$row->nama_barang."</td>
											<td>".$txtNoSeri."</td>
											<td>".$row->qty_retur."</td>
											<td>".$row->nama_satuan."</td>
											<td>".format_rupiah($row->harga_satuan)."</td>
											<td>".format_rupiah($row->sub_total)."</td>
											<td>".$row->pesan."</td>
										</tr>
									";
	}
	
	if($htm_table_retur_penjualan == ""){
		$htm_table_retur_penjualan .= "<tr><th colspan='20' class=\"text-center\">. : Data Kosong : .</th></tr>";
	}
		$htm_table_retur_penjualan .= "<tr><th colspan='20'>Menampilkan ".(($no>0)?1:0)." .. ".$no." dari ".$no." Baris</th></tr>";
		
	if($action == "tambah"){
		$tambahRequired = " required='required' ";
	}else if($action == "edit"){
		$editRequired = " required='required' ";
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method(); ?>">Laporan Retur Penjualan <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row mb-3">
	<div class="col-9">
		<form action="" method="post" class="form-horizontal form-inline" name="form_laporan_filter">
			<div class="form-group">
				<input type="date" class="form-control form-control-sm" name="tanggal_dari" value="<?php echo(isset($_REQUEST['tanggal_dari']))?$_REQUEST['tanggal_dari']:date('Y-m-d'); ?>"/>
			</div>
			<div class="form-group">
				&nbsp;S/D&nbsp;
			</div>
			<div class="form-group">
				<input type="date" class="form-control form-control-sm" name="tanggal_ke" value="<?php echo(isset($_REQUEST['tanggal_ke']))?$_REQUEST['tanggal_ke']:date('Y-m-d'); ?>"/>
			</div>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<div class="form-group">
				<select class="form-control form-control-sm" name="periode">
					<option value="">Pilih...</option>
					<option value="hari_ini">Hari Ini</option>
					<option value="minggu_ini">Minggu Ini</option>
					<option value="bulan_ini">Bulan Ini</option>
					<option value="tahun_ini">Tahun Ini</option>
					<option value="kemarin">Kemarin</option>
					<option value="minggu_lalu">Minggu Lalu</option>
					<option value="bulan_lalu">Bulan Lalu</option>
					<option value="tahun_lalu">Tahun Lalu</option>
				</select>
			</div>
			<script>
				$j("select[name='periode']").val("<?php echo @$_REQUEST['periode']; ?>");
			</script>
			&nbsp;
			<button type="submit" class="btn btn-outline-info btn-sm" name="bt_rentang"><i class="fa fa-search"></i></button>
		</form>
	</div>
	<div class="col-3 text-right">
		<button type="button" class="btn btn-outline-success btn-sm btn-export" data-type="to_excel"><i class="fa fa-file-excel-o"></i>&nbsp; to Excel</button>
		<button type="button" class="btn btn-outline-danger btn-sm btn-export" data-type="to_pdf"><i class="fa fa-file-pdf-o"></i>&nbsp; to PDF</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="bg-info">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Tanggal</th>
					<th scope="col">Nomor</th>
					<th scope="col">Customer</th>
					<th scope="col">Nomor FJ</th>
					<th scope="col">Tanggal JT</th>
					<th scope="col">Nama Barang</th>
					<th scope="col">No Seri</th>
					<th scope="col">Qty Retur</th>
					<th scope="col">Satuan</th>
					<th scope="col">Harga Satuan</th>
					<th scope="col">Sub Total</th>
					<th scope="col">Keterangan</th>
				</tr>
			</thead>
			<tbody>
				<?php echo $htm_table_retur_penjualan; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>

<script>
	var VG_onpage_data_table = "tbl_retur_penjualan";
	var BASE_URL = "<?php echo base_url(); ?>";
	var aksesHapus = "<?php echo @$aksesHapus; ?>";
	var action = "<?php echo $action; ?>";
	var btnDetailNoSeri = null;
	var no=0;
	$j("form[name='form_laporan_filter']").on("change","select[name='periode']",function(){
		var val = $j(this).val();
			var Dt = new Date();
			var dayTo = String(Dt.getDay() + 1).padStart(2, '0');
			var dd = String(Dt.getDate()).padStart(2, '0');
			var mm = String(Dt.getMonth() + 1).padStart(2, '0');
			var yyyy = Dt.getFullYear();
			var today = yyyy+'-'+mm+'-'+dd;
		if(val == "hari_ini"){
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_dari']").val(today);
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_ke']").val(today);
		}
		else if(val == "minggu_ini"){
			var this_week = yyyy+'-'+mm+'-'+String(dd-dayTo+1).padStart(2, '0');
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_dari']").val(this_week);
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_ke']").val(today);
		}
		else if(val == "bulan_ini"){
			var this_mount = yyyy+'-'+mm+'-01';
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_dari']").val(this_mount);
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_ke']").val(today);
		}
		else if(val == "tahun_ini"){
			var this_year = yyyy+'-01-01';
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_dari']").val(this_year);
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_ke']").val(today);
		}
		else if(val == "kemarin"){
			Dt.setDate(Dt.getDate() - 1);
			var dd = String(Dt.getDate()).padStart(2, '0');
			var mm = String(Dt.getMonth() + 1).padStart(2, '0');
			var yyyy = Dt.getFullYear();
			var yesterday = yyyy+'-'+mm+'-'+dd;
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_dari']").val(yesterday);
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_ke']").val(yesterday);
		}
		else if(val == "minggu_lalu"){
			Dt.setDate(Dt.getDate() - 7);
			var dayTo = String(Dt.getDay() + 1).padStart(2, '0');
			var dd = String(Dt.getDate()).padStart(2, '0');
			var mm = String(Dt.getMonth() + 1).padStart(2, '0');
			var yyyy = Dt.getFullYear();
			var last_week_start = yyyy+'-'+mm+'-'+String(dd-dayTo+1).padStart(2, '0');
			var last_week_end = yyyy+'-'+mm+'-'+String((dd-dayTo+1)+6).padStart(2, '0');
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_dari']").val(last_week_start);
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_ke']").val(last_week_end);
		}
		else if(val == "bulan_lalu"){
			//Dt.setMonth(Dt.getMonth() - 1);
			Dt.setDate(0);
			var dd = String(Dt.getDate()).padStart(2, '0');
			var mm = String(Dt.getMonth() + 1).padStart(2, '0');
			var yyyy = Dt.getFullYear();
			var last_mount_start = yyyy+'-'+mm+'-01';
			var last_mount_end = yyyy+'-'+mm+'-'+dd;
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_dari']").val(last_mount_start);
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_ke']").val(last_mount_end);
		}
		else if(val == "tahun_lalu"){
			//Dt.setMonth(Dt.getMonth() - 1);
			Dt.setMonth(0);
			Dt.setDate(0);
			var dd = String(Dt.getDate()).padStart(2, '0');
			var mm = String(Dt.getMonth() + 1).padStart(2, '0');
			var yyyy = Dt.getFullYear();
			var last_year_start = yyyy+'-01-01';
			var last_year_end = yyyy+'-'+mm+'-'+dd;
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_dari']").val(last_year_start);
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_ke']").val(last_year_end);
		}
	});
	$j("button.btn-export").click(function(){
		var data_type = $j(this).data("type");
		var dtH = {};
		var i = 0;
		$j("table.table-data").find("thead tr th").each(function(){
			var dt = $j(this).html();
			dtH[i] = dt;
			i++;
		});
		
		var dtB = {};
		var i = 0;
		$j("table.table-data").find("tbody tr").each(function(){
			var dtR = {};
			var ir = 0;
			$j(this).find("td").each(function(){
				var dt = $j(this).html();
				if(String(dt).trim() != ""){
					dtR[ir] = dt;
					ir++;		
				}				
			});
			dtB[i] = dtR;
			i++;
		});
		var dataFD = {'header':dtH,'body':dtB};
		var encData = btoa(JSON.stringify(dataFD));
		if(data_type == "to_excel"){
			var export_type = "file_excel";
		}else if(data_type == "to_pdf"){
			var export_type = "file_pdf";
		}
		$j.ajax({
			type:"POST",
			url:"<?php echo base_url()."admin/apiweb"; ?>",
			data:{action:"post_download_data",type:export_type,data:encData},
			dataType:"json",
			success: function(obj){
				if(obj == true){
					window.open("<?php echo base_url()."download/"; ?>"+export_type,"_blank");
				}else{
					alert("error download, code: "+obj);
				}
			}
		});
	});
	$j(document).ready(function(){
		
	});
</script>
<?php } ?>