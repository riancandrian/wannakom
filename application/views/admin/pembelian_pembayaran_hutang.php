<?php 
$aksesKey = $this->router->fetch_class()."/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug();
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}
	
	$no=0;
	$htm_table_pembayaran_hutang = "";
	foreach($get_pembayaran_hutang as $row){
		$htm_table_pembayaran_hutang.="
										<tr data-id=\"".$row->id."\">
											<th scope=\"row\">".($no+=1)."</th>
											<td>".repair_date($row->tanggal)."</td>
											<td>".$row->nomor."</td>
											<td>".$row->nama_suplier."</td>
											<td>".str_replace(", ","</br>",$row->no_faktur_pembelian)."</td>
											<td>".format_rupiah($row->pembayaran)."</td>
											<td>".$row->pesan."</td>
											<td>".btnStatLabel($row->status)."</td>
											<td>";
												if(isset($aksesUbah)) $htm_table_pembayaran_hutang.=" <a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/edit/".$row->id."\" class=\"btn btn-outline-success btn-sm\"><i class=\"fa fa-edit\"></i>&nbsp; Edit</a> ";
												if(isset($aksesHapus)) $htm_table_pembayaran_hutang.=" <a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/hapus/".$row->id."\" class=\"btn btn-outline-danger btn-sm\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";
						  $htm_table_pembayaran_hutang.="</td>
										</tr>
									";
	}
	
	if($htm_table_pembayaran_hutang == ""){
		$htm_table_pembayaran_hutang .= "<tr><th colspan='10' class=\"text-center\">. : Data Kosong : .</th></tr>";
	}
		$htm_table_pembayaran_hutang .= "<tr><th colspan='10'>Menampilkan ".(($no>0)?1:0)." .. ".$no." dari ".$no." Baris</th></tr>";

	$htm_option_suplier ="<option value=\"\" data-alamat=\"\" >Pilih...</option>";
	foreach($get_data_suplier as $row){
		$htm_option_suplier.="<option value=\"".$row->id."\" ".((isset($get_edit_pembayaran_hutang[0]->id_suplier) and $get_edit_pembayaran_hutang[0]->id_suplier == $row->id)?"selected=selected":"")." >".$row->nama_suplier."</option>";
	}
	if($action == "tambah"){
		$tambahRequired = " required='required' ";
	}else if($action == "edit"){
		$editRequired = " required='required' ";
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method(); ?>">Pembayaran Hutang <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-6">
		<form action="" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari" placeholder="Pencarian..." class="form-control form-control-sm" required="required" />
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari"><i class="fa fa-search"></i></button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<?php if(isset($aksesTambah)){ ?>
		<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/tambah"; ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil"></i>&nbsp; Tambah</a>
		<?php } ?>
		<button type="button" class="btn btn-outline-warning btn-sm" id="bt_print"><i class="fa fa-print"></i>&nbsp; Cetak</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="bg-info">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Tanggal</th>
					<th scope="col">Nomor</th>
					<th scope="col">Supplier</th>
					<th scope="col">No FB</th>
					<th scope="col">Pembayaran</th>
					<th scope="col">Pesan</th>
					<th scope="col">Status</th>
					<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
					<th scope="col">Aksi</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php echo @$htm_table_pembayaran_hutang; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>
<?php if($action == "tambah" or $action == "edit"){ ?>
<div class="card">
	<div class="card-body">
		<form method="post" action="" enctype="multipart/form-data" name="form_pembayaran_hutang" class="image-editor">
			<input type="hidden" name="form_action" value="<?php echo $action; ?>">
			<input type="hidden" name="id_pembayaran_hutang" value="<?php echo @$get_edit_pembayaran_hutang[0]->id; ?>">
			<div class="row">
				<div class="col-4">
					<div class="form-group">
						<label>Nomor</label>
						<input type="text" class="form-control" name="nomor" value="<?php echo (isset($get_edit_pembayaran_hutang[0]->nomor))?$get_edit_pembayaran_hutang[0]->nomor:"[Auto Generate After Submit]"; ?>" readonly="readonly">
					</div>
					<div class="form-group">
						<label>Tanggal</label>
						<input type="date" class="form-control" name="tanggal" value="<?php echo (isset($get_edit_pembayaran_hutang[0]->tanggal))?date_format(date_create($get_edit_pembayaran_hutang[0]->tanggal),'Y-m-d'):date('Y-m-d'); ?>" required="required">
					</div>
					<div class="form-group">
						<label>Cara Pembayaran</label>
						<select data-placeholder="Pilih..." class="form-control" name="cara_bayar" tabindex="1" required="required">
							<option value="" label="Pilih..."></option>
							<option value="kas_tunai" label="Kas Tunai"></option>
							<option value="transfer_bank" label="Transfer Bank"></option>
							<option value="kartu_kredit" label="Kartu Kredit"></option>
						</select>
						<script>
							$j("select[name='cara_bayar']").val("<?php echo @$get_edit_pembayaran_hutang[0]->cara_bayar; ?>");
						</script>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label>Supplier</label>
						<!--
						<select data-placeholder="Pilih..." class="form-control standardSelect" name="id_suplier" tabindex="1">
							<option value="" label="Pilih..."></option>
							<?php echo $htm_option_suplier; ?>
						</select>
						-->
						<div class="input-group">
							<input type="text" class="form-control" name="nama_suplier" placeholder="Nama Suplier" value="<?php echo @$get_edit_pembayaran_hutang[0]->nama_suplier; ?>" required="required">
							<input type="hidden" name="id_suplier"  value="<?php echo @$get_edit_pembayaran_hutang[0]->id_suplier; ?>" required="required">
							<div class="input-group-append">
								<span class="input-group-text"><i class="fa fa-search"></i></span>
							</div>
						</div>
					</div>
					<div class="form-group d-none">
						<label>Jatuh Tempo</label>
						<input type="date" class="form-control" name="jatuh_tempo" value="<?php echo (isset($get_edit_pembayaran_hutang[0]->jatuh_tempo))?$get_edit_pembayaran_hutang[0]->jatuh_tempo:date('Y-m-d', strtotime('+5 years')); ?>" required="required">
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<h3 class="font-weight-bold">Total</h3>
						<h2 class="text-total text-primary font-weight-bold">Rp. 0,-</h2>
					</div>
				</div>
			</div>
			<table class="table table-form">
				<thead class="bg-info">
					<tr>
						<th scope="col">#</th>
						<th scope="col">No Faktur Pembelian</th>
						<th scope="col">Keterangan</th>
						<th scope="col">Jatuh Tempo</th>
						<th scope="col">Nilai Hutang</th>
						<th scope="col">Total Retur</th>
						<th scope="col">Sisa Hutang</th>
						<th scope="col">Pembayaran</th>
						<th scope="col">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<tr><th colspan="10" class="text-center">. : Data Belum Tersedia : .</th></tr>
					<tr><th colspan="10">&nbsp;</th></tr>
				</tbody>
			</table>
			<div class="row">
				<div class="col-6">
					<div class="form-group">
						<label>Pesan</label>
						<textarea class="form-control" name="pesan"><?php echo @$get_edit_pembayaran_hutang[0]->pesan; ?></textarea>
					</div>
				</div>
				<div class="col-6">
					<div class="row">
						<div class="col-6">
							Pemotongan
						</div>
						<div class="col-6">
							&nbsp;
						</div>
						<div class="col-6">
							<div class="input-group input-group-sm">
							  <input type="number" class="form-control" name="pemotongan" placeholder="0" value="<?php echo (isset($get_edit_pembayaran_hutang[0]->pemotongan))?$get_edit_pembayaran_hutang[0]->pemotongan:'0'; ?>">
							  <input type="hidden" name="pemotongan_tipe" value="<?php echo (isset($get_edit_pembayaran_hutang[0]->pemotongan_tipe))?$get_edit_pembayaran_hutang[0]->pemotongan_tipe:'persen'; ?>">
							  <div class="input-group-append">
								<button class="btn btn-outline-secondary btn-pemotongan-type <?php echo (isset($get_edit_pembayaran_hutang[0]->pemotongan_tipe) and $get_edit_pembayaran_hutang[0]->pemotongan_tipe == 'persen')?'active':(!isset($get_edit_pembayaran_hutang[0]->pemotongan_tipe))?'active':''; ?>" type="button" data-type="persen">%</button>
								<button class="btn btn-outline-secondary btn-pemotongan-type <?php echo (isset($get_edit_pembayaran_hutang[0]->pemotongan_tipe) and $get_edit_pembayaran_hutang[0]->pemotongan_tipe == 'nominal')?'active':''; ?>" type="button" data-type="nominal">Rp</button>
							  </div>
							</div>
						</div>
						<div class="col-6 text-right text-pemotongan">
							Rp. 0,-
						</div>
						<div class="col-6">
							Total
							<input type="hidden" name="total" value="<?php echo @$get_edit_faktur_pembelian[0]->total; ?>">
						</div>
						<div class="col-6 text-right text-total">
							Rp. 0,-
						</div>
					</div>
				</div>
			</div>
			<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method(); ?>" class="btn btn-outline-danger btn-sm btn-flat">Kembali</a>
			<button type="submit" name="submit_pembayaran_hutang" class="btn btn-success btn-sm btn-flat">Submit</button>
		</form>
	</div>
</div>
<?php } ?>
<div class="modal" id="modalDetailSuplier" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Pilih Suplier</span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<table class="table table-sm">
		  <thead>
			<tr>
			  <th class="border-top-0" scope="col">Kode</th>
			  <th class="border-top-0" scope="col">Nama Suplier</th>
			  <th class="border-top-0" scope="col">Alamat</th>
			  <th class="border-top-0" scope="col">Kota</th>
			  <th class="border-top-0" scope="col">Telepon</th>
			  <th class="border-top-0" scope="col">Jatuh Tempo</th>
			</tr>
		  </thead>
		  <tbody>
		  </tbody>
		</table>
      </div>
    </div>
  </div>
</div>
<script>
	var VG_onpage_data_table = "tbl_pembayaran_hutang";
	var BASE_URL = "<?php echo base_url(); ?>";
	var aksesHapus = "<?php echo @$aksesHapus; ?>";
	var action = "<?php echo $action; ?>";
	var action_id = "<?php echo $action_id; ?>";
	var btnDetailNoSeri = null;
	function grandTotal(){
		var form = $j("form[name='form_pembayaran_hutang']");
		var pemotongan = form.find("input[name='pemotongan']").val();
		var pemotongan_tipe = form.find(".btn-pemotongan-type.active").data("type");
		var tgrand_total = 0;
		var tsisa_tagihan = 0;
		var tpembayaran = 0;
		
		form.find("table.table-form tbody").find("tr").each(function(){
			let row = $j(this);
			let grand_total = row.find("input#grand_total").val();
			let sisa_tagihan = row.find("input#sisa_tagihan").val();
			let pembayaran = row.find("input#pembayaran").val();
				if(pembayaran == "") pembayaran = 0;
			tgrand_total += parseInt(grand_total);
			tsisa_tagihan += parseInt(sisa_tagihan);
			tpembayaran += parseInt(pembayaran);
		});
		
		
		if(pemotongan_tipe == "persen")
			var pemotongan_value = parseInt(tpembayaran)*parseInt(pemotongan)/100;
		else if(pemotongan_tipe == "nominal")
			var pemotongan_value = parseInt(pemotongan);
		
		form.find(".text-pemotongan").html(formatRupiah(pemotongan_value));
		form.find("input[name='total']").val((tpembayaran-pemotongan_value));
		form.find(".text-total").html(formatRupiah((tpembayaran-pemotongan_value)));
	}
	function setDataSuplierFakturPembeliantoTable(dataJSON){
		var htm_table_form_detail_faktur_pembelian = "";
		var get_faktur_pembelian = JSON.parse(dataJSON);
		var no=0;
		console.log(get_faktur_pembelian);
		for(var x in get_faktur_pembelian){
			
			var row = get_faktur_pembelian[x];
			var pembayaran = 0;
			var sisa_tagihan = 0;
			if(typeof(row['pembayaran']) !== "undefined" && row['pembayaran'] !== null){
				var pembayaran = row['pembayaran'];
			}
			sisa_tagihan = (parseInt(row['sisa_tagihan'])+parseInt(pembayaran)); //
			//if(sisa_tagihan > 0){}
				no++;
				htm_table_form_detail_faktur_pembelian +=" <tr>"+
																"<input type=\"hidden\" name=\"id_faktur_pembelian[]\" id=\"id_faktur_pembelian\" value=\""+row['id']+"\" />"+
																"<input type=\"hidden\" name=\"no[]\" id=\"no\" value=\""+no+"\" disabled='disabled' />"+
																"<th scope=\"row\">"+no+"</th>"+
																"<td><input type=\"text\" name=\"nomor[]\" id=\"nomor\" placeholder=\"Nomor\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['nomor']+"\" /></td>"+
																"<td><input type=\"text\" name=\"pesan[]\" id=\"pesan\" placeholder=\"Pesan\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['pesan']+"\" /></td>"+
																"<td><input type=\"text\" name=\"jatuh_tempo_f[]\" id=\"jatuh_tempo_f\" placeholder=\"Jatuh Tempo\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['jatuh_tempo']+"\" style=\"width: 100px;\" /></td>"+
																"<td><input type=\"number\" name=\"grand_total[]\" id=\"grand_total\" placeholder=\"Grand Total\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['grand_total']+"\" /></td>"+
																"<td><input type=\"number\" name=\"total_retur[]\" id=\"total_retur\" placeholder=\"Total Retur\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['total_retur']+"\" /></td>"+
																"<td><input type=\"number\" name=\"sisa_tagihan[]\" id=\"sisa_tagihan\" placeholder=\"Sisa Tagihan\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+sisa_tagihan+"\" /></td>"+
																"<td><input type=\"number\" name=\"pembayaran[]\" id=\"pembayaran\" placeholder=\"Pembayaran\" class=\"form-control form-control-sm\" required=\"required\" value=\""+pembayaran+"\" /></td>"+
																"<td class=\"text-center\">";
																	htm_table_form_detail_faktur_pembelian+=" <button type=\"button\" class=\"btn btn-outline-danger btn-sm btn-hapus-add-detail-barang\" ><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</button> ";
																htm_table_form_detail_faktur_pembelian+="</td>"+
																"</tr>";
			
		}
		if(no == 0){
			htm_table_form_detail_faktur_pembelian = "<tr><th colspan='10' class=\"text-center\">. : Data Kosong : .</th></tr>";
		}
		$j(".table-form tbody").html(htm_table_form_detail_faktur_pembelian);
		grandTotal();
	}
	function loadSuplierFakturPembelian(obj){
		var id_suplier = $j("form[name='form_pembayaran_hutang']").find("input[name='id_suplier']").val();
		var jatuh_tempo = $j("form[name='form_pembayaran_hutang']").find("input[name='jatuh_tempo']").val();
		//console.log(id_suplier); console.log(jatuh_tempo);
		if(id_suplier != "" && jatuh_tempo != ""){
			var act = "";
			if(action == "tambah")
				act = "get_faktur_pembelian";
			else if(action == "edit")
				act = "get_pembayaran_hutang_detail_faktur";
			$j.ajax({
				type:"POST",
				url:"<?php echo base_url()."admin/apiweb"; ?>",
				data:{action:act,id_suplier:id_suplier,jatuh_tempo:jatuh_tempo,id_pembayaran_hutang:"<?php echo @$get_edit_pembayaran_hutang[0]->id; ?>"},
				success:function(dataJSON){
					setDataSuplierFakturPembeliantoTable(dataJSON);
				}
			});
		}else{
			setDataSuplierFakturPembeliantoTable("[]");
		}
	}
	function deleteRow(obj){
		$j(obj).parents('tr').hide();
	}
	$j(document).on("click","#bt_print",function(){
		printTableData();
	});
	$j(".standardSelect").chosen({
		disable_search_threshold: 10,
		no_results_text: "Oops, nothing found!",
		width: "100%"
	});
	$j("form[name='form_pembayaran_hutang']").submit(function(er){
		var ret = true;
		var id_suplier = $j(this).find("input[name='id_suplier']").val();
		var jatuh_tempo = $j(this).find("input[name='jatuh_tempo']").val();
		if(id_suplier == "" || jatuh_tempo == ""){
			alert("Kolom Suplier/Jatuh Tempo belum di isi!");
			ret = false;
		}
		return ret;
	});
	$j("form[name='form_pembayaran_hutang']").on("change","select[name='id_suplier'], input[name='jatuh_tempo']",function(){
		loadSuplierFakturPembelian();
	});
	$j("form[name='form_pembayaran_hutang']").on("focus keyup change blur","input[name='pembayaran[]'], input[name='pemotongan']",function(){
		grandTotal();
	});
	$j("form[name='form_pembayaran_hutang']").on("keypress","input[name='nama_suplier']",function(event){
		var value = $j(this).val();
		var keycode = (event.keyCode ? event.keyCode : event.which);
		var ret = true;
		if(keycode == '13'){
			$j.ajax({
				type:"POST",
				url:"<?php echo base_url()."admin/apiweb"; ?>",
				data:{action:"get_data_suplier",bt_cari:1,tx_cari:value},
				dataType:"json",
				success: function(obj){
					var htmTblData = "";
					for(x in obj){
						var row = obj[x];
						htmTblData += "<tr>"+
										"<td>"+row['kode_suplier']+"</td>"+
										"<td><button type=\"button\" class=\"btn btn-link btnSetIdSuplier\" data-id=\""+row['id']+"\" data-nama=\""+row['nama_suplier']+"\" >"+row['nama_suplier']+"</button></td>"+
										"<td>"+row['alamat']+"</td>"+
										"<td>"+row['nama_kota']+"</td>"+
										"<td>"+row['telepon']+"</td>"+
										"<td>"+row['jatuh_tempo']+"</td>"+
									"</tr>";
					}
					$j('.modal#modalDetailSuplier').find("table tbody").html(htmTblData);
					$j('.modal#modalDetailSuplier').modal('show');
				}
			});
			ret = false;
		}
		return ret;
	});
	$j("form[name='form_pembayaran_hutang']").on("click",".btn-pemotongan-type",function(){
		$j(".btn-pemotongan-type").removeClass("active");
		$j(this).addClass("active");
		$j("input[name='pemotongan_tipe']").val($j(this).data("type"));
		grandTotal();
	});
	$j(".table-form").on("click",".btn-hapus-add-detail-barang",function(){
		//$j(this).parents("tr").remove();
		deleteRow($j(this));
		grandTotal();
	});
	$j('.modal#modalDetailSuplier').on('hidden.bs.modal', function (e){
		$j('.modal#modalDetailSuplier').find("table tbody").html("");
	})
	$j(".modal#modalDetailSuplier").on("click","button.btnSetIdSuplier",function(){
		var id = $j(this).data('id');
		var nama = $j(this).data('nama');
		$j("form[name='form_pembayaran_hutang']").find("input[name='id_suplier']").val(id);
		$j("form[name='form_pembayaran_hutang']").find("input[name='nama_suplier']").val(nama);
		$j('.modal#modalDetailSuplier').modal('hide');
		loadSuplierFakturPembelian();
	})
	$j(document).ready(function(){
		if(action == "edit"){
			loadSuplierFakturPembelian();
		}else if(action == "tambah" && action_id != ""){
			$j("input[name='id_suplier']").val("<?php echo @$get_edit_data_suplier[0]->id?>");
			$j("input[name='nama_suplier']").val("<?php echo @$get_edit_data_suplier[0]->nama_suplier?>");
			$j("input[name='jatuh_tempo']").val("<?php echo date('Y-m-t',strtotime("+1 month")); ?>");
			$j(".standardSelect").trigger("chosen:updated");
			loadSuplierFakturPembelian();
		}
	});
</script>
<?php } ?>