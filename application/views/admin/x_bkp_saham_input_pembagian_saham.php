<?php 
$aksesKey = $this->router->fetch_class()."/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug();
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}
	
	$no=0;
	$htm_table_pembagian_saham = "";
	foreach($get_pembagian_saham as $row){
		$htm_table_pembagian_saham.="
										<tr data-id=\"".$row->id."\">
											<th scope=\"row\">".($no+=1)."</th>
											<td>".date_format(date_create($row->tanggal),"Y-m-d")."</td>";
							
			if(isset($aksesUbah)) 
				$htm_table_pembagian_saham.="<td><a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/preview/".$row->id."\" class=\"text-primary\">".$row->nomor."</a></td>";
			else
				$htm_table_pembagian_saham.="<td>".$row->nomor."</td>";
					$htm_table_pembagian_saham.="<td>".$row->nilai_pendapatan_perusahaan."</td>
											<td>".($row->zakat+$row->biaya_gaji+$row->biaya_atk+$row->biaya_lain_lain)."</td>
											<td>";
												//if(isset($aksesUbah)) $htm_table_pembagian_saham.=" <a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/edit/".$row->id."\" class=\"btn btn-outline-success btn-sm\"><i class=\"fa fa-edit\"></i>&nbsp; Edit</a> ";
												if(isset($aksesHapus)) $htm_table_pembagian_saham.=" <a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/hapus/".$row->id."\" class=\"btn btn-outline-danger btn-sm\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";
						  $htm_table_pembagian_saham.="</td>
										</tr>
									";
	}
	
	if($htm_table_pembagian_saham == ""){
		$htm_table_pembagian_saham .= "<tr><th colspan='10' class=\"text-center\">. : Data Kosong : .</th></tr>";
	}
		$htm_table_pembagian_saham .= "<tr><th colspan='10'>Menampilkan ".(($no>0)?1:0)." .. ".$no." dari ".$no." Baris</th></tr>";
	
	if(isset($get_edit_pembagian_saham_detail_investor) and is_array($get_edit_pembagian_saham_detail_investor)){
		$no=0;
		$htm_table_pembagian_saham_detail_barang = "";
		foreach($get_edit_pembagian_saham_detail_investor as $row){
			$htm_table_pembagian_saham_detail_barang.="
											<tr data-id=\"".$row->id."\">
												<th scope=\"row\">".($no+=1)."</th>
												<td>".$row->id_investor."</td>
												<td>".$row->nama_investor."</td>
												<td>".$row->email."</td>
												<td>".format_rupiah($row->nilai_investasi)."</td>
												<td>".$row->persentase."%</td>
												<td>".format_rupiah($row->pendapatan_investasi)."</td>
											</tr>
										";
		}
		
		if($htm_table_pembagian_saham_detail_barang == ""){
			$htm_table_pembagian_saham_detail_barang .= "<tr><th colspan='10' class=\"text-center\">. : Data Kosong : .</th></tr>";
		}
			$htm_table_pembagian_saham_detail_barang .= "<tr><th colspan='10'>&nbsp;</th></tr>";
	}
	
	if($action == "tambah"){
		$tambahRequired = " required='required' ";
	}else if($action == "edit"){
		$editRequired = " required='required' ";
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method(); ?>">Input Pembagian Saham <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-4">
		<div class="card">
			<div class="card-header bg-warning p-2">
				<strong class="card-title">Pendapatan Bulan Ini</strong>
			</div>
			<div class="card-body p-2">
				<div style="font-size:12px;">Total</div>
				<div><h2 class="card-text font-weight-bold"><?php echo format_rupiah(@$get_laporan_cepat_pembagian_saham['pendapatan_bulan_ini']); ?></h2></div>
			</div>
		</div>
	</div>
	<div class="col-4">
		<div class="card">
			<div class="card-header bg-danger p-2">
				<strong class="card-title">Pendapatan Per Tahun</strong>
			</div>
			<div class="card-body p-2">
				<div style="font-size:12px;">Total</div>
				<div><h2 class="card-text font-weight-bold"><?php echo format_rupiah(@$get_laporan_cepat_pembagian_saham['pendapatan_per_tahun']); ?></h2></div>
			</div>
		</div>
	</div>
	<div class="col-4">
		<div class="card">
			<div class="card-header bg-primary p-2">
				<strong class="card-title">Pembagian Saham Bulan ini</strong>
			</div>
			<div class="card-body p-2">
				<div style="font-size:12px;">Total</div>
				<div><h2 class="card-text font-weight-bold"><?php echo format_rupiah(@$get_laporan_cepat_pembagian_saham['pembagian_saham_bulan_ini']); ?></h2></div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-6">
		<form action="" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari" placeholder="Pencarian..." class="form-control form-control-sm" required="required" />
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari"><i class="fa fa-search"></i></button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<?php if(isset($aksesTambah)){ ?>
		<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/tambah"; ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil"></i>&nbsp; Tambah</a>
		<?php } ?>
		<button type="button" class="btn btn-outline-warning btn-sm" id="bt_print"><i class="fa fa-print"></i>&nbsp; Cetak</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="bg-info">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Tanggal</th>
					<th scope="col">Nomor</th>
					<th scope="col">Nilai Pendapatan Perusahaan</th>
					<th scope="col">Biaya - Biaya</th>
					<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
					<th scope="col">Aksi</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php echo @$htm_table_pembagian_saham; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>
<?php if($action == "preview"){ ?>
<div class="card">
	<div class="card-body">
			<div class="row">
				<div class="col-4">
					<div class="form-group row">
						<label class="col-form-label col-5">Tanggal</label>
						<label class="col-form-label col-7"><?php echo date_format(date_create($get_edit_pembagian_saham[0]->tanggal),'Y-m-d'); ?></label>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group row">
						<label class="col-form-label col-5">No Transaksi</label>
						<label class="col-form-label col-7"><?php echo $get_edit_pembagian_saham[0]->nomor; ?></label>
					</div>
				</div>
				<div class="col-4 text-right">
					<div class="font-weight-bold text-primary">Nilai Pendapatan Perusahaan</div>
					<div class="font-weight-bold text-warning txt-nilai-pendapatan-perusahaan" style="font-size:30px;">
						<?php echo format_rupiah($get_edit_pembagian_saham[0]->nilai_pendapatan_perusahaan); ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 font-weight-bold text-primary" style="font-size:23px;">
					Biaya - Biaya sudah di Kalkulasikan<hr class="mt-0"/>
				</div>
			</div>
			<div class="row">
				<div class="col-4">
					<div class="form-group">
						<label>Biaya Gaji</label><br/>
						<?php echo format_rupiah($get_edit_pembagian_saham[0]->biaya_gaji); ?>
					</div>
					<div class="form-group">
						<label>Biaya Atk</label><br/>
						<?php echo format_rupiah($get_edit_pembagian_saham[0]->biaya_atk); ?>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label>Biaya Lain-lain</label><br/>
						<?php echo format_rupiah($get_edit_pembagian_saham[0]->biaya_lain_lain); ?>
					</div>
					<div class="form-group">
						<label>Pajak (<?php echo $get_edit_pembagian_saham[0]->persentase_pajak; ?>%)</label><br/>
						<?php echo format_rupiah($get_edit_pembagian_saham[0]->nilai_pajak); ?>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group text-right">
						<div style="font-size:10px;">Zakat / Infaq (otomatis 2,5%) dari Nilai Pendapatan Perusahaan</div>
						<div class="font-weight-bold text-info txt-zakat" style="font-size:30px;" >
							<?php echo format_rupiah($get_edit_pembagian_saham[0]->zakat); ?>
						</div>
					</div>
					<div class="form-group text-right">
						<div class="font-weight-bold">Grand Total Pendapatan Bersih</div>
						<div class="font-weight-bold text-warning txt-total-pendapatan-bersih" style="font-size:30px;" >
							<?php echo format_rupiah($get_edit_pembagian_saham[0]->total_pendapatan_bersih); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="font-weight-bold text-primary" style="font-size:23px;">Pembagian Hasil</div>
					<div style="font-size:12px;">Pembagian sudah di potong Biaya - Biaya dan Zakat / Infaq (otomatis 2,5%) dari Nilai Pendapatan Perusahaan</div>
					<hr class="mt-0"/>
				</div>
			</div>
			<table class="table table-form">
				<thead class="bg-info">
					<tr>
						<th scope="col">#</th>
						<th scope="col" style="width:150px;">Id Investor</th>
						<th scope="col">Nama Investor</th>
						<th scope="col">Email</th>
						<th scope="col">Nilai Investasi</th>
						<th scope="col">Persentase</th>
						<th scope="col">Pendapatan Investasi</th>
					</tr>
				</thead>
				<tbody>
					<?php echo $htm_table_pembagian_saham_detail_barang; ?>
				</tbody>
			</table>
			<div class="row">
				<div class="col-6">&nbsp;</div>
				<div class="col-6">
					<div class="row">
						<div class="col-8 text-right">
							Total Pembagian Hasil
						</div>
						<div class="col-4 text-right text-total-pembagian-hasil">
							<?php echo format_rupiah($get_edit_pembagian_saham[0]->total_pembagian_hasil); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-4">
					<?php echo " <a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/hapus/".@$get_edit_pembagian_saham[0]->id."\" class=\"btn btn-sm btn-flat btn-secondary\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";?>
				</div>
				<div class="col-4 text-center">
					<button type="button" class="btn btn-sm btn-flat btn-info" id="bt_print_preview"><i class="fa fa-print"></i>&nbsp; Cetak</button>
				</div>
				<div class="col-4 text-right">
					<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method(); ?>" class="btn btn-sm btn-flat btn-danger">Kembali</a>
					<?php 
						echo " <a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/edit/".@$get_edit_pembagian_saham[0]->id."\" class=\"btn btn-sm btn-flat btn-success ";
						if(isset($get_edit_pembagian_saham[0]->status_retur) and @$get_edit_pembagian_saham[0]->status_retur == '1') echo " disabled ";
						echo " \">Ubah</a> "; 
					?>
				</div>
			</div>
	</div>
</div>
<?php } ?>
<?php if($action == "tambah" or $action == "edit"){ ?>
<div class="card">
	<div class="card-body">
		<form method="post" action="" enctype="multipart/form-data" name="form_input_pembagian_saham" class="image-editor">
			<input type="hidden" name="form_action" value="<?php echo $action; ?>">
			<input type="hidden" name="id_pembagian_saham" value="<?php echo @$get_edit_pembagian_saham[0]->id; ?>">
			<div class="row">
				<div class="col-4">
					<div class="form-group row">
						<label class="col-form-label col-5">Tanggal</label>
						<div class="col-7">
							<input type="date" class="form-control" name="tanggal" required="required" value="<?php echo (isset($get_edit_pembagian_saham[0]->tanggal))?date_format(date_create($get_edit_pembagian_saham[0]->tanggal),'Y-m-d'):""; ?>"> <!-- value="<?php echo (isset($get_edit_pembagian_saham[0]->tanggal))?date_format(date_create($get_edit_pembagian_saham[0]->tanggal),'Y-m-d'):date('Y-m-d'); ?>" -->
						</div>
					</div>
				</div>
				<div class="col-4">
					<div class="form-group row">
						<label class="col-form-label col-5">No Transaksi</label>
						<div class="col-7">
							<input type="text" class="form-control" name="nomor" value="<?php echo (isset($get_edit_pembagian_saham[0]->nomor))?$get_edit_pembagian_saham[0]->nomor:"[Auto Generate After Submit]"; ?>" readonly="readonly">
						</div>
					</div>
				</div>
				<div class="col-4 text-right">
					<div class="font-weight-bold text-primary">Nilai Pendapatan Perusahaan</div>
					<div class="font-weight-bold text-warning txt-nilai-pendapatan-perusahaan" style="font-size:30px;" data-value="<?php echo (isset($get_edit_pembagian_saham[0]->nilai_pendapatan_perusahaan))?$get_edit_pembagian_saham[0]->nilai_pendapatan_perusahaan:0; ?>" ><?php echo (isset($get_edit_pembagian_saham[0]->nilai_pendapatan_perusahaan))?format_rupiah($get_edit_pembagian_saham[0]->nilai_pendapatan_perusahaan):0; ?></div>
					<input type="hidden" name="nilai_pendapatan_perusahaan" value="<?php echo (isset($get_edit_pembagian_saham[0]->nilai_pendapatan_perusahaan))?$get_edit_pembagian_saham[0]->nilai_pendapatan_perusahaan:0; ?>">
				</div>
			</div>
			<div class="row">
				<div class="col-12 font-weight-bold text-primary" style="font-size:23px;">
					Biaya - Biaya sudah di Kalkulasikan<hr class="mt-0"/>
				</div>
			</div>
			<div class="row">
				<div class="col-4">
					<div class="form-group">
						<label>Biaya Gaji</label>
						<input type="number" class="form-control" name="biaya_gaji" value="<?php echo @$get_edit_pembagian_saham[0]->biaya_gaji; ?>" required="required">
					</div>
					<div class="form-group">
						<label>Biaya Atk</label>
						<input type="number" class="form-control" name="biaya_atk" value="<?php echo @$get_edit_pembagian_saham[0]->biaya_atk; ?>" required="required">
					</div>
				</div>
				<div class="col-4">
					<div class="form-group">
						<label>Biaya Lain-lain</label>
						<input type="number" class="form-control" name="biaya_lain_lain" value="<?php echo @$get_edit_pembagian_saham[0]->biaya_lain_lain; ?>" required="required">
					</div>
					<div class="form-group">
						<label>Pajak</label>
						<div class="input-group">
						<input type="number" class="form-control" name="persentase_pajak" value="<?php echo (isset($get_edit_pembagian_saham[0]->persentase_pajak))?$get_edit_pembagian_saham[0]->persentase_pajak:$get_setting_app[0]->pajak; ?>" readonly="readonly">
						  <div class="input-group-append">
							<span class="input-group-text">%</span>
						  </div>
						</div>
						<input type="number" class="form-control" name="nilai_pajak" value="<?php echo @$get_edit_pembagian_saham[0]->nilai_pajak; ?>" readonly="readonly">
					</div>
				</div>
				<div class="col-4">
					<div class="form-group text-right">
						<div style="font-size:10px;">Zakat / Infaq (otomatis 2,5%) dari Nilai Pendapatan Perusahaan</div>
						<div class="font-weight-bold text-info txt-zakat" style="font-size:30px;" data-value="0" >Rp 0,00</div>
						<input type="hidden" name="zakat" value="0">
					</div>
					<div class="form-group text-right">
						<div class="font-weight-bold">Grand Total Pendapatan Bersih</div>
						<div class="font-weight-bold text-warning txt-total-pendapatan-bersih" style="font-size:30px;" data-value="0" >Rp 0,00</div>
						<input type="hidden" name="total_pendapatan_bersih" value="0">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="font-weight-bold text-primary" style="font-size:23px;">Pembagian Hasil</div>
					<div style="font-size:12px;">Pembagian sudah di potong Biaya - Biaya dan Zakat / Infaq (otomatis 2,5%) dari Nilai Pendapatan Perusahaan</div>
					<hr class="mt-0"/>
				</div>
			</div>
			<table class="table table-form">
				<thead class="bg-info">
					<tr>
						<th scope="col">#</th>
						<th scope="col" style="width:150px;">Id Investor</th>
						<th scope="col">Nama Investor</th>
						<th scope="col">Email</th>
						<th scope="col">Nilai Investasi</th>
						<th scope="col">Persentase</th>
						<th scope="col">Pendapatan Investasi</th>
						<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
						<th scope="col" class="text-center">Aksi</th>
						<?php } ?>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
			<div class="row">
				<div class="col-6">&nbsp;</div>
				<div class="col-6">
					<div class="row">
						<div class="col-8 text-right">
							Total Pembagian Hasil
							<input type="hidden" name="total_pembagian_hasil" value="<?php echo @$get_edit_pembagian_saham[0]->total_pembagian_hasil; ?>">
						</div>
						<div class="col-4 text-right text-total-pembagian-hasil">
							Rp. 0,-
						</div>
					</div>
				</div>
			</div>
			<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/preview/".@$get_edit_pembagian_saham[0]->id; ?>" class="btn btn-outline-danger btn-sm btn-flat">Kembali</a>
			<button type="submit" name="submit_input_pembagian_saham" class="btn btn-success btn-sm btn-flat">Submit</button>
		</form>
	</div>
</div>
<?php } ?>
<!-- Modal -->
<div class="modal" id="modalDetailNoSeri" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Detail No Seri</span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<table class="table table-sm">
		  <thead>
			<tr>
			  <th class="border-top-0" scope="col">No Seri</th>
			  <th class="border-top-0 scope="col">Qty</th>
			  <!--<th class="border-top-0 scope="col">&nbsp;</th>-->
			</tr>
		  </thead>
		  <tbody>
			<tr>
			  <td>
				<input type="text" class="form-control form-control-sm" name="no_seri" type="text" placeholder="No Seri">
			  </td>
			  <td>
				<input type="number" class="form-control form-control-sm" name="qty" type="text" placeholder="Qty">
			  </td>
			  <!--<td>
				<button type="button" class="btn btn-sm btn-flat btn-danger btn-del-no-seri-detail"><i class="fa fa-minus"></i></button>
			  </td>-->
			</tr>
		  </tbody>
		</table>
      </div>
      <div class="modal-footer">
		<!--<button type="button" class="btn btn-sm btn-flat btn-success btn-add-no-seri-detail"><i class="fa fa-plus"></i></button>-->
		<button type="button" class="btn btn-sm btn-flat btn-secondary" data-dismiss="modal">Cancel</button>
		<button type="button" class="btn btn-sm btn-flat btn-primary btn-save-no-seri-detail">Save</button>
      </div>
    </div>
  </div>
</div>
<div class="modal" id="modalDetailCustomer" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Pilih Customer</span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<table class="table table-sm">
		  <thead>
			<tr>
			  <th class="border-top-0" scope="col">Kode</th>
			  <th class="border-top-0" scope="col">Nama Customer</th>
			  <th class="border-top-0" scope="col">Alamat</th>
			  <th class="border-top-0" scope="col">Kota</th>
			  <th class="border-top-0" scope="col">Telepon</th>
			  <th class="border-top-0" scope="col">Jatuh Tempo</th>
			</tr>
		  </thead>
		  <tbody>
		  </tbody>
		</table>
      </div>
    </div>
  </div>
</div>
<script>
	var VG_onpage_data_table = "tbl_input_pembagian_saham";
	var BASE_URL = "<?php echo base_url(); ?>";
	var aksesHapus = "<?php echo @$aksesHapus; ?>";
	var action = "<?php echo $action; ?>";
	var btnDetailNoSeri = null;
	var no=0;
	function grandTotal(){
		var nilai_pendapatan_perusahaan = parseInt($j(".txt-nilai-pendapatan-perusahaan").attr("data-value"));
		var biaya_gaji =  parseInt($j("input[name='biaya_gaji']").val());
		var biaya_atk =  parseInt($j("input[name='biaya_atk']").val());
		var biaya_lain_lain =  parseInt($j("input[name='biaya_lain_lain']").val());
		var persentase_pajak =  parseInt($j("input[name='persentase_pajak']").val());
		
		var zakat = 0;
		if(nilai_pendapatan_perusahaan > 0){
			zakat = (nilai_pendapatan_perusahaan*2.5)/100;
		}
		
		var nilai_pajak = 0;
		if(nilai_pendapatan_perusahaan > 0 && persentase_pajak > 0){
			nilai_pajak = (nilai_pendapatan_perusahaan*persentase_pajak)/100
		}
		
		$j("input[name='zakat']").val(zakat);
		$j(".txt-zakat").attr("data-value",zakat);
		$j(".txt-zakat").html("Rp. "+numberWithCommas(zakat)+",-");
		
		$j("input[name='nilai_pajak']").val(nilai_pajak);
		
		var total_pendapatan_bersih = nilai_pendapatan_perusahaan-zakat-nilai_pajak-biaya_gaji-biaya_atk-biaya_lain_lain;
		$j("input[name='total_pendapatan_bersih']").val(total_pendapatan_bersih);
		$j(".txt-total-pendapatan-bersih").attr("data-value",total_pendapatan_bersih);
		$j(".txt-total-pendapatan-bersih").html("Rp. "+numberWithCommas(total_pendapatan_bersih)+",-");
	}
	function totalPembagianSaham(){
		var form_transaksi = $j("form[name='form_input_pembagian_saham']");
		var total_pembagian_hasil = 0;		
		form_transaksi.find("input[name='pendapatan_investasi[]']").each(function(){
			total_pembagian_hasil += parseInt($j(this).val());
		});
		form_transaksi.find("input[name='total_pembagian_hasil']").val(total_pembagian_hasil);
		form_transaksi.find(".text-total-pembagian-hasil").html("Rp. "+numberWithCommas(total_pembagian_hasil)+",-");
	}
	function setAddButtonDataInvestor(){
		$j(".btn-add-investor").parents("tr").remove();
		var htm_table_form_investor = "";
		htm_table_form_investor+= "<tr><td colspan='10'><button type=\"button\" class=\"btn btn-outline-success btn-sm btn-add-investor\" >+ Tambah Investor</button></td></tr>";
		$j(".table-form tbody").append(htm_table_form_investor);
		$j(".standardSelect").chosen({disable_search_threshold: 10,no_results_text: "Oops, nothing found!",width: "100%"});
	}
	function setAddDataInvestor(){
		var htm_option_investor = "<option value=\"\" data-nama-investor=\"\" data-email=\"\" data-nilai-investasi=\"\"  data-bagi-hasil-investor=\"\" >Cari Investor</option>";
		var get_data_investor = JSON.parse(`<?php echo json_encode($get_data_investor); ?>`);
		for(var i in get_data_investor){
			var row_investor = get_data_investor[i];
			htm_option_investor+="<option value=\""+row_investor['id']+"\" data-nama-investor=\""+row_investor['nama_investor']+"\" data-email=\""+row_investor['email']+"\" data-nilai-investasi=\""+row_investor['nilai_investasi']+"\"  data-bagi-hasil-investor=\""+row_investor['bagi_hasil_investor']+"\" >"+row_investor['nama_investor']+"</option>";
		}
		no++;
		var htm_table_form_investor = "";
		htm_table_form_investor+="<tr>"+
											"<input type=\"hidden\" name=\"no[]\" id=\"no\" value=\""+no+"\" disabled='disabled' />"+
											"<th scope=\"row\">"+no+"</th>"+
											"<td>"+
												"<select data-placeholder=\"Cari Investor\" class=\"form-control standardSelect\" name=\"id_investor[]\" id=\"id_investor\" >"+
													htm_option_investor+
												"</select>"+
											"</td>"+
											"<td><input type=\"text\" name=\"nama_investor[]\" id=\"nama_investor\" placeholder=\"Nama Investor\" class=\"form-control form-control-sm\" readonly=\"readonly\" /></td>"+
											"<td><input type=\"text\" name=\"email[]\" id=\"email\" placeholder=\"Email\" class=\"form-control form-control-sm\" readonly=\"readonly\" /></td>"+
											"<td><input type=\"text\" name=\"nilai_investasi[]\" id=\"nilai_investasi\" placeholder=\"Nilai Investasi\" class=\"form-control form-control-sm\" readonly=\"readonly\" /></td>"+
											"<td><input type=\"text\" name=\"persentase[]\" id=\"persentase\" placeholder=\"Persentase\" class=\"form-control form-control-sm\" readonly=\"readonly\" /></td>"+
											"<td><input type=\"text\" name=\"pendapatan_investasi[]\" id=\"pendapatan_investasi\" placeholder=\"Pendapatan Investasi\" class=\"form-control form-control-sm\" readonly=\"readonly\" /></td>"+
											"<td class=\"text-center\">";
						htm_table_form_investor+=" <button type=\"button\" class=\"btn btn-outline-danger btn-sm btn-hapus-add-investor\" ><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</button> ";
											htm_table_form_investor+="</td></tr>";
		$j(".table-form tbody").append(htm_table_form_investor);
		//setAddButtonDataInvestor();
	}
	function setDataInvestortoTable(dataJSON){
		var htm_table_form_investor = "";
		var get_detail_investor_input_pembagian_saham = JSON.parse(dataJSON);
		
		for(var x in get_detail_investor_input_pembagian_saham){
			var row = get_detail_investor_input_pembagian_saham[x];
			
			var htm_option_investor = "<option value=\"\" data-nama-investor=\"\" data-email=\"\" data-nilai-investasi=\"\"  data-bagi-hasil-investor=\"\" >Cari Investor</option>";
			var get_data_investor = JSON.parse(`<?php echo json_encode($get_data_investor); ?>`);
			for(var i in get_data_investor){
				var row_investor = get_data_investor[i];
				htm_option_investor +="<option value=\""+row_investor['id']+"\" data-nama-investor=\""+row_investor['nama_investor']+"\" data-email=\""+row_investor['email']+"\" data-nilai-investasi=\""+row_investor['nilai_investasi']+"\"  data-bagi-hasil-investor=\""+row_investor['bagi_hasil_investor']+"\" ";
				if(typeof(row['id_investor']) !== "undefined" && row['id_investor'] == row_investor['id']) htm_option_investor += " selected=selected ";
				htm_option_investor +=" >"+row_investor['nama_investor']+"</option>";
			}
			
			no++;
			htm_table_form_investor +=" <tr>"+
											"<input type=\"hidden\" name=\"no[]\" id=\"no\" value=\""+no+"\" disabled='disabled' />"+
											"<th scope=\"row\">"+no+"</th>"+
											"<td>"+
												"<select data-placeholder=\"Cari Investor\" class=\"form-control standardSelect\" name=\"id_investor[]\" id=\"id_investor\" >"+
													htm_option_investor+
												"</select>"+
											"</td>"+
											"<td><input type=\"text\" name=\"nama_investor[]\" id=\"nama_investor\" placeholder=\"Nama Investor\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['nama_investor']+"\" /></td>"+
											"<td><input type=\"text\" name=\"email[]\" id=\"email\" placeholder=\"Email\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['email']+"\" /></td>"+
											"<td><input type=\"text\" name=\"nilai_investasi[]\" id=\"nilai_investasi\" placeholder=\"Nilai Investasi\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['nilai_investasi']+"\" /></td>"+
											"<td><input type=\"text\" name=\"persentase[]\" id=\"persentase\" placeholder=\"Persentase\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['persentase']+"\" /></td>"+
											"<td><input type=\"text\" name=\"pendapatan_investasi[]\" id=\"pendapatan_investasi\" placeholder=\"Pendapatan Investasi\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['pendapatan_investasi']+"\" /></td>";
			htm_table_form_investor+=
											"<td class=\"text-center\">";
												if(aksesHapus == "1") 
													htm_table_form_investor+=" <button type=\"button\" class=\"btn btn-outline-danger btn-sm btn-hapus-add-investor\" ><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</button> ";
					htm_table_form_investor+="</td></tr>";
		}
		
		$j(".table-form tbody").html(htm_table_form_investor);
		//setAddButtonDataInvestor();
		totalPembagianSaham();
		grandTotal();
	}
	function loadDataInvestor(){
		$j.ajax({
			type:"POST",
			url:"<?php echo base_url()."admin/apiweb"; ?>",
			data:{action:"get_pembagian_saham_detail_investor",id_pembagian_saham:"<?php echo @$get_edit_pembagian_saham[0]->id; ?>"},
			success: function(dataJSON){
				setDataInvestortoTable(dataJSON);
			}
		});
	}
	$j(document).on("click","#bt_print",function(){
		printTableData();
	});
	$j(document).on("click","#bt_print_preview",function(){
		var data = $j(this).parents("div.card").html();
		printTableData("<div class='card'>"+data+"</div>");
	});
	$j(".standardSelect").chosen({
		disable_search_threshold: 10,
		no_results_text: "Oops, nothing found!",
		width: "100%"
	});
	$j("form[name='form_input_pembagian_saham']").on("change","input[name='tanggal']",function(){
		var tanggal = $j(this).val();
		$j.ajax({
			type:"POST",
			url:"<?php echo base_url()."admin/apiweb"; ?>",
			data:{action:"get_nilai_pendapatan_perusahaan",tanggal:tanggal},
			success: function(data){
				$j("input[name='nilai_pendapatan_perusahaan']").val(data);
				$j(".txt-nilai-pendapatan-perusahaan").attr("data-value",data);
				$j(".txt-nilai-pendapatan-perusahaan").html("Rp. "+numberWithCommas(data)+",-");
				grandTotal();
			}
		});
	});
	$j("form[name='form_input_pembagian_saham']").on("keyup","input[name='biaya_gaji'], input[name='biaya_atk'], input[name='biaya_lain_lain']",function(){
		grandTotal();
	});
	$j("form[name='form_input_pembagian_saham']").submit(function(er){
		var ret = true;
		$j(this).find("select[name='id_investor[]']").each(function(){
			var val = $j(this).val();
			if(val == ""){
				alert("Pastikan Nama Investor di isi dengan benar!");
				ret = false;
			}
		});
		return ret;
	});
	$j(".table-form").on("click",".btn-add-investor",function(){
		setAddDataInvestor();
	});
	$j(".table-form").on("click",".btn-hapus-add-investor",function(){
		$j(this).parents("tr").remove();
		totalPembagianSaham();
	});
	$j(".table-form").on("change","select[name='id_investor[]']",function(){
		var row = $j(this).parents("tr");
		var nama_investor = $j(this).find(":selected").data("nama-investor");
		var email = $j(this).find(":selected").data("email");
		var nilai_investasi = parseInt($j(this).find(":selected").data("nilai-investasi"));
		var bagi_hasil_investor = parseInt($j(this).find(":selected").data("bagi-hasil-investor"));
		row.find("input[name='nama_investor[]']").val(nama_investor);
		row.find("input[name='email[]']").val(email);
		row.find("input[name='nilai_investasi[]']").val(nilai_investasi);
		row.find("input[name='persentase[]']").val(bagi_hasil_investor);
		row.find("input[name='pendapatan_investasi[]']").val((nilai_investasi*bagi_hasil_investor)/100);
		totalPembagianSaham();
	});
	$j(document).ready(function(){
		<?php
			if($action == "tambah"){
				echo "setAddDataInvestor();";
			}else if($action == "edit"){
				echo "loadDataInvestor();";
			}
		?>
		$j(".standardSelect").chosen({disable_search_threshold: 10,no_results_text: "Oops, nothing found!",width: "100%"});
	});
</script>
<?php } ?>