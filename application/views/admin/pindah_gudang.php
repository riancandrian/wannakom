<?php

$sub_slug = "";
$action = '';
if($action <> NULL){
  $sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
}

$notif_message = "";
if(isset($message) and $message <>""){
  $notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>">Data Design Customer <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>

<div class="card">
  <div class="card-body">
    <form class="image-editor" method="POST" action="<?=base_url('gudang/update')?>">
      <div class="form-group">
        <label>No. Faktur</label>
        <input type="text" class="form-control" name="faktur" placeholder="Masukan Nomor Faktur Pembelian">
      </div>

      <div class="form-group">
        <label>Tanggal Order</label>
        <input type="text" class="form-control" name="tanggal" value="" readonly>
      </div>

      <div class="form-group">
        <label>Supplier</label>
        <input type="text" class="form-control" name="nama_suplier" value="" readonly>
      </div>

      <div class="form-group">
        <label>Gudang Asal</label>
        <input type="text" class="form-control" name="nama_gudang" value="" readonly>
      </div>

      <div class="form-group">
        <label>Gudang Tujuan</label>
        <select class="form-control" name="gudang">
          <?php foreach ($get_gudang as $x): ?>
            <option value='<?=$x->id?>' ><?=$x->nama_gudang?></option>
          <?php endforeach; ?>
        </select>
      </div>

      <div class="form-group">
        <input type="submit" class="btn btn-outline-success" name="" value="Pindahkan">
      </div>
    </form>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script type="text/javascript">
  $(document).ready(function(){

    $('input[name="faktur"]').change(function(){
      var faktur = $(this).val();

      $.ajax({
        url : "<?=base_url('gudang/cekFaktur')?>",
        data: {faktur : faktur},
        type: 'POST',
        success: function(result){
          var jsonData = JSON.parse(result);

          if(jsonData.success){
            $('input[name="tanggal"]').val(jsonData.data['tanggal']);
            $('input[name="nama_suplier"]').val(jsonData.data['nama_suplier']);
            $('input[name="nama_gudang"]').val(jsonData.data['nama_gudang']);
          }
        }
      })
    })

  })
</script>
