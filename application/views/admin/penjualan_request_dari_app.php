<?php 
$aksesKey = $this->router->fetch_class()."/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug();
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}
	
	$no=0;
	$htm_table_data_mobile_order = "";
	//dd($get_data_mobile_order);
	foreach($get_data_mobile_order as $row){
		$htm_table_data_mobile_order.="
										<tr data-id=\"".$row->id."\">
											<th scope=\"row\">".($no+=1)."</th>
											<td>".repair_date2($row->tgl_input)."</td>
											<td>".$row->nama_customer."</td>
											<td>".$row->nama_toko."</td>
											<td>".$row->alamat_tujuan."</td>
											<td>".$row->catatan."</td>
											<td>".$row->pembayaran."</td>
											<td>".format_rupiah($row->total_bayar)."</td>
											<td>".format_rupiah($row->ongkir)."</td>											
											
												<td>";
													if(isset($aksesUbah) and $row->status_apply == "0") 
														$htm_table_data_mobile_order.=" <a href=\"javascript:void(0);\" class=\"btn btn-sm btn-success\" data-id-order=\"".$row->id."\" data-detail-order=\"".@$row->data_detail_order."\" onclick=\"showDataDetailOrderBeforeApply(this,".@$row->id_group_customer.");\" title=\"Menuggu konfirmasi admin\">View Order</a> ";
													else if(isset($aksesUbah) and $row->status_apply == "1") 
														$htm_table_data_mobile_order.=" <a href=\"javascript:void(0);\" class=\"btn btn-sm btn-danger\" data-id-order=\"".$row->id."\" data-detail-order=\"".@$row->data_detail_order."\" onclick=\"showDataDetailOrderAfterApply(this);\" title=\"Order sukses\">View Order</a> ";
													else if(isset($aksesUbah) and $row->status_apply == "2") 
														$htm_table_data_mobile_order.=" <a href=\"javascript:void(0);\" class=\"btn btn-sm btn-warning\" data-id-order=\"".$row->id."\" data-detail-order=\"".@$row->data_detail_order."\" onclick=\"showDataDetailOrderAfterApply(this);\" title=\"Menunggu konfirmasi customer\">View Order</a>";
							  $htm_table_data_mobile_order.="</td>
											
											
											
												<!--
												<td>";
													if(isset($aksesUbah) and $row->status_apply == "0") 
														$htm_table_data_mobile_order.=" <a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/apply/".$row->id."\" class=\"btn btn-success btn-sm\"  onclick=\"return confirm('Anda akan Apply Order ini?');\"><i class=\"fa fa-edit\"></i>&nbsp; Apply</a> ";
													else
														$htm_table_data_mobile_order.=" <a href=\"javascript:void(0);\" class=\"btn btn-secondary btn-sm disabled\" ><i class=\"fa fa-edit\"></i>&nbsp; Apply</a> ";
													//if(isset($aksesHapus)) $htm_table_data_mobile_order.=" <a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/hapus/".$row->id."\" class=\"btn btn-outline-danger btn-sm\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";
							  $htm_table_data_mobile_order.="</td>
											-->
										</tr>
									";
	}
	
	if($htm_table_data_mobile_order == ""){
		$htm_table_data_mobile_order .= "<tr><th colspan='15' class=\"text-center\">. : Data Kosong : .</th></tr>";
	}
		$htm_table_data_mobile_order .= "<tr><th colspan='15'>Menampilkan ".(($no>0)?1:0)." .. ".$no." dari ".$no." Baris</th></tr>";
		
	if($action == "tambah"){
		$tambahRequired = " required='required' ";
	}else if($action == "edit"){
		$editRequired = " required='required' ";
	}
?>
<style>
	.privatElment{
		display:none;
	}
</style>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method(); ?>">Request Pembelian Dari Mobile App <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-6">
		<form action="" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari" placeholder="Pencarian..." class="form-control form-control-sm" required="required" />
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari"><i class="fa fa-search"></i></button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<!--
		<?php if(isset($aksesTambah)){ ?>
		<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/tambah"; ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil"></i>&nbsp; Tambah</a>
		<?php } ?>
		-->
		<button type="button" class="btn btn-outline-warning btn-sm" id="bt_print"><i class="fa fa-print"></i>&nbsp; Cetak</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="bg-info">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Tanggal</th>
					<th scope="col">Nama Customer</th>
					<th scope="col">Nama Toko</th>
					<th scope="col">Alamat Tujuan</th>
					<th scope="col">Catatan</th>
					<th scope="col">Cara Pembayaran</th>
					<th scope="col">Total Bayar</th>
					<th scope="col">Ongkir</th>
					<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
					<th scope="col">Aksi</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php echo $htm_table_data_mobile_order; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>

<!-- Modal -->
<div class="modal" id="modalDataDetailOrder" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
	  <form action="" method="post" name="formApplyOrder">
		<input type="hidden" name="form_action" value="apply">
		<input type="hidden" name="id_order" value="">
		<div class="modal-content">
		  <div class="modal-header">
			<span class="modal-title">Detail Order</span>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div class="modal-body">
			<table class="table table-sm apply">
			  <thead>
				<tr>
				  <th class="border-top-0" scope="col">Nama Barang</th>
				  <th class="border-top-0" scope="col">Qty Pesanan</th>
				  <th class="border-top-0" scope="col">Real Stock</th>
				  <th class="border-top-0 privatElment" scope="col">Accept Qty</th>
				  <th class="border-top-0 privatElment" scope="col">No Seri</th>
				  <th class="border-top-0" scope="col">Harga Jual</th>
				  <th class="border-top-0" scope="col">Sub Total</th>
				  <!--<th class="border-top-0" scope="col">Expire Date</th>-->
				  
				</tr>
			  </thead>
			  <tbody>
			  </tbody>
			</table>
		  </div>
		  <div class="modal-footer">
			<div class="row">
				<div class="col text-right font-weight-bold">
					Grand Total : <span class="grand-total">Rp 0,-</span>
				</div>
			</div>
		  </div>
		  <div class="modal-footer">
			<div class="row">
				<div class="col form-inline">
					  <!--
					  <div class="form-group mx-sm-3 mb-2">
						<label>Jatuh Tempo</label>
						<input type="date" name="jatuh_tempo" class="form-control mx-sm-3" required="required">
					  </div> 
					  -->
					  <button type="submit" class="btn btn-success mb-2">Apply</button>
				</div>
			</div>
		  </div>
		</div>
	  </form>
  </div>
</div>

<div class="modal" id="modalDetailNoSeri" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">Detail No Seri</span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<div class="row mb-2 text-center">
			<div class="col">
				<input type="text" class="form-control form-control-sm w-100 mb-3" name="no_seri_awal" placeholder="Input no seri awal..." >
			</div>
			<div class="col">
				<input type="text" class="form-control form-control-sm w-100 mb-3" name="no_seri_akhir" placeholder="Input no seri akhir..." >
			</div>
		</div>		
		<table class="table table-sm">
		  <thead>
			<tr>
			  <th class="border-top-0" scope="col" colspan="3">
				<input type="text" class="form-control form-control-sm w-100 mb-3" name="search_no_seri" placeholder="Input no seri..." autofocus >
			   </th>
			</tr>
		  </thead>
		  <thead>
			<tr>
			  <th class="border-top-0" scope="col">No Seri</th>
			  <th class="border-top-0" scope="col" style="width:25%;">Qty</th>
			  <th class="border-top-0" scope="col">&nbsp;</th>
			</tr>
		  </thead>
		  <tbody>
			<tr>
			  <td>
				<input type="text" class="form-control form-control-sm" name="no_seri" type="text" placeholder="No Seri">
			  </td>
			  <td>
				<input type="number" class="form-control form-control-sm" name="qty" type="text" placeholder="Qty">
			  </td>
			  <td>
				<button type="button" class="btn btn-sm btn-flat btn-danger btn-del-no-seri-detail"><i class="fa fa-minus"></i></button>
			  </td>
			</tr>
		  </tbody>
		</table>
      </div>
      <div class="modal-footer">
		<!--<button type="button" class="btn btn-sm btn-flat btn-success btn-add-no-seri-detail"><i class="fa fa-plus"></i></button>-->
		<button type="button" class="btn btn-sm btn-flat btn-secondary" data-dismiss="modal">Cancel</button>
		<button type="button" class="btn btn-sm btn-flat btn-primary btn-save-no-seri-detail">Save</button>
      </div>
    </div>
  </div>
</div>

<script>
	var VG_skip = false;
	var VG_onpage_data_table = "tbl_transaksi_penjualan";
	var PG_TmpNoSeriStok = {};
	var PG_TmpNoSeriOnTable = {};
	var BASE_URL = "<?php echo base_url(); ?>";
	var aksesHapus = "<?php echo @$aksesHapus; ?>";
	var action = "<?php echo $action; ?>";
	var btnDetailNoSeri = null;
	var no=0;
	function showDataDetailOrderBeforeApply(obj, id_group_customer){
		var privatElment =(id_group_customer == 1 || id_group_customer == 2 || id_group_customer == 3)?true:false;
		var detail_order = JSON.parse(atob($j(obj).data("detail-order")));
		var htmDetailOrder = "";
		var tSubTotal = 0;
		for(var i in detail_order){
			var row = detail_order[i];
			tSubTotal += parseInt(row['sub_total']);
			htmDetailOrder += "<tr idrow="+row['id_barang']+" >"+
								"<input type=\"hidden\" name=\"id_barang["+row['id_barang']+"]\" value=\""+row['id_barang']+"\">"+
								"<input type=\"hidden\" name=\"nama_barang["+row['id_barang']+"]\" value=\""+row['nama_barang']+"\">"+
								"<input type=\"hidden\" name=\"harga_jual["+row['id_barang']+"]\" value=\""+row['harga_jual']+"\">"+
								"<input type=\"hidden\" name=\"detail_no_seri["+row['id_barang']+"]\" value=\"\">"+
								 "<td class='nama_barang'>"+row['nama_barang']+"</td>"+
								 "<td><input type=\"number\" name=\"qty["+row['id_barang']+"]\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['qty']+"\"></td>"+
								 "<td><input type=\"number\" name=\"real_stock["+row['id_barang']+"]\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\"0\"></td>"+
								 "<td class=\"privatElment\"><input type=\"number\" name=\"accept_qty["+row['id_barang']+"]\" class=\"form-control form-control-sm\" value=\"0\" data-harga-jual=\""+row['harga_jual']+"\" readonly></td>"+ //"+row['qty']+"
								 "<td class=\"privatElment\"><button type=\"button\" class=\"btn btn-outline-secondary btn-sm btn-flat btnDetailNoSeri\">Detail</button></td>"+
								 "<td>"+format_rupiah(row['harga_jual'])+"</td>"+
								 "<td class=\"sub_total\">"+format_rupiah(row['sub_total'])+"</td>"+
								 //"<td><input type=\"date\" name=\"expire_date["+row['id_barang']+"]\" class=\"form-control form-control-sm\" required=\"required\"></td>"+
							   "</tr>";
		}
		
		$j('.modal#modalDataDetailOrder').modal('show');
		
		$j('.modal#modalDataDetailOrder').find("input[name='id_order']").val($j(obj).data("id-order"));
		$j('.modal#modalDataDetailOrder').find("input[name='jatuh_tempo']").prop("disabled",false);
		$j('.modal#modalDataDetailOrder').find("table tbody").html(htmDetailOrder);
		$j('.modal#modalDataDetailOrder').find(".grand-total").html(format_rupiah(tSubTotal));
		$j('.modal#modalDataDetailOrder').find("button[type='submit']").prop("disabled",true);
		
		if(privatElment){
			$j(".privatElment").show();
		}else{
			$j(".privatElment").hide();
		}
		
		$j.ajax({
			type:"POST",
			url:"<?php echo base_url()."admin/apiweb"; ?>",
			data:{action:"get_real_stock"},
			dataType:"JSON",
			success: function(obj){
				var buttonSubmitDis = false;
				for(var i in obj){
					var r = obj[i];
					var id_barang = r.id_barang;
					var qty_stock = parseInt(r.qty);
					var qty_current = parseInt($j('.modal#modalDataDetailOrder').find("table tbody tr[idrow='"+id_barang+"'] input[name^='qty']").val());
					$j('.modal#modalDataDetailOrder').find("table tbody tr[idrow='"+id_barang+"'] input[name^='real_stock']").val(qty_stock);
					if(qty_stock < qty_current){
						buttonSubmitDis = true;
					}
				}
				if(buttonSubmitDis === true){
					alert("Transaksi tidak dapat dilakukan, \nAda qty barang yang kurang!");
				}
				$j('.modal#modalDataDetailOrder').find("button[type='submit']").prop("disabled",buttonSubmitDis);
			}
		});
	}
	function showDataDetailOrderAfterApply(obj){
		var detail_order = JSON.parse(atob($j(obj).data("detail-order")));
		var htmDetailOrder = "";
		var tSubTotal = 0;
		for(var i in detail_order){
			var row = detail_order[i];
			tSubTotal += parseInt(row['sub_total']);
			htmDetailOrder += "<tr idrow="+row['id_barang']+" >"+
								 "<td>"+row['nama_barang']+"</td>"+
								 "<td><input type=\"number\" name=\"qty\" class=\"form-control form-control-sm\" readonly=\"readonly\" value=\""+row['qty']+"\"></td>"+
								 "<td><input type=\"number\" name=\"real_stock\" class=\"form-control form-control-sm\" disabled=\"disabled\" value=\"0\"></td>"+
								 "<td>"+format_rupiah(row['harga_jual'])+"</td>"+
								 "<td>"+format_rupiah(row['sub_total'])+"</td>"+
								 //"<td><input type=\"date\" name=\"expire_date["+row['id_barang']+"]\" class=\"form-control form-control-sm\" disabled=\"disabled\"></td>"+
								 
							   "</tr>";
		}
		
		$j('.modal#modalDataDetailOrder').modal('show');
		
		
		$j('.modal#modalDataDetailOrder').find("input[name='id_order']").val($j(obj).data("id-order"));
		$j('.modal#modalDataDetailOrder').find("input[name='jatuh_tempo']").prop("disabled",true);
		$j('.modal#modalDataDetailOrder').find("table tbody").html(htmDetailOrder);
		$j('.modal#modalDataDetailOrder').find(".grand-total").html(format_rupiah(tSubTotal));
		$j('.modal#modalDataDetailOrder').find("button[type='submit']").prop("disabled",true);
		
	}
	function createHtmlBarcodeNoSeriOnEnter(){
		var rowData = "";
		//console.log('createHtmlBarcodeNoSeriOnEnter');
		//console.log(PG_TmpNoSeriOnTable);
		for(i in PG_TmpNoSeriOnTable){
			var noseri = i
			var qty = parseInt(PG_TmpNoSeriOnTable[i]['qty']);
			var qty_max = parseInt(PG_TmpNoSeriOnTable[i]['qty_max']);
			rowData += "<tr>"+
						  "<td>"+
							"<input type=\"text\" class=\"form-control form-control-sm\" name=\"no_seri\" type=\"text\" placeholder=\"No Seri\" value=\""+noseri+"\" readonly=\"readonly\" >"+
						  "</td>"+
						  "<td>"+
							"<input type=\"number\" class=\"form-control form-control-sm\" name=\"qty\" type=\"text\" placeholder=\"Qty\" ";
			rowData	+= " data-max-qty=\""+qty_max+"\" value=\""+qty+"\" ";
			rowData += " >"+
						  "</td>"+
						  "<td>"+
							"<button type=\"button\" class=\"btn btn-sm btn-flat btn-danger btn-del-no-seri-detail\"><i class=\"fa fa-minus\"></i></button>"+
						  "</td>"+
						"</tr>";
		}
		//console.log(rowData);
		$j(".modal#modalDetailNoSeri").find("tbody").html(rowData);
		$j(".modal#modalDetailNoSeri").find("input[name='search_no_seri']").trigger('focus');
	}
	function barcodeNoSeriOnEnter(pval = null){
		//console.log('barcodeNoSeriOnEnter');
		//console.log(PG_TmpNoSeriStok);
		var str_alert = "";
		var stat_alert = true;
		for(i in PG_TmpNoSeriStok){
			var noseri = i
			var qty_stok = parseInt(PG_TmpNoSeriStok[i]);
			//console.log(noseri+" || "+qty);
			if(pval == noseri && qty_stok > 0){
				if(typeof(PG_TmpNoSeriOnTable[noseri]) == "undefined"){
					let tmp = {};
						tmp['qty'] = 1;
						tmp['qty_max'] = qty_stok;
					PG_TmpNoSeriOnTable[noseri] = tmp;
				}else{
					if( qty_stok > parseInt(PG_TmpNoSeriOnTable[noseri]['qty'])){
						let tmp = {};
							tmp['qty'] = (parseInt(PG_TmpNoSeriOnTable[noseri]['qty'])+1);
							tmp['qty_max'] = qty_stok;
						PG_TmpNoSeriOnTable[noseri] = tmp;
					}else{
						alert("max qty no seri ini: "+qty_stok);
						
					}
				}
				stat_alert = false;
			}else{
				str_alert = "No seri \""+pval+"\" tidak tersedia";
			}
		}
		if(stat_alert){
			if(VG_skip === false)
			alert(str_alert);
		}
		//console.log(PG_TmpNoSeriOnTable);
		createHtmlBarcodeNoSeriOnEnter();
	}
	function subTotal(xParent){
		var qty = parseInt(xParent.find("input[name^='accept_qty']").val());
		var harga_jual = parseInt(xParent.find("input[name^='harga_jual']").val());
		xParent.find(".sub_total").text(format_rupiah(qty*harga_jual));
		grandTotal();
	}
	function grandTotal(){
		var tTotal = 0;
		$j(".modal#modalDataDetailOrder").find("tbody>tr").each(function(){
			let row = $j(this);
			let qty = parseInt(row.find("input[name^='accept_qty']").val());
			let harga_jual = parseInt(row.find("input[name^='harga_jual']").val());
			let sub_total = (qty*harga_jual);
			tTotal += sub_total;
		});
		$j('.modal#modalDataDetailOrder').find(".grand-total").text(format_rupiah(tTotal));
	}
	$j("table.apply").on("click",".btnDetailNoSeri",function(){
		btnDetailNoSeri = $j(this);
		var dataRow = btnDetailNoSeri.parents("tr");
		var id_barang = dataRow.find("input[name^='id_barang']").val();
		if(id_barang != ""){
			//ambil data no seri dari stok
			$j.ajax({
				type:"POST",
				url:"<?php echo base_url()."admin/apiweb"; ?>",
				data:{action:"get_data_barang_stok",id_barang:id_barang},
				dataType:"json",
				success: function(obj){
					//console.log(obj);
					$j('#modalDetailNoSeri').modal('show');
					
					//-- Data yang sudah di insert
					var detail_no_seri = dataRow.find("input[name^='detail_no_seri']").val();
					if(typeof(detail_no_seri) != "undefined" && detail_no_seri != ""){
						var no_seri_set = [];
						var objDetail = JSON.parse(detail_no_seri);
						var objNoSeri = objDetail['no_seri'];
						var objQty = objDetail['qty'];
						for(x in objNoSeri){ 
							var n = objNoSeri[x];
							var q = objQty[x];
							no_seri_set[n] = q;
						}
					}
					
					var rowData = "";
					var objTmpStok = {};
					var objTmpValInput = {};
					//console.log(obj);
					for(x in obj){
						var no_seri = obj[x]['no_seri'];
						var qty = obj[x]['qty'];
						
						rowData += "<tr>"+
									  "<td>"+
										"<input type=\"text\" class=\"form-control form-control-sm\" name=\"no_seri\" type=\"text\" placeholder=\"No Seri\" value=\""+no_seri+"\" readonly=\"readonly\" >"+
									  "</td>"+
									  "<td>"+
										"<input type=\"number\" class=\"form-control form-control-sm\" name=\"qty\" type=\"text\" placeholder=\"Qty\" ";
						if(typeof(no_seri_set) != "undefined" && typeof(no_seri_set[no_seri]) != "undefined"){
							rowData	+= " data-max-qty=\""+(parseInt(qty)+parseInt(no_seri_set[no_seri]))+"\" value=\""+no_seri_set[no_seri]+"\" ";
							objTmpValInput[no_seri] = no_seri_set[no_seri];
							objTmpStok[no_seri] = parseInt(qty)+parseInt(no_seri_set[no_seri]);
						}else{
							rowData	+= " data-max-qty=\""+qty+"\" value=\"0\" ";
							objTmpStok[no_seri] = qty;
						}
						rowData += " >"+
									  "</td>"+
									  "<td>"+
										"<button type=\"button\" class=\"btn btn-sm btn-flat btn-danger btn-del-no-seri-detail\"><i class=\"fa fa-minus\"></i></button>"+
									  "</td>"+
									"</tr>";
					}
					//alert(rowData);
					if(rowData == ""){
						rowData = "<tr><td colspan='5' class='text-center pt-4'>Stok barang tidak tersedia</td></tr>";
					}else if(typeof(detail_no_seri) == "undefined" || detail_no_seri == ""){
						rowData = "<tr><td colspan='5' class='text-center pt-4'>Silahkan masukan No Seri!</td></tr>";
					}
					//alert(rowData);
					$j(".modal#modalDetailNoSeri").find("tbody").html(rowData);
					
					PG_TmpNoSeriStok = objTmpStok;
					
					
					//data yang sudah diinput masukan ke transaksi barcodeNoSeriOnEnter
					if(typeof(detail_no_seri) != "undefined" && detail_no_seri != ""){
						for(var i in objTmpValInput){
							var noseri = i;
							var qty = parseInt(objTmpValInput[i]);
							//console.log(noseri+" | "+qty);
							for(xi = 0; xi < qty; xi++){
								//console.log(noseri);
								barcodeNoSeriOnEnter(noseri);
							}
						}
					}
					
				}
			});
			$j(".modal#modalDetailNoSeri").find("input[name='search_no_seri']").trigger('focus');
		}else{
			alert("Barang harus dipilih terlebih dahulu!");
		}
	});
	$j("input[name='search_no_seri']").keyup(function(e){
		if(e.which == 13) {
			VG_skip = false;
			var val = $j(this).val();
			barcodeNoSeriOnEnter(val);
    	    $j(this).val("");
		}
	});
	$j("input[name='no_seri_awal']").keyup(function(e){
		if(e.which == 13) {
			$j("input[name='no_seri_akhir']").focus();
		}
	});
	$j("input[name='no_seri_akhir']").keyup(function(e){
		if(e.which == 13){
			var no_seri_awal = $j("input[name='no_seri_awal']").val();
			var no_seri_akhir = $j("input[name='no_seri_akhir']").val();
			for(var i = no_seri_awal; i<=no_seri_akhir; i++ ){
				VG_skip = true;
				barcodeNoSeriOnEnter(i);
			}
		}
	});	
	$j('.modal#modalDetailNoSeri').on('hidden.bs.modal', function (e) {
		btnDetailNoSeri = null;
		VG_skip = false;
		PG_TmpNoSeriStok = {};
		PG_TmpNoSeriOnTable = {};
		$j(".modal#modalDetailNoSeri").find("tbody").html("");
		$j(".modal#modalDetailNoSeri").find("input[name='search_no_seri']").val("");
	})
	$j(".modal#modalDetailNoSeri").on("focus keyup change blur ","input[name='qty']",function(){
		var noseri = $j(this).parents("tr").find("input[name='no_seri']").val();
		var val = parseInt($j(this).val());
		var max_qty = parseInt($j(this).data("max-qty"));
		if(val > max_qty){
			$j(this).val(max_qty);
			PG_TmpNoSeriOnTable[noseri]['qty'] = max_qty;
			alert("max qty no seri ini: "+max_qty);
		}else{
			PG_TmpNoSeriOnTable[noseri]['qty'] = val;
		}
	});
	
	
	$j(".modal#modalDetailNoSeri").on("click",".btn-add-no-seri-detail",function(){
		var rowData = " <tr>"+
						  "<td>"+
							"<input class=\"form-control form-control-sm\" name=\"no_seri\" type=\"text\" placeholder=\"No Seri\">"+
						  "</td>"+
						  "<td>"+
							"<input class=\"form-control form-control-sm\" name=\"qty\" type=\"text\" placeholder=\"Qty\">"+
						  "</td>"+
						  "<td>"+
							"<button type=\"button\" class=\"btn btn-sm btn-flat btn-danger btn-del-no-seri-detail\"><i class=\"fa fa-minus\"></i></button>"+
						  "</td>"+
						"</tr>";
		$j(".modal#modalDetailNoSeri").find("tbody").append(rowData);
	});
	$j(".modal#modalDetailNoSeri").on("click",".btn-del-no-seri-detail",function(){
		var noseri = $j(this).parents("tr").find("input[name='no_seri']").val();
		var tmp = {};
		for(i in PG_TmpNoSeriOnTable){
			if(i != noseri){
				tmp[i] = PG_TmpNoSeriOnTable[i];
			}
		}
		PG_TmpNoSeriOnTable = tmp;
		$j(this).parents("tr").remove();
	});
	$j(".modal#modalDetailNoSeri").on("click",".btn-save-no-seri-detail",function(){
		PG_TmpNoSeriStok = {};
		PG_TmpNoSeriOnTable = {};
		var modal = $j(".modal#modalDetailNoSeri");
		var tbody = modal.find("tbody>tr");
		var data = {};
		var dNoSeri = {};
		var dQty = {};
		var tQty = 0;
		var n = 0;
		tbody.each(function(){
			var no_seri = $j(this).find("input[name='no_seri']").val();
			var qty     = $j(this).find("input[name='qty']").val();
			if(no_seri != "" || qty != ""){
				dNoSeri[n] = no_seri;
				dQty[n] = qty;
				data['no_seri'] = dNoSeri;
				data['qty'] = dQty;
				tQty += parseInt(qty);
				n++;
			}
		});
		var xParent = btnDetailNoSeri.parents("tr");
		//xParent.find("input#detail_no_seri").val(JSON.stringify(data));
		//xParent.find("input#qty").val(tQty);
		xParent.find("input[name^='detail_no_seri']").val(JSON.stringify(data));
		xParent.find("input[name^='accept_qty']").val(tQty);
		
		subTotal(xParent);
		$j('#modalDetailNoSeri').modal('hide');
	});
	$j(document).on("click","#bt_print",function(){
		printTableData();
	});
	$j(document).on("click","#bt_print_preview",function(){
		var data = $j(this).parents("div.card").html();
		printTableData("<div class='card'>"+data+"</div>");
	});
	$j('.modal#modalDataDetailOrder').on('hidden.bs.modal', function (e){
		$j('.modal#modalDataDetailOrder').find("table tbody").html("");
		$j('.modal#modalDataDetailOrder').find(".grand-total").html("Rp 0,-");
	})
	$j('.modal#modalDataDetailOrder').on("change","[name^='accept_qty']", function (e){
		var row = $j(this).parents("tr");
		var value = parseInt($j(this).val());
		var harga_jual = parseInt($j(this).data("harga-jual"));
		var sub_total = (harga_jual*value);
		row.find("td.sub_total").html(format_rupiah(sub_total));
	})
	$j("form[name='formApplyOrder']").submit(function(){
		var ret = true;
		$j(this).find("input[name^='detail_no_seri']").each(function(){
			let row = $j(this).parents("tr");
			let nama_barang = row.find(".nama_barang").text();
			if($j(this).val() == ""){
				alert(nama_barang+" belum di beri no seri");
				ret = false;
			}
		});
		return ret;
	});
	$j(document).ready(function(){
		firebaseTblInitialize.update({"status_order_baru" : "0"});
	});
</script>
<?php } ?>