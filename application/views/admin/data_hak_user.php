<?php 
$aksesKey = "admin/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug($arMenuHakAkses);
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}

	$no=0;
	$htm_table_hak = "";
	foreach($get_admin_menu_hak as $row){
		$htm_table_hak.="
						<tr data-id=\"".$row->id."\">
							<th scope=\"row\">".($no+=1)."</th>
							<td>".$row->name."</td>
							<td>".$row->description."</td>
							<td>".btnStatLabel($row->status)."</td>
							<td>";
								if(isset($aksesUbah)) $htm_table_hak.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/edit/".$row->id."\" class=\"btn btn-outline-success btn-sm\"><i class=\"fa fa-edit\"></i>&nbsp; Edit</a> ";
								if(isset($aksesHapus)) $htm_table_hak.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/hapus/".$row->id."\" class=\"btn btn-outline-danger btn-sm\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";
			$htm_table_hak.="</td>
						</tr>
					";
	}
	if($htm_table_hak == ""){
		$htm_table_hak .= "<tr><th colspan='5' class=\"text-center\">. : Data Kosong : .</th></tr>";
		$htm_table_hak .= "<tr><th colspan='5' class=\"text-center\">&nbsp;</th></tr>";
	}
	
	$no=0;
	$htm_table_menu = "";
	foreach($get_admin_menu as $row){
		$id = $row->id;
		$name = $row->name;
		if($row->parent=="0" and $row->parent_stat=="0"){
			$htm_table_menu .= "<input type=\"hidden\" name=\"id_menu[]\" value=\"".$id."\">";
			$htm_table_menu .= "<tr class='bg-light font-weight-bold'>
										<td>".($no+=1)."</td><td>".$name."</td>
										<td><input type=\"checkbox\" name=\"lihat_".$id."\" ".((isset($arMenuHakAkses[$id]['lihat']) and $arMenuHakAkses[$id]['lihat'] == 'on')?'checked=checked':'')."></td>
										<td><input type=\"checkbox\" name=\"tambah_".$id."\" ".((isset($arMenuHakAkses[$id]['tambah']) and $arMenuHakAkses[$id]['tambah'] == 'on')?'checked=checked':'')."></td>
										<td><input type=\"checkbox\" name=\"ubah_".$id."\" ".((isset($arMenuHakAkses[$id]['ubah']) and $arMenuHakAkses[$id]['ubah'] == 'on')?'checked=checked':'')."></td>
										<td><input type=\"checkbox\" name=\"hapus_".$id."\" ".((isset($arMenuHakAkses[$id]['hapus']) and $arMenuHakAkses[$id]['hapus'] == 'on')?'checked=checked':'')."></td>
								</tr>";
		}else if($row->parent=="0" and $row->parent_stat=="1"){
			$htm_table_menu .= "<tr class='bg-light font-weight-bold'><td>".($no+=1)."</td><td>".$name."</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
			foreach($get_admin_menu as $row_sub){
				$idSub = $row_sub->id;
				$nameSub = $row_sub->name;
				if($row->id == $row_sub->parent){
					$htm_table_menu .= "<input type=\"hidden\" name=\"id_menu[]\" value=\"".$idSub."\">";
					$htm_table_menu .= "<tr><td>".($no+=1)."</td><td class='pl-5'>".$nameSub."</td>
													<td><input type=\"checkbox\" name=\"lihat_".$idSub."\" ".((isset($arMenuHakAkses[$idSub]['lihat']) and $arMenuHakAkses[$idSub]['lihat'] == 'on')?'checked=checked':'')."></td>
													<td><input type=\"checkbox\" name=\"tambah_".$idSub."\" ".((isset($arMenuHakAkses[$idSub]['tambah']) and $arMenuHakAkses[$idSub]['tambah'] == 'on')?'checked=checked':'')."></td>
													<td><input type=\"checkbox\" name=\"ubah_".$idSub."\" ".((isset($arMenuHakAkses[$idSub]['ubah']) and $arMenuHakAkses[$idSub]['ubah'] == 'on')?'checked=checked':'')."></td>
													<td><input type=\"checkbox\" name=\"hapus_".$idSub."\" ".((isset($arMenuHakAkses[$idSub]['hapus']) and $arMenuHakAkses[$idSub]['hapus'] == 'on')?'checked=checked':'')."></td>
										</tr>";
				}
			}
		}
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>">Hak Akses <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-6">
		<form action="" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari_hak_akses" placeholder="Cari Hak Akses" class="form-control form-control-sm" required="required" />
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<?php if(isset($aksesTambah)){ ?>
		<a href="<?php echo base_url()."admin/".$this->router->fetch_method()."/tambah"; ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil"></i>&nbsp; Tambah</a>
		<?php } ?>
		<button type="button" class="btn btn-outline-warning btn-sm" id="bt_print"><i class="fa fa-print"></i>&nbsp; Cetak</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="thead-dark">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Nama Hak Akses</th>
					<th scope="col">Keterangan</th>
					<th scope="col">Status</th>
					<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
					<th scope="col">Aksi</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php echo $htm_table_hak; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>
<?php if($action == "tambah" or $action == "edit"){ ?>
<div class="card">
	<div class="card-body">
		<form method="post" action="">
			<input type="hidden" name="form_action" value="<?php echo $action; ?>">
			<input type="hidden" name="id_hak_akses" value="<?php echo @$get_edit_admin_menu_hak[0]->id; ?>">
			<div class="form-group">
				<label>Nama Hak Akses</label>
				<input type="text" class="form-control" name="name" value="<?php echo @$get_edit_admin_menu_hak[0]->name; ?>" required="required">
			</div>
			<div class="form-group">
				<label>Keterangan</label>
				<textarea class="form-control" name="description" required="required"><?php echo @$get_edit_admin_menu_hak[0]->description; ?></textarea>
			</div>
			<div class="form-group">
				<label>Status</label>
				<select class="form-control" name="status" required="required">
					<option value='1' <?php echo(isset($get_edit_admin_menu_hak[0]->status) and $get_edit_admin_menu_hak[0]->status == "1")?"selected=selected":""; ?>><?php echo statLabel("1"); ?></option>
					<option value='0' <?php echo(isset($get_edit_admin_menu_hak[0]->status) and $get_edit_admin_menu_hak[0]->status == "0")?"selected=selected":""; ?>><?php echo statLabel("0"); ?></option>
				</select>
			</div>
			<table class="table table-data">
				<thead class="thead-dark">
					<tr>
						<th scope="col">#</th>
						<th scope="col">Nama Menu</th>
						<th scope="col">Lihat</th>
						<th scope="col">Tambah</th>
						<th scope="col">Ubah</th>
						<th scope="col">Hapus</th>
					</tr>
				</thead>
				<tbody>
					<?php echo $htm_table_menu; ?>
				</tbody>
			</table>
			<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>" class="btn btn-secondaray btn-flat">Back</a>
			<button type="submit" name="submit_hak_akses" class="btn btn-primary btn-flat">Submit</button>
		</form>
	</div>
</div>
<?php } ?>
<script>
	var VG_onpage_data_table = "admin_menu_hak";
	$j(document).on("click","#bt_print",function(){
		printTableData();
	});
</script>
<?php } ?>