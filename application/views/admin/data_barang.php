<?php 
$aksesKey = "admin/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug();
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}

	$no=0;
	$htm_table_data_barang = "";
	foreach($get_data_barang as $row){
		$htm_table_data_barang.="
						<tr data-id=\"".$row->id."\">
							<th scope=\"row\">".($no+=1)."</th>
							<td>".$row->kode_barang."</td>
							<td>".$row->nama_barang."</td>
							<td>".$row->nama_group."</td>
							<td>".$row->deskripsi."</td>
							<td>".$row->nama_satuan."</td>
							<td>".$row->min_stok."</td>
							<td>".btnStatLabel($row->status)."</td>
							<td>";
								if(isset($aksesUbah)) $htm_table_data_barang.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/edit/".$row->id."\" class=\"btn btn-outline-success btn-sm\"><i class=\"fa fa-edit\"></i>&nbsp; Edit</a> ";
								if(isset($aksesHapus)) $htm_table_data_barang.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/hapus/".$row->id."\" class=\"btn btn-outline-danger btn-sm\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";
		  $htm_table_data_barang.="</td>
						</tr>
					";
	}
	if($htm_table_data_barang == ""){
		$htm_table_data_barang .= "<tr><th colspan='10' class=\"text-center\">. : Data Kosong : .</th></tr>";
		$htm_table_data_barang .= "<tr><th colspan='10' class=\"text-center\">&nbsp;</th></tr>";
	}
	
	$htm_option_group_barang = "";
	foreach($get_group_barang as $row){
		//
		$htm_option_group_barang.="<option value=\"".$row->id."\" ".((isset($get_edit_data_barang[0]->id_group) and $get_edit_data_barang[0]->id_group == $row->id)?"selected=selected":"")." >".$row->nama_group."</option>";
	}
	
	$htm_option_satuan = "";
	foreach($get_satuan as $row){
		$htm_option_satuan.="<option value=\"".$row->id."\" ".((isset($get_edit_data_barang[0]->id_satuan) and $get_edit_data_barang[0]->id_satuan == $row->id)?"selected=selected":"")." >".$row->nama_satuan."</option>";
	}
	
	if($action == "tambah"){
		$tambahRequired = " required='required' ";
	}else if($action == "edit"){
		$editHintSkip = "<span class=\"help-block text-warning\">Biarkan kolom ini jika tidak akan menggantinya.</span>";
		$editRequired = " required='required' ";
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>">Data Barang <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-6">
		<form action="" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari" placeholder="Cari Data Barang" class="form-control form-control-sm" required="required" />
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<?php if(isset($aksesTambah)){ ?>
		<a href="<?php echo base_url()."admin/".$this->router->fetch_method()."/tambah"; ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil"></i>&nbsp; Tambah</a>
		<?php } ?>
		<button type="button" class="btn btn-outline-warning btn-sm" id="bt_print"><i class="fa fa-print"></i>&nbsp; Cetak</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="thead-dark">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Kode Barang</th>
					<th scope="col">Nama Barang</th>
					<th scope="col">Group Barang</th>
					<th scope="col">Deskripsi</th>
					<th scope="col">Satuan</th>
					<th scope="col">Min Stok</th>
					<th scope="col">Status</th>
					<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
					<th scope="col">Aksi</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php echo $htm_table_data_barang; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>
<?php if($action == "tambah" or $action == "edit"){ ?>
<div class="card">
	<div class="card-body">
		<form method="post" action="" enctype="multipart/form-data" name="form_barang" class="image-editor">
			<input type="hidden" name="form_action" value="<?php echo $action; ?>">
			<input type="hidden" name="id_data_barang" value="<?php echo @$get_edit_data_barang[0]->id; ?>">
			<div class="form-group">
				<label>Group Barang</label>
				<select data-placeholder="Pilih..." class="form-control standardSelect" name="id_group" required="required" tabindex="1">
					<option value="" label="Pilih..."></option>
					<?php echo $htm_option_group_barang; ?>
				</select>
			</div>
			<div class="form-group">
				<label>Kode Barang</label>
				<input type="text" class="form-control" name="kode_barang" value="<?php echo @$get_edit_data_barang[0]->kode_barang; ?>" required="required">
			</div>
			<div class="form-group">
				<label>Nama Barang</label>
				<input type="text" class="form-control" name="nama_barang" value="<?php echo @$get_edit_data_barang[0]->nama_barang; ?>" required="required">
			</div>
			<div class="form-group">
				<label>Min Stok</label>
				<input type="text" class="form-control" name="min_stok" value="<?php echo @$get_edit_data_barang[0]->min_stok; ?>" required="required">
			</div>
			<div class="form-group">
				<label>Satuan</label>
				<select data-placeholder="Pilih..." class="form-control standardSelect" name="id_satuan" required="required" tabindex="1">
					<option value="" label="Pilih..."></option>
					<?php echo $htm_option_satuan; ?>
				</select>
			</div>
			<div class="form-group">
				<label>Deskripsi</label>
				<textarea class="form-control" name="deskripsi"><?php echo @$get_edit_data_barang[0]->deskripsi; ?></textarea>
			</div>
			<div class="form-group">
				<label>Status</label>
				<select class="form-control" name="status" required="required">
					<option value='1' <?php echo(isset($get_edit_data_barang[0]->status) and $get_edit_data_barang[0]->status == "1")?"selected=selected":""; ?>><?php echo statLabel("1"); ?></option>
					<option value='0' <?php echo(isset($get_edit_data_barang[0]->status) and $get_edit_data_barang[0]->status == "0")?"selected=selected":""; ?>><?php echo statLabel("0"); ?></option>
				</select>
			</div>
			<div class="form-group">
				<label>File</label>
				<input type="file" class="form-control-file cropit-image-input" name="image">
				<?php echo @$editHintSkip; ?>
				<?php if(isset($get_edit_data_barang[0]->image)) echo "<br/><img src='".base_url()."assets/upload_product/100/".$get_edit_data_barang[0]->image."' style='max-width:100px;margin:10px 0px 0px 0px;' />"; ?>
				<div class="cropit-preview"></div>
				<div class="image-size-label">Resize image</div>
				<input type="range" class="cropit-image-zoom-input" name="image-zoom">
				<input type="hidden" class="hidden-image-data" name="image-data"/>
				<input type="hidden" name="file_image_name" value="<?php echo @$get_edit_data_barang[0]->image; ?>"/>
			</div>
			<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>" class="btn btn-secondaray btn-flat">Back</a>
			<button type="submit" name="submit_data_barang" class="btn btn-primary btn-flat">Submit</button>
		</form>
	</div>
</div>
<?php } ?>
<script>
	var VG_onpage_data_table = "tbl_data_barang";
	$j(document).on("click","#bt_print",function(){
		printTableData();
	});

	$j(".standardSelect").chosen({
		disable_search_threshold: 10,
		no_results_text: "Oops, nothing found!",
		width: "100%"
	});
	$j("form[name='form_barang']").submit(function(){
		var imageData = $j('.image-editor').cropit('export');
		$j('.hidden-image-data').val(window.btoa(imageData));
		//return false;
	});
// 	$j("select[name='id_group']").change(function(){
// 		var id_group = $j(this).val();
// 		var id_data_barang = "<?php echo @$get_edit_data_barang[0]->id; ?>";
// 		$j.ajax({
// 			type:"POST",
// 			url:"<?php echo base_url()."admin/apiweb"; ?>",
// 			data:{action:"get_kode_barang",id_group:id_group,id_data_barang:id_data_barang},
// 			success: function(res){
// 				$j("input[name='kode_barang']").val(res);
// 			}
// 		});
// 	});
	$j(document).ready(function(){
		$j('.image-editor').cropit();
	});
</script>
<?php } ?>