<?php 
$aksesKey = $this->router->fetch_class()."/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug();
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}
	
	//echo $get_laporan_bagi_hasil_saham['get_laporan_pendapatan_length'];
	//debug($get_laporan_bagi_hasil_saham['get_laporan_pendapatan']);
	
	$no=0;
	$htm_table_investor = "";
	if($get_laporan_bagi_hasil_saham['get_laporan_pendapatan_length'] > 0){
		foreach($get_laporan_bagi_hasil_saham['get_data_investor'] as $row){
			$htm_table_investor.="
							<tr data-id=\"".$row['id']."\">
								<td scope=\"row\"><b>".($no+=1)."</b></td>
								<td>".repair_date($row['tanggal'])."</td>
								<td>".$row['id_investor']."</td>
								<td>".$row['nama_investor']."</td>
								<td>".$row['email']."</td>
								<td>".format_rupiah($row['nilai_investasi'])."</td>
								<td>".@$row['persentase']."%</td>
								<td>".@format_rupiah(@$row['pendapatan_investasi'])."</td>
							</tr>
						";
		}
	}
	if($htm_table_investor == ""){
		$htm_table_investor .= "<tr><th colspan='15' class=\"text-center\">. : Data Kosong : .</th></tr>";
		$htm_table_investor .= "<tr><th colspan='15' class=\"text-center\">&nbsp;</th></tr>";
	}
	
	
	
	if($action == "tambah"){
		$tambahRequired = " required='required' ";
	}else if($action == "edit"){
		$editRequired = " required='required' ";
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method(); ?>">Laporan Bagi Hasil Saham <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-3">
		<div class="card">
			<div class="card-body p-2">
				<div style="font-size:12px;">Nilai Pendapatan Perusahaan</div>
				<div style="font-size:12px;">&nbsp;</div>
				<div><h2 class="card-text font-weight-bold text-warning"><?php echo format_rupiah(@$get_laporan_bagi_hasil_saham['get_laporan_pendapatan']['nilai_pendapatan_perusahaan']); ?></h2></div>
			</div>
		</div>
	</div>
	<div class="col-3">
		<div class="card">
			<div class="card-body p-2">
				<div style="font-size:12px;">Zakat / Infaq (otomatis 2,5%)</div>
				<div style="font-size:12px;">dari Nilai Pendapatan Perusahaan</div>
				<div><h2 class="card-text font-weight-bold text-info"><?php echo format_rupiah(@$get_laporan_bagi_hasil_saham['get_laporan_pendapatan']['zakat']); ?></h2></div>
			</div>
		</div>
	</div>
	<div class="col-3">
		<div class="card">
			<div class="card-body p-2">
				<div style="font-size:12px;">Total Biaya - Biaya</div>
				<div style="font-size:12px;">&nbsp;</div>
				<div><h2 class="card-text font-weight-bold text-danger"><?php echo format_rupiah(@$get_laporan_bagi_hasil_saham['get_laporan_pendapatan']['biaya_biaya']); ?></h2></div>
			</div>
		</div>
	</div>
	<div class="col-3">
		<div class="card">
			<div class="card-body p-2">
				<div style="font-size:12px;">Grand Total Pendapatan Bersih</div>
				<div style="font-size:12px;">&nbsp;</div>
				<div><h2 class="card-text font-weight-bold text-success"><?php echo format_rupiah(@$get_laporan_bagi_hasil_saham['get_laporan_pendapatan']['total_pendapatan_bersih']); ?></h2></div>
			</div>
		</div>
	</div>
</div>
<div class="row mb-3">
	<div class="col-9">
		<form action="" method="post" class="form-horizontal form-inline" name="form_laporan_filter">
			<div class="form-group">
				<input type="date" class="form-control form-control-sm" name="tanggal_dari" value="<?php echo(isset($_REQUEST['tanggal_dari']))?$_REQUEST['tanggal_dari']:date('Y-m-d'); ?>"/>
			</div>
			<div class="form-group">
				&nbsp;S/D&nbsp;
			</div>
			<div class="form-group">
				<input type="date" class="form-control form-control-sm" name="tanggal_ke" value="<?php echo(isset($_REQUEST['tanggal_ke']))?$_REQUEST['tanggal_ke']:date('Y-m-d'); ?>"/>
			</div>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<div class="form-group">
				<select class="form-control form-control-sm" name="periode">
					<option value="">Pilih...</option>
					<option value="hari_ini">Hari Ini</option>
					<option value="minggu_ini">Minggu Ini</option>
					<option value="bulan_ini">Bulan Ini</option>
					<option value="tahun_ini">Tahun Ini</option>
					<option value="kemarin">Kemarin</option>
					<option value="minggu_lalu">Minggu Lalu</option>
					<option value="bulan_lalu">Bulan Lalu</option>
					<option value="tahun_lalu">Tahun Lalu</option>
				</select>
			</div>
			<script>
				$j("select[name='periode']").val("<?php echo @$_REQUEST['periode']; ?>");
			</script>
			&nbsp;
			<button type="submit" class="btn btn-outline-info btn-sm" name="bt_rentang"><i class="fa fa-search"></i></button>
		</form>
	</div>
	<div class="col-3 text-right">
		<button type="button" class="btn btn-outline-success btn-sm btn-export" data-type="to_excel"><i class="fa fa-file-excel-o"></i>&nbsp; to Excel</button>
		<button type="button" class="btn btn-outline-danger btn-sm btn-export" data-type="to_pdf"><i class="fa fa-file-pdf-o"></i>&nbsp; to PDF</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="bg-info">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Tanggal</th>
					<th scope="col">Id Investor</th>
					<th scope="col">Nama Investor</th>
					<th scope="col">Email</th>
					<th scope="col">Nilai Investasi</th>
					<th scope="col">Persentase</th>
					<th scope="col">Pendapatan Investasi</th>
				</tr>
			</thead>
			<tbody>
				<?php echo @$htm_table_investor; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Bulan', 'Pendapatan'],
          <?php echo @$get_laporan_bagi_hasil_saham['chart_pendapatan_tahun_ini']; ?>
        ]);
		
		/*
        var data = google.visualization.arrayToDataTable([
          ['Bulan', 'Pendapatan'],
          ['Jan', 1000],
          ['Feb', 1170],
          ['Mar', 660],
          ['Apr', 1030]
        ]);
		*/

        var options = {
          chart: {
            //title: 'Company Performance',
            //subtitle: 'Sales, Expenses, and Profit: 2014-2017',
          }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }

</script>
<script>
	var VG_onpage_data_table = "tbl_transaksi_penjualan";
	var BASE_URL = "<?php echo base_url(); ?>";
	var aksesHapus = "<?php echo @$aksesHapus; ?>";
	var action = "<?php echo $action; ?>";
	var btnDetailNoSeri = null;
	var no=0;
	$j("form[name='form_laporan_filter']").on("change","select[name='periode']",function(){
		var val = $j(this).val();
			var Dt = new Date();
			var dayTo = String(Dt.getDay() + 1).padStart(2, '0');
			var dd = String(Dt.getDate()).padStart(2, '0');
			var mm = String(Dt.getMonth() + 1).padStart(2, '0');
			var yyyy = Dt.getFullYear();
			var today = yyyy+'-'+mm+'-'+dd;
		if(val == "hari_ini"){
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_dari']").val(today);
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_ke']").val(today);
		}
		else if(val == "minggu_ini"){
			var this_week = yyyy+'-'+mm+'-'+String(dd-dayTo+1).padStart(2, '0');
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_dari']").val(this_week);
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_ke']").val(today);
		}
		else if(val == "bulan_ini"){
			var this_mount = yyyy+'-'+mm+'-01';
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_dari']").val(this_mount);
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_ke']").val(today);
		}
		else if(val == "tahun_ini"){
			var this_year = yyyy+'-01-01';
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_dari']").val(this_year);
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_ke']").val(today);
		}
		else if(val == "kemarin"){
			Dt.setDate(Dt.getDate() - 1);
			var dd = String(Dt.getDate()).padStart(2, '0');
			var mm = String(Dt.getMonth() + 1).padStart(2, '0');
			var yyyy = Dt.getFullYear();
			var yesterday = yyyy+'-'+mm+'-'+dd;
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_dari']").val(yesterday);
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_ke']").val(yesterday);
		}
		else if(val == "minggu_lalu"){
			Dt.setDate(Dt.getDate() - 7);
			var dayTo = String(Dt.getDay() + 1).padStart(2, '0');
			var dd = String(Dt.getDate()).padStart(2, '0');
			var mm = String(Dt.getMonth() + 1).padStart(2, '0');
			var yyyy = Dt.getFullYear();
			var last_week_start = yyyy+'-'+mm+'-'+String(dd-dayTo+1).padStart(2, '0');
			var last_week_end = yyyy+'-'+mm+'-'+String((dd-dayTo+1)+6).padStart(2, '0');
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_dari']").val(last_week_start);
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_ke']").val(last_week_end);
		}
		else if(val == "bulan_lalu"){
			//Dt.setMonth(Dt.getMonth() - 1);
			Dt.setDate(0);
			var dd = String(Dt.getDate()).padStart(2, '0');
			var mm = String(Dt.getMonth() + 1).padStart(2, '0');
			var yyyy = Dt.getFullYear();
			var last_mount_start = yyyy+'-'+mm+'-01';
			var last_mount_end = yyyy+'-'+mm+'-'+dd;
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_dari']").val(last_mount_start);
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_ke']").val(last_mount_end);
		}
		else if(val == "tahun_lalu"){
			//Dt.setMonth(Dt.getMonth() - 1);
			Dt.setMonth(0);
			Dt.setDate(0);
			var dd = String(Dt.getDate()).padStart(2, '0');
			var mm = String(Dt.getMonth() + 1).padStart(2, '0');
			var yyyy = Dt.getFullYear();
			var last_year_start = yyyy+'-01-01';
			var last_year_end = yyyy+'-'+mm+'-'+dd;
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_dari']").val(last_year_start);
			$j("form[name='form_laporan_filter']").find("input[name='tanggal_ke']").val(last_year_end);
		}
	});
	$j(document).on("click",".btn-print",function(){
		var data = $j(this).parents("div.col-lg-12").html();
		printScreen("<div class='row'><div class='col-lg-12'>"+data+"</div></div>");
	});
	$j("button.btn-export").click(function(){
		var data_type = $j(this).data("type");
		var dtH = {};
		var i = 0;
		$j("table.table-data").find("thead tr th").each(function(){
			var dt = $j(this).html();
			dtH[i] = dt;
			i++;
		});
		
		var dtB = {};
		var i = 0;
		$j("table.table-data").find("tbody tr").each(function(){
			var dtR = {};
			var ir = 0;
			$j(this).find("td").each(function(){
				var dt = $j(this).html();
				if(String(dt).trim() != ""){
					dtR[ir] = dt;
					ir++;		
				}				
			});
			dtB[i] = dtR;
			i++;
		});
		var dataFD = {'header':dtH,'body':dtB};
		var encData = btoa(JSON.stringify(dataFD));
		if(data_type == "to_excel"){
			var export_type = "file_excel";
		}else if(data_type == "to_pdf"){
			var export_type = "file_pdf";
		}
		$j.ajax({
			type:"POST",
			url:"<?php echo base_url()."admin/apiweb"; ?>",
			data:{action:"post_download_data",type:export_type,data:encData},
			dataType:"json",
			success: function(obj){
				if(obj == true){
					window.open("<?php echo base_url()."download/"; ?>"+export_type,"_blank");
				}else{
					alert("error download, code: "+obj);
				}
			}
		});
	});
	$j(document).ready(function(){
		$j('table#transaksi_barang').DataTable({
			"pageLength": 100
		});
	});
</script>
<?php } ?>