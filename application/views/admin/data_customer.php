<style>
.card .nav-tabs a {
    border-top-left-radius: 5px;
	border-top-right-radius: 5px;
    color: #b5bcc2;
    font-size: 15px;
    font-weight: normal;
    padding: 5px 10px;
    text-transform: none;
}
</style>
<?php
$aksesKey = "admin/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug();
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}

	$no=0;
	$htm_table_data_customer = "";
	foreach($get_data_customer as $row){
		$htm_table_data_customer.="
						<tr data-id=\"".$row->id."\">
							<th scope=\"row\">".($no+=1)."</th>
							<td>".$row->kode_customer."</td>
							<td>".$row->nama_customer."</td>
							<td>".$row->nama_group_customer."</td>
							<td>".$row->alamat."</td>
							<td>".$row->nama_kota."</td>
							<td>".$row->telepon."</td>
							<td>".$row->jatuh_tempo."</td>
							<td>".btnStatLabel($row->status)."</td>
							<td>";
								if(isset($aksesUbah)) $htm_table_data_customer.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/edit/".$row->id."\" class=\"btn btn-outline-success btn-sm\"><i class=\"fa fa-edit\"></i>&nbsp; Edit</a> ";
								if(isset($aksesHapus)) $htm_table_data_customer.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/hapus/".$row->id."\" class=\"btn btn-outline-danger btn-sm\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";
		  $htm_table_data_customer.="</td>
						</tr>
					";
	}
	if($htm_table_data_customer == ""){
		$htm_table_data_customer .= "<tr><th colspan='10' class=\"text-center\">. : Data Kosong : .</th></tr>";
		$htm_table_data_customer .= "<tr><th colspan='10' class=\"text-center\">&nbsp;</th></tr>";
	}
	
	$htm_option_group_customer = "";
	foreach($get_group_customer as $row){
		$htm_option_group_customer.="<option value=\"".$row->id."\" ".((isset($get_edit_data_customer[0]->id_group_customer) and $get_edit_data_customer[0]->id_group_customer == $row->id)?"selected=selected":"")." >".$row->nama_group_customer."</option>";
	}
	
	
	$htm_option_id_kota = "";
	foreach($get_kota as $row){
		$htm_option_id_kota.="<option value=\"".$row->id."\" data-kode-kota=\"".$row->kode_kota."\" data-nama-kota=\"".$row->nama_kota."\"
									".((isset($get_edit_data_customer[0]->id_kota) and $get_edit_data_customer[0]->id_kota == $row->id)?"selected=selected":"")." 
								>".$row->nama_kota."</option>";
	}
	
	if($action == "tambah"){
		$tambahRequired = " required='required' ";
		$passRequired = " required='required' ";
	}else if($action == "edit"){
		$hintPassword = "<span class=\"help-block text-warning\">Kosongkan kolom Password jika tidak akan mengganti Password.</span>";
		$editHintSkip = "<span class=\"help-block text-warning\">Biarkan kolom ini jika tidak akan menggantinya.</span>";
		$editRequired = " required='required' ";
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>">Data Customer <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-6">
		<form action="" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari" placeholder="Cari Data Customer" class="form-control form-control-sm" required="required" />
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<?php if(isset($aksesTambah)){ ?>
		<a href="<?php echo base_url()."admin/".$this->router->fetch_method()."/tambah"; ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil"></i>&nbsp; Tambah</a>
		<?php } ?>
		<button type="button" class="btn btn-outline-warning btn-sm" id="bt_print"><i class="fa fa-print"></i>&nbsp; Cetak</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="thead-dark">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Kode Customer</th>
					<th scope="col">Nama Customer</th>
					<th scope="col">Group Customer</th>
					<th scope="col">Alamat</th>
					<th scope="col">Kota</th>
					<th scope="col">No Telp</th>
					<th scope="col">JT</th>
					<th scope="col">Status</th>
					<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
					<th scope="col">Aksi</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php echo $htm_table_data_customer; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>
<?php if($action == "tambah" or $action == "edit"){ ?>
<div class="card">
	<div class="card-body">
		<form method="post" action="" name="form_kota" enctype="multipart/form-data">
			<input type="hidden" name="form_action" value="<?php echo $action; ?>">
			<input type="hidden" name="id_customer" value="<?php echo @$get_edit_data_customer[0]->id; ?>">
			<nav>
			  <div class="nav nav-tabs" id="nav-tab" role="tablist">
				<a class="nav-item nav-link active" id="nav-data-customer-tab" data-toggle="tab" href="#nav-data-customer" role="tab" aria-controls="nav-data-customer" aria-selected="true">Data Customer</a>
				<a class="nav-item nav-link" id="nav-data-ahli-waris-tab" data-toggle="tab" href="#nav-data-ahli-waris" role="tab" aria-controls="nav-data-ahli-waris" aria-selected="false">Data Ahli Waris</a>
			  </div>
			</nav>
			<div class="tab-content pt-4" id="nav-tabContent">
			  <div class="tab-pane show active" id="nav-data-customer" role="tabpanel" aria-labelledby="nav-data-customer-tab">
					<div class="form-group">
						<label>Group Customer</label>
						<select data-placeholder="Pilih..." class="form-control standardSelect" name="id_group_customer" required="required" tabindex="1">
							<option value="" label="Pilih..."></option>
							<?php echo $htm_option_group_customer; ?>
						</select>
					</div>
					<div class="form-group">
						<label>Kode Customer</label>
						<input type="text" class="form-control" name="kode_customer" value="<?php echo @$get_edit_data_customer[0]->kode_customer; ?>" required="required" readonly="readonly">
					</div>
					<div class="form-group">
						<label>Nama Customer</label>
						<input type="text" class="form-control" name="nama_customer" value="<?php echo @$get_edit_data_customer[0]->nama_customer; ?>" required="required">
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="email" class="form-control" name="email" value="<?php echo @$get_edit_data_customer[0]->email; ?>" required="required">
					</div>
					<div class="form-group">
						<label>Telepon</label>
						<input type="text" class="form-control" name="telepon" value="<?php echo @$get_edit_data_customer[0]->telepon; ?>" required="required">
					</div>
					<div class="form-group">
						<label>Alamat</label>
						<textarea class="form-control" name="alamat"><?php echo @$get_edit_data_customer[0]->alamat; ?></textarea>
					</div>
					<div class="form-group">
						<label>No Rekening</label>
						<input type="text" class="form-control" name="no_rekening" value="<?php echo @$get_edit_data_customer[0]->no_rekening; ?>">
					</div>
					<div class="form-group">
						<label>A/n Rekening</label>
						<input type="text" class="form-control" name="atas_nama_rekening" value="<?php echo @$get_edit_data_customer[0]->atas_nama_rekening; ?>">
					</div>
					<div class="form-group">
						<label>Id Kota</label>
						<select data-placeholder="Pilih..." class="form-control standardSelect" name="id_kota" tabindex="1">
							<option value="" data-kode-kota="" data-nama-kota="">Pilih...</option>
							<?php echo $htm_option_id_kota; ?>
						</select>
					</div>
					<div class="form-group">
						<label>Kode Kota</label>
						<input type="text" class="form-control" name="kode_kota" value="<?php echo @$get_edit_data_customer[0]->kode_kota; ?>" readonly="readonly">
					</div>
					<div class="form-group">
						<label>Nama Kota</label>
						<input type="text" class="form-control" name="nama_kota" value="<?php echo @$get_edit_data_customer[0]->nama_kota; ?>" readonly="readonly">
					</div>
					<div class="form-group">
						<label>Nama Toko</label>
						<input type="text" class="form-control" name="nama_toko" value="<?php echo @$get_edit_data_customer[0]->nama_toko; ?>">
					</div>
					<div class="form-group">
						<label>Alamat Toko</label>
						<textarea class="form-control" name="alamat_toko"><?php echo @$get_edit_data_customer[0]->alamat_toko; ?></textarea>
					</div>
					<div class="form-group">
						<label>Jatuh Tempo</label>
						<input type="text" class="form-control" name="jatuh_tempo" value="<?php echo @$get_edit_data_customer[0]->jatuh_tempo; ?>">
					</div>
					<div class="form-group">
						<label>Status</label>
						<select class="form-control" name="status" required="required">
							<option value='1' <?php echo(isset($get_edit_data_customer[0]->status) and $get_edit_data_customer[0]->status == "1")?"selected=selected":""; ?>><?php echo statLabel("1"); ?></option>
							<option value='0' <?php echo(isset($get_edit_data_customer[0]->status) and $get_edit_data_customer[0]->status == "0")?"selected=selected":""; ?>><?php echo statLabel("0"); ?></option>
						</select>
					</div>
					<div class="form-group">
						<label>KTP</label>
						<input type="file" class="form-control-file" name="image_ktp" accept="image/jpg,image/x-png">
						<?php echo @$editHintSkip; ?>
						<?php if(isset($get_edit_data_customer[0]->image_ktp)) echo "<br/><img src='".base_url()."assets/upload_ktp_customer/original/".$get_edit_data_customer[0]->image_ktp."' style='max-width:100px;margin:10px 0px 0px 0px;' />"; ?>
						<input type="hidden" name="file_image_ktp_name" value="<?php echo @$get_edit_data_customer[0]->image_ktp; ?>"/>
					</div>
					<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>" class="btn btn-secondaray btn-flat">Back</a>
					<a href="javascript:void(0);" class="btn btn-info btn-flat nav-data-ahli-waris-tab-show">Next</a>
			  </div>
			  <div class="tab-pane" id="nav-data-ahli-waris" role="tabpanel" aria-labelledby="nav-data-ahli-waris-tab">
					<div class="form-group">
						<label>Nama Istri</label>
						<input type="text" class="form-control" name="nama_istri" value="<?php echo @$get_edit_data_customer[0]->nama_istri; ?>">
					</div>
					<div class="form-group">
						<label>Email Istri</label>
						<input type="email" class="form-control" name="email_istri" value="<?php echo @$get_edit_data_customer[0]->email_istri; ?>">
					</div>
					<div class="form-group">
						<label>Telepon Istri</label>
						<input type="text" class="form-control" name="telepon_istri" value="<?php echo @$get_edit_data_customer[0]->telepon_istri; ?>">
					</div>
					<div class="form-group">
						<label>Alamat Istri</label>
						<textarea class="form-control" name="alamat_istri"><?php echo @$get_edit_data_customer[0]->alamat_istri; ?></textarea>
					</div>
					<div class="form-group">
						<label>Jumlah Anak</label>
						<input type="number" class="form-control" name="jumlah_anak" value="<?php echo @$get_edit_data_customer[0]->jumlah_anak; ?>">
					</div>
					<div class="form-group">
						<label>Nama Anak</label>
						<input type="text" class="form-control" name="nama_anak" value="<?php echo @$get_edit_data_customer[0]->nama_anak; ?>">
					</div>
					<div class="form-group">
						<label>KTP Istri</label>
						<input type="file" class="form-control-file" name="image_ktp_istri" accept="image/jpg,image/x-png">
						<?php echo @$editHintSkip; ?>
						<?php if(isset($get_edit_data_customer[0]->image_ktp_istri)) echo "<br/><img src='".base_url()."assets/upload_ktp_customer/display/".$get_edit_data_customer[0]->image_ktp_istri."' style='max-width:100px;margin:10px 0px 0px 0px;' />"; ?>
						<input type="hidden" name="file_image_ktp_istri_name" value="<?php echo @$get_edit_data_customer[0]->image_ktp_istri; ?>"/>
					</div>
					<a href="javascript:void(0);" class="btn btn-info btn-flat nav-data-customer-tab-show">Back</a>
					<button type="submit" name="submit_data_customer" class="btn btn-primary btn-flat">Submit</button>
			  </div>
			</div>
		</form>
	</div>
</div>
<?php } ?>
<script>
	var VG_onpage_data_table = "tbl_customer";
	$j(document).on("click","#bt_print",function(){
		printTableData();
	});
	$j("select[name='id_group_customer']").change(function(){
		var id_group_customer = $j(this).val();
		$j("input[name='kode_customer']").val(id_group_customer);
	});
	$j("select[name='id_kota']").change(function(){
		var kode_kota = $j('option:selected', this).data("kode-kota");
		var nama_kota = $j('option:selected', this).data("nama-kota");
		$j("input[name='kode_kota']").val(kode_kota);
		$j("input[name='nama_kota']").val(nama_kota);
	});
	$j("form[name='form_kota']").submit(function(){
		var ret = true;
		/*
		var id_kota = $j(this).find("select[name='id_kota']").val();
		if(id_kota == ""){
			ret = false;
			alert("kolom kota belum di pilih");
		}
		*/
		return ret;
	});
	$j(".nav-data-ahli-waris-tab-show").click(function(){
		$j("a#nav-data-ahli-waris-tab").trigger("click");
	});
	$j(".nav-data-customer-tab-show").click(function(){
		$j("a#nav-data-customer-tab").trigger("click");
	});
	$j(".standardSelect").chosen({
		disable_search_threshold: 10,
		no_results_text: "Oops, nothing found!",
		width: "100%"
	});
</script>
<?php } ?>