<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Wanna Syariah</title>
    <meta name="description" content="Wanna Syariah">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
    <link rel="manifest" href="<?php echo base_url("/"); ?>manifest.json">
	<link rel="apple-touch-icon" href="https://i.imgur.com/QRAUqs9.png">
    <link rel="shortcut icon" href="https://i.imgur.com/QRAUqs9.png">
	
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css">
	<link rel="stylesheet" href="<?php echo base_url("/assets/"); ?>ElaAdmin-master/assets/css/lib/chosen/chosen.min.css?n=116">
    <link rel="stylesheet" href="<?php echo base_url("/assets/"); ?>ElaAdmin-master/assets/css/cs-skin-elastic.css?n=116">
    <link rel="stylesheet" href="<?php echo base_url("/assets/"); ?>ElaAdmin-master/assets/css/style.css?n=116">
	<link rel="stylesheet" href="<?php echo base_url("/assets/"); ?>default_style.css?n=116">
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    <link href="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css" rel="stylesheet">
    <!-- <link href="https://cdn.jsdelivr.net/npm/weathericons@2.1.0/css/weather-icons.css" rel="stylesheet" /> -->
    <link href="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css" rel="stylesheet" />
	<link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
	
	
	
    <!-- Scripts -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="<?php echo base_url("/assets/"); ?>ElaAdmin-master/assets/js/main.js?n=116"></script>

    <!--  Chart js -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.bundle.min.js"></script>

    <!--Chartist Chart-->
    <script src="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartist-plugin-legend@0.6.2/chartist-plugin-legend.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/jquery.flot@0.8.3/jquery.flot.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flot-pie@1.0.0/src/jquery.flot.pie.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flot-spline@0.0.1/js/jquery.flot.spline.min.js"></script>
	<!--
    <script src="https://cdn.jsdelivr.net/npm/simpleweather@3.1.0/jquery.simpleWeather.min.js"></script>
    <script src="<?php echo base_url("/assets/"); ?>ElaAdmin-master/assets/js/init/weather-init.js?n=116"></script>
	-->
    <script src="https://cdn.jsdelivr.net/npm/moment@2.22.2/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.js"></script>
    <script src="<?php echo base_url("/assets/"); ?>ElaAdmin-master/assets/js/init/fullcalendar-init.js?n=116"></script>
	<script src="<?php echo base_url("/assets/"); ?>ElaAdmin-master/assets/js/lib/chosen/chosen.jquery.min.js?n=116"></script>
	<script src="<?php echo base_url("/assets/"); ?>cropit-master/dist/jquery.cropit.js?n=116"></script>
	<script src="<?php echo base_url("/assets/"); ?>default_javascript.js?n=116"></script>
	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	
    <script> 
		var $j = jQuery.noConflict(); 
	</script>
	<!-- The core Firebase JS SDK is always required and must be listed first -->
	<script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
	<script>
	function getCountDataMobileOrder(){
			$j.ajax({
				type:"POST",
				url:"<?php echo base_url(); ?>admin/apiweb",
				data:{action:"get_count_data_mobile_order"},
				dataType:"JSON",
				success: function(obj){
					firebaseTblInitialize.update({"count_order_baru" : obj.count});
				}
			});
	}
	// Your web app's Firebase configuration
	var firebaseConfig = {
		apiKey: "AIzaSyDj1MRlX268YrkdTx9-9EAgPVR15xwPI5c",
		authDomain: "project-advess.firebaseapp.com",
		databaseURL: "https://project-advess.firebaseio.com",
		projectId: "project-advess",
		storageBucket: "project-advess.appspot.com",
		messagingSenderId: "71458147912",
		appId: "1:71458147912:web:b2496c60ff761942"
	};
	// Initialize Firebase
	firebase.initializeApp(firebaseConfig);
	firebaseTblInitialize = firebase.database().ref('db_advess_wanna_syariah/tbl_initialize/');
	firebaseTblInitialize.on('value',function(snap){ 
		var row = snap.val(); 
		if(typeof(row.status_order_baru) == "string" && row.status_order_baru == "1"){
			getCountDataMobileOrder();
		}
		if(typeof(row.count_order_baru) == "string" && parseInt(row.count_order_baru) > 0){
			getCountDataMobileOrder();
			$j(".order-new").show();
			$j(".order-new").html("<sup>"+row.count_order_baru+"</sup>");
		}
	});
	</script>
</head>
<?php 
	$get_admin_menu_hak_akses = $this->admin_model->get_admin_menu_hak_akses($_SESSION['admin_id_hak_akses']);
	$arrIdMenuOpen = array();
	foreach($get_admin_menu_hak_akses as $row){
		$arrIdMenuOpen[] = $row->id_menu;
	}
	$active_menu = $this->router->fetch_class()."/".$this->router->fetch_method();
	$list_menu = array();
	$get_admin_menu = $this->admin_model->get_admin_menu();
	foreach($get_admin_menu as $row){
		if($row->parent=="0" and $row->parent_stat=="0"){
			$id = $row->id;
			$url = $row->url;
			if (in_array($id, $arrIdMenuOpen))
				$list_menu[$url] = $row->icon." ".$row->name;
		}else if($row->parent=="0" and $row->parent_stat=="1"){
			$parent_name = $row->icon." ".$row->name;
			foreach($get_admin_menu as $row_sub){
				if($row->id == $row_sub->parent){
					$id_sub = $row_sub->id;
					$url_sub = $row_sub->url;
					if (in_array($id_sub, $arrIdMenuOpen))
						$list_menu[$parent_name][$url_sub] = " ".$row_sub->name;
				}
			}
		}
	}
	
	$htm_menu = "";
	foreach($list_menu as $key => $val){
		if(is_array($list_menu[$key])){
			$list_sub_menu = $list_menu[$key];
			$htm_menu .= "<li class=\"menu-item-has-children dropdown"; if(isset($list_sub_menu[$active_menu])) $htm_menu .= " active show ";
						$htm_menu .= "\"><a href=\"javascript:void(0);\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">".$key."</a>";
			$htm_menu .= "<ul class=\"sub-menu children dropdown-menu";
							if(isset($list_sub_menu[$active_menu])) $htm_menu .= " show ";
							$htm_menu .= "\">";
							foreach($list_sub_menu as $key_sub => $val_sub){
								$htm_menu .= "<li"; if($key_sub == $active_menu) $htm_menu .= " class=\"active\" ";
								$htm_menu .= "><i class=\"menu-icon fa fa-circle-o\"></i><a href=\"".base_url().$key_sub."\">".$val_sub."</a></li>";
							}
			$htm_menu .= "</ul>
						  </li>";
		}else{
			$htm_menu .= "<li";
				if($key == $active_menu) $htm_menu .= " class=\"active\" ";
			$htm_menu .= "><a href=\"".base_url().$key."\">".$val."</a></li>";
		}
	}
?>
<body>
    <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
			<a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url("/assets/"); ?>images/logo2.png" alt="Logo"></a>
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
					<?php echo $htm_menu; ?>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>
    <!-- /#left-panel -->
    <!-- Right Panel -->
    <div id="right-panel" class="right-panel">
        <!-- Header-->
        <header id="header" class="header">
            <div class="top-left">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url(); ?>"><!--<img src="<?php echo base_url("/assets/"); ?>images/logo2x.png" alt="Logo">--></a>
                    <a class="navbar-brand hidden" href="<?php echo base_url(); ?>"><img src="<?php echo base_url("/assets/"); ?>images/logo3.png" alt="Logo"></a>
                    <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
                </div>
            </div>
            <div class="top-right">
                <div class="header-menu">
					<div class="col">
						<div class="header-left">
							<div class="dropdown">
								<a href="<?php echo base_url("/penjualan/transaksi_penjualan/tambah"); ?>" class="btn btn-sm btn-header-def color-white" id="gear">
										Jual
								</a>
								<a href="<?php echo base_url("/pembelian/faktur_pembelian/tambah"); ?>" class="btn btn-sm btn-header-def color-white" id="gear">
										Beli
								</a>
								<a href="<?php echo base_url("/saham/input_pembagian_saham/tambah"); ?>" class="btn btn-sm btn-header-def color-white" id="gear">
										Biaya
								</a>
							</div>
						</div>
						<div class="header-left float-right">
							<div class="dropdown for-profile">
								<button class="btn btn-secondary dropdown-toggle" type="button" id="profile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-user"></i>
								</button>
								<div class="dropdown-menu" aria-labelledby="profile">
									<a class="nav-link" href="<?php echo base_url("/admin/profile"); ?>"><i class="fa fa- user"></i>My Profile</a>
									<a class="nav-link" href="<?php echo base_url("/admin/setting_app"); ?>"><i class="fa fa- user"></i>Setting</a>
									<a class="nav-link" href="<?php echo base_url("/admin/logout"); ?>"><i class="fa fa-power -off"></i>Logout</a>
								</div>
							</div>
						</div>
						<div class="header-left float-right">
							<div class="dropdown for-cloud">
								<a href="<?php echo base_url(); ?>" class="btn btn-secondary dropdown-toggle">
									<i class="fa fa-home"></i>
									<label style="color:white;">Dashboard</label>
								</a>
							</div>
						</div>
						<div class="header-left float-right">
							<div class="dropdown for-cloud">
								<a href="<?php echo base_url("/penjualan/request_dari_app"); ?>" class="btn btn-secondary dropdown-toggle">
									<i class="fa fa-info-circle bounce" ></i>
									<label class="bounce" style="color:white;">Order</label>
									<label class="order-new text-danger font-weight-bold bounce" style="display:none; font-size:18px;"><sup>New</sup></label>
								</a>
							</div>
						</div>
						<div class="header-left float-right">
							<div class="dropdown for-cloud">
								<a href="<?php echo base_url("/penjualan/request_dari_app"); ?>" class="btn btn-secondary dropdown-toggle">
									<i class="fa fa-info-circle"></i>
									<label style="color:white;">New Pay</label>
									<label class="pay-new text-warning font-weight-bold bounce" style="display:none;"><sup>New</sup></label>
								</a>
							</div>
						</div>
					</div>
                </div>
            </div>
        </header>
        <!-- /#header -->
        <!-- Content -->
        <div class="content">
            <!-- Animated -->
            <div class="animated fadeIn">
                <!--  Dashboard  -->
                <div class="row">
                    <div class="col-lg-12">
					<?php //debug($arrIdMenuOpen); ?>