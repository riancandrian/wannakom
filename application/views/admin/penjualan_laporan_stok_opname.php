<?php 
$aksesKey = $this->router->fetch_class()."/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug();
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}
	
	$no=0;
	$htm_table_stok_opname = "";
	foreach($get_laporan_barang_stok_opname as $row){
		$htm_table_stok_opname.="
										<tr>
											<td scope=\"row\">".($no+=1)."</td>
											<td>".$row->kode_barang."</td>
											<td><a href=\"".base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/detail/".$row->id."\" class=\"btn btn-link\">".$row->nama_barang."</a></td>
											<td>".$row->nama_satuan."</td>
											<td>".$row->stok_qty."</td>
											<td>".$row->deskripsi."</td>
										</tr>
									";
	}
	if($htm_table_stok_opname == ""){
		$htm_table_stok_opname .= "<tr><th colspan='20' class=\"text-center\">. : Data Kosong : .</th></tr>";
	}
		$htm_table_stok_opname .= "<tr><th colspan='20'>Menampilkan ".(($no>0)?1:0)." .. ".$no." dari ".$no." Baris</th></tr>";
	
	if(isset($get_laporan_stok_opname_transaksi_barang)){
		$pembelian = 0;
		$retur_pembelian = 0;
		$penjualan = 0;
		$retur_penjualan = 0;
		$no=0;
		$htm_table_transaksi_barang = "";
		foreach($get_laporan_stok_opname_transaksi_barang as $row){
			$transaksi = format_rupiah($row->transaksi);
			$jumlah = $row->qty;
			if($row->jenis_transaksi == "pembelian"){
				$pembelian += $row->transaksi;
			}else if($row->jenis_transaksi == "retur_pembelian"){
				$retur_pembelian += $row->transaksi;
				$transaksi = "- ".$transaksi;
				$jumlah = "- ".$jumlah;
			}else if($row->jenis_transaksi == "penjualan"){
				$penjualan += $row->transaksi;
				$transaksi = "- ".$transaksi;
				$jumlah = "- ".$jumlah;
			}else if($row->jenis_transaksi == "retur_penjualan"){
				$retur_penjualan += $row->transaksi;
			}
			
			$htm_table_transaksi_barang.="
											<tr>
												<td scope=\"row\">".($no+=1)."</td>
												<td>".repair_date($row->tanggal)."</td>
												<td>".ucwords(str_replace("_"," ",$row->jenis_transaksi))."</td>
												<td>".$transaksi."</td>
												<td>".$jumlah."</td>
											</tr>
										";
		}
		$tot_pembelian = $pembelian - $retur_pembelian;
		$tot_penjualan = $penjualan - $retur_penjualan;
		
		if($htm_table_stok_opname == ""){
			$htm_table_transaksi_barang .= "<tr><th colspan='20' class=\"text-center\">. : Data Kosong : .</th></tr>";
		}
			//$htm_table_transaksi_barang .= "<tr><th colspan='20'>Menampilkan ".(($no>0)?1:0)." .. ".$no." dari ".$no." Baris</th></tr>";
	}
	if($action == "tambah"){
		$tambahRequired = " required='required' ";
	}else if($action == "edit"){
		$editRequired = " required='required' ";
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url().$this->router->fetch_class()."/".$this->router->fetch_method(); ?>">Laporan Stok Opname <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-4">
		<div class="card">
			<div class="card-body p-2">
				<div class="row align-items-center">
					<div class="col-3 text-center">
						<i class="fa fa-dropbox fa-2x"></i>
					</div>
					<div class="col-9">
						<div style="font-size:30px;font-weight: bold;"><?php echo $get_laporan_stok_opname_jenis['stok_tersedia'];?> Jenis</div>
						<div style="font-size:12px;">Stok Tersedia</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-4">
		<div class="card">
			<div class="card-body p-2">
				<div class="row align-items-center">
					<div class="col-3 text-center">
						<i class="fa fa-dropbox fa-2x"></i>
					</div>
					<div class="col-9">
						<div style="font-size:30px;font-weight: bold;"><?php echo $get_laporan_stok_opname_jenis['stok_segera_habis'];?> Jenis</div>
						<div style="font-size:12px;">Stok Segera Habis</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-4">
		<div class="card">
			<div class="card-body p-2">
				<div class="row align-items-center">
					<div class="col-3 text-center">
						<i class="fa fa-dropbox fa-2x"></i>
					</div>
					<div class="col-9">
						<div style="font-size:30px;font-weight: bold;"><?php echo $get_laporan_stok_opname_jenis['stok_habis'];?>  Jenis</div>
						<div style="font-size:12px;">Stok Habis</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-6">
		<form action="" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari" placeholder="Pencarian..." class="form-control form-control-sm" required="required" />
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari"><i class="fa fa-search"></i></button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<button type="button" class="btn btn-outline-success btn-sm btn-export" data-type="to_excel"><i class="fa fa-file-excel-o"></i>&nbsp; to Excel</button>
		<button type="button" class="btn btn-outline-danger btn-sm btn-export" data-type="to_pdf"><i class="fa fa-file-pdf-o"></i>&nbsp; to PDF</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="bg-info">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Kode Barang</th>
					<th scope="col">Nama Barang</th>
					<th scope="col">Satuan</th>
					<th scope="col">Qty</th>
					<th scope="col">Keterangan</th>
					<th scope="col">Gudang</th>
				</tr>
			</thead>
			<tbody>
				<?php echo @$htm_table_stok_opname; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>
<?php if($action == "detail"){ ?>
<div class="card">
	<div class="card-body">
		<div class="row">
			<div class="col-9">
				<div class="row">
					<div class="col">
						<span class="font-weight-bold" style="font-size:25px;"><?php echo $get_edit_laporan_barang_stok_opname[0]->nama_barang; ?></span>
						<div class="w-100"></div>
						<?php echo $get_edit_laporan_barang_stok_opname[0]->kode_barang; ?>
					</div>
				</div>
				<div class="row mt-3">
					<div class="col">
						<span class="font-weight-bold"><i class="fa fa-dropbox"></i> Informasi</span>
					</div>
					<div class="w-100 border border-bottom my-2"></div>
					<div class="col">
						Stok Saat Ini
					</div>
					<div class="col">
						<?php echo $get_edit_laporan_barang_stok_opname[0]->stok_qty." ".$get_edit_laporan_barang_stok_opname[0]->nama_satuan; ?>
					</div>
					<div class="w-100 border border-bottom my-2"></div>
					<div class="col">
						Batas Stok Minimum
					</div>
					<div class="col">
						<?php echo $get_edit_laporan_barang_stok_opname[0]->min_stok." ".$get_edit_laporan_barang_stok_opname[0]->nama_satuan; ?>
					</div>
					<div class="w-100 border border-bottom my-2"></div>
					<div class="col">
						Group Barang
					</div>
					<div class="col">
						<?php echo $get_edit_laporan_barang_stok_opname[0]->nama_group; ?>
					</div>
					<div class="w-100 border border-bottom my-2"></div>
					<div class="col">
						Keterangan
					</div>
					<div class="col">
						<?php echo $get_edit_laporan_barang_stok_opname[0]->deskripsi; ?>
					</div>
					<div class="w-100 border border-bottom my-2"></div>
				</div>
			</div>
			<div class="col-3">
				<div class="card">
					<div class="card-body p-2">
						<div class="row align-items-center">
							<div class="col-12">
								<div style="font-size:17px;font-weight: bold;"><i class="fa fa-shopping-cart"></i> Pembelian</div>
								<div class="text-secondary" style="font-size:12px;">Harga Beli Satuan</div>
								<div style="font-size:16px;"><?php echo format_rupiah($tot_pembelian); ?></div>
							</div>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-body p-2">
						<div class="row align-items-center">
							<div class="col-12">
								<div style="font-size:17px;font-weight: bold;"><i class="fa fa-tags"></i> Penjualan</div>
								<div class="text-secondary" style="font-size:12px;">Harga Beli Satuan</div>
								<div style="font-size:16px;"><?php echo format_rupiah($tot_penjualan); ?></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row mt-4">
			<div class="col font-weight-bold">
				Transaksi Barang
				<hr/>
			</div>
		</div>
		<table class="table table-form" id="transaksi_barang">
			<thead class="bg-info">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Tanggal</th>
					<th scope="col">Jenis Transaksi</th>
					<th scope="col">Transaksi</th>
					<th scope="col">Jumlah</th>
				</tr>
			</thead>
			<tbody>
				<?php echo @$htm_table_transaksi_barang; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>
<script>
	var VG_onpage_data_table = "tbl_transaksi_penjualan";
	var BASE_URL = "<?php echo base_url(); ?>";
	var aksesHapus = "<?php echo @$aksesHapus; ?>";
	var action = "<?php echo $action; ?>";
	var btnDetailNoSeri = null;
	var no=0;
	$j("button.btn-export").click(function(){
		var data_type = $j(this).data("type");
		var dtH = {};
		var i = 0;
		$j("table.table-data").find("thead tr th").each(function(){
			var dt = $j(this).html();
			dtH[i] = dt;
			i++;
		});
		
		var dtB = {};
		var i = 0;
		$j("table.table-data").find("tbody tr").each(function(){
			var dtR = {};
			var ir = 0;
			$j(this).find("td").each(function(){
				var dt = $j(this).html();
				if(String(dt).trim() != ""){
					dtR[ir] = dt;
					ir++;		
				}				
			});
			dtB[i] = dtR;
			i++;
		});
		var dataFD = {'header':dtH,'body':dtB};
		var encData = btoa(JSON.stringify(dataFD));
		if(data_type == "to_excel"){
			var export_type = "file_excel";
		}else if(data_type == "to_pdf"){
			var export_type = "file_pdf";
		}
		$j.ajax({
			type:"POST",
			url:"<?php echo base_url()."admin/apiweb"; ?>",
			data:{action:"post_download_data",type:export_type,data:encData},
			dataType:"json",
			success: function(obj){
				if(obj == true){
					window.open("<?php echo base_url()."download/"; ?>"+export_type,"_blank");
				}else{
					alert("error download, code: "+obj);
				}
			}
		});
	});
	$j(document).ready(function(){
		$j('table#transaksi_barang').DataTable({
			"pageLength": 100
		});
	});
</script>
<?php } ?>