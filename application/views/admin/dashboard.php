<?php
$aksesKey = $this->router->fetch_class()."/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
?>

<div class="row">
	<div class="col-6">
		<div class="card">
			<div class="card-header p-2">
				<strong class="card-title">Pendapatan Hari Ini</strong>
			</div>
			<div class="card-body p-2">
				<div style="font-size:12px;color:#cccccc;">Pendapatan - <?php echo $tanggal_sekarang; ?></div>
				<div><h2 class="card-text font-weight-bold"><?php echo format_rupiah(@$get_laporan_dashboard['pendapatan_hari_ini']); ?></h2></div>
			</div>
		</div>
	</div>
	<div class="col-6">
		<div class="card">
			<div class="card-header p-2">
				<strong class="card-title">Transaksi Hari Ini</strong>
			</div>
			<div class="card-body p-2 text-center">
				<div style="font-size:12px;color:#cccccc;">&nbsp;</div>
				<div><h2 class="card-text font-weight-bold"><i class="fa fa-money"></i> <?php echo number_format(@$get_laporan_dashboard['transaksi_hari_ini'],0,",","."); ?></h2></div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-6">
		<div class="card">
			<div class="card-body">
				<h4 class="mb-3 font-weight-bold">Profit - <?php echo $tanggal_sekarang; ?></h4>
				<div id="chart_profit" style="width:100%;height:200px;"></div>
			</div>
		</div>
	</div>
	<div class="col-6">
		<div class="card">
			<div class="card-body">
				<h4 class="mb-3 font-weight-bold">Pergerakan Penjualan - <?php echo $tanggal_sekarang; ?></h4>
				<div id="chart_pergerakan_penjualan" style="width:100%;height:200px;"></div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<h4 class="mb-3 font-weight-bold">Best Seller Penjualan Per Wilayah</h4>
				<div id="chart_best_seller_per_wilayah" style="width:100%;height:400px;"></div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
    google.charts.load("current", {packages:['corechart']});
    google.charts.setOnLoadCallback(drawChart1);
    function drawChart1() {
		  var data = google.visualization.arrayToDataTable([
			["Element", "Density", { role: "style" } ],
			["Pemasukan", <?php echo @$get_laporan_dashboard['pendapatan_hari_ini']; ?>, "#0000ff"],
			["Pengeluaran", <?php echo @$get_laporan_dashboard['pengeluaran_hari_ini']; ?>, "#ff0000"],
			["Keuntungan", <?php echo (@$get_laporan_dashboard['pendapatan_hari_ini']-@$get_laporan_dashboard['pengeluaran_hari_ini']); ?>, "#00ff00"]
		  ]);

		  var view = new google.visualization.DataView(data);
		  view.setColumns([0, 1,
						   { calc: "stringify",
							 sourceColumn: 1,
							 type: "string",
							 role: "annotation" },
						   2]);

		  var options = {
			//title: "Density of Precious Metals, in g/cm^3",
			bar: {groupWidth: "95%"},
			legend: { position: "none" },
		  };
		  var chart = new google.visualization.ColumnChart(document.getElementById("chart_profit"));
		  chart.draw(view, options);
	}
	
	
	
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart2);
      function drawChart2() {

        var data = google.visualization.arrayToDataTable([
          ['Kartu Perdana', 'Qty'],
		  <?php echo @$get_laporan_dashboard['chart_pergerakan_penjualan']; ?>
        ]);
		
		/*
		var data = google.visualization.arrayToDataTable([
          ['Kartu Perdana', 'Qty'],
          ['Simpati',     11],
          ['Axis',      5]
        ]);
		*/

        var options = {
          //title: 'My Daily Activities'
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_pergerakan_penjualan'));

        chart.draw(data, options);
      }



      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart3);
      function drawChart3() {
        var data = google.visualization.arrayToDataTable([
				  [<?php echo "'Bulan', '".@$get_laporan_dashboard['chart_per_wilayah']['nama_kota']."'"; ?>],
				  <?php echo @$get_laporan_dashboard['chart_per_wilayah']['tahun_bulan_penjualan']; ?>
        ]);
		/*
        var data = google.visualization.arrayToDataTable([
          ['Bulan Ini', 'Bandung', 'Jakarta'],
          ['2014/05', 1000, 400]
        ]);
		*/
        var options = {
          chart: {
            //title: 'Company Performance',
            //subtitle: 'Sales, Expenses, and Profit: 2014-2017',
          },
		  bar: {groupWidth: "95%"},
        };

        var chart = new google.charts.Bar(document.getElementById('chart_best_seller_per_wilayah'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }

    </script>