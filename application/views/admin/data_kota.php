<?php
$aksesKey = "admin/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug();
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}

	$no=0;
	$htm_table_kota = "";
	foreach($get_kota as $row){
		$htm_table_kota.="
						<tr data-id=\"".$row->id."\">
							<th scope=\"row\">".($no+=1)."</th>
							<td>".$row->kode_kota."</td>
							<td>".$row->nama_kota."</td>
							<td>".$row->wilayah."</td>
							<td>".btnStatLabel($row->status)."</td>
							<td>";
								if(isset($aksesUbah)) $htm_table_kota.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/edit/".$row->id."\" class=\"btn btn-outline-success btn-sm\"><i class=\"fa fa-edit\"></i>&nbsp; Edit</a> ";
								if(isset($aksesHapus)) $htm_table_kota.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/hapus/".$row->id."\" class=\"btn btn-outline-danger btn-sm\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";
		  $htm_table_kota.="</td>
						</tr>
					";
	}
	if($htm_table_kota == ""){
		$htm_table_kota .= "<tr><th colspan='7' class=\"text-center\">. : Data Kosong : .</th></tr>";
		$htm_table_kota .= "<tr><th colspan='7' class=\"text-center\">&nbsp;</th></tr>";
	}
	
	if($action == "tambah"){
		$passRequired = " required='required' ";
	}else if($action == "edit"){
		$hintPassword = "<span class=\"help-block text-warning\">Kosongkan kolom Password jika tidak akan mengganti Password.</span>";
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>">Kota <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-6">
		<form action="" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari" placeholder="Cari Kota" class="form-control form-control-sm" required="required" />
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<?php if(isset($aksesTambah)){ ?>
		<a href="<?php echo base_url()."admin/".$this->router->fetch_method()."/tambah"; ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil"></i>&nbsp; Tambah</a>
		<?php } ?>
		<button type="button" class="btn btn-outline-warning btn-sm" id="bt_print"><i class="fa fa-print"></i>&nbsp; Cetak</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="thead-dark">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Kode Kota</th>
					<th scope="col">Nama Kota</th>
					<th scope="col">Wilayah</th>
					<th scope="col">Status</th>
					<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
					<th scope="col">Aksi</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php echo $htm_table_kota; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>
<?php if($action == "tambah" or $action == "edit"){ ?>
<div class="card">
	<div class="card-body">
		<form name="form_crud" method="post" action="">
			<input type="hidden" name="form_action" value="<?php echo $action; ?>">
			<input type="hidden" name="id_kota" value="<?php echo @$get_edit_kota[0]->id; ?>">
			<div class="form-group">
				<label>Kode Kota</label>
				<input type="text" class="form-control" name="kode_kota" value="<?php echo @$get_edit_kota[0]->kode_kota; ?>" required="required">
				<span class="help-block color-red"></span>
			</div>
			<div class="form-group">
				<label>Nama Kota</label>
				<input type="text" class="form-control" name="nama_kota" value="<?php echo @$get_edit_kota[0]->nama_kota; ?>" required="required">
				<span class="help-block color-red"></span>
			</div>
			<div class="form-group">
				<label>Wilayah</label>
				<input type="text" class="form-control" name="wilayah" value="<?php echo @$get_edit_kota[0]->wilayah; ?>" required="required">
				<span class="help-block color-red"></span>
			</div>
			<div class="form-group">
				<label>Status</label>
				<select class="form-control" name="status" required="required">
					<option value='1' <?php echo(isset($get_edit_kota[0]->status) and $get_edit_kota[0]->status == "1")?"selected=selected":""; ?>><?php echo statLabel("1"); ?></option>
					<option value='0' <?php echo(isset($get_edit_kota[0]->status) and $get_edit_kota[0]->status == "0")?"selected=selected":""; ?>><?php echo statLabel("0"); ?></option>
				</select>
			</div>
			<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>" class="btn btn-secondaray btn-flat">Back</a>
			<button type="submit" name="submit_kota" class="btn btn-primary btn-flat">Submit</button>
		</form>
	</div>
</div>
<?php } ?>
<script>
	var action = "<?php echo $action; ?>";
	var VG_onpage_data_table = "tbl_kota";
	var submit_kode_kota = true;
	var submit_nama_kota = true;
	var msg_alert_kode_kota = "";
	var msg_alert_nama_kota = "";
	$j(document).on("click","#bt_print",function(){
		printTableData();
	});
	
	$j("form[name='form_crud']").on("keyup","input[name='kode_kota']",function(){
		var kode_kota_def = "<?php echo @$get_edit_kota[0]->kode_kota; ?>";
		var obj_kode_kota = $j(this);
		var kode_kota = obj_kode_kota.val();
		var data_table = VG_onpage_data_table;
		var data_where = {kode_kota:kode_kota,delete:0};
		if(action == "edit") data_where['kode_kota !='] = kode_kota_def;
		$j.ajax({
			type:"POST",
			url:"<?php echo base_url()."admin/apiweb"; ?>",
			data:{action:"if_exist_data_table",data_table:data_table,data_where:data_where},
			success: function(res){
				if(res > 0){
					submit_kode_kota = false;
					msg_alert_kode_kota = "Kode Kota sudah ada!";
					obj_kode_kota.next().html(msg_alert_kode_kota);
				}else{
					submit_kode_kota = true;
					obj_kode_kota.next().html("");
				}
			}
		});
	});

	$j("form[name='form_crud']").on("keyup","input[name='nama_kota'], input[name='wilayah']",function(){
		var nama_kota_def = "<?php echo @$get_edit_kota[0]->nama_kota; ?>";
		var wilayah_def = "<?php echo @$get_edit_kota[0]->wilayah; ?>";
		var nama_kota = $j("form[name='form_crud']").find("input[name='nama_kota']").val();
		var wilayah = $j("form[name='form_crud']").find("input[name='wilayah']").val();
		var data_table = VG_onpage_data_table;
		var data_where = {nama_kota:nama_kota,wilayah:wilayah,delete:0};
		if(action == "edit") {
			data_where['nama_kota !='] = nama_kota_def;
			data_where['wilayah !='] = wilayah_def;
		}
		$j.ajax({
			type:"POST",
			url:"<?php echo base_url()."admin/apiweb"; ?>",
			data:{action:"if_exist_data_table",data_table:data_table,data_where:data_where},
			success: function(res){
				if(res > 0){
					submit_nama_kota = false;
					msg_alert_nama_kota = "Nama Kota join Wilayah sudah ada!";
					$j("form[name='form_crud']").find("input[name='nama_kota']").next().html(msg_alert_nama_kota);
					$j("form[name='form_crud']").find("input[name='wilayah']").next().html(msg_alert_nama_kota);
				}else{
					submit_nama_kota = true;
					$j("form[name='form_crud']").find("input[name='nama_kota']").next().html("");
					$j("form[name='form_crud']").find("input[name='wilayah']").next().html("");
				}
			}
		});
	});
	
	$j(document).on("submit","form[name='form_crud']",function(){
		if(submit_kode_kota == false && msg_alert_kode_kota !="")  alert(msg_alert_kode_kota);
		else if(submit_nama_kota == false && msg_alert_nama_kota !="") alert(msg_alert_nama_kota);
		
		if(submit_kode_kota === true && submit_nama_kota === true)
			return true;
		else
			return false;
	});
</script>
<?php } ?>