<?php 
$aksesKey = "admin/".$this->router->fetch_method();
$AppHakAkses = $this->admin_model->get_app_hak_akses();
if(isset($AppHakAkses[$aksesKey]['lihat']) and $AppHakAkses[$aksesKey]['lihat'] == "on") $aksesLihat = 1;
if(isset($AppHakAkses[$aksesKey]['tambah']) and $AppHakAkses[$aksesKey]['tambah'] == "on") $aksesTambah = 1;
if(isset($AppHakAkses[$aksesKey]['ubah']) and $AppHakAkses[$aksesKey]['ubah'] == "on") $aksesUbah = 1;
if(isset($AppHakAkses[$aksesKey]['hapus']) and $AppHakAkses[$aksesKey]['hapus'] == "on") $aksesHapus = 1;

if(isset($aksesLihat)){
	//debug($get_admin_menu_hak);
	$sub_slug = "";
	if($action <> NULL){
		$sub_slug = "<a href=\"javascript:void(0);\">".ucfirst($action)." <i class=\"fa fa-angle-right\"></i></a>";
	}
	$notif_message = "";
	if(isset($message) and $message <>""){
		$notif_message = "<div class=\"alert alert-info p-1\" role=\"alert\">".$message."</div>";
	}

	$no=0;
	$htm_table_user = "";
	foreach($get_admin_user as $row){
		$htm_table_user.="
						<tr data-id=\"".$row->id."\">
							<th scope=\"row\">".($no+=1)."</th>
							<td>".$row->full_name."</td>
							<td>".$row->user_name."</td>
							<td>".$row->email."</td>
							<td>".$row->hak_akses_name."</td>
							<td>".btnStatLabel($row->status)."</td>
							<td>";
								if(isset($aksesUbah)) $htm_table_user.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/edit/".$row->id."\" class=\"btn btn-outline-success btn-sm\"><i class=\"fa fa-edit\"></i>&nbsp; Edit</a> ";
								if(isset($aksesHapus)) $htm_table_user.=" <a href=\"".base_url()."admin/".$this->router->fetch_method()."/hapus/".$row->id."\" class=\"btn btn-outline-danger btn-sm\" onclick=\"return confirm('Anda akan menghapus data ini?');\"><i class=\"fa fa-trash-o\"></i>&nbsp; Hapus</a> ";
		  $htm_table_user.="</td>
						</tr>
					";
	}
	if($htm_table_user == ""){
		$htm_table_user .= "<tr><th colspan='7' class=\"text-center\">. : Data Kosong : .</th></tr>";
		$htm_table_user .= "<tr><th colspan='7' class=\"text-center\">&nbsp;</th></tr>";
	}
	
	$htm_menu_hak = "<option value=''>Pilih...</option>";
	foreach($get_admin_menu_hak as $row){
		if($row->status == "1")
		$htm_menu_hak .= "<option value='".$row->id."' ".((isset($get_edit_admin_user[0]->id_hak) and $get_edit_admin_user[0]->id_hak == $row->id)?"selected=selected":"").">".$row->name."</option>";
	}
	
	if($action == "tambah"){
		$passRequired = " required='required' ";
	}else if($action == "edit"){
		$hintPassword = "<span class=\"help-block text-warning\">Kosongkan kolom Password jika tidak akan mengganti Password.</span>";
	}
?>
<div class="alert alert-light p-1" role="alert">
	<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>">Admin User <i class="fa fa-angle-right"></i></a>
	<?php echo $sub_slug; ?>
</div>
<?php echo $notif_message; ?>
<?php if($action == NULL){ ?>
<div class="row">
	<div class="col-6">
		<form action="" method="post" class="form-horizontal">
			<div class="row form-group">
				<div class="col-12 col-sm-12 col-md-8">
					<div class="input-group">
						<input type="text" name="tx_cari" placeholder="Cari Admin User" class="form-control form-control-sm" required="required"/>
						<div class="input-group-btn">
							<button type="submit" class="btn btn-primary btn-sm" name="bt_cari">Submit</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="col-6 text-right">
		<?php if(isset($aksesTambah)){ ?>
		<a href="<?php echo base_url()."admin/".$this->router->fetch_method()."/tambah"; ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-pencil"></i>&nbsp; Tambah</a>
		<?php } ?>
		<button type="button" class="btn btn-outline-warning btn-sm" id="bt_print"><i class="fa fa-print"></i>&nbsp; Cetak</button>
	</div>
</div>
<div class="card">
	<div class="card-body">
		<table class="table table-data">
			<thead class="thead-dark">
				<tr>
					<th scope="col">#</th>
					<th scope="col">Nama Lengkap</th>
					<th scope="col">Username</th>
					<th scope="col">Email</th>
					<th scope="col">Nama Hak Akses</th>
					<th scope="col">Status</th>
					<?php if(isset($aksesUbah) or isset($aksesHapus)){ ?>
					<th scope="col">Aksi</th>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php echo $htm_table_user; ?>
			</tbody>
		</table>
	</div>
</div>
<?php } ?>
<?php if($action == "tambah" or $action == "edit"){ ?>
<div class="card">
	<div class="card-body">
		<form method="post" action="" name="form_data_user">
			<input type="hidden" name="form_action" value="<?php echo $action; ?>">
			<input type="hidden" name="id_admin_user" value="<?php echo @$get_edit_admin_user[0]->id; ?>">
			<div class="form-group">
				<label>Nama Lengkap</label>
				<input type="text" class="form-control" name="full_name" value="<?php echo @$get_edit_admin_user[0]->full_name; ?>" required="required">
			</div>
			<div class="form-group">
				<label>Email</label>
				<input type="email" class="form-control" name="email" value="<?php echo @$get_edit_admin_user[0]->email; ?>" required="required">
			</div>
			<div class="form-group">
				<label>User Name</label>
				<input type="text" class="form-control" name="user_name" value="<?php echo @$get_edit_admin_user[0]->user_name; ?>" required="required">
			</div>
			<div class="form-group">
				<label>Password</label>
				<input type="password" class="form-control" name="password" value="" <?php echo @$passRequired; ?>>
				<?php echo @$hintPassword; ?>
			</div>
			<div class="form-group">
				<label>Ulang Password</label>
				<input type="password" class="form-control" name="upassword" value="" <?php echo @$passRequired; ?>>
				<?php echo @$hintPassword; ?>
			</div>
			<div class="form-group">
				<label>Hak Akses</label>
				<select class="form-control" name="id_hak" required="required">
					<?php echo $htm_menu_hak; ?>
				</select>
			</div>
			<div class="form-group">
				<label>Status</label>
				<select class="form-control" name="status" required="required">
					<option value='1' <?php echo(isset($get_edit_admin_user[0]->status) and $get_edit_admin_user[0]->status == "1")?"selected=selected":""; ?>><?php echo statLabel("1"); ?></option>
					<option value='0' <?php echo(isset($get_edit_admin_user[0]->status) and $get_edit_admin_user[0]->status == "0")?"selected=selected":""; ?>><?php echo statLabel("0"); ?></option>
				</select>
			</div>
			<a href="<?php echo base_url()."admin/".$this->router->fetch_method(); ?>" class="btn btn-secondaray btn-flat">Back</a>
			<button type="submit" name="submit_admin_user" class="btn btn-primary btn-flat">Submit</button>
		</form>
	</div>
</div>
<?php } ?>
<script>
	var VG_onpage_data_table = "admin_user";
	$j("form[name='form_data_user']").submit(function(){
		var action = "<?php echo $action; ?>";
		var res = false;
		var password = $j(this).find("input[name='password']").val();
		var upassword = $j(this).find("input[name='upassword']").val();
		if(action == "tambah" || action == "edit"){
			if(password != upassword){
				alert("password yang dimasukan tidak sama!");
			}else{
				res = true;
			}
		}else{
			res = true;
		}
		return res;
	});
	$j(document).on("click","#bt_print",function(){
		printTableData();
	});
</script>
<?php } ?>