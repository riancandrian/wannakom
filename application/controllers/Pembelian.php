<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
		
		if($this->router->fetch_method() <> "login" and $this->router->fetch_method() <> "forgot_password"){
			if(!isset($_SESSION['admin_login']) or $_SESSION['admin_login']<>1){
				redirect('/login');
			}
		}
	}
	public function index()
	{
		redirect('/pembelian/faktur_pembelian');
	}
	public function faktur_pembelian($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;
		
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$id_faktur_pembelian = $this->admin_model->add_faktur_pembelian($_REQUEST);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/preview/".$id_faktur_pembelian);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->admin_model->edit_faktur_pembelian($_REQUEST);
		}
		if(($action == "edit" or $action == "preview") and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_faktur_pembelian'] = $this->admin_model->get_faktur_pembelian($edit_id);
			$data['get_faktur_pembelian_detail_barang'] = $this->admin_model->get_faktur_pembelian_detail_barang(NULL,array('id_faktur_pembelian' => $edit_id));
			$data['get_edit_data_suplier'] = $this->admin_model->get_data_suplier($data['get_edit_faktur_pembelian'][0]->id_suplier);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->admin_model->hapus_faktur_pembelian($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}
		
		
		$data['get_laporan_cepat_faktur_pembelian'] = $this->admin_model->get_laporan_cepat_faktur_pembelian();
		$data['get_data_suplier'] = $this->admin_model->get_data_suplier();
		$data['get_faktur_pembelian'] = $this->admin_model->get_faktur_pembelian(NULL,$_REQUEST);
		$data['get_data_barang'] = $this->admin_model->get_data_barang(NULL, NULL);
		$data['get_data_gudang'] = $this->admin_model->get_gudang(NULL, NULL);
		$this->load->view('admin/app_header');
		$this->load->view('admin/pembelian_faktur_pembelian',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." faktur_pembelian view");
	}
	public function retur_pembelian($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;
		$data['action_id'] = $edit_id;
		
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$data['message'] = $this->admin_model->add_retur_pembelian($_REQUEST);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->admin_model->edit_retur_pembelian($_REQUEST);
		}
		if(($action == "edit" or $action == "preview") and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_retur_pembelian'] = $this->admin_model->get_retur_pembelian($edit_id);
			$data['get_edit_faktur_pembelian'] = $this->admin_model->get_faktur_pembelian($data['get_edit_retur_pembelian'][0]->id_faktur_pembelian);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->admin_model->hapus_retur_pembelian($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}
		if($action == "tambah" and $data['action_id']<>NULL){
			$data['get_edit_faktur_pembelian'] = $this->admin_model->get_faktur_pembelian($data['action_id']);
		}
		$data['get_retur_pembelian'] = $this->admin_model->get_retur_pembelian(NULL,$_REQUEST);
		$data['get_faktur_pembelian'] = $this->admin_model->get_faktur_pembelian(NULL, array('bisa_retur'=>'1'));
		$this->load->view('admin/app_header');
		$this->load->view('admin/pembelian_retur_pembelian',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." retur_pembelian view");
	}
	public function pembayaran_hutang($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;
		$data['action_id'] = $edit_id;
		
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$data['message'] = $this->admin_model->add_pembayaran_hutang($_REQUEST);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->admin_model->edit_pembayaran_hutang($_REQUEST);
		}
		if(($action == "edit" or $action == "preview") and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_pembayaran_hutang'] = $this->admin_model->get_pembayaran_hutang($edit_id);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->admin_model->hapus_pembayaran_hutang($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}
		if($action == "tambah" and $data['action_id']<>NULL){
			$data['get_edit_data_suplier'] = $this->admin_model->get_data_suplier($data['action_id']);
		}
		$data['get_data_suplier'] = $this->admin_model->get_data_suplier();
		$data['get_pembayaran_hutang'] = $this->admin_model->get_pembayaran_hutang(NULL,$_REQUEST);
		$data['get_faktur_pembelian'] = $this->admin_model->get_faktur_pembelian(NULL, NULL);
		$this->load->view('admin/app_header');
		$this->load->view('admin/pembelian_pembayaran_hutang',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." pembayaran_hutang view");
	}
	public function laporan_faktur_pembelian($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;
		$data['action_id'] = $edit_id;
		$data['get_laporan_faktur_pembelian'] = $this->admin_model->get_laporan_faktur_pembelian(NULL,$_REQUEST);
		$this->load->view('admin/app_header');
		$this->load->view('admin/pembelian_laporan_faktur_pembelian',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." laporan_faktur_pembelian view");
	}
	public function laporan_retur_pembelian($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;
		$data['action_id'] = $edit_id;
		$data['get_laporan_retur_pembelian'] = $this->admin_model->get_laporan_retur_pembelian(NULL,$_REQUEST);
		$this->load->view('admin/app_header');
		$this->load->view('admin/pembelian_laporan_retur_pembelian',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." laporan_retur_pembelian view");
	}
	public function laporan_pembayaran_hutang($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;
		$data['action_id'] = $edit_id;
		$data['get_laporan_pembayaran_hutang'] = $this->admin_model->get_laporan_pembayaran_hutang(NULL,$_REQUEST);
		$this->load->view('admin/app_header');
		$this->load->view('admin/pembelian_laporan_pembayaran_hutang',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." laporan_pembayaran_hutang view");
	}
}