<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Saham extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->model('saham_model');
		
		if($this->router->fetch_method() <> "login" and $this->router->fetch_method() <> "forgot_password"){
			if(!isset($_SESSION['admin_login']) or $_SESSION['admin_login']<>1){
				redirect('/login');
			}
		}
	}
	public function index()
	{
		redirect('/saham/input_data_investor');
	}
	public function input_data_investor($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;
		
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$data['message'] = $this->saham_model->add_data_investor($_REQUEST, $_FILES);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->saham_model->edit_data_investor($_REQUEST, $_FILES);
		}
		if(($action == "edit" or $action == "preview") and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_investor'] = $this->saham_model->get_data_investor($edit_id);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->saham_model->hapus_data_investor($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}
		
		$data['get_data_investor'] = $this->saham_model->get_data_investor(NULL,$_REQUEST);
		$data['get_setting_app'] = $this->admin_model->get_setting_app("1");
		
		$this->load->view('admin/app_header');
		$this->load->view('admin/saham_input_data_investor',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." input_data_investor view");
	}
	public function input_pembagian_saham($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;
		
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$id_pembagian_saham = $this->saham_model->add_pembagian_saham($_REQUEST);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/preview/".$id_pembagian_saham);
		}
		
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->saham_model->edit_pembagian_saham($_REQUEST);
		}
		
		if(($action == "edit" or $action == "preview") and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_pembagian_saham'] = $this->saham_model->get_pembagian_saham($edit_id);
			$data['get_edit_pembagian_saham_detail_investor'] = $this->saham_model->get_pembagian_saham_detail_investor(NULL,array("id_pembagian_saham" => $data['get_edit_pembagian_saham'][0]->id));
		}
		
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->saham_model->hapus_pembagian_saham($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}
				
		$data['get_laporan_cepat_pembagian_saham'] = $this->saham_model->get_laporan_cepat_pembagian_saham();
		$data['get_pembagian_saham'] = $this->saham_model->get_pembagian_saham(NULL,$_REQUEST);
		$data['get_data_investor'] = $this->saham_model->get_data_investor();
		$data['get_setting_app'] = $this->admin_model->get_setting_app("1");
		
		$this->load->view('admin/app_header');
		$this->load->view('admin/saham_input_pembagian_saham',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." input_pembagian_saham view");
	}
	public function laporan_best_seller_per_wilayah($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;
		$data['get_laporan_cepat_wilayah_penjualan'] = $this->saham_model->get_laporan_cepat_wilayah_penjualan($_REQUEST);
		$this->load->view('admin/app_header');
		$this->load->view('admin/saham_laporan_best_seller_per_wilayah',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." laporan_best_seller_per_wilayah view");
	}
	public function laporan_profit($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;
		$data['get_laporan_profit'] = $this->saham_model->get_laporan_profit($_REQUEST);
		$this->load->view('admin/app_header');
		$this->load->view('admin/saham_laporan_profit',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." laporan_profit view");
	}
	public function laporan_bagi_hasil_saham($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;
		$data['get_laporan_bagi_hasil_saham'] = $this->saham_model->get_laporan_bagi_hasil_saham($_REQUEST);
		
		$this->load->view('admin/app_header');
		$this->load->view('admin/saham_laporan_bagi_hasil_saham',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." laporan_bagi_hasil_saham view");
	}
}