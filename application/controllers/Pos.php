<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Pos extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('admin_model');
    $this->load->model('penjualan_model');
    $this->load->model('mobile_service_model');

    if($this->router->fetch_method() <> "login" and $this->router->fetch_method() <> "forgot_password"){
      if(!isset($_SESSION['admin_login']) or $_SESSION['admin_login']<>1){
        redirect('/login');
      }
    }
  }

  public function cari_barang(){
    $cari = $this->input->post('value');

    $query = "SELECT b.*, s.nama_satuan from tbl_data_barang b JOIN satuan s ON s.id = b.id_satuan where b.nama_barang like '%$cari%' ";
    $data  = $this->db->query($query)->result();

    echo json_encode(array('success' => true, 'data' => $data));
  }

  public function insert_tmp(){
    $data = array('idbarang' => $this->input->post('idbarang')
                , 'user_id' => $this->session->userdata('admin_id')
              );

    # cek apakah sudah ada di list
    $cek  = $this->db->get_where('tbl_penjualan_tmp', $data)->num_rows();

    if($cek > 0){
      $result = array('success' => false);
    }else{
      $this->db->insert('tbl_penjualan_tmp', $data);
      $result = array('success' => true);
    }

    echo json_encode($result);
  }

  function get_tmp(){
    $user = $this->session->userdata('admin_id');
    $data = $this->db->query("SELECT t.*, b.nama_barang, b.kode_barang from tbl_penjualan_tmp t JOIN tbl_data_barang b ON b.id = t.idbarang WHERE t.user_id = '$user' ")->result();
    $result = array('success' => true, 'data' => $data);
    echo json_encode($result);
  }

  function update_tmp(){
    $id     = $this->input->post('id');
    $harga  = $this->input->post('harga');
    $qty    = $this->input->post('qty');

    $data   = array('qty' => $qty, 'harga' => $harga);
    $update = $this->db->update('tbl_penjualan_tmp', $data, array('id' => $id));

    if($update){
      echo json_encode(array('success' => true));
    }
  }

  function hapus_tmp(){
    $id     = $this->input->post('id');
    $this->db->where('id', $id);
    $this->db->delete('tbl_penjualan_tmp');

    echo json_encode(array('success' => true));
  }

  function simpan(){
    # get nomor
    $query = $this->db->query("SELECT nomor_urut FROM tbl_transaksi_penjualan WHERE EXTRACT(YEAR_MONTH FROM tgl_input) = '".date('Ym')."' ORDER BY nomor_urut DESC limit 1;");
		$row = $query->row();
		if(isset($row->nomor_urut) and $row->nomor_urut > 0)
			$newNomorUrut = $row->nomor_urut+1;
		else
			$newNomorUrut = 1;
		$newNomor = numLength($newNomorUrut,4)."/FJ/".numberToRoman(date('m'))."/".date('y');

    $tgl = $this->input->post('tanggal_kirim');
    $tanggal = date('Y-m-d h:i:s', strtotime($tgl));

    # header parameter
    $dataHeader = array('nomor' => $newNomor
                      , 'nomor_urut' => $newNomorUrut
                      , 'tanggal' => $tanggal
                      , 'jatuh_tempo' => date('Y-m-d')
                      , 'id_customer' => $this->input->post('customer_kirim')
                      , 'pesan' => $this->input->post('keterangan_kirim')
                      , 'total' => $this->input->post('total')
                      , 'pemotongan' => $this->input->post('diskon')
                      , 'pemotongan_tipe' => 'persen'
                      , 'grand_total' => $this->input->post('grand_total')
                      , 'pembayaran' => $this->input->post('pembayaran')
                      , 'kembali' => $this->input->post('kembali')
                      , 'email' => ''
                      , 'status_penjualan' => 'open'
                      , 'sisa_tagihan' => 0
                    );

    $simpan = $this->db->insert('tbl_transaksi_penjualan', $dataHeader);
    // print_r($this->db->last_query());die;
    if($simpan){
      $idheader = $this->db->insert_id();
      $idrow = $this->input->post('idtemp');

      foreach ($idrow as $key => $id) {
        // code...
        $dataDet = array('id_transaksi_penjualan' => $idheader
                       , 'id_barang' => $this->input->post('idbarang')[$key]
                       , 'expire_date' => date('Y-m-d')
                       , 'qty' => $this->input->post('qty')[$key]
                       , 'qty_cetak_retur' => 0
                       , 'no_seri' => ''
                       , 'no_seri_cetak_retur' => ''
                       , 'harga_satuan' => $this->input->post('harga')[$key]
                       , 'sub_total' => $this->input->post('sub')[$key]
                    );

        $simpandet = $this->db->insert('tbl_transaksi_penjualan_detail_barang', $dataDet);
        if($simpandet){
          $this->db->where('id', $id);
          $this->db->delete('tbl_penjualan_tmp');
        }
      }
    }else{
      print_r($this->db->error());
    }

    echo json_encode(array('success' => true));
  }



  # Titipan Function
  function getDesignDet(){
    $id   = $this->input->post('id');

    $this->db->where('id', $id);
    $data = $this->db->get('tbl_design')->row_array();

    echo json_encode(array('data' => $data));
  }

}
