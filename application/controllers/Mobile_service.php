<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: */*");
defined('BASEPATH') OR exit('No direct script access allowed');

class Mobile_service extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('mobile_service_model');
		$this->load->model('admin_model');
		$this->load->model('penjualan_model');
	}
	public function index()
	{
		$action = @$_REQUEST['action'];
		$data_input = json_decode(base64_decode(@$_REQUEST['data_input']),true);
		
		if(isset($action) and $action == "user_register"){
			$res = $this->mobile_service_model->user_register($data_input);
			echo $res;
		}
		else if(isset($action) and $action == "user_login"){
			$res = $this->mobile_service_model->user_login($data_input);
			echo $res;
		}
		else if(isset($action) and $action == "user_update_token"){
			$res = $this->mobile_service_model->user_update_token($data_input);
			echo $res;
		}
		else if(isset($action) and $action == "update_profile"){
			$res = $this->mobile_service_model->update_profile($data_input);
			echo $res;
		}
		else if(isset($action) and $action == "forget_password"){
			$res = $this->mobile_service_model->forget_password($data_input);
			echo $res;
		}
		else if(isset($action) and $action == "get_promo_slider"){
			$res = $this->mobile_service_model->get_promo_slider();
			echo $res;
		}
		else if(isset($action) and $action == "get_group_customer"){
			$res = $this->admin_model->get_group_customer();
			echo json_encode($res);
		}
		else if(isset($action) and $action == "get_data_barang_for_sale"){
			$res = $this->mobile_service_model->get_data_barang_for_sale();
			echo $res;
		}
		else if(isset($action) and $action == "get_histori_transaksi"){
			$res = $this->mobile_service_model->get_histori_transaksi($data_input);
			echo $res;
		}
		else if(isset($action) and $action == "get_kota"){
			$res = $this->admin_model->get_kota();
			echo json_encode($res);
		}
		else if(isset($action) and $action == "order_app"){
			$res = $this->mobile_service_model->order_app($data_input);
			echo $res;
		}
		else if(isset($action) and $action == "confirm_order_by_customer"){
			$res = $this->mobile_service_model->confirm_order_by_customer($data_input);
			echo $res;
		}
		else if(isset($action) and $action == "cancel_order_app"){
			$res = $this->mobile_service_model->cancel_order_app($data_input);
			echo $res;
		}
		else if(isset($action) and $action == "api_test"){
			$res['_FILES'] = ($_FILES);
			$res['_REQUEST'] = ($_REQUEST);
			$res['_POST'] = ($_POST);
			$res['_GET'] = ($_GET);
			echo json_encode($res);
		}
		else {
			/*
			$data['heading'] = "404 Page Not Found";
			$data['message'] = "<p>The <b>Mobile_service</b> page you requested was not found.</p>";
			$this->load->view('errors/html/error_404',$data);
			*/
			print_r($_FILES);
			print_r($_REQUEST);
			print_r($_POST);
			print_r($_GET);
		}
	}
}