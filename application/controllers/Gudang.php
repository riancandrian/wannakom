<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Gudang extends CI_Controller
{

  public function __construct(){
    parent::__construct();
    $this->load->model('admin_model');

    if($this->router->fetch_method() <> "login" and $this->router->fetch_method() <> "forgot_password"){
      if(!isset($_SESSION['admin_login']) or $_SESSION['admin_login']<>1){
        redirect('/login');
      }
    }
  }

  public function index(){
    $data['get_gudang'] = $this->admin_model->get_gudang(NULL,$_REQUEST);
		$this->load->view('admin/app_header');
		$this->load->view('admin/pindah_gudang',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." pindah_gudang view");
  }

  public function cekFaktur(){
    $faktur = $this->input->post('faktur');

    $query  = "SELECT * from tbl_faktur_pembelian p JOIN tbl_suplier s ON p.id_suplier = s.id
               LEFT JOIN tbl_gudang g ON g.id = p.id_gudang where p.nomor = '$faktur' ";
    $data   = $this->db->query($query)->row_array();

    $result['success'] = true;
    $result['data'] = $data;

    echo json_encode($result);
  }

  public function update(){
    $faktur = $this->input->post('faktur');
    $gudang = $this->input->post('gudang');

    $this->db->query("UPDATE tbl_faktur_pembelian SET id_gudang = '$gudang' WHERE nomor = '$faktur' ");

    $data['get_gudang'] = $this->admin_model->get_gudang(NULL,$_REQUEST);
    $data['message'] = 'Data sudah dipindahkan';
		$this->load->view('admin/app_header');
		$this->load->view('admin/pindah_gudang',$data);
		$this->load->view('admin/app_footer');
  }
}
