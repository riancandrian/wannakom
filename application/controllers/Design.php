<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Design extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('admin_model');

    if($this->router->fetch_method() <> "login" and $this->router->fetch_method() <> "forgot_password"){
      if(!isset($_SESSION['admin_login']) or $_SESSION['admin_login']<>1){
        redirect('/login');
      }
    }
  }

  public function data_design_det(){
    $id = $this->uri->segment(3);
    $data['parent'] = $id;
    $data['detail'] = $this->getDetail($id);

    $this->db->where('id', $id);
    $data['data'] = $this->db->get('tbl_design')->row_array();

    $this->load->view('admin/app_header');
    $this->load->view('admin/data_design_det',$data);
    $this->load->view('admin/app_footer');
    $this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." data_design_det view");
  }

  function getDetail($id){
    $this->db->where('id_design', $id);
    $data = $this->db->get('tbl_design_det')->result();

    return $data;
  }

  public function simpan_det(){
    $message_upload_image = '';
    $id_design  = $this->input->post('idparent');

    $insertData = array('id_design' => $id_design);

    if(isset($_FILES['filenya']['name']) and $_FILES['filenya']['name']<>""){
			$request['file']       = date("Ymdhisa")."-".str_replace(" ","-",$_FILES['filenya']['name']);
			$message_upload_image .= $this->upload_design($request['file'],'filenya');
			$insertData['gambar']  = $request['file'];
		}

    $this->db->insert("tbl_design_det", $insertData);
		$insert_id = $this->db->insert_id();

		if(is_numeric($insert_id)){
			$message = "Berhasil Input Data";
		}else{
			$message = "Gagal Input Data!";
		}

    redirect('design/data_design_det/'.$id_design);
  }

  public function upload_design($file_image_name,$file){
		$config['upload_path']     = './assets/upload_design/detail/original/';
		$config['allowed_types']   = 'jpg|png';
		$config['file_name']       = $file_image_name;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$do_upload = $this->upload->do_upload($file);

		if (!$do_upload){
			$message = 'Simpan gambar gagal: '.$this->upload->display_errors();
		}else{
			$this->upload_design_display($config,$file);
			$this->load->library('image_lib');
			$message = 'Simpan gambar berhasil';
		}
		unset($config);
		return $message;
	}

  public function upload_design_display($data_config,$file){
		$input_file = $data_config['upload_path'].$data_config['file_name'];
		$output_file = str_replace("original/","display/",$data_config['upload_path']).$data_config['file_name'];

		if($_FILES[$file]['type'] == "image/jpg") $im = imagecreatefromjpeg($input_file);
		if($_FILES[$file]['type'] == "image/png") $im = imagecreatefrompng($input_file);

		$cropped = imagecropauto($im, IMG_CROP_DEFAULT);
		if($cropped !== false){
			imagedestroy($im);
			$im = $cropped;
		}
		if($_FILES[$file]['type'] == "image/jpg") imagejpeg($im, $output_file);
		if($_FILES[$file]['type'] == "image/png") imagepng($im, $output_file);
		imagedestroy($im);
	}

  public function approve(){
    $id_design = $this->input->post('id_design');

    $this->db->where('id', $id_design);
    $this->db->update('tbl_design', array('status' => '1'));

    // print_r($this->db->last_query());die;

    echo json_encode(array('success' => true));
  }
}
