<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->model('penjualan_model');
		$this->load->model('mobile_service_model');

		if($this->router->fetch_method() <> "login" and $this->router->fetch_method() <> "forgot_password"){
			if(!isset($_SESSION['admin_login']) or $_SESSION['admin_login']<>1){
				redirect('/login');
			}
		}
	}

	public function index()
	{
		redirect('/penjualan/transaksi_penjualan');
	}

	public function request_dari_app($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;
		//if($action == "apply" and $edit_id <> NULL and is_numeric($edit_id)){
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="apply"){
			$id_group_customer = $this->penjualan_model->get_id_group_customer_from_mobile_order($_REQUEST['id_order']);
			if($id_group_customer == "1" or $id_group_customer == "2" or $id_group_customer == "3"){
				$data['message'] = $this->penjualan_model->apply_request_dari_app2($_REQUEST['id_order'], $_REQUEST);
			}else{
				$data['message'] = $this->penjualan_model->apply_request_dari_app($_REQUEST['id_order'], $_REQUEST);
			}
		}

		$data['get_data_mobile_order'] = $this->penjualan_model->get_data_mobile_order(NULL,$_REQUEST);

		$this->load->view('admin/app_header');
		$this->load->view('admin/penjualan_request_dari_app',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." request_dari_app view");
	}

	public function transaksi_penjualan($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;

		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$id_transaksi_penjualan = $this->penjualan_model->add_transaksi_penjualan($_REQUEST);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method()."/preview/".$id_transaksi_penjualan);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->penjualan_model->edit_transaksi_penjualan($_REQUEST);
		}
		if(($action == "edit" or $action == "preview") and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_transaksi_penjualan'] = $this->penjualan_model->get_transaksi_penjualan($edit_id);
			$data['get_transaksi_penjualan_detail_barang'] = $this->penjualan_model->get_transaksi_penjualan_detail_barang(NULL,array('id_transaksi_penjualan' => $edit_id));
			$data['get_edit_data_customer'] = $this->admin_model->get_data_customer($data['get_edit_transaksi_penjualan'][0]->id_customer);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->penjualan_model->hapus_transaksi_penjualan($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}

		$data['get_laporan_cepat_transaksi_penjualan'] = $this->penjualan_model->get_laporan_cepat_transaksi_penjualan();
		$data['get_data_customer'] = $this->admin_model->get_data_customer();
		$data['get_transaksi_penjualan'] = $this->penjualan_model->get_transaksi_penjualan(NULL,$_REQUEST);
		$data['get_data_barang'] = $this->admin_model->get_data_barang(NULL, NULL);
		$data['get_data_gudang'] = $this->admin_model->get_gudang(NULL, NULL);
		$this->load->view('admin/app_header');
		$this->load->view('admin/penjualan_transaksi_penjualan',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." transaksi_penjualan view");
	}

	public function retur_penjualan($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;
		$data['action_id'] = $edit_id;

		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$data['message'] = $this->penjualan_model->add_retur_penjualan($_REQUEST);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->penjualan_model->edit_retur_penjualan($_REQUEST);
		}
		if(($action == "edit" or $action == "preview") and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_retur_penjualan'] = $this->penjualan_model->get_retur_penjualan($edit_id);
			$data['get_edit_transaksi_penjualan'] = $this->penjualan_model->get_transaksi_penjualan($data['get_edit_retur_penjualan'][0]->id_transaksi_penjualan);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->penjualan_model->hapus_retur_penjualan($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}
		if($action == "tambah" and $data['action_id']<>NULL){
			$data['get_edit_transaksi_penjualan'] = $this->penjualan_model->get_transaksi_penjualan($data['action_id']);
		}
		$data['get_retur_penjualan'] = $this->penjualan_model->get_retur_penjualan(NULL,$_REQUEST);
		$data['get_transaksi_penjualan'] = $this->penjualan_model->get_transaksi_penjualan(NULL, array('bisa_retur'=>'1'));
		$this->load->view('admin/app_header');
		$this->load->view('admin/penjualan_retur_penjualan',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." retur_penjualan view");
	}

	public function laporan_transaksi_penjualan($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;
		$data['action_id'] = $edit_id;
		$data['get_laporan_transaksi_penjualan'] = $this->penjualan_model->get_laporan_transaksi_penjualan(NULL,$_REQUEST);
		$this->load->view('admin/app_header');
		$this->load->view('admin/penjualan_laporan_transaksi_penjualan',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." laporan_transaksi_penjualan view");
	}

	public function laporan_retur_penjualan($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;
		$data['action_id'] = $edit_id;
		$data['get_laporan_retur_penjualan'] = $this->penjualan_model->get_laporan_retur_penjualan(NULL,$_REQUEST);
		$this->load->view('admin/app_header');
		$this->load->view('admin/penjualan_laporan_retur_penjualan',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." laporan_retur_penjualan view");
	}

	public function laporan_stok_opname($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;
		$data['action_id'] = $edit_id;
		if(($action == "edit" or $action == "detail") and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_laporan_barang_stok_opname'] = $this->penjualan_model->get_laporan_barang_stok_opname($edit_id);
			$data['get_laporan_stok_opname_transaksi_barang'] = $this->penjualan_model->get_laporan_stok_opname_transaksi_barang($edit_id);
		}

		$data['get_laporan_barang_stok_opname'] = $this->penjualan_model->get_laporan_barang_stok_opname(NULL,$_REQUEST);
		$data['get_laporan_stok_opname_jenis'] = $this->penjualan_model->get_laporan_stok_opname_jenis();


		$this->load->view('admin/app_header');
		$this->load->view('admin/penjualan_laporan_stok_opname',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." laporan_retur_penjualan view");
	}

	public function point_of_sales(){
		$data['customer'] = $this->db->get('tbl_customer')->result();

		$this->load->view('admin/app_header');
		$this->load->view('admin/penjualan_pos',$data);
		$this->load->view('admin/app_footer');
	}



}
