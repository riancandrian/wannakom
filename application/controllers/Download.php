<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Download extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
		
		if($this->router->fetch_method() <> "login" and $this->router->fetch_method() <> "forgot_password"){
			if(!isset($_SESSION['admin_login']) or $_SESSION['admin_login']<>1){
				redirect('/login');
			}
		}
	}
	public function index()
	{
		redirect('/admin/index');
	}
	public function file_excel($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;
		$data['action_id'] = $edit_id;
		$data['get_download_data'] = $this->admin_model->get_download_data("file_excel");
		$this->load->view('admin/download_excel',$data);
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." download_excel view");
		//$this->admin_model->clear_download_data("file_excel");
	}
	public function file_pdf($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;
		$data['action_id'] = $edit_id;
		$data['get_download_data'] = $this->admin_model->get_download_data("file_pdf");
		$this->load->view('admin/download_pdf',$data);
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." download_pdf view");
		//$this->admin_model->clear_download_data("file_pdf");
	}
}