<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->model('penjualan_model');
		$this->load->model('saham_model');

		if($this->router->fetch_method() <> "login" and $this->router->fetch_method() <> "forgot_password"){
			if(!isset($_SESSION['admin_login']) or $_SESSION['admin_login']<>1){
				redirect('/login');
			}
		}
	}
	public function index()
	{
		$data['get_laporan_dashboard'] = $this->admin_model->get_laporan_dashboard($_REQUEST);
		$data['tanggal_sekarang'] = date("d/M/Y");
		$this->load->view('admin/app_header');
		$this->load->view('admin/dashboard',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." dashboard view");
	}
	public function login()
	{
		//echo"<pre>"; print_r($_REQUEST); echo"</pre>";
		$data = NULL;
		if(isset($_SESSION['admin_login']) and $_SESSION['admin_login']==1){
			$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." login success");
			redirect('/');
		}
		if(isset($_REQUEST['submit_login'])){
			$data['message'] = $this->admin_model->submit_login($_REQUEST);
			$this->admin_model->admin_log_add("login submit_login");
		}
		$this->load->view('admin/login', $data);
		$this->admin_model->admin_log_add("login view");
		unset($data);
	}
	public function forgot_password()
	{
		//echo"<pre>"; print_r($_REQUEST); echo"</pre>";
		$data = NULL;
		if(isset($_SESSION['admin_login']) and $_SESSION['admin_login']==1){
			$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." login success");
			redirect('/');
		}
		if(isset($_REQUEST['submit_forgot_password'])){
			//$data['message'] = $this->admin_model->submit_forgot_password($_REQUEST);
			$this->admin_model->admin_log_add("forgot_password submit_forgot_password");
		}
		$this->load->view('admin/forgot_password', $data);
		$this->admin_model->admin_log_add("forgot_password view");
		unset($data);
	}
	public function logout()
	{
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." logout");
		$array_items = array('admin_id','admin_username','admin_password','admin_fullname','admin_email','admin_phone','admin_login');
		$this->session->unset_userdata($array_items);
		redirect('/');
	}
	public function apiweb()
	{
		$res = 0;
		if(isset($_REQUEST['action']) and $_REQUEST['action'] == "get_kode_barang"){
			$res = $this->admin_model->get_kode_barang($_REQUEST['id_group'], $_REQUEST['id_data_barang'], 1);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "get_data_detail_promo_barang"){
			$res = $this->admin_model->get_data_promo_barang(NULL,$_REQUEST);
			$res = json_encode($res);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "add_data_detail_promo_barang"){
			$res = $this->admin_model->add_data_promo_barang($_REQUEST, $_FILES, $_REQUEST['id_data_promo']);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "edit_data_detail_promo_barang"){
			$res = $this->admin_model->edit_data_promo_barang($_REQUEST, $_FILES);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "hapus_data_detail_promo_barang"){
			$res = $this->admin_model->hapus_data_promo_barang($_REQUEST['id_data_detail_promo_barang'],$_REQUEST['file_image_name']);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "update_table"){
			$res = $this->admin_model->update_table_database($_REQUEST);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "if_exist_data_table"){
			$res = $this->admin_model->if_exist_data_table($_REQUEST);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "get_faktur_pembelian"){
			$res = $this->admin_model->get_faktur_pembelian(NULL,$_REQUEST);
			$res = json_encode($res);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "get_faktur_pembelian_detail_barang"){
			$res = $this->admin_model->get_faktur_pembelian_detail_barang(NULL,$_REQUEST);
			$res = json_encode($res);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "get_retur_pembelian_detail_barang"){
			$res = $this->admin_model->get_retur_pembelian_detail_barang(NULL,$_REQUEST);
			$res = json_encode($res);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "get_pembayaran_hutang_detail_faktur"){
			$res = $this->admin_model->get_pembayaran_hutang_detail_faktur(NULL,$_REQUEST);
			$res = json_encode($res);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "get_data_suplier"){
			$res = $this->admin_model->get_data_suplier(NULL,$_REQUEST);
			$res = json_encode($res);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "get_data_customer"){
			$res = $this->admin_model->get_data_customer(NULL,$_REQUEST);
			$res = json_encode($res);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "post_download_data"){
			$res = $this->admin_model->post_download_data($_REQUEST);
			$res = json_encode($res);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "get_transaksi_penjualan"){
			$res = $this->penjualan_model->get_transaksi_penjualan(NULL,$_REQUEST);
			$res = json_encode($res);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "get_transaksi_penjualan_detail_barang"){
			$res = $this->penjualan_model->get_transaksi_penjualan_detail_barang(NULL,$_REQUEST);
			$res = json_encode($res);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "get_retur_penjualan_detail_barang"){
			$res = $this->penjualan_model->get_retur_penjualan_detail_barang(NULL,$_REQUEST);
			$res = json_encode($res);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "get_data_barang_stok"){
			$res = $this->admin_model->get_data_barang_stok(NULL,$_REQUEST);
			$res = json_encode($res);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "get_nilai_pendapatan_perusahaan"){
			$res = $this->admin_model->get_nilai_pendapatan_perusahaan($_REQUEST);
			$res = json_encode($res);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "get_pembagian_saham_detail_investor"){
			$res = $this->saham_model->get_pembagian_saham_detail_investor(NULL,$_REQUEST);
			$res = json_encode($res);
		}

		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "get_real_stock"){
			$res = $this->penjualan_model->get_real_stock();
			$res = json_encode($res);
		}
		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "get_data_investor"){
			$res = $this->saham_model->get_data_investor(NULL,$_REQUEST);
			$res = json_encode($res);
		}
		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "get_count_data_mobile_order"){
			$res = $this->penjualan_model->get_count_data_mobile_order(NULL,$_REQUEST);
			$res = json_encode($res);
		}
		else if(isset($_REQUEST['action']) and $_REQUEST['action'] == "get_no_seri_barang"){
			$res = $this->admin_model->get_no_seri_barang(NULL,$_REQUEST);
			$res = json_encode($res);
		}
		echo $res;
	}
	public function data_user($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;

		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$data['message'] = $this->admin_model->add_admin_user($_REQUEST);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->admin_model->edit_admin_user($_REQUEST);
		}
		if($action == "edit" and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_admin_user'] = $this->admin_model->get_admin_user($edit_id);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->admin_model->hapus_admin_user($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}

		$data['get_admin_user'] = $this->admin_model->get_admin_user(NULL,$_REQUEST);
		$data['get_admin_menu_hak'] = $this->admin_model->get_admin_menu_hak();
		$this->load->view('admin/app_header');
		$this->load->view('admin/data_user',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." data_user view");
	}
	public function profile()
	{
		$action = "edit";
		$edit_id = $_SESSION['admin_id'];
		$data['action'] = $action;

		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$data['message'] = $this->admin_model->add_admin_user($_REQUEST);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->admin_model->edit_admin_user($_REQUEST);
		}
		if($action == "edit" and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_admin_user'] = $this->admin_model->get_admin_user($edit_id);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->admin_model->hapus_admin_user($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}

		$data['get_admin_user'] = $this->admin_model->get_admin_user(NULL,$_REQUEST);
		$data['get_admin_menu_hak'] = $this->admin_model->get_admin_menu_hak();
		$this->load->view('admin/app_header');
		$this->load->view('admin/data_user',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." data_user view");
	}
	public function setting_app()
	{
		$id_setting_app = "1";
		$data['action'] = "edit";

		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->admin_model->edit_setting_app($_REQUEST);
		}

		$data['get_edit_setting_app'] = $this->admin_model->get_setting_app($id_setting_app);
		$this->load->view('admin/app_header');
		$this->load->view('admin/setting_app',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." setting_app view");
	}
	public function data_hak_user($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;

		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$data['message'] = $this->admin_model->add_hak_akses($_REQUEST);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->admin_model->edit_hak_akses($_REQUEST);
		}
		if($action == "edit" and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_admin_menu_hak'] = $this->admin_model->get_admin_menu_hak($edit_id);
			$get_edit_admin_menu_hak_akses = $this->admin_model->get_admin_menu_hak_akses($edit_id);

			$arMenuHakAkses = array();
			foreach($get_edit_admin_menu_hak_akses as $row){
				$id_menu = $row->id_menu;
				$arMenuHakAkses[$id_menu]['lihat'] = $row->lihat;
				$arMenuHakAkses[$id_menu]['tambah'] = $row->tambah;
				$arMenuHakAkses[$id_menu]['ubah'] = $row->ubah;
				$arMenuHakAkses[$id_menu]['hapus'] = $row->hapus;
			}
			$data['arMenuHakAkses'] = $arMenuHakAkses;
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->admin_model->hapus_hak_akses($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}

		$data['get_admin_menu_hak'] = $this->admin_model->get_admin_menu_hak(NULL,$_REQUEST);
		$data['get_admin_menu'] = $this->admin_model->get_admin_menu();

		$this->load->view('admin/app_header');
		$this->load->view('admin/data_hak_user',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." data_hak_user view");
	}
	public function data_group_barang($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;

		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$data['message'] = $this->admin_model->add_group_barang($_REQUEST);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->admin_model->edit_group_barang($_REQUEST);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="setting"){
			$data['message'] = $this->admin_model->edit_group_barang_setting($_REQUEST);
		}
		if(($action == "edit" or $action == "setting") and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_group_barang'] = $this->admin_model->get_group_barang($edit_id);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->admin_model->hapus_group_barang($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}

		$data['get_group_barang'] = $this->admin_model->get_group_barang(NULL,$_REQUEST);
		$this->load->view('admin/app_header');
		$this->load->view('admin/data_group_barang',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." data_group_barang view");
	}
	public function data_satuan($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;

		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$data['message'] = $this->admin_model->add_satuan($_REQUEST);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->admin_model->edit_satuan($_REQUEST);
		}
		if($action == "edit" and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_satuan'] = $this->admin_model->get_satuan($edit_id);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->admin_model->hapus_satuan($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}

		$data['get_satuan'] = $this->admin_model->get_satuan(NULL,$_REQUEST);
		$this->load->view('admin/app_header');
		$this->load->view('admin/data_satuan',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." data_satuan view");
	}
	public function data_barang($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;


		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$data['message'] = $this->admin_model->add_data_barang($_REQUEST, $_FILES);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->admin_model->edit_data_barang($_REQUEST, $_FILES);
		}
		if($action == "edit" and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_data_barang'] = $this->admin_model->get_data_barang($edit_id);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->admin_model->hapus_data_barang($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}

		$data['get_data_barang'] = $this->admin_model->get_data_barang(NULL,$_REQUEST);
		$data['get_group_barang'] = $this->admin_model->get_group_barang(NULL,$_REQUEST);
		$data['get_satuan'] = $this->admin_model->get_satuan(NULL,$_REQUEST);
		$this->load->view('admin/app_header');
		$this->load->view('admin/data_barang',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." data_barang view");
	}
	public function data_harga_jual($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;

		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$data['message'] = $this->admin_model->add_harga_jual($_REQUEST);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->admin_model->edit_harga_jual($_REQUEST);
		}
		if($action == "edit" and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_harga_jual'] = $this->admin_model->get_harga_jual($edit_id);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->admin_model->hapus_harga_jual($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}

		$data['get_harga_jual'] = $this->admin_model->get_harga_jual(NULL,$_REQUEST);
		$data['get_data_barang'] = $this->admin_model->get_data_barang(NULL,$_REQUEST);
		$this->load->view('admin/app_header');
		$this->load->view('admin/data_harga_jual',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." data_harga_jual view");
	}
	public function data_kota($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;

		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$data['message'] = $this->admin_model->add_kota($_REQUEST);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->admin_model->edit_kota($_REQUEST);
		}
		if($action == "edit" and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_kota'] = $this->admin_model->get_kota($edit_id);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->admin_model->hapus_kota($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}

		$data['get_kota'] = $this->admin_model->get_kota(NULL,$_REQUEST);
		$this->load->view('admin/app_header');
		$this->load->view('admin/data_kota',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." data_kota view");
	}
	public function data_gudang($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;

		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$data['message'] = $this->admin_model->add_gudang($_REQUEST);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->admin_model->edit_gudang($_REQUEST);
		}
		if($action == "edit" and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_gudang'] = $this->admin_model->get_gudang($edit_id);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->admin_model->hapus_gudang($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}

		$data['get_gudang'] = $this->admin_model->get_gudang(NULL,$_REQUEST);
		$this->load->view('admin/app_header');
		$this->load->view('admin/data_gudang',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." data_gudang view");
	}
	public function data_design($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;

		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$data['message'] = $this->admin_model->add_design($_REQUEST,$_FILES);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->admin_model->edit_design($_REQUEST);
		}
		if($action == "edit" and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_design'] = $this->admin_model->get_design($edit_id);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->admin_model->hapus_design($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}

		$data['get_design'] = $this->admin_model->get_design(NULL,$_REQUEST);
		$this->load->view('admin/app_header');
		$this->load->view('admin/data_design',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." data_design view");
	}
	public function data_group_suplier($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;

		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$data['message'] = $this->admin_model->add_group_suplier($_REQUEST);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->admin_model->edit_group_suplier($_REQUEST);
		}
		if($action == "edit" and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_group_suplier'] = $this->admin_model->get_group_suplier($edit_id);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->admin_model->hapus_group_suplier($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}

		$data['get_group_suplier'] = $this->admin_model->get_group_suplier(NULL,$_REQUEST);
		$this->load->view('admin/app_header');
		$this->load->view('admin/data_group_suplier',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." data_group_suplier view");
	}
	public function data_suplier($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;

		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$data['message'] = $this->admin_model->add_data_suplier($_REQUEST);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->admin_model->edit_data_suplier($_REQUEST);
		}
		if($action == "edit" and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_data_suplier'] = $this->admin_model->get_data_suplier($edit_id);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->admin_model->hapus_data_suplier($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}

		$data['get_data_suplier'] = $this->admin_model->get_data_suplier(NULL,$_REQUEST);
		$data['get_group_suplier'] = $this->admin_model->get_group_suplier(NULL,$_REQUEST);
		$this->load->view('admin/app_header');
		$this->load->view('admin/data_suplier',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." data_suplier view");
	}
	public function data_group_customer($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;

		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$data['message'] = $this->admin_model->add_group_customer($_REQUEST);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->admin_model->edit_group_customer($_REQUEST);
		}
		if($action == "edit" and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_group_customer'] = $this->admin_model->get_group_customer($edit_id);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->admin_model->hapus_group_customer($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}

		$data['get_group_customer'] = $this->admin_model->get_group_customer(NULL,$_REQUEST);
		$this->load->view('admin/app_header');
		$this->load->view('admin/data_group_customer',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." data_group_customer view");
	}
	public function data_customer($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;

		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$data['message'] = $this->admin_model->add_data_customer($_REQUEST,$_FILES);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->admin_model->edit_data_customer($_REQUEST,$_FILES);
		}
		if($action == "edit" and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_data_customer'] = $this->admin_model->get_data_customer($edit_id);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->admin_model->hapus_data_customer($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}

		$data['get_data_customer'] = $this->admin_model->get_data_customer(NULL,$_REQUEST);
		$data['get_group_customer'] = $this->admin_model->get_group_customer(NULL,$_REQUEST);
		$data['get_kota'] = $this->admin_model->get_kota(NULL,$_REQUEST);
		$this->load->view('admin/app_header');
		$this->load->view('admin/data_customer',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." data_customer view");
	}
	public function data_promo($action = NULL, $edit_id = NULL)
	{
		$data['action'] = $action;

		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="tambah"){
			$data['message'] = $this->admin_model->add_data_promo($_REQUEST);
		}
		if(isset($_REQUEST['form_action']) and $_REQUEST['form_action']=="edit"){
			$data['message'] = $this->admin_model->edit_data_promo($_REQUEST);
		}
		if($action == "edit" and $edit_id <> NULL and is_numeric($edit_id)){
			$data['get_edit_data_promo'] = $this->admin_model->get_data_promo($edit_id);
		}
		if($action == "hapus" and $edit_id <> NULL and is_numeric($edit_id)){
			$this->admin_model->hapus_data_promo($edit_id);
			redirect(base_url().$this->router->fetch_class()."/".$this->router->fetch_method());
		}

		$data['get_data_promo'] = $this->admin_model->get_data_promo(NULL,$_REQUEST);
		//$get_id_barang_data_promo_barang = $this->admin_model->get_id_barang_data_promo_barang();
		$data['get_data_barang'] = $this->admin_model->get_data_barang(NULL, NULL, @$get_id_barang_data_promo_barang);
		$this->load->view('admin/app_header');
		$this->load->view('admin/data_promo',$data);
		$this->load->view('admin/app_footer');
		$this->admin_model->admin_log_add($_SESSION['admin_id'].":".$_SESSION['admin_username']." data_promo view");
	}
}
