//default_javascript.js
var BASE_URL = window.location.origin;//+"/myweb/Wanna_Komunika"
function printTableData(data = null){
	var myWindow = window.open("", "PrintWindow", "width=800,height=600");
	if(data === null)
	var data = $j(document).find(".table-data").html();
	// style='margin:30px 10px;padding:30px 10px;background-color:transparent;'
	myWindow.document.write("<html>"+
									"<head>"+
										"<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css'>"+
										"<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css'>"+
										"<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css'>"+
										"<link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css'>"+
										"<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css'>"+
										"<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css'>"+
										"<link rel='stylesheet' href='"+BASE_URL+"/assets/ElaAdmin-master/assets/css/lib/chosen/chosen.min.css?n=104'>"+
										"<link rel='stylesheet' href='"+BASE_URL+"/assets/ElaAdmin-master/assets/css/cs-skin-elastic.css?n=104'>"+
										"<link rel='stylesheet' href='"+BASE_URL+"/assets/ElaAdmin-master/assets/css/style.css?n=104'>"+
										"<link rel='stylesheet' href='"+BASE_URL+"/assets/default_style.css?n=104'>"+
										"<link href='https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css' rel='stylesheet'>"+
										"<link href='https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css' rel='stylesheet'>"+
										"<link href='https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css' rel='stylesheet'>"+
										"<link href='https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet'>"+
										"<style>"+
											"@media print {#header, #footer {display:none !important;}}"+
										"</style>"+
										"<title>Print Window</title>"+
									"</head>"+
									"<body onload='winPrint()'>"+
										"<table style='width:100%;background:none;'>"+data+"</table>"+
										"<script type='text/javascript'>"+
											"window.print();"+
										"</script>"+
									"</body>"+
							"</html>");
}
function printScreen(data = null){
	var myWindow = window.open("", "PrintWindow", "width=800,height=600");
	if(data === null)
	var data = $j(document).find(".table-data").html();
	// style='margin:30px 10px;padding:30px 10px;background-color:transparent;'
	myWindow.document.write("<html>"+
									"<head>"+
										"<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/normalize.css@8.0.0/normalize.min.css'>"+
										"<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css'>"+
										"<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/font-awesome@4.7.0/css/font-awesome.min.css'>"+
										"<link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css'>"+
										"<link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css'>"+
										"<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css'>"+
										"<link rel='stylesheet' href='"+BASE_URL+"/assets/ElaAdmin-master/assets/css/lib/chosen/chosen.min.css?n=104'>"+
										"<link rel='stylesheet' href='"+BASE_URL+"/assets/ElaAdmin-master/assets/css/cs-skin-elastic.css?n=104'>"+
										"<link rel='stylesheet' href='"+BASE_URL+"/assets/ElaAdmin-master/assets/css/style.css?n=104'>"+
										"<link rel='stylesheet' href='"+BASE_URL+"/assets/default_style.css?n=104'>"+
										"<link href='https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.css' rel='stylesheet'>"+
										"<link href='https://cdn.jsdelivr.net/npm/jqvmap@1.5.1/dist/jqvmap.min.css' rel='stylesheet'>"+
										"<link href='https://cdn.jsdelivr.net/npm/fullcalendar@3.9.0/dist/fullcalendar.min.css' rel='stylesheet'>"+
										"<link href='https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet'>"+
										"<style>"+
											"@media print {#header, #footer {display:none !important;}}"+
										"</style>"+
										"<title>Print Window</title>"+
									"</head>"+
									"<body onload='winPrint()'>"+
										data+
										"<script type='text/javascript'>"+
											"window.print();"+
										"</script>"+
									"</body>"+
							"</html>");
}
function getBase64(file) {
  return new Promise((resolve, reject) => {
	const reader = new FileReader();
	reader.readAsDataURL(file);
	reader.onload = () => resolve(reader.result);
	reader.onerror = error => reject(error);
  });
}
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}
function format_rupiah(x){
	return "Rp "+numberWithCommas(x)+",-";
}
function formatRupiah(x){
	return "Rp. "+numberWithCommas(x)+",-";
}
function repair_date(x){
	var s1 = x.toString().split(" ")[0];
	var s2 = s1.split("-");
	return s2[2]+"/"+s2[1]+"/"+s2[0];
}
function repair_date2(x){
	var s1 = x.toString().split(" ");
	var s2 = (s1[0]).toString().split("-");
	return s2[2]+"/"+s2[1]+"/"+s2[0]+" <br/>"+s1[1];
}